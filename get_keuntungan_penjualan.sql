SELECT a.id_tr_det_penjualan, d.kode_penjualan, d.tanggal_penjualan, f.nama_produk, a.jumlah, c.harga_pendapatan,
b.harga_produk AS harga_jual, (c.harga_pendapatan * a.jumlah) AS total_harga_pendapatan,
 (b.harga_produk * a.jumlah) AS total_harga_jual   FROM tr_det_penjualan AS a 
JOIN detail_penjualan AS b ON a.id_detail_penjualan = b.id_detail_penjualan
JOIN detail_produk AS c ON a.id_detail_produk = c.id_detail_produk
JOIN penjualan AS d ON b.kode_penjualan = d.kode_penjualan
LEFT JOIN anggota AS e ON d.no_anggota = e.no_anggota
LEFT JOIN produk AS f ON c.kode_produk = f.kode_produk
