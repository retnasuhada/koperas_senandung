<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('home', 'App\Http\Controllers\AkuntansiController@dashboard')->name('home');
Route::get('home', 'App\Http\Controllers\AnggotaController@profil_anggota2')->name('home');
Route::get('dash_aset', 'App\Http\Controllers\AkuntansiController@dashboard')->name('dash_aset');

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    return "Cache is cleared";
});

Route::get('keluar', 'App\Http\Controllers\AuthController@logout')->name('keluar');

Route::get('sqlsrv', 'App\Http\Controllers\AuthController@sqlsrv');

Route::get('phpinfo', 'App\Http\Controllers\AuthController@phpinfo');

// Route::get('/', 'App\Http\Controllers\EcommerceController@e_produk');
// Route::get('/', 'App\Http\Controllers\AkuntansiController@dashboard');
Route::get('/', 'App\Http\Controllers\AuthController@showFormLogin');
Route::get('keluar2', 'App\Http\Controllers\AuthController@showFormLogin')->name('keluar2');

Route::post('login', 'App\Http\Controllers\AuthController@login');
Route::get('top_nav', 'App\Http\Controllers\HomeControllers@top');
Route::get('e_comm','App\Http\Controllers\EcommerceController@e_comm')->name('e_comm');
Route::get('e-commerce','App\Http\Controllers\HomeController@ecommerce')->name('e-commerce');
Route::get('register_ecomm', 'App\Http\Controllers\EcommerceController@register_ecomm');
Route::post('add_to_cart','App\Http\Controllers\EcommerceController@add_to_cart')->name('add_to_cart');
Route::post('ecom_regist','App\Http\Controllers\EcommerceController@ecom_regist')->name('ecom_regist');

// Route::get('/','App\Http\Controllers\HomeController@e_comm')->name('e_comm');

// All About Produk
Route::get('produk','App\Http\Controllers\HomeControllers@index')->name('produk');

// Pembelian
Route::get('pembelian','App\Http\Controllers\PembelianController@index')->name('pembelian');
Route::get('add_pembelian','App\Http\Controllers\PembelianController@add_pembelian')->name('add_pembelian');
Route::post('add_pembelian_act','App\Http\Controllers\PembelianController@add_pembelian_act')->name('add_pembelian_act');
Route::get('edit_detail_pembelian/{id}','App\Http\Controllers\PembelianController@edit_detail_pembelian');
Route::post('edit_pembelian_act','App\Http\Controllers\PembelianController@edit_pembelian_act')->name('edit_pembelian_act');
Route::get('hapus_detail_pembelian/{id}','App\Http\Controllers\PembelianController@hapus_detail_pembelian');
Route::post('save_pembelian_act','App\Http\Controllers\PembelianController@save_pembelian_act')->name('save_pembelian_act');

Route::post('save_verif_pembelian_act','App\Http\Controllers\PembelianController@save_verif_pembelian_act')->name('save_verif_pembelian_act');

Route::get('edit_pembelian/{id}','App\Http\Controllers\PembelianController@edit_pembelian');
Route::post('act_edit_pembelian','App\Http\Controllers\PembelianController@act_edit_pembelian')->name('act_edit_pembelian');
Route::get('detail_pembelian/{id}','App\Http\Controllers\PembelianController@detail_pembelian');
Route::get('verif_pembelian/{id}','App\Http\Controllers\PembelianController@verif_pembelian');
Route::get('verifikasi_detail/{id}','App\Http\Controllers\PembelianController@verifikasi_detail');
Route::get('verif_pembelian2','App\Http\Controllers\PembelianController@verif_pembelian2')->name('verif_pembelian2');
Route::get('view_verified/{id}','App\Http\Controllers\PembelianController@view_verified');








// Penjualan
Route::get('penjualan','App\Http\Controllers\PenjualanController@index')->name('penjualan');
Route::get('add_penjualan','App\Http\Controllers\PenjualanController@add_penjualan')->name('add_penjualan');
Route::post('add_det_penjualan','App\Http\Controllers\PenjualanController@add_det_penjualan')->name('add_det_penjualan');
Route::get('edit_detail_penjualan/{id}','App\Http\Controllers\PenjualanController@edit_detail_penjualan');
Route::post('edit_det_penjualan_act','App\Http\Controllers\PenjualanController@edit_det_penjualan_act')->name('edit_det_penjualan_act');
Route::get('hapus_detail_penjualan/{id}','App\Http\Controllers\PenjualanController@hapus_detail_penjualan');
Route::post('save_penjualan_act','App\Http\Controllers\PenjualanController@save_penjualan_act')->name('save_penjualan_act');
Route::get('cetak_struk_penjualan/{id}','App\Http\Controllers\PenjualanController@cetak_struk_penjualan');
Route::get('info_penjualan/{id}','App\Http\Controllers\PenjualanController@info_penjualan');






// All About Simpanan
Route::get('simpanan_wajib_list','App\Http\Controllers\SimpananController@list_sw')->name('simpanan_wajib_list');
Route::get('jenis_simpanan','App\Http\Controllers\SimpananController@jenis_simpanan')->name('jenis_simpanan');
Route::post('add_jenis_simpanan','App\Http\Controllers\SimpananController@add_jenis_simpanan')->name('add_jenis_simpanan');
Route::get('edit_jenis_simpanan/{id}','App\Http\Controllers\SimpananController@edit_jenis_simpanan');
Route::post('edit_jenis_simpanan_act','App\Http\Controllers\SimpananController@edit_jenis_simpanan_act')->name('edit_jenis_simpanan_act');
Route::get('hapus_jenis_simpanan/{id}','App\Http\Controllers\SimpananController@hapus_jenis_simpanan');
Route::get('add_sw','App\Http\Controllers\SimpananController@add_sw')->name('add_sw');
Route::post('add_simpanan_act','App\Http\Controllers\SimpananController@add_simpanan_act')->name('add_simpanan_act');
Route::get('edit_simpanan/{id}','App\Http\Controllers\SimpananController@edit_simpanan');
Route::post('edit_sw_act','App\Http\Controllers\SimpananController@edit_sw_act')->name('edit_sw_act');
Route::get('detail_simpanan/{id}','App\Http\Controllers\SimpananController@detail_simpanan');

Route::get('detail_simpanan_agt/{id}','App\Http\Controllers\SimpananController@detail_simpanan_agt');
Route::get('detail_simpanan_sp/{id}','App\Http\Controllers\SimpananController@detail_simpanan_sp');
Route::get('detail_simpanan_ss/{id}','App\Http\Controllers\SimpananController@detail_simpanan_ss');
Route::get('detail_modal_det/{id}','App\Http\Controllers\SimpananController@detail_modal_det');
Route::get('detail_hibah_agt/{id}','App\Http\Controllers\SimpananController@detail_hibah_agt');

Route::get('agt_sw','App\Http\Controllers\SimpananAnggotaController@agt_sw')->name('agt_sw');
Route::get('agt_ss','App\Http\Controllers\SimpananController@agt_ss')->name('agt_ss');
Route::get('agt_sp','App\Http\Controllers\SimpananController@agt_sp')->name('agt_sp');
Route::get('agt_modal','App\Http\Controllers\SimpananController@agt_modal')->name('agt_modal');
Route::get('agt_hibah','App\Http\Controllers\SimpananController@agt_hibah')->name('agt_hibah');



// simpanan pokok
Route::get('simpanan_pokok_list','App\Http\Controllers\SimpananController@list_sp')->name('simpanan_pokok_list');
Route::get('add_sp','App\Http\Controllers\SimpananController@add_sp')->name('add_sp');
Route::post('add_sp_act','App\Http\Controllers\SimpananController@add_sp_act')->name('add_sp_act');
Route::get('edit_sp/{id}','App\Http\Controllers\SimpananController@edit_sp');
Route::post('edit_sp_act','App\Http\Controllers\SimpananController@edit_sp_act')->name('edit_sp_act');
Route::get('detail_simpanan_pokok/{id}','App\Http\Controllers\SimpananController@detail_simpanan_pokok');


// simpanan sukarela
Route::get('simpanan_sukarela_list','App\Http\Controllers\SimpananController@list_ss')->name('simpanan_sukarela_list');
Route::get('add_ss','App\Http\Controllers\SimpananController@add_ss')->name('add_ss');
Route::post('add_ss_act','App\Http\Controllers\SimpananController@add_ss_act')->name('add_ss_act');
Route::get('edit_ss/{id}','App\Http\Controllers\SimpananController@edit_ss');
Route::post('edit_ss_act','App\Http\Controllers\SimpananController@edit_ss_act')->name('edit_ss_act');
Route::get('detail_simpanan_sukarela/{id}','App\Http\Controllers\SimpananController@detail_simpanan_sukarela');



// simpanan Modal Anggota
Route::get('modal_anggota','App\Http\Controllers\SimpananController@modal_anggota')->name('modal_anggota');
Route::get('add_modal_anggota','App\Http\Controllers\SimpananController@add_modal_anggota')->name('add_modal_anggota');
Route::post('add_modal_act','App\Http\Controllers\SimpananController@add_modal_act')->name('add_modal_act');
Route::get('edit_modal_anggota/{id}','App\Http\Controllers\SimpananController@edit_modal_anggota');
Route::post('edit_modal_anggota_act','App\Http\Controllers\SimpananController@edit_modal_anggota_act')->name('edit_modal_anggota_act');



// simpanan Hibah
Route::get('dana_hibah','App\Http\Controllers\SimpananController@dana_hibah')->name('dana_hibah');
Route::get('add_dana_hibah','App\Http\Controllers\SimpananController@add_dana_hibah')->name('add_dana_hibah');
Route::post('add_dana_hibah_act','App\Http\Controllers\SimpananController@add_dana_hibah_act')->name('add_dana_hibah_act');
Route::get('edit_dana_hibah/{id}','App\Http\Controllers\SimpananController@edit_dana_hibah');
Route::post('edit_dana_hibah_act','App\Http\Controllers\SimpananController@edit_dana_hibah_act')->name('edit_dana_hibah_act');


// All About Pinjaman
Route::get('jenis_pinjaman','App\Http\Controllers\PinjamanController@jenis_pinjaman')->name('jenis_pinjaman');
Route::post('add_jenis_pinjaman','App\Http\Controllers\PinjamanController@add_jenis_pinjaman')->name('add_jenis_pinjaman');
Route::get('edit_jenis_pinjaman/{id}','App\Http\Controllers\PinjamanController@edit_jenis_pinjaman');
Route::post('edit_jenis_pinjaman_act','App\Http\Controllers\PinjamanController@edit_jenis_pinjaman_act')->name('edit_jenis_pinjaman_act');
Route::get('hapus_jenis_pinjaman/{id}','App\Http\Controllers\PinjamanController@hapus_jenis_pinjaman');




// All About jabatan
Route::get('jabatan','App\Http\Controllers\JabatanController@list_jabatan')->name('jabatan');
Route::post('add_jabatan','App\Http\Controllers\JabatanController@add_jabatan')->name('add_jabatan');
Route::get('edit_jabatan/{id}','App\Http\Controllers\JabatanController@edit_jabatan');


// All About Anggota
Route::get('anggota','App\Http\Controllers\AnggotaController@anggota')->name('anggota');
Route::post('add_anggota','App\Http\Controllers\AnggotaController@add_anggota')->name('add_anggota');
Route::get('edit_anggota/{id}','App\Http\Controllers\AnggotaController@edit_anggota');
Route::get('hapus_anggota/{id}','App\Http\Controllers\AnggotaController@hapus_anggota');
Route::post('edit_anggota_act','App\Http\Controllers\AnggotaController@edit_anggota_act')->name('edit_anggota_act');
Route::get('add_frm_anggota','App\Http\Controllers\AnggotaController@add')->name('add_frm_anggota');
Route::get('profil_anggota/{id}','App\Http\Controllers\AnggotaController@profil_anggota');

Route::get('cetak_kartu/{id}','App\Http\Controllers\AnggotaController@cetak_kartu');

// All About User
Route::get('user','App\Http\Controllers\UserController@user')->name('user');
Route::post('add_user','App\Http\Controllers\UserController@add_user')->name('add_user');
Route::get('edit_user/{id}','App\Http\Controllers\UserController@edit_user');
Route::post('edit_user_act','App\Http\Controllers\UserController@edit_user_act')->name('edit_user_act');
Route::get('edit_user_agt/{id}','App\Http\Controllers\UserController@edit_user_agt');
Route::post('edit_user_agt_act','App\Http\Controllers\UserController@edit_user_agt_act')->name('edit_user_agt_act');




// All About Suplier
Route::get('suplier','App\Http\Controllers\SuplierController@suplier')->name('suplier');
Route::get('add_frm_suplier','App\Http\Controllers\SuplierController@add')->name('add_frm_suplier');
Route::post('add_suplier','App\Http\Controllers\SuplierController@add_suplier')->name('add_suplier');
Route::get('edit_suplier/{id}','App\Http\Controllers\SuplierController@edit_suplier');
Route::post('edit_suplier_act','App\Http\Controllers\SuplierController@edit_suplier_act')->name('edit_suplier_act');
Route::get('detail_suplier/{id}','App\Http\Controllers\SuplierController@detail_suplier');
Route::get('m_suplier_stok','App\Http\Controllers\SuplierController@m_suplier_stok')->name('m_suplier_stok');
Route::get('m_penjualan_suplier','App\Http\Controllers\SuplierController@m_penjualan_suplier')->name('m_penjualan_suplier');
Route::get('add_m_penjualan_suplier','App\Http\Controllers\SuplierController@add_m_penjualan_suplier')->name('add_m_penjualan_suplier');

Route::get('detail_penjualan_suplier/{id}','App\Http\Controllers\SuplierController@detail_penjualan_suplier');
Route::get('m_pembayaran_suplier','App\Http\Controllers\SuplierController@m_pembayaran_suplier')->name('m_pembayaran_suplier');
Route::get('m_detail_pembayaran_suplier/{id}','App\Http\Controllers\SuplierController@m_detail_pembayaran_suplier');
Route::post('add_penjualanspl_act','App\Http\Controllers\SuplierController@add_penjualanspl_act')->name('add_penjualanspl_act');
Route::post('save_penjualanspl_act','App\Http\Controllers\SuplierController@save_penjualanspl_act')->name('save_penjualanspl_act');




// All About Suplier
Route::get('laporan_pembelian','App\Http\Controllers\LaporanController@laporan_pembelian')->name('laporan_pembelian');
Route::post('cetak_lap_pembelian','App\Http\Controllers\LaporanController@cetak_lap_pembelian')->name('cetak_lap_pembelian');
Route::post('cetak_lap_penjualan','App\Http\Controllers\LaporanController@cetak_lap_penjualan')->name('cetak_lap_penjualan');
Route::get('laporan_penjualan','App\Http\Controllers\LaporanController@laporan_penjualan')->name('laporan_penjualan');






// All About invoice
Route::get('invoice','App\Http\Controllers\InvoiceController@invoice')->name('invoice');
Route::get('detail_invoice/{id}','App\Http\Controllers\InvoiceController@detail_invoice');
Route::get('edit_invoice/{id}','App\Http\Controllers\InvoiceController@edit_invoice');
Route::get('add_invoice','App\Http\Controllers\InvoiceController@add_invoice')->name('add_invoice');
Route::post('edit_invoice_act','App\Http\Controllers\InvoiceController@edit_invoice_act')->name('edit_invoice_act');
Route::get('bayar_invoice/{id}','App\Http\Controllers\InvoiceController@bayar_invoice');
Route::post('byr_invoice_act','App\Http\Controllers\InvoiceController@byr_invoice_act')->name('byr_invoice_act');

Route::get('info_pembayaran/{id}','App\Http\Controllers\InvoiceController@info_pembayaran');

Route::get('bayar_suplier','App\Http\Controllers\InvoiceController@bayar_suplier')->name('bayar_suplier');
Route::get('add_bayar_suplier','App\Http\Controllers\InvoiceController@add_bayar_suplier')->name('add_bayar_suplier');
Route::post('cek_penjualan','App\Http\Controllers\InvoiceController@cek_penjualan')->name('cek_penjualan');
Route::get('add_prd_bayar/{id}','App\Http\Controllers\InvoiceController@add_prd_bayar');
Route::get('act_bayar_suplier/{kode_pembayaran}/{kode_pembelian}','App\Http\Controllers\InvoiceController@act_bayar_suplier');
Route::get('cek_penjualan_get','App\Http\Controllers\InvoiceController@cek_penjualan_get')->name('cek_penjualan_get');

Route::get('cetak_pembayaran_suplier/{id}','App\Http\Controllers\InvoiceController@cetak_pembayaran_suplier');


// All About Manage Produk
Route::get('list_produk','App\Http\Controllers\ProdukController@list_produk')->name('list_produk');
Route::get('size','App\Http\Controllers\ProdukController@list_size')->name('size');
Route::post('add_size','App\Http\Controllers\ProdukController@add_size')->name('add_size');
Route::get('edit_size/{id}','App\Http\Controllers\ProdukController@edit_size');
Route::post('edit_size_act','App\Http\Controllers\ProdukController@edit_size_act')->name('edit_size_act');

Route::get('warna','App\Http\Controllers\ProdukController@list_warna')->name('warna');
Route::post('add_warna','App\Http\Controllers\ProdukController@add_warna')->name('add_warna');
Route::get('edit_warna/{id}','App\Http\Controllers\ProdukController@edit_warna');
Route::post('edit_warna_act','App\Http\Controllers\ProdukController@edit_warna_act')->name('edit_warna_act');

Route::get('satuan','App\Http\Controllers\ProdukController@list_satuan')->name('satuan');
Route::post('add_satuan','App\Http\Controllers\ProdukController@add_satuan')->name('add_satuan');
Route::get('edit_satuan/{id}','App\Http\Controllers\ProdukController@edit_satuan');
Route::post('edit_satuan_act','App\Http\Controllers\ProdukController@edit_satuan_act')->name('edit_satuan_act');

Route::get('kategori','App\Http\Controllers\ProdukController@list_kategori')->name('kategori');
Route::get('kategori2','App\Http\Controllers\ProdukController@list_kategori2')->name('kategori2');
Route::get('kategori3','App\Http\Controllers\ProdukController@list_kategori3')->name('kategori3');
Route::post('add_kategori','App\Http\Controllers\ProdukController@add_kategori')->name('add_kategori');
Route::get('edit_kategori/{id}','App\Http\Controllers\ProdukController@edit_kategori');
Route::post('edit_kategori_act','App\Http\Controllers\ProdukController@edit_kategori_act')->name('edit_kategori_act');
Route::post('add_sub_kategori','App\Http\Controllers\ProdukController@add_sub_kategori')->name('add_sub_kategori');
Route::get('hapus_kategori/{id}','App\Http\Controllers\ProdukController@hapus_kategori');
Route::get('hapus_warna/{id}','App\Http\Controllers\ProdukController@hapus_warna');

Route::get('edit_sub_kategori/{id}','App\Http\Controllers\ProdukController@edit_sub_kategori');
Route::post('edit_sub_kategori_act','App\Http\Controllers\ProdukController@edit_sub_kategori_act')->name('edit_sub_kategori_act');
Route::get('hapus_sub_kategori/{id}','App\Http\Controllers\ProdukController@hapus_sub_kategori');
Route::get('hapus_sub_kategori2/{id}','App\Http\Controllers\ProdukController@hapus_sub_kategori2');

Route::get('/getSubKategori/{id}','App\Http\Controllers\ProdukController@getSubKategori');
Route::get('/getSubKategoriA/{id}','App\Http\Controllers\ProdukController@getSubKategoriA');
Route::post('add_child_kategori','App\Http\Controllers\ProdukController@add_child_kategori')->name('add_child_kategori');

Route::get('edit_sub_kategori2/{id}','App\Http\Controllers\ProdukController@edit_sub_kategori2');
Route::post('edit_child_kategori_act','App\Http\Controllers\ProdukController@edit_child_kategori_act')->name('edit_child_kategori_act');





Route::post('add_produk','App\Http\Controllers\ProdukController@add_produk')->name('add_produk');
Route::get('add_detail_produk/{id}','App\Http\Controllers\ProdukController@add_detail_produk');
Route::get('detail_back_produk/{id}','App\Http\Controllers\ProdukController@detail_back_produk');
Route::post('add_det_produk','App\Http\Controllers\ProdukController@add_det_produk')->name('add_det_produk');
Route::get('back_detail_produk/{id}','App\Http\Controllers\ProdukController@back_detail_produk');
Route::post('add_foto','App\Http\Controllers\ProdukController@add_foto')->name('add_foto');
Route::post('upload_foto_produk','App\Http\Controllers\ProdukController@upload_foto_produk')->name('upload_foto_produk');

Route::get('add_back_produk','App\Http\Controllers\ProdukController@add_back_produk')->name('add_back_produk');
Route::post('add_back_produk_act','App\Http\Controllers\ProdukController@add_back_produk_act')->name('add_back_produk_act');
Route::post('edit_back_produk_act','App\Http\Controllers\ProdukController@edit_back_produk_act')->name('edit_back_produk_act');
Route::post('edit_back_produkspl_act','App\Http\Controllers\ProdukController@edit_back_produkspl_act')->name('edit_back_produkspl_act');
Route::get('add_new_detail_produk/{id}','App\Http\Controllers\ProdukController@add_new_detail_produk');
Route::get('edit_back_produk/{id}','App\Http\Controllers\ProdukController@edit_back_produk');
Route::get('detail_produk/{id}','App\Http\Controllers\ProdukController@detail_produk');
Route::post('add_stok_produk','App\Http\Controllers\ProdukController@add_stok_produk')->name('add_stok_produk');
Route::get('edit_det_produk/{id}','App\Http\Controllers\ProdukController@edit_det_produk');
Route::get('delete_det_produk/{id}','App\Http\Controllers\ProdukController@delete_det_produk');
Route::post('edit_stok_act','App\Http\Controllers\ProdukController@edit_stok_act')->name('edit_stok_act');
Route::get('foto_produk/{id}','App\Http\Controllers\ProdukController@foto_produk');
Route::get('foto_produkspl/{id}','App\Http\Controllers\ProdukController@foto_produkspl');
Route::post('add_foto_produk','App\Http\Controllers\ProdukController@add_foto_produk')->name('add_foto_produk');
Route::post('add_foto_produk_spl','App\Http\Controllers\ProdukController@add_foto_produk_spl')->name('add_foto_produk_spl');
Route::get('hapus_foto_produk/{id}','App\Http\Controllers\ProdukController@hapus_foto_produk');
Route::get('foto_utama_create/{id}','App\Http\Controllers\ProdukController@foto_utama_create');
Route::get('detail_det_produk/{id}','App\Http\Controllers\ProdukController@detail_det_produk');

Route::get('edit_produk/{id}','App\Http\Controllers\ProdukController@edit_produk');

Route::get('edit_produkspl/{id}','App\Http\Controllers\ProdukController@edit_produkspl');





// All About E-Commerce
Route::get('eproduk','App\Http\Controllers\EcommerceController@e_produk')->name('eproduk');
Route::get('eproduk2','App\Http\Controllers\EcommerceController@eproduk2')->name('eproduk2');
Route::get('buy_produk/{id}','App\Http\Controllers\EcommerceController@buy_produk');

Route::get('e_ecom_detail/{id}','App\Http\Controllers\EcommerceController@e_ecom_detail');
Route::get('checkout','App\Http\Controllers\EcommerceController@checkout');





// All About Petugas
Route::get('petugas','App\Http\Controllers\PetugasController@petugas')->name('petugas');
Route::post('add_petugas','App\Http\Controllers\PetugasController@add_petugas')->name('add_petugas');
Route::get('edit_petugas/{id}','App\Http\Controllers\PetugasController@edit_petugas');
Route::post('edit_petugas_act','App\Http\Controllers\PetugasController@edit_petugas_act')->name('edit_petugas_act');
Route::get('hapus_petugas/{id}','App\Http\Controllers\PetugasController@hapus_petugas');


// All About Akuntansi (Aset)
Route::get('aset','App\Http\Controllers\AkuntansiController@list_aset')->name('aset');
Route::get('add_aktiva','App\Http\Controllers\AkuntansiController@add_aktiva')->name('add_aktiva');
Route::get('add_pasiva','App\Http\Controllers\AkuntansiController@add_pasiva')->name('add_pasiva');
Route::post('add_aset_act','App\Http\Controllers\AkuntansiController@add_aset_act')->name('add_aset_act');
Route::get('detail_aset/{id}','App\Http\Controllers\AkuntansiController@detail_aset');
Route::get('add_detail_aset/{id}','App\Http\Controllers\AkuntansiController@add_detail_aset');
Route::post('add_detail_aset_act','App\Http\Controllers\AkuntansiController@add_detail_aset_act')->name('add_detail_aset_act');  

//jurnal umum
Route::get('jurnal_umum','App\Http\Controllers\AkuntansiController@jurnal_umum')->name('jurnal_umum');
Route::get('add_jurnal_umum','App\Http\Controllers\AkuntansiController@add_jurnal_umum')->name('add_jurnal_umum');
Route::get('/getForm','App\Http\Controllers\AkuntansiController@getForm');
Route::get('/getForm2','App\Http\Controllers\AkuntansiController@getForm2');
Route::post('add_arus_kas_act','App\Http\Controllers\AkuntansiController@add_arus_kas_act')->name('add_arus_kas_act');  



Route::get('logout','App\Http\Controllers\AuthController@logout')->name('logout');

Route::get('under_maintenance','App\Http\Controllers\AuthController@under_maintenance')->name('under_maintenance');

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

