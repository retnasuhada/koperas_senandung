 @extends('template')
 @section('content')
 @yield('content')
 <div class="row">

   <div class="col-md-6">
     <!-- general form elements -->
     <div class="card card-success">
       <div class="card-header">
         <h3 class="card-title">Tambah Jabatan</h3>
       </div>
       <!-- /.card-header -->
       <!-- form start -->
       <form action="{{ route('add_jabatan') }}" method="post">
         @csrf

         <div class="card-body">
           <div class="form-group">
             <label for="exampleInputEmail1">Nama Jabatan</label>
             @if($data['flag_edit']=='1')
             <input type="texs" namse="nama_jabatan" class="form-control" value="{{$data['nama_jabatan']}}">
             @else
             <input type="texs" name="nama_jabatan" class="form-control" placeholder="Masukan Nama Jabatan">
             @endif
           </div>

         </div>
         <!-- /.card-body -->

         <div class="card-footer">
           <button type="submit" class="btn btn-primary">Simpan</button>
         </div>
       </form>
     </div>
   </div>
   <div class="col-md-6">

     <div class="card">
       <div class="card-header">
         <h3 class="card-title">List Jenis Pinjaman</h3>
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table class="table table-bordered">
           <thead>
             <tr>
               <th style="width: 10px">#</th>
               <th>Nama Jenis Pinjaman</th>

               <th style="width: 100px">Aksi</th>
             </tr>
           </thead>
           <tbody>
             <?php
              $no = 1;
              foreach ($data['list_jabatan'] as $row) { ?>
               <tr>
                 <td>{{$no}}</td>
                 <td>{{$row->nama_jabatan}}</td>

                 <td>
                   <a href="{{ url('edit_jabatan/'.$row->id_jabatan) }}"><i class="fas fa-edit"></i></a>
                   &nbsp;&nbsp;
                   <a href="#"><i class="fas fa-trash"></i></a>

                 </td>
               </tr>

             <?php $no++;
              } ?>



           </tbody>
         </table>
       </div>
       <!-- /.card-body -->

     </div>
   </div>
 </div>
 @endsection