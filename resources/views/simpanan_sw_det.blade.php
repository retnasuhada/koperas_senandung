 @extends('template')
 @section('content')
 @yield('content')
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif



 <div class="card">
   <div class="card-header">
     <a href="{{route('add_sw')}}" class="btn btn-primary">
       <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Simpanan Wajib
     </a>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
     <table id="example1" class="table table-bordered table-striped">
       <thead>
         <tr>
           <th>No</th>
           <th>Tanggal</th>
           <th>Nama Anggota</th>
           <th>Jenis Penerimaan</th>
           <th>Periode Simpanan</th>
           <th>Jumlah Simpanan</th>
           <th width="50px;">Aksi</th>
         </tr>
       </thead>
       <tbody>

         <?php
          $no = 1;
          foreach ($data['simpanan_det'] as $row) {
            $arr_periode = explode("-", $row->periode_simpanan);
            $bln         = $arr_periode[0];
            $thn         = $arr_periode[1];
            if ($bln == '1') {
              $periode_simpanan = 'Januari - ' . $thn;
            }
            if ($bln == '2') {
              $periode_simpanan = 'Februari - ' . $thn;
            }
            if ($bln == '3') {
              $periode_simpanan = 'Maret - ' . $thn;
            }
            if ($bln == '4') {
              $periode_simpanan = 'April - ' . $thn;
            }
            if ($bln == '5') {
              $periode_simpanan = 'Mei - ' . $thn;
            }
            if ($bln == '6') {
              $periode_simpanan = 'Juni - ' . $thn;
            }
            if ($bln == '7') {
              $periode_simpanan = 'Juli - ' . $thn;
            }
            if ($bln == '8') {
              $periode_simpanan = 'Agustus - ' . $thn;
            }
            if ($bln == '9') {
              $periode_simpanan = 'September - ' . $thn;
            }
            if ($bln == '10') {
              $periode_simpanan = 'Oktober - ' . $thn;
            }
            if ($bln == '11') {
              $periode_simpanan = 'November - ' . $thn;
            }
            if ($bln == '12') {
              $periode_simpanan = 'Desember - ' . $thn;
            }

          ?>
           <tr>
             <td>{{$no}}</td>
             <td>{{$row->tgl_simpanan}}</td>
             <td>{{$row->nama_anggota}}</td>
             <td>{{$row->nama_akun}}</td>
             <td>{{$periode_simpanan}}</td>
             <td>{{"Rp " . number_format($row->jumlah_simpanan ,2,',','.')}}</td>
             <td>
               <a href="{{ url('edit_simpanan/'.$row->id_simpanan) }}"><i class="fas fa-edit"></i></a>
               &nbsp;&nbsp;
               <a href="{{ url('detail_simpanan/'.$row->id_simpanan) }}"><i class="fas fa-eye"></i></a>

             </td>
           </tr>
         <?php }
          ?>
         </tfoot>
     </table>
   </div>
   <!-- /.card-body -->
 </div>
 @endsection