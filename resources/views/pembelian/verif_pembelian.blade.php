 @extends('template')
 @section('content')
 @yield('content')
 <div class="row">

   
     <div class="col-md-12">

       <div class="card card-success">
         <div class="card-header">
           <h3 class="card-title">List Detail Pembelian</h3>
         </div>
         <!-- /.card-header -->
         <div class="card-body">
          @if($data['jml'] > 0)
           <table class="table table-bordered">
             <thead>
               <tr>
                 <th style="width: 10px">#</th>
                 <th>Nama Produk</th>
                 <th>Jumlah</th>
                 <th>Harga</th>
                 <th style="width: 100px">Aksi</th>
               </tr>
             </thead>
             <tbody>
               @foreach($data['list_pembelian'] as $row)
                 <tr>
                   <td>#</td>
                   <td>{{$row->nama_produk}}</td>
                   <td>{{$row->jumlah}}</td>
                   <td> {{"Rp " . number_format($row->harga_semua ,2,',','.')}}</td>

                   <td>
                    <a href="{{ url('verifikasi_detail/'.$row->id_detail_pembelian) }}"><i class="fas fa-check"></i></a> &nbsp;&nbsp;
                     <a href="#"><i class="fas fa-edit"></i></a>
                     &nbsp;&nbsp;
                     <a href="#"><i class="fas fa-trash"></i></a>

                   </td>
                 </tr>

               @endforeach



             </tbody>
           </table>
          @endif
            <a href="{{ url('view_verified/'.$data['kode_pembelian']) }}"> Total Produk Terverifikasi {{$data['jml_terverifikasi']}} </a>
          <br>
         </div>
         <!-- /.card-body -->

       </div>
     </div>

   </div>




   <div class="row">

     <div class="col-md-12">
       <!-- general form elements -->
       <div class="card card-success">
         <div class="card-header">
           <h3 class="card-title">Pembelian</h3>
         </div>

         <form action="{{ route('save_verif_pembelian_act') }}" method="post">

           @csrf

           <div class="card-body">

             <div class="form-group">
               <label for="exampleInputEmail1">Kode Pembelian</label>
              
               <input type="text" readonly="" name="kode_pembelian" class="form-control" value="{{$data['kode_pembelian']}}">
               <input type="hidden" readonly="" name="kode_suplier" class="form-control" value="{{$data['kode_suplier']}}">
               
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Tanggal Pembelian</label>
               <div class="input-group date" id="reservationdate" data-target-input="nearest">

                 <input type="text" name="tgl_pembelian" class="form-control datetimepicker-input" data-target="#reservationdate">
                 <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                   <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                 </div>

               </div>
             </div>
             
             
             <div class="result form-group">
               
             </div>
             


             <div class="form-group">
               <label for="exampleInputEmail1">Biaya Tambahan</label>
               @if($data['flag_edit']=='1')
               <input type="text" name="biaya_tambahan" id="rupiah2" class="rupiah2 form-control" value="{{$data['harga']}}">
               @else
               <input type="text" name="biaya_tambahan" id="rupiah2" class="rupiah2 form-control" placeholder="Masukan Biaya Tambahan">
               @endif
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Potongan / Diskon</label>
               @if($data['flag_edit']=='1')
               <input type="text" name="diskon" id="rupiah3" class="rupiah3 form-control" value="{{$data['diskon']}}">
               @else
               <input type="text" name="diskon" id="rupiah3" class="rupiah3 form-control" placeholder="Masukan Potongan Pembayaran / Diskon">
               @endif
             </div>

             <div class="form-group">
               <label for="exampleInputEmail1">Jenis Pembelian</label>
               <select class="form-control" name="jenis_pembelian">
                 <option value="1">Pembelian Cash</option>
                 <option value="2">Titip Barang</option>
                 <option value="3">Pembayarn Tempo</option>
               </select>
             </div>
           </div>
           <!-- /.card-body -->

           <div class="card-footer">
             <button type="submit" class="btn btn-primary">Simpan</button>
           </div>
         </form>
       </div>
     </div>

   </div>
   

   <script type="text/javascript">
    function option() {
        $(".result").html(null);
       var n = $("#jenis_invoice").val();
       if(n==1){
          var html = "<label for='exampleInputEmail1'>No Invoice</label>";
            
            html += "<input type='text' id='invoice' name='no_invoice' class='form-control' value='{{KodeInvoice()}}' >";
            
       }else{
          var html = "<label for='exampleInputEmail1'>No Invoice</label>";
            html += "@if($data['flag_edit']=='1')";
            html += "<input type='text' name='no_invoice' class='form-control' value='{{$data['jumlah']}}'>";
            html += "@else";
            html += "<input type='text' id='invoice' required=' name='no_invoice' class='form-control' placeholder='Masukan Nomor Invoice'>";
            html += "@endif";
       }
       
        $(".result").append(html);

      
     }

     var rupiah = document.getElementById('rupiah');
     rupiah.addEventListener('keyup', function(e) {

       rupiah.value = formatRupiah(this.value, 'Rp. ');
     });

     var rupiah2 = document.getElementById('rupiah2');
     rupiah2.addEventListener('keyup', function(e) {

       rupiah2.value = formatRupiah(this.value, 'Rp. ');
     });
     var rupiah3 = document.getElementById('rupiah3');
     rupiah3.addEventListener('keyup', function(e) {

       rupiah3.value = formatRupiah(this.value, 'Rp. ');
     });

     /* Fungsi formatRupiah */
     function formatRupiah(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 3,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{3}/gi);

       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
     }

     

   </script>


   @endsection