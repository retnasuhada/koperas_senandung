 @extends('template')
 @section('content')
 @yield('content')
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 <div class="card">
   <div class="card-header">
     <a href="{{route('add_pembelian')}}" class="btn btn-primary">
       <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Pembelian
     </a>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
     <table id="example1" class="table table-bordered table-striped">
       <thead>

         <tr>
           <th>No</th>
           <th>Tanggal</th>
           <th>Invoice</th>
           <th>Suplier</th>
           <th>Jenis Pembelian</th>
           <th>Total Biaya</th>
           <th>Status</th>
           <th width="50px;">Aksi</th>
         </tr>
       </thead>
       <tbody>
         <?php
          $no = 1;
          foreach ($data['list_pembelian'] as $row) {
            $total = $row->total + $row->biaya_tambahan;
            if($row->jenis_pembelian == '2'){
                $jenis_pembelian = 'Titip';
            }else{
                $jenis_pembelian = 'Cash';
            }
            

          ?>
           <tr>
             <td>{{$no}}</td>
             <td>{{$row->tgl_pembelian}}</td>
             <td>{{$row->no_invoice}}</td>
             <td>{{$row->nama_suplier}}</td>
             <td>{{$jenis_pembelian}}</td>
             
             <td>{{"Rp " . number_format($total ,2,',','.')}}</td>
             <td><?php if($row->verified=='0'){ ?>
              <a href="{{ url('verif_pembelian/'.$row->kode_pembelian) }}" class="btn btn-block btn-outline-warning btn-xs">Belum Di verifikasi</a>
            <?php } else {?>
              <a href="#" class="btn btn-block btn-outline-success btn-xs">Verified</a> <?php } ?>
            </td>

             <td>
               <a href="{{ url('edit_pembelian/'.$row->kode_pembelian) }}"><i class="fas fa-edit"></i></a>
               &nbsp;&nbsp;
               <a href="{{ url('detail_pembelian/'.$row->kode_pembelian) }}"><i class="fas fa-eye"></i></a>
             </td>
           </tr>

         <?php $no++;
          } ?>

         </tfoot>
     </table>
   </div>
   <!-- /.card-body -->
 </div>
 @endsection