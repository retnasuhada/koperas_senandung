 @extends('template')
 @section('content')
 @yield('content')
 @if($data['lunas'] == '0')
 <div class="row">
    <div class="col-md-12">
       <div class="card">
         <div class="card-header">
           <h3 class="card-title">List Detail Pembelian</h3>
         </div>
         <!-- /.card-header -->
         <div class="card-body">
           <table class="table table-bordered">
             <thead>
               <tr>
                 <th style="width: 10px">#</th>
                 <th>Nama Produk</th>
                 <th>Jumlah</th>
                 <th>Harga</th>
                 
               </tr>
             </thead>
             <tbody>
               <?php
                $no = 1;
                foreach ($data['list_pembelian'] as $row) { ?>
                 <tr>
                   <td>{{$no}}</td>
                   <td>{{$row->nama_produk}}</td>
                   <td>{{$row->jumlah}}</td>
                   <td> {{"Rp " . number_format($row->harga_semua ,2,',','.')}}</td>
                 </tr>

               <?php $no++;
                } ?>



             </tbody>
           </table>

         </div>
         <!-- /.card-body -->

       </div>
     </div>
   </div>

   <div class="row">

     <div class="col-md-12">
       <!-- general form elements -->
       <div class="card card-success">
         <div class="card-header">
           <h3 class="card-title">Pembelian</h3>
         </div>

         <form action="{{ route('act_edit_pembelian') }}" method="post">

           @csrf

           <div class="card-body">

             <div class="form-group">
               <label for="exampleInputEmail1">Kode Pembelian</label>
              
               <input type="text" readonly="" name="kode_pembelian" class="form-control" value="{{$data['kode_pembelian']}}">
             
             </div>
             <div class="form-group">
             <label for="exampleInputEmail1">Tanggal Invoice</label>
             <div class="input-group date" id="reservationdate" data-target-input="nearest">

               <input type="text" name="tgl_invoice" value="{{$data['tgl_pembelian']}}" class="form-control datetimepicker-input" data-target="#reservationdate">
               <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                 <div class="input-group-text"><i class="fa fa-calendar"></i></div>
               </div>

             </div>
           </div>
             <div class="form-group">
               <label for="exampleInputEmail1">No Invoice</label>
              
               <input type="text" name="no_invoice" class="form-control" value="{{$data['no_invoice']}}">
               <input type="hidden" name="no_invoice_old" class="form-control" value="{{$data['no_invoice']}}">
              
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Suplier</label>
               <select class="form-control select2bs4" name="kode_suplier">
                 <option value=""></option>
                 @foreach($data['list_suplier'] as $row)
               <option value="{{$row->kode_suplier}}" <?php if ($data['kode_suplier'] == $row->kode_suplier) {
                                                        echo "selected";
                                                      } ?>>{{$row->nama_suplier}}</option>
               @endforeach
               </select>
             </div>


             <div class="form-group">
               <label for="exampleInputEmail1">Biaya Tambahan</label>
               
               <input type="text" name="biaya_tambahan" id="rupiah2" class="rupiah2 form-control" value=" {{"Rp " . number_format($data['biaya_tambahan'] ,0,',','.')}}">
              
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Potongan / Diskon</label>
              
               <input type="text" name="diskon" id="rupiah3" class="rupiah3 form-control" value=" {{"Rp " . number_format($data['diskon'] ,0,',','.')}}">
              
             </div>
             
             <div class="form-group">
               <label for="exampleInputEmail1">Jenis Pembelian</label>
               <select class="form-control" name="jenis_pembelian">
                 <option value="1" <?php if ($data['jenis_pembelian'] == '1') {
                                                        echo "selected"; }  ?>>Pembelian Cash</option>
                 <option value="2"  <?php if ($data['jenis_pembelian'] == '2') {
                                                        echo "selected"; }  ?>>Titip Barang</option>
                 <option value="3"  <?php if ($data['jenis_pembelian'] == '3') {
                                                        echo "selected"; }  ?>>Pembayarn Tempo</option>
               </select>
             </div>
           </div>
           <!-- /.card-body -->

           <div class="card-footer">
             <button type="submit" class="btn btn-primary">Simpan</button>
           </div>
         </form>
       </div>
     </div>

   </div>
@else
<div class="row">
    <div class="col-md-12">
       <div class="card">
         <div class="card-header">
           <h3 class="card-title">List Detail Pembelian</h3>
         </div>
         <!-- /.card-header -->
         <div class="card-body">
           <table class="table table-bordered">
             <thead>
               <tr>
                 <th style="width: 10px">#</th>
                 <th>Nama Produk</th>
                 <th>Jumlah</th>
                 <th>Harga</th>
                 
               </tr>
             </thead>
             <tbody>
               <?php
                $no = 1;
                foreach ($data['list_pembelian'] as $row) { ?>
                 <tr>
                   <td>{{$no}}</td>
                   <td>{{$row->nama_produk}}</td>
                   <td>{{$row->jumlah}}</td>
                   <td> {{"Rp " . number_format($row->harga_semua ,2,',','.')}}</td>
                 </tr>

               <?php $no++;
                } ?>



             </tbody>
           </table>
           <br>
           <p>Pembelian ini sudah dilakukan pembayar invoice, harap menghubungi bagian IT</p>
         </div>
         <!-- /.card-body -->
 
       </div>
     </div>
   </div>

@endif
   <script type="text/javascript">
     var rupiah = document.getElementById('rupiah');
    
     var rupiah2 = document.getElementById('rupiah2');
     rupiah2.addEventListener('keyup', function(e) {

       rupiah2.value = formatRupiah(this.value, 'Rp. ');
     });
     var rupiah3 = document.getElementById('rupiah3');
     rupiah3.addEventListener('keyup', function(e) {

       rupiah3.value = formatRupiah(this.value, 'Rp. ');
     });

   

     /* Fungsi formatRupiah */
     function formatRupiah(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 3,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{3}/gi);

       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
     }
   </script>


   @endsection