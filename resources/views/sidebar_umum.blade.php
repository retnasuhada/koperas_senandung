 <!-- Sidebar -->
 
 <div class="sidebar">
   <!-- Sidebar user (optional) -->
  
   <nav class="mt-2">
     <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

       <li class="nav-item <?php if ($data['menu'] == 'simpanan') {
                              echo "menu-open";
                            } ?>">
         <a href="#" class="nav-link">
           <i class="nav-icon fas fa-book"></i>
           <p>
             Kategori
             <i class="right fas fa-angle-left"></i>
           </p>
         </a>
         <ul class="nav nav-treeview">
           <li class="nav-item">
             <a href="#" class="nav-link <?php if ($data['Active'] == 'list_sw') {
                                                                echo "active";
                                                              } ?>">
               <i class="far fa-circle nav-icon"></i>

               <p>Simpanan Wajib</p>
             </a>
             <ul class="nav nav-treeview">
              <li class="nav-item">
                <li class="nav-item">
             <a href="#" class="nav-link <?php if ($data['Active'] == 'list_sw') {
                                                                echo "active";
                                                              } ?>">
               <i class="far fa-circle nav-icon"></i>
               <i class="far fa-circle nav-icon"></i>
               <p>Simpanan 2</p>
             </a>
           </li>
           </li>
         </ul>
           <li class="nav-item">
             <a href="{{ route('agt_ss') }}" class="nav-link <?php if ($data['Active'] == 'list_ss') {
                                                                echo "active";
                                                              } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Simpanan Sukarela</p>
             </a>
           </li>
           <li class="nav-item">
             <a href="{{ route('agt_sp') }}" class="nav-link <?php if ($data['Active'] == 'list_sp') {
                                                                echo "active";
                                                              } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Simpanan Pokok</p>
             </a>
           </li>
           <li class="nav-item">
             <a href="{{ route('agt_modal') }}" class="nav-link <?php if ($data['Active'] == 'modal_anggota') {
                                                                  echo "active";
                                                                } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Modal Anggota</p>
             </a>
           </li>
           <li class="nav-item">
             <a href="{{ route('agt_hibah') }}" class="nav-link <?php if ($data['Active'] == 'dana_hibah') {
                                                                  echo "active";
                                                                } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Dana Hibah</p>
             </a>
           </li>
         </ul>
       </li>
       <li class="nav-item <?php if ($data['menu'] == 'pinjaman') {
                              echo "menu-open";
                            } ?>">
         <a href="#" class="nav-link">
           <i class="nav-icon fas fa-columns"></i>

           <p>
             Pinjaman
             <i class="right fas fa-angle-left"></i>
           </p>
         </a>
         <ul class="nav nav-treeview">
           <li class="nav-item">
             <a href="../../index.html" class="nav-link <?php if ($data['Active'] == 'form_pinjaman') {
                                                          echo "active";
                                                        } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Ajukan Pinjanman</p>
             </a>
           </li>
           <li class="nav-item">
             <a href="../../index2.html" class="nav-link <?php if ($data['Active'] == 'list_pinjaman') {
                                                            echo "active";
                                                          } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>List Pinjaman</p>
             </a>
           </li>
         </ul>
       </li>
       <li class="nav-item <?php if ($data['menu'] == 'akuntansi') {
                              echo "menu-open";
                            } ?>">
         <a href="{{ url('profil_anggota/'.$id_anggota) }}" class="nav-link">
           <i class="nav-icon fa fa-user"></i>
           <p>
             Profil
             <i class="right fas fa-angle-left"></i>
           </p>
         </a>

       </li>



       <li class="nav-item ">
         <a href="#" class="nav-link">
           <i class="nav-icon far fa-plus-square"></i>
           <p>
             Usaha
             <i class="right fas fa-angle-left"></i>
           </p>
         </a>
         <ul class="nav nav-treeview">
           <li class="nav-item">
             <a href="{{ route('jenis_simpanan') }}" class="nav-link active <?php if ($data['Active'] == 'toko') {
                                                                              echo "active";
                                                                            } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Toko</p>
             </a>
           </li>
           <li class="nav-item">
             <a href="{{ route('produk') }}" class="nav-link <?php if ($data['Active'] == 'produk') {
                                                                echo "active";
                                                              } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Produk</p>
             </a>
           </li>
           <li class="nav-item">
             <a href="{{ route('produk') }}" class="nav-link <?php if ($data['Active'] == 'suplier') {
                                                                echo "active";
                                                              } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Suplier</p>
             </a>
           </li>

         </ul>
       </li>
       

     </ul>
   </nav>
 </div>