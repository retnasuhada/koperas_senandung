 @extends('login_view')
 @section('content')
 <div class="login-box">
   <!-- /.login-logo -->
   <div class="card card-outline card-primary">
     <div class="card-header text-center">
       <a href="../../index2.html" class="h1"><b>KOPERASI</b></a>
       <p><b>Senandung 165</b></p>
     </div>
     <div class="card-body">

       <form action="{{ route('login') }}" method="post">
         @csrf
         @if(session('errors'))
         <div class="alert alert-danger alert-dismissible fade show" role="alert">
           Something it's wrong:
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
             <span aria-hidden="true">×</span>
           </button>
           <ul>
             @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
             @endforeach
           </ul>
         </div>
         @endif
         @if (Session::has('success'))
         <div class="alert alert-success">
           {{ Session::get('success') }}
         </div>
         @endif
         @if (Session::has('error'))
         <div class="alert alert-danger">
           {{ Session::get('error') }}
         </div>
         @endif
         <div class="input-group mb-3">
           <input type="teks" name="username" class="form-control" placeholder="username">
           <div class="input-group-append">
             <div class="input-group-text">
               <span class="fas fa-envelope"></span>
             </div>
           </div>
         </div>
         <div class="input-group mb-3">
           <input type="password" name="password" class="form-control" placeholder="Password">
           <div class="input-group-append">
             <div class="input-group-text">
               <span class="fas fa-lock"></span>
             </div>
           </div>
         </div>
         <div class="row">
           <div class="col-8">
             <div class="icheck-primary">
               <input type="checkbox" id="remember">
               <label for="remember">
                 Remember Me
               </label>
             </div>
           </div>
           <!-- /.col -->
           <div class="col-4">
             <button type="submit" class="btn btn-primary btn-block">Sign In</button>
           </div>
           <!-- /.col -->
         </div>
       </form>


       <p class="mb-1">
         <a href="forgot-password.html">I forgot my password</a>
       </p>
       <p class="mb-0">
         <a href="register.html" class="text-center">Register a new membership</a>
       </p>
     </div>
     <!-- /.card-body -->
   </div>
   <!-- /.card -->
 </div>
 <!-- /.login-box -->

 @endsection