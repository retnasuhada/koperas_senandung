 @extends('template')
 @section('content')
 @yield('content')
 <!-- Main content -->
 <div class="invoice p-3 mb-3">
   <!-- title row -->
   <div class="row">
     <div class="col-12">
       <h4>
         <i class="fas fa-envelope-square"></i> No Invoice : {{$data['no_invoice']}}
         <small class="float-right">Tanggal {{$data['tgl_invoice']}}</small>
       </h4>
     </div>
     <!-- /.col -->
   </div>
   <!-- info row -->
   <div class="row invoice-info">
     <div class="col-sm-6 invoice-col">
       Dari
       <address>
         @foreach($data['det_suplier'] as $suplier)
         <strong>{{$suplier->nama_suplier}}</strong><br>
         {{$suplier->alamat}}<br>
         {{$suplier->no_hp}}<br>
         {{$suplier->telp}}<br>
         Email: info@almasaeedstudio.com

         @endforeach
       </address>
     </div>

     <!-- /.col -->
     <div class="col-sm-6 invoice-col">
       Kepada
       <address>
         <strong>KSU 165</strong><br>
         Argosubur, Blok S11/11<br>
         Solear, Tangerang<br>
         Phone: 0821 1243 3114<br>
         Email: koperasisenandung165@gmail.com
       </address>
     </div>
     <!-- /.col -->

     <!-- /.col -->
   </div>
   <!-- /.row -->

   <!-- Table row -->
   <div class="row">
     <div class="col-12 table-responsive">
       <table class="table table-striped">
         <thead>
           <tr>
             <th>No</th>
             <th>Nama Produk</th>
             <th>Harga</th>
             <th>Qty</th>
             <th>Total</th>
           </tr>
         </thead>
         <tbody>
           <?php
            $no = 1;
            $sub_total = 0;
            foreach ($data['det_pembelian'] as $row) {
              $harga = $row->harga_semua / $row->jumlah;
              $sub_total = $sub_total + $row->harga_semua;
            ?>
             <tr>
               <td>{{$no}}</td>
               <td>{{$row->nama_produk}}</td>
               <td>{{"Rp " . number_format($harga ,2,',','.')}}</td>
               <td>{{$row->jumlah}}</td>
               <td>{{"Rp " . number_format($row->harga_semua ,2,',','.')}}</td>
             </tr>
           <?php  } ?>
         </tbody>
       </table>
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->

   <div class="row">
     <!-- accepted payments column -->
     <div class="col-6">
       <p class="lead">Metode Pembayaran</p>
       <img src="{{ asset('Admin/dist/img/credit/bca.png')}}" alt="Visa">
       <img src="{{ asset('Admin/dist/img/credit/bni.jpg')}}" alt="Mastercard">
       <img src="{{ asset('Admin/dist/img/credit/bri.png')}}" alt="American Express">
       <img src="{{ asset('Admin/dist/img/credit/mandiri.png')}}" alt="Paypal">

       <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
         Dapatkan Nomor Rekening dan tunjukan bukti pembayaran kepada kasir (admin) toko Koperasi Senandung 165
       </p>
     </div>
     <!-- /.col -->
     <div class="col-6">
       <p class="lead">Tanggal Cetak : {{Date('Y-m-d')}}</p>

       <div class="table-responsive">
         <table class="table">
           <tr>
             <th style="width:50%">Subtotal:</th>
             <td>{{"Rp " . number_format($sub_total,2,',','.')}}</td>
           </tr>
           <tr>
             <th>Biaya Tambahan</th>
             <td>{{"Rp " . number_format($data['biaya_tambahan'],2,',','.')}}</td>
           </tr>
           <tr>
             <th>Diskon:</th>
             <td>{{"Rp " . number_format($data['diskon'],2,',','.')}}</td>
           </tr>
           <tr>
             <th>Total:</th>
             <td>{{"Rp " . number_format($data['total'],2,',','.')}}</td>
           </tr>
         </table>
       </div>
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->

   <!-- this row will not appear when printing -->
   <div class="row no-print">
     <div class="col-12">
       <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Cetak</a>
       <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Lakukan Pembayaran
       </button>
       <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
         <i class="fas fa-download"></i> Download PDF
       </button>
     </div>
   </div>
 </div>
 <!-- /.invoice -->
 </div><!-- /.col -->
 </div><!-- /.row -->
 </div><!-- /.container-fluid -->

 @endsection