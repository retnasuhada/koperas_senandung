 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif

     <div class="card card-primary">
       <div class="card-header">
        <a href="{{route('add_bayar_suplier')}}" class="btn btn-primary">
          <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Pembayaran Suplier
        </a>

       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table id="example1" class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>#</th>
               <th>No Pembayaran</th>
               <th>Tgl Pembayaran</th>
               <th>Nama Suplier</th>
               <th>Total Pembayaran</th>
               <th>Opsi</th>

             </tr>
           </thead>
           <tbody>
            @php 
            $no=0;
            @endphp
           @foreach($data['list_pembayaran_suplier'] as $row)
              @php 
            $no++;
            @endphp
                <tr>
                 <td>{{$no}}</td>
                 <td>{{$row->kode_pembayaran_suplier}}</td>
                 <td>{{$row->tgl_pembayaran}}</td>
                 <td>{{$row->nama_suplier}}</td>
                 <td>{{"Rp " . number_format($row->total ,2,',','.')}}</td> 
                 <td>
                  
                   <a title="lihat detail" href="{{ url('cetak_pembayaran_suplier/'.$row->kode_pembayaran_suplier) }}"><i class="fa fa-print"></i></a>
                 

                 </td>
           
               </tr>
          @endforeach           
           
             </tfoot>
         </table>
       </div>
       <!-- /.card-body -->
     </div>

     @endsection