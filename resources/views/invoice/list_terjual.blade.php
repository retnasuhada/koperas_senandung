 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif

     <div class="card card-primary">
       <div class="card-header">
        <a href="{{route('add_bayar_suplier')}}" class="btn btn-primary">
          <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Pembayaran Suplier
        </a>

       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table id="example1" class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>No</th>
               <th>Tanggal Penjualan</th>
               <th>Nama Produk</th>
               <th>Harga Suplier</th>
               <th>Jumlah</th>
               <th>Total</th>
               <th>Opsi</th>

             </tr>
           </thead>
           <tbody>
           
              
                   <tr>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 
                 <td>
                   <a title="edit" href="#"><i class="fas fa-edit"></i></a>
                   &nbsp;&nbsp;
                   <a title="lihat detail" href="#"><i class="far fa-eye"></i></a>
                 

                 </td>
               </tr>
           
           
             </tfoot>
         </table>
       </div>
       <!-- /.card-body -->
     </div>

     @endsection