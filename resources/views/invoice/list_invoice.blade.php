 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif

     <div class="card card-primary">
       <div class="card-header">

         <i class="fas fa-info"></i> &nbsp; &nbsp;Daftar Invoice

       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table id="example1" class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>No</th>
               <th>No Invoice</th>
               <th>Tgl Invoice</th>
               <th>Nama Suplier</th>
               <th>Total Invoice</th>
               <th>Dibayar</th>
               <th>Status</th>
               <th>Opsi</th>

             </tr>
           </thead>
           <tbody>

             <?php
              $no = 1;
              foreach ($data['list_invoice'] as $row) {
                if ($row->status == '0') {
                  $status = 'Belum Lunas';
                } else {
                  $status = 'Lunas';
                }
              ?>
               <tr>
                 <td>{{$no}}</td>
                 <td>{{$row->no_invoice}}</td>
                 <td>{{$row->tgl_invoice}}</td>
                 <td>{{$row->nama_suplier}}</td>
                 <td>{{"Rp " . number_format($row->total ,2,',','.')}}</td>
                 <td>{{"Rp " . number_format($row->pembayaran ,2,',','.')}}</td>
                 <td>
                   <button type="button" class="btn btn-block btn-outline-warning btn-xs">{{$status}}</button>
                 </td>
                 <td>
                   <a title="edit" href="{{ url('edit_invoice/'.$row->no_invoice) }}"><i class="fas fa-edit"></i></a>
                   &nbsp;&nbsp;
                   <a title="lihat detail" href="{{ url('detail_invoice/'.$row->no_invoice) }}"><i class="far fa-eye"></i></a>
                   &nbsp;&nbsp;
                   @if($row->status=='0')
                   <a title="bayar" href="{{ url('bayar_invoice/'.$row->no_invoice) }}"><i class="fa fa-credit-card"></i></a>
                   @else
                   <a title="Lihat" href="{{ url('info_pembayaran/'.$row->no_invoice) }}"><i class="fa fa-info"></i></a>
                   @endif


                 </td>
               </tr>

             <?php $no++;
              } ?>
             </tfoot>
         </table>
       </div>
       <!-- /.card-body -->
     </div>

     @endsection