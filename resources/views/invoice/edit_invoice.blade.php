 @extends('template')
 @section('content')
 @yield('content')
 <div class="row">
   <div class="col-md-12">
     <!-- general form elements -->

     <div class="card card-success">
       <div class="card-header">
         <h3 class="card-title">Invoice</h3>
       </div>

       <form action="{{ route('edit_invoice_act') }}" method="post">

         @csrf

         <div class="card-body">

           <div class="form-group">
             <label for="exampleInputEmail1">no Invoice</label>
             <input type="hidden" name="id_invoice" class="form-control" value="{{$data['id_invoice']}}">
             <input type="text" readonly="" name="no_invoice" class="form-control" value="{{$data['no_invoice']}}">

           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Tanggal Invoice</label>
             <div class="input-group date" id="reservationdate" data-target-input="nearest">

               <input type="text" name="tgl_invoice" value="{{$data['tgl_invoice']}}" class="form-control datetimepicker-input" data-target="#reservationdate">
               <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                 <div class="input-group-text"><i class="fa fa-calendar"></i></div>
               </div>

             </div>
           </div>

           <div class="form-group">
             <label for="exampleInputEmail1">Suplier</label>
             <select class="form-control select2bs4" name="kode_suplier">
               <option value=""></option>
               @foreach($data['list_suplier'] as $row)
               <option value="{{$row->kode_suplier}}" <?php if ($data['kode_suplier'] == $row->kode_suplier) {
                                                        echo "selected";
                                                      } ?>>{{$row->nama_suplier}}</option>
               @endforeach

             </select>
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Kode Pembelian</label>
             <input type="text" name="kode_pembelian" class="form-control" value="{{$data['kode_pembelian']}}">
           </div>

           <div class="form-group">
             <label for="exampleInputEmail1">Total</label>

             <input type="text" name="total" id="rupiah" class="rupiah form-control" value=" {{"Rp " . number_format($data['total'] ,0,',','.')}}">


           </div>

         </div>
         <!-- /.card-body -->

         <div class="card-footer">
           <button type="submit" class="btn btn-primary">Simpan</button>
         </div>
       </form>
     </div>
   </div>

 </div>

 <script type="text/javascript">
   var rupiah = document.getElementById('rupiah');
   rupiah.addEventListener('keyup', function(e) {

     rupiah.value = formatRupiah(this.value, 'Rp. ');
   });

   var rupiah2 = document.getElementById('rupiah2');
   rupiah2.addEventListener('keyup', function(e) {

     rupiah2.value = formatRupiah(this.value, 'Rp. ');
   });
   var rupiah3 = document.getElementById('rupiah3');
   rupiah3.addEventListener('keyup', function(e) {

     rupiah3.value = formatRupiah(this.value, 'Rp. ');
   });

   /* Fungsi formatRupiah */
   function formatRupiah(angka, prefix) {
     var number_string = angka.replace(/[^,\d]/g, '').toString(),
       split = number_string.split(','),
       sisa = split[0].length % 3,
       rupiah = split[0].substr(0, sisa),
       ribuan = split[0].substr(sisa).match(/\d{3}/gi);

     // tambahkan titik jika yang di input sudah menjadi angka ribuan
     if (ribuan) {
       separator = sisa ? '.' : '';
       rupiah += separator + ribuan.join('.');
     }

     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
     return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
   }
 </script>


 @endsection