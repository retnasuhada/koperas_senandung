 @extends('template')
 @section('content')
 @yield('content')
 <div class="row">

   <div class="col-md-4">
     <!-- general form elements -->


     @if($data['flag_edit']=='1')
     <div class="card card-warning">
       <div class="card-header">
         <h3 class="card-title">Edit Invoice </h3>
       </div>
       <form action="{{ route('edit_pembelian_act') }}" method="post">
         @else
         <div class="card card-success">
           <div class="card-header">
             <h3 class="card-title">Tambah Invoice</h3>
           </div>
           <form action="{{ route('add_pembelian_act') }}" method="post">
             @endif

             @csrf



             <div class="row">

               <div class="col-md-12">
                 <!-- general form elements -->
                 <div class="card card-success">
                   <div class="card-header">
                     <h3 class="card-title">Invoice</h3>
                   </div>

                   <form action="{{ route('save_pembelian_act') }}" method="post">

                     @csrf

                     <div class="card-body">

                       <div class="form-group">
                         <label for="exampleInputEmail1">no Invoice</label>
                         @if($data['flag_edit']=='1')
                         <input type="text" name="kode_pembelian" class="form-control" value="{{$data['jumlah']}}">
                         @else
                         <input type="text" readonly="" name="kode_pembelian" class="form-control" value="{{$data['kode_pembelian']}}">
                         @endif
                       </div>
                       <div class="form-group">
                         <label for="exampleInputEmail1">Tanggal Invoice</label>
                         <div class="input-group date" id="reservationdate" data-target-input="nearest">

                           <input type="text" name="tgl_pembelian" class="form-control datetimepicker-input" data-target="#reservationdate">
                           <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                             <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                           </div>

                         </div>
                       </div>

                       <div class="form-group">
                         <label for="exampleInputEmail1">Suplier</label>
                         <select class="form-control select2bs4" name="kode_suplier">
                           <option value=""></option>
                           @foreach($data['list_suplier'] as $row)

                           <option value="{{$row->kode_suplier}}">{{$row->nama_suplier}}</option>
                           @endforeach
                         </select>
                       </div>


                       <div class="form-group">
                         <label for="exampleInputEmail1">Biaya Tambahan</label>
                         @if($data['flag_edit']=='1')
                         <input type="text" name="biaya_tambahan" id="rupiah2" class="rupiah2 form-control" value="{{$data['harga']}}">
                         @else
                         <input type="text" name="biaya_tambahan" id="rupiah2" class="rupiah2 form-control" placeholder="Masukan Biaya Tambahan">
                         @endif
                       </div>
                       <div class="form-group">
                         <label for="exampleInputEmail1">Potongan / Diskon</label>
                         @if($data['flag_edit']=='1')
                         <input type="text" name="diskon" id="rupiah3" class="rupiah3 form-control" value="{{$data['diskon']}}">
                         @else
                         <input type="text" name="diskon" id="rupiah3" class="rupiah3 form-control" placeholder="Masukan Potongan Pembayaran / Diskon">
                         @endif
                       </div>

                       <div class="form-group">
                         <label for="exampleInputEmail1">Jenis Pembelian</label>
                         <select class="form-control" name="jenis_pembelian">
                           <option value="1">Pembelian Cash</option>
                           <option value="2">Titip Barang</option>
                           <option value="3">Pembayarn Tempo</option>
                         </select>
                       </div>
                     </div>
                     <!-- /.card-body -->

                     <div class="card-footer">
                       <button type="submit" class="btn btn-primary">Simpan</button>
                     </div>
                   </form>
                 </div>
               </div>

             </div>

             <script type="text/javascript">
               var rupiah = document.getElementById('rupiah');
               rupiah.addEventListener('keyup', function(e) {

                 rupiah.value = formatRupiah(this.value, 'Rp. ');
               });

               var rupiah2 = document.getElementById('rupiah2');
               rupiah2.addEventListener('keyup', function(e) {

                 rupiah2.value = formatRupiah(this.value, 'Rp. ');
               });
               var rupiah3 = document.getElementById('rupiah3');
               rupiah3.addEventListener('keyup', function(e) {

                 rupiah3.value = formatRupiah(this.value, 'Rp. ');
               });

               /* Fungsi formatRupiah */
               function formatRupiah(angka, prefix) {
                 var number_string = angka.replace(/[^,\d]/g, '').toString(),
                   split = number_string.split(','),
                   sisa = split[0].length % 3,
                   rupiah = split[0].substr(0, sisa),
                   ribuan = split[0].substr(sisa).match(/\d{3}/gi);

                 // tambahkan titik jika yang di input sudah menjadi angka ribuan
                 if (ribuan) {
                   separator = sisa ? '.' : '';
                   rupiah += separator + ribuan.join('.');
                 }

                 rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                 return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
               }
             </script>


             @endsection