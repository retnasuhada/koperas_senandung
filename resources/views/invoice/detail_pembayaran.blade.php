 @extends('template')
 @section('content')
 @yield('content')
 <!-- Main content -->
 <div class="invoice p-3 mb-3">
   <!-- title row -->
   <div class="row">
     <div class="col-12">
       <h4>
         <i class="fas fa-envelope-square"></i> Bukti Pembayaran Invoice

       </h4>
     </div>
     <!-- /.col -->
   </div>
   <!-- info row -->
   <div class="row invoice-info">
     <div class="col-sm-4 invoice-col">
       Informasi Invoice
       <address>
         <table>
           @foreach($data['det_suplier'] as $suplier)
           <tr>
             <td width="30%"><strong>Suplier</strong></td>
             <td width="2%"><strong>:</strong></td>
             <td>{{$suplier->nama_suplier}}</td>
           </tr>
           <tr>
             <td width="30%"><strong>No Invoice</strong></td>
             <td width="2%"><strong>:</strong></td>
             <td>{{$data['no_invoice']}}</td>
           </tr>
           <tr>
             <td width="30%"><strong>Total Invoice</strong></td>
             <td width="2%"><strong>:</strong></td>
             <td>{{"Rp " . number_format($data['total'] ,2,',','.')}}</td>
           </tr>
         </table>
         @endforeach
       </address>

     </div>


     <!-- /.col -->

     <!-- /.col -->
   </div>
   <!-- /.row -->

   <!-- Table row -->
   <div class="row">
     <div class="col-12 table-responsive">
       <table class="table table-striped">
         <thead>
           <tr>
             <th>No</th>
             <th>No Pembayaran</th>
             <th>Tanggal Pembayaran</th>
             <th>Nominal Pembayaran</th>
             <th>Keterangan</th>
           </tr>
         </thead>
         <tbody>
           <?php
            $no = 1;
            $sub_total = 0;
            foreach ($data['reff_pembayaran'] as $reff) {
              if ($reff->metode_pembayaran == '2') {
                $keterangan = 'Return barang';
              } else {
                $keterangan = 'Dibayar';
              }
              $sub_total = $sub_total + $reff->total_pembayaran;
            ?>
             <tr>
               <td>{{$no}}</td>
               <td>{{$reff->no_pembayaran_invoice}}</td>
               <td>{{$reff->tgl_pembayaran}}</td>
               <td>{{"Rp " . number_format($reff->total_pembayaran ,2,',','.')}}</td>
               <td>{{$keterangan}}</td>
             </tr>
           <?php  }
            $balance = $sub_total - $data['total'];
            ?>
         </tbody>
       </table>
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->

   <div class="row">
     <!-- accepted payments column -->
     <div class="col-6">
       <p class="lead">Metode Pembayaran</p>
       <img src="{{ asset('Admin/dist/img/credit/bca.png')}}" alt="Visa">
       <img src="{{ asset('Admin/dist/img/credit/bni.jpg')}}" alt="Mastercard">
       <img src="{{ asset('Admin/dist/img/credit/bri.png')}}" alt="American Express">
       <img src="{{ asset('Admin/dist/img/credit/mandiri.png')}}" alt="Paypal">

       <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
         Dapatkan Nomor Rekening dan tunjukan bukti pembayaran kepada kasir (admin) toko Koperasi Senandung 165
       </p>
     </div>
     <!-- /.col -->
     <div class="col-6">
       <p class="lead">Tanggal Cetak : {{Date('Y-m-d')}}</p>

       <div class="table-responsive">
         <table class="table">

           <tr>
             <th>Total Invoice:</th>
             <td>{{"Rp " . number_format($data['total'] ,2,',','.')}}</td>
           </tr>
           <tr>
             <th>Total Pembayaran:</th>
             <td>{{"Rp " . number_format($sub_total,2,',','.')}}</td>
           </tr>
           <tr>
             <th>Balance:</th>
             <td>{{"Rp " . number_format($balance,2,',','.')}}</td>
           </tr>
         </table>
       </div>
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->

   <!-- this row will not appear when printing -->
   <div class="row no-print">
     <div class="col-12">
       <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Cetak</a>
       <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Lakukan Pembayaran
       </button>
       <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
         <i class="fas fa-download"></i> Download PDF
       </button>
     </div>
   </div>
 </div>
 <!-- /.invoice -->
 </div><!-- /.col -->
 </div><!-- /.row -->
 </div><!-- /.container-fluid -->

 @endsection