<!-- Sidebar -->
<?php
  if (session_status() !== PHP_SESSION_ACTIVE) session_start();
  // session_start();
  $id_anggota = $_SESSION['id_anggota'];
  $id_jabatan = $_SESSION['id_jabatan'];
  $kd_suplier = $_SESSION['kode_suplier'];
  $foto_profil = $_SESSION['foto'];
  $nama_profil = $_SESSION['nama'];
  ?>
<div class="sidebar">
  <!-- Sidebar user (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
   <div class="image">

       <img src="{{ asset('foto/'.$foto_profil)}}" class="img-circle elevation-2" alt="User Image">
     </div>
     <div class="info">
       <a href="{{url('profil_anggota/'.$id_anggota) }}" class="d-block">{{$nama_profil}}</a>
     </div>
   </div>
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <li class="nav-item <?php if ($data['menu'] == 'simpanan') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-book"></i>
          <p>
            Simpanan
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('simpanan_wajib_list') }}" class="nav-link <?php if ($data['Active'] == 'list_sw') {
                                                                            echo "active";
                                                                          } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Simpanan Wajib</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('simpanan_sukarela_list') }}" class="nav-link <?php if ($data['Active'] == 'list_ss') {
                                                                              echo "active";
                                                                            } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Simpanan Sukarela</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('simpanan_pokok_list') }}" class="nav-link <?php if ($data['Active'] == 'list_sp') {
                                                                            echo "active";
                                                                          } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Simpanan Pokok</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('modal_anggota') }}" class="nav-link <?php if ($data['Active'] == 'modal_anggota') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Modal Anggota</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('dana_hibah') }}" class="nav-link <?php if ($data['Active'] == 'dana_hibah') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Dana Hibah</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item <?php if ($data['menu'] == 'pinjaman') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-columns"></i>

          <p>
            Pinjaman
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'form_pinjaman') {
                                                                          echo "active";
                                                                        } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Ajukan Pinjanman</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'list_pinjaman') {
                                                                          echo "active";
                                                                        } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>List Pinjaman</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item <?php if ($data['menu'] == 'akuntansi') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-book"></i>

          <p>
            Akuntansi
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('jurnal_umum') }}" class="nav-link <?php if ($data['Active'] == 'jurnal_umum') {
                                                                    echo "active";
                                                                  } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Jurnal Umum</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'list_ss') {
                                                                          echo "active";
                                                                        } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Neraca</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'list_sp') {
                                                                          echo "active";
                                                                        } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Laba Rugi</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('aset') }}" class="nav-link <?php if ($data['Active'] == 'aset') {
                                                            echo "active";
                                                          } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Aset</p>
            </a>
          </li>

        </ul>
      </li>


      <li class="nav-item <?php if ($data['menu'] == 'master') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-th"></i>
          <p>
            Master
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('jenis_simpanan') }}" class="nav-link <?php if ($data['Active'] == 'jenis_simpanan') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Jenis Simpanan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('jenis_pinjaman') }}" class="nav-link <?php if ($data['Active'] == 'jenis_pinjaman') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Jenis Pinjaman</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('jabatan') }}" class="nav-link <?php if ($data['Active'] == 'jabatan') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Jabatan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('anggota') }}" class="nav-link <?php if ($data['Active'] == 'anggota') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Anggota</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('user') }}" class="nav-link <?php if ($data['Active'] == 'user') {
                                                            echo "active";
                                                          } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>User</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('petugas') }}" class="nav-link <?php if ($data['Active'] == 'petugas') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pengurus</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../../index2.html" class="nav-link <?php if ($data['Active'] == 'jenis_penerimaan') {
                                                          echo "active";
                                                        } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Jenis Penerimaan</p>
            </a>
          </li>

        </ul>
      </li>
      <li class="nav-item <?php if ($data['menu'] == 'm_suplier') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-th"></i>
          <p>
            Suplier
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('m_suplier_stok') }}" class="nav-link <?php if ($data['Active'] == 'stok_suplier') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Stok Produk</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('m_pembayaran_suplier') }}" class="nav-link <?php if ($data['Active'] == 'm_pembayaran_suplier') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pembayaran</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'jenis_simpanan') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pembayaran</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'jenis_simpanan') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Penjualan Produk</p>
            </a>
          </li>
          

        </ul>
      </li>

      <li class="nav-item <?php if ($data['menu'] == 'toko') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon far fa-plus-square"></i>
          <p>
            Toko
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('list_produk') }}" class="nav-link  <?php if ($data['Active'] == 'list_produk') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Stok Produk</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('pembelian') }}" class="nav-link  <?php if ($data['Active'] == 'pembelian') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pembelian</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('penjualan') }}" class="nav-link <?php if ($data['Active'] == 'penjualan') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Penjualan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('suplier') }}" class="nav-link <?php if ($data['Active'] == 'suplier') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Suplier</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('invoice') }}" class="nav-link <?php if ($data['Active'] == 'invoice') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Invoice</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('bayar_suplier') }}" class="nav-link <?php if ($data['Active'] == 'bayar_suplier') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pembayaran Suplier</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'hutang_usaha') {
                                                                          echo "active";
                                                                        } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Hutang Usaha</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'piutang_usaha') {
                                                                          echo "active";
                                                                        } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Piutang Usaha</p>
            </a>
          </li>

        </ul>
      </li>

      <li class="nav-item <?php if ($data['menu'] == 'm_produk') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-copy"></i>
          <p>
            Managemen Produk
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <!-- <li class="nav-item">
            <a href="{{ route('list_produk') }}" class="nav-link <?php if ($data['Active'] == 'list_produk2') {
                                                                    echo "active";
                                                                  } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Produk</p>
            </a>
          </li> -->
          <li class="nav-item active">
            <a href="{{ route('size') }}" class="nav-link <?php if ($data['Active'] == 'list_size') {
                                                            echo "active";
                                                          } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Size</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('warna') }}" class="nav-link <?php if ($data['Active'] == 'list_warna') {
                                                              echo "active";
                                                            } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Warna</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('satuan') }}" class="nav-link <?php if ($data['Active'] == 'list_satuan') {
                                                              echo "active";
                                                            } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Satuan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('kategori') }}" class="nav-link <?php if ($data['Active'] == 'list_kategori') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Kategori</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('kategori2') }}" class="nav-link <?php if ($data['Active'] == 'list_kategori2') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Sub Kategori</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('kategori3') }}" class="nav-link <?php if ($data['Active'] == 'list_kategori3') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Child Sub Kategori</p>
            </a>
          </li>
        </ul>
      </li>

      <li class="nav-item <?php if ($data['menu'] == 'laporan') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-copy"></i>
          <p>
            Laporan
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('laporan_pembelian') }}" class="nav-link <?php if ($data['Active'] == 'laporan_pembelian') {
                                                                    echo "active";
                                                                  } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pembelian</p>
            </a>
          </li>
          <li class="nav-item active">
            <a href="{{ route('laporan_penjualan') }}" class="nav-link <?php if ($data['Active'] == 'laporan_penjualan') {
                                                            echo "active";
                                                          } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Penjualan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_simpanan') {
                                                              echo "active";
                                                            } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Simpanan</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_pinjaman') {
                                                              echo "active";
                                                            } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pinjaman</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_produk') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Produk</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_hutang_usaha') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Hutang Usaha</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_piutang_usaha') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Piutang Usaha</p>
            </a>
          </li>
        </ul>
      </li>

      <li class="nav-item <?php if ($data['menu'] == 'e-commerce') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-file"></i>
          <p>
            E-Commerce
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('eproduk') }}" class="nav-link <?php if ($data['Active'] == 'e_produk') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Produk</p>
            </a>
          </li>

        </ul>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('e_comm') }}" class="nav-link <?php if ($data['Active'] == 'e_produk') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Test</p>
            </a>
          </li>
        </ul>
        <
      </li>
      <li class="nav-item">
        <a href="{{ url('keluar') }}" class="nav-link">
          <i class="nav-icon fas fa-file"></i>
          <p>
            Keluar
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
      </li>
    </ul>
  </nav>
</div>