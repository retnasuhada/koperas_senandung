 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif
     <div class="card-header">
       <h3 class="card-title">Form Anggota Koperasi Senandung Yatim</h3>

       <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
     </div>
     <!-- /.card-header -->
     @if($data['flag_edit']=='1')
     <form action="{{ route('edit_anggota_act') }}" method="post" enctype="multipart/form-data">
       @else
       <form action="{{ route('add_anggota') }}" method="post" enctype="multipart/form-data">
         @endif

         @csrf
         <div class="card-body">
           <div class="row">
             <div class="col-md-6">
               <div class="form-group">
                 <label for="exampleInputEmail1">No Anggota</label>
                 @if($data['flag_edit']=='1')
                 <input type="text" readonly="" name="no_anggota" class="form-control" value="{{$data['no_anggota']}}">
                 <input type="hidden" name="id_anggota" class="form-control" value="{{$data['id_anggota']}}">
                 @else
                 <input type="texs" name="no_anggota" readonly="" class="form-control" value="{{ $data['no_anggota'] }}">
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Nama Anggota</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="nama_anggota" class="form-control" value="{{$data['nama_anggota']}}">
                 @else
                 <input type="texs" name="nama_anggota" class="form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">NIK</label>

                 @if($data['flag_edit']=='1')
                 <input type="texs" name="nik" class="form-control" value="{{$data['nik']}}">
                 @else
                 <input type="texs" name="nik" class="form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">NPWP</label>

                 @if($data['flag_edit']=='1')
                 <input type="texs" name="npwp" class="form-control" value="{{$data['npwp']}}">
                 @else
                 <input type="texs" name="npwp" class="form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label>Alamat</label>

                 @if($data['flag_edit']=='1')
                 <textarea class="form-control" name="alamat" rows="2">{{$data['alamat']}}</textarea>
                 @else
                 <textarea class="form-control" name="alamat" rows="2" placeholder="Enter ..."></textarea>
                 @endif
               </div>
               <div class="form-group">
                 <label>Tanggal Bergabung</label>

                 @if($data['flag_edit']=='1')

                
                   <input type="date" class="form-control" name="start_date" id="start_date" value="{{$data['join_date']}}">
                   
                
                 @else
                 <input type="date" class="form-control" name="start_date" id="start_date" required="">
                   
                 
                 @endif
               </div>

               <!-- /.form-group -->
             </div>
             <!-- /.col -->
             <div class="col-md-6">
               <div class="form-group">
                 <label>Jenis Kelamin</label>
                 <select name="jenis_kelamin" class="form-control">
                   @if($data['flag_edit']=='1')
                   <option value="1" <?php if ($data['jenis_kelamin'] == '1') {
                                        echo 'selected:';
                                      } ?>>Laki-Laki</option>
                   <option value="2" <?php if ($data['jenis_kelamin'] == '2') {
                                        echo 'selected:';
                                      } ?>>Perempuan</option>
                   @else
                   <option value="1">Laki-Laki</option>
                   <option value="2">Perempuan</option>
                   @endif

                 </select>
               </div>
               <div class="form-group">
                 <label for="exampleInputFile">Foto</label> 
                 @if($data['flag_edit']=='1')           
                       <input type="file" class="form-control" name="foto" required="">
                @else
                  <input type="file" class="form-control" name="foto" value="{{$data['foto']}}">
                 @endif
               </div>

               @if($data['flag_edit']=='1')
               <div class="text-center">
                 <img src="{{ asset('foto/'.$data['foto']) }}" alt="User profile picture">
               </div>
               @endif
               <div class="form-group">
                 <label>Status</label>
                 <select name="status" class="form-control">
                   @if($data['flag_edit']=='1')
                   <option value="1" <?php if ($data['status'] == '1') {
                                        echo 'selected:';
                                      } ?>>Aktif</option>
                   <option value="0" <?php if ($data['status'] == '0') {
                                        echo 'selected:';
                                      } ?>>Non-Aktif</option>
                   @else
                   <option value="1">Aktif</option>
                   <option value="0">Non-Aktif</option>
                   @endif
                 </select>

               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Telephone</label>

                 @if($data['flag_edit']=='1')
                 <input type="texs" name="no_hp" class="form-control" value="{{$data['no_hp']}}">
                 @else
                 <input type="texs" name="no_hp" class="form-control" value="">
                 @endif
               </div>

               <div class="form-group">
                 <label for="exampleInputEmail1">Created At</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="created_at" readonly="" class="form-control" value="{{$data['created_at']}}">
                 @else
                 <input type="texs" name="created_at" readonly="" class="form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Created By</label>

                 @if($data['flag_edit']=='1')
                 <input type="texs" name="created_by" readonly="" class="form-control" value="{{$data['created_by']}}">
                 @else
                 <input type="texs" name="created_by" readonly="" class="form-control" value="">
                 @endif
               </div>

             </div>
             <!-- /.col -->
           </div>
           <!-- /.row -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Simpan</button>
           <button type="reset" class="btn btn-warning">Cancel</button>
         </div>
       </form>
   </div>

   @endsection