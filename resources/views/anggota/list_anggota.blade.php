 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif

     <div class="card card-primary">
       <div class="card-header">
         <a href="{{route('add_frm_anggota')}}" class="btn btn-primary">
           <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Anggota
         </a>
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table id="example1" class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>No</th>
               <th>Nama</th>
               <th>ID Anggota</th>
               <th>Jenis Kelamin</th>
               <th>Status</th>
               <th>Aksi</th>

             </tr>
           </thead>
           <tbody>

             <?php
              $no = 1;
              foreach ($data['list_anggota'] as $row) { ?>
               <tr>
                 <td>{{$no}}</td>
                 <td>{{$row->nama_anggota}}</td>
                 <td>{{$row->no_anggota}}</td>
                 <td>{{$row->jenis_kelamin}}</td>
                 <td>{{$row->status}}</td>

                 <td>
                   <a href="{{ url('edit_anggota/'.$row->id_anggota) }}"><i class="fas fa-edit"></i></a>
                   &nbsp;&nbsp;
                   <a href="{{ url('profil_anggota/'.$row->id_anggota) }}"><i class="far fa-eye"></i></a>
                   &nbsp;&nbsp;
                   <a href="{{ url('hapus_anggota/'.$row->id_anggota) }}"><i class="fas fa-trash"></i></a>

                 </td>
               </tr>

             <?php $no++;
              } ?>
             </tfoot>
         </table>
       </div>
       <!-- /.card-body -->
     </div>
     @endsection