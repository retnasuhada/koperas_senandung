@extends('template')
@section('content')
@yield('content')



<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Bio Data</a></li>
              <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Asset</a></li>
              <li class="nav-item"><a class="nav-link" href="#transaksi" data-toggle="tab">Pembelanjaan</a></li>
              <li class="nav-item"><a class="nav-link" href="#pinjaman" data-toggle="tab">Pinjaman</a></li>
              <li class="nav-item"><a class="nav-link" href="#shu" data-toggle="tab">SHU</a></li>

            </ul>
          </div><!-- /.card-header -->
          <div class="card-body">
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="post">
                  <div class="row invoice-info">
                    <div class="col-sm-12 invoice-col">
                      <table>
                        <tr>
                          <td width="150px;"><b>Nama Anggota</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>ID Anggota</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Tanggal Bergabung</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>NIK</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Alamat</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Jenis Kelamin</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Status Anggota</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>NPWP</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td></td>
                        </tr>
                      </table>

                    </div>
                    <!-- /.col -->

                    <!-- /.col -->
                  </div>
                </div>
                <!-- /.post -->


              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <th style="width:50%"> Simpanan Pokok</th>
                        <td></td>
                      </tr>
                      <tr>
                        <th>Simpanan Wajib</th>
                        <td></td>
                      </tr>
                      <tr>
                        <th>Simpanan Sukarela</th>
                        <td></td>
                      </tr>
                      <tr>
                        <th>Modal Anggota</th>
                        <td></td>
                      </tr>
                      <tr>
                        <th>Total</th>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="tab-pane" id="transaksi">
                <!-- The transaksi -->
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <td><b> Bulan</b></td>
                        <td><b>Total Transaksi</b></td>
                      </tr>
                     
                     
                    </tbody>
                  </table>
                </div>
              </div>




              <div class="tab-pane" id="pinjaman">
                <!-- The transaksi -->
                <div class="table-responsive">
                  <b>On Progress Development</b>

                </div>
              </div>



              <div class="tab-pane" id="shu">
                <!-- The transaksi -->
                <div class="table-responsive">
                  <b>On Progress Development</b>

                </div>
              </div>
              <!-- /.tab-pane -->


              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

@endsection