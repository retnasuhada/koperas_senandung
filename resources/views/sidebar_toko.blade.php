
<?php
  if (session_status() !== PHP_SESSION_ACTIVE) session_start();
  // session_start();
  $id_anggota = $_SESSION['id_anggota'];
  $id_jabatan = $_SESSION['id_jabatan'];
  $foto_profil = $_SESSION['foto'];
  $nama_profil = $_SESSION['nama'];
  ?>
<!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
   <div class="image">

       <img src="{{ asset('foto/'.$foto_profil)}}" class="img-circle elevation-2" alt="User Image">
     </div>
     <div class="info">
       <a href="{{url('profil_anggota/'.$id_anggota) }}" class="d-block">{{$nama_profil}}</a>
     </div>
   </div>
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      
      <li class="nav-item <?php if ($data['menu'] == 'toko') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon far fa-plus-square"></i>
          <p>
            Toko
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('list_produk') }}" class="nav-link  <?php if ($data['Active'] == 'list_produk') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Stok Produk</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('pembelian') }}" class="nav-link  <?php if ($data['Active'] == 'pembelian') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pembelian</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('penjualan') }}" class="nav-link <?php if ($data['Active'] == 'penjualan') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Penjualan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('suplier') }}" class="nav-link <?php if ($data['Active'] == 'suplier') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Suplier</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('invoice') }}" class="nav-link <?php if ($data['Active'] == 'invoice') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Invoice</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'hutang_usaha') {
                                                                          echo "active";
                                                                        } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Hutang Usaha</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'piutang_usaha') {
                                                                          echo "active";
                                                                        } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Piutang Usaha</p>
            </a>
          </li>

        </ul>
      </li>

      <li class="nav-item <?php if ($data['menu'] == 'm_produk') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-copy"></i>
          <p>
            Managemen Produk
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <!-- <li class="nav-item">
            <a href="{{ route('list_produk') }}" class="nav-link <?php if ($data['Active'] == 'list_produk2') {
                                                                    echo "active";
                                                                  } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Produk</p>
            </a>
          </li> -->
          <li class="nav-item active">
            <a href="{{ route('size') }}" class="nav-link <?php if ($data['Active'] == 'list_size') {
                                                            echo "active";
                                                          } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Size</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('warna') }}" class="nav-link <?php if ($data['Active'] == 'list_warna') {
                                                              echo "active";
                                                            } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Warna</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('satuan') }}" class="nav-link <?php if ($data['Active'] == 'list_satuan') {
                                                              echo "active";
                                                            } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Satuan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('kategori') }}" class="nav-link <?php if ($data['Active'] == 'list_kategori') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Kategori</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('kategori2') }}" class="nav-link <?php if ($data['Active'] == 'list_kategori2') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Sub Kategori</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('kategori3') }}" class="nav-link <?php if ($data['Active'] == 'list_kategori3') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Child Sub Kategori</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item <?php if ($data['menu'] == 'laporan') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-copy"></i>
          <p>
            Laporan
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('laporan_pembelian') }}" class="nav-link <?php if ($data['Active'] == 'laporan_pembelian') {
                                                                    echo "active";
                                                                  } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pembelian</p>
            </a>
          </li>
          <li class="nav-item active">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_penjualan') {
                                                            echo "active";
                                                          } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Penjualan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_simpanan') {
                                                              echo "active";
                                                            } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Simpanan</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_pinjaman') {
                                                              echo "active";
                                                            } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pinjaman</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_produk') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Produk</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_hutang_usaha') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Hutang Usaha</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'laporan_piutang_usaha') {
                                                                  echo "active";
                                                                } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Piutang Usaha</p>
            </a>
          </li>
        </ul>
      </li>

      <li class="nav-item">
        <a href="{{ url('keluar') }}" class="nav-link">
          <i class="nav-icon fas fa-file"></i>
          <p>
            Keluar
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
      </li>
    </ul>
  </nav>
</div>