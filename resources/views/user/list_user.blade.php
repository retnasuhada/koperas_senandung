 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif
     <div class="card-header">
       <h3 class="card-title">Form user Koperasi Senandung Yatim</h3>

       <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
     </div>
     <!-- /.card-header -->
     @if($data['flag_edit']=='1')
     <form action="{{ route('edit_user_act') }}" method="post" enctype="multipart/form-data">
       <input type="hidden" name="id_user" value="{{$data['id_user']}}">
       @else
       <form action="{{ route('add_user') }}" method="post" enctype="multipart/form-data">
         @endif

         @csrf
         <div class="card-body">
           <div class="row">
             <div class="col-md-4">

               <div class="form-group">
                 <label for="exampleInputEmail1">Nama Anggota</label>
                 @if($data['flag_edit']=='1')
                 <select class="form-control select2" disabled="" name="id_anggota" style="width: 100%;">
                   @foreach($data['list_anggota'] as $row)
                   <option value="{{$row->id_anggota}}" <?php if ($data['id_anggota'] == $row->id_anggota) {
                                                          echo "selected";
                                                        } ?>>{{$row->nama_anggota}}</option>
                   @endforeach
                 </select>
                 @else
                 <select class="form-control select2" name="id_anggota" style="width: 100%;">
                   @foreach($data['list_anggota'] as $row)
                   <option value=""></option>
                   <option value="{{$row->id_anggota}}">{{$row->nama_anggota}}</option>
                   @endforeach
                 </select>
                 @endif

               </div>

               <!-- /.form-group -->
             </div>
             <!-- /.col -->
             <div class="col-md-4">
               <div class="form-group">
                 <label>Username</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="username" class="form-control" value="{{$data['username']}}">
                 @else
                 <input type="texs" name="username" class="form-control" value="">
                 @endif
               </div>
             </div>
             <div class="col-md-4">

               <div class="form-group">
                 <label for="exampleInputEmail1">Password</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="password" class="form-control" value="{{Deskrip($data['password'])}}">
                 @else
                 <input type="texs" name="password" class="form-control" value="">
                 @endif
               </div>

             </div>
             <!-- /.col -->
           </div>
           <!-- /.row -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Simpan</button>
           <button type="reset" class="btn btn-warning">Cancel</button>
         </div>
       </form>
   </div>
   <div class="card card-primary">
     <div class="card-header">
       <h3 class="card-title">List User Aplikasi Koperasi</h3>
     </div>
     <!-- /.card-header -->
     <div class="card-body">
       <table id="example1" class="table table-bordered table-striped">
         <thead>
           <tr>
             <th>No</th>
             <th>Nama</th>
             <th>Username</th>
             <th>password</th>
             <th>Jabatan</th>
             <th>Status</th>
             <th>Aksi</th>

           </tr>
         </thead>
         <tbody>

           <?php
            $no = 1;
            foreach ($data['list_user'] as $row) { ?>
             <tr>
               <td>{{$no}}</td>
               <td>{{$row->nama_anggota}}</td>
               <td>{{$row->username}}</td>
               <td>{{Deskrip($row->password)}}</td>
               <td>{{$row->nama_jabatan}}</td>
               <td>{{$row->status}}</td>

               <td>
                 <a href="{{ url('edit_user/'.$row->id_user) }}"><i class="fas fa-edit"></i></a>
                 &nbsp;&nbsp;
                 <a href="{{ url('hapus_anggota/'.$row->id_user) }}"><i class="fas fa-trash"></i></a>

               </td>
             </tr>

           <?php $no++;
            } ?>
           </tfoot>
       </table>
     </div>
     <!-- /.card-body -->
   </div>
   @endsection