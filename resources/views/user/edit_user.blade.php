 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 
   <div class="card card-success">
   
     <div class="card-header">
       <h3 class="card-title">Edit User</h3>

       <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
     </div>
     <!-- /.card-header -->
    
       <form action="{{ route('edit_user_agt_act') }}" method="post" enctype="multipart/form-data">
     
         @csrf
         <div class="card-body">
           <div class="row">
             
             <!-- /.col -->
             <div class="col-md-6">
               <div class="form-group">
                 <label>Username</label>
                <input type="hidden" name="id_user" value="{{$data['id_user']}}">
                 <input type="texs" name="username" class="form-control" value="{{$data['username']}}">
                 
               </div>
             </div>
             <div class="col-md-6">

               <div class="form-group">
                 <label for="exampleInputEmail1">Password</label>
                 
                 <input type="texs" name="password" class="form-control" value="{{Deskrip($data['password'])}}">
                
               </div>

             </div>
             <!-- /.col -->
           </div>
           <!-- /.row -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Simpan</button>
          
         </div>
       </form>
   </div>
     <!-- /.card-body -->
   </div>
   @endsection