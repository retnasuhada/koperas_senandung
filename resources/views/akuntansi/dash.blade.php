 @extends('template')
 @section('content')
 @yield('content')
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif

 <div class="row">
   <div class="col-md-6">
     <div class="card">
       <div class="card-header">
         
         <h3 class="card-title">Aktiva (Kekayaan Koperasi)</h3>
       </div>
       <!-- /.card-header -->
       <div class="card-body">

         <table class="table table-bordered">
           <thead>
             <tr>
               <th style="width: 10px">#</th>
               <th>Kode</th>
               <th>Nama Akun</th>
               <th>Nilai</th>

             </tr>
           </thead>
           <tbody>
             <?php $no = 1; ?>
             @foreach($data['aktiva'] as $aktiva)
             <?php $no++; ?>
             <tr>
               <td>{{ $no}}</td>
               <td>{{$aktiva->kode_akun}}</td>

               <td>{{$aktiva->nama_akun}}</td>
               <td>{{"Rp " . number_format($aktiva->total ,2,',','.')}}</td>

             </tr>

             @endforeach
             <tr>
               <td colspan="3"><b>Total</b></td>
               <td><b>{{"Rp " . number_format($data['t_aktiva'] ,2,',','.')}}</b></td>
             </tr>
           </tbody>
         </table>
       </div>
       <!-- /.card-body -->
     </div>
     <!-- /.card -->

     <!-- /.card -->
   </div>

   <div class="col-md-6">
     <div class="card">
       <div class="card-header">
         <h3 class="card-title">Pasiva (Kewajiban Koperasi)</h3>
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table class="table table-bordered">
           <thead>
             <tr>
               <th style="width: 10px">#</th>
               <th>Kode</th>
               <th>Nama Akun</th>
               <th>Nilai</th>

             </tr>
           </thead>
           <tbody>
             <?php $number = 1; ?>
             @foreach($data['pasiva'] as $pasiva)
             <?php $number++; ?>
             <tr>
               <td>{{ $number}}</td>
               <td>{{$pasiva->kode_akun}}</td>

               <td>{{$pasiva->nama_akun}}</td>
               <td>{{"Rp " . number_format($pasiva->total ,2,',','.')}}</td>
             </tr>
             @endforeach
             <tr>
               <td colspan="3"><b>Total</b></td>
               <td><b>{{"Rp " . number_format($data['t_pasiva'] ,2,',','.')}}</b></td>
             </tr>
           </tbody>
         </table>
       </div>
       <!-- /.card-body -->
     </div>
     <!-- /.card -->
     <!-- /.card -->
   </div>
   <!-- /.col -->
 </div>

 @endsection