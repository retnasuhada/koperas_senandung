 @extends('template')
 @section('content')
 @yield('content')
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 <div class="card">
   <div class="card-header">
     <a href="{{route('add_jurnal_umum')}}" class="btn btn-primary">
       <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Laporan Jurnal Umum
     </a>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
     <table id="example1" class="table table-bordered table-striped">
       <thead>
         <tr>
           <th width="5%">No</th>
           <th width="10%">Tanggal</th>
           <th width="50%">Uraian</th>
           <th width="10%">Akun Asal</th>
           <th width="10%">Akun Tujuan</th>
           <th width="10%">Nilai</th>
           <th width="5%;">Aksi</th>
         </tr>
       </thead>
       <tbody>
         <?php $no = 0; ?>
         @foreach($data['list_jurnal'] as $row)
         <?php $no++; ?>
         <tr>
           <td>{{ $no }}</td>
           <td>{{ $row->tgl_transaksi }}</td>
           <td>{{ $row->keterangan }}</td>
           <td>{{ $row->asal }}</td>
           <td>{{ $row->tujuan }}</td>
           <td>{{ $row->nominal_transaksi }}</td>
           <td>
             <a href="#"><i class="fas fa-edit"></i></a>
             &nbsp;&nbsp;
             <a href="#"><i class="fas fa-eye"></i></a>

           </td>
         </tr>
         @endforeach
         </tfoot>
     </table>
   </div>
   <!-- /.card-body -->
 </div>
 @endsection