@extends('template')
@section('content')
@yield('content')
<!-- SELECT2 EXAMPLE -->
@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h5><i class="icon fas fa-ban"></i> Alert!</h5>
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
@if ($data['save']=='1')
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
  Data Berhasil Disimipan
</div>
@elseif ($data['save']=='3')
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
  Data Berhasil Dihapus
</div>
@endif
<div class="col-md-6">
  @if($data['flag_edit']=='1')
  <div class="card card-warning">
    <div class="card-header">
      <h3 class="card-title">Form Edit Dana Hibah</h3>
      @else
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Keluar Masuk Kas</h3>
          @endif


          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <!-- /.card-header -->
        @if($data['flag_edit']=='1')
        <form action="{{ route('edit_dana_hibah_act') }}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="id_simpanan" value="{{$data['id_simpanan']}}">
          <input type="hidden" name="unik_kode" value="{{$data['unik_kode']}}">
          @else
          <form action="{{ route('add_arus_kas_act') }}" method="post" enctype="multipart/form-data">
            @endif

            @csrf
            <div class="card-body">
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-4 col-form-label">Jenis Transaksi</label>
                <div class="col-sm-8">
                  <select name="jenis_transaksi" onchange="onpiton1()" id="jenis_transaksi" class="custom-select rounded-0" id="exampleSelectRounded0">
                    <option value="1">Debit ( Kas Masuk )</option>
                    <option value="2">Kredit ( Pengeluaran Kas )</option>
                  </select>
                </div>
              </div>
              <div class="result"></div>
              <div class="nextResult">
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 col-form-label">Nominal</label>
                  <div class="col-sm-8">
                    <input type="text" name="nominal" class="form-control" id="rupiah" placeholder="Enter ...">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 col-form-label">Tgl Transaksi</label>
                  <div class="col-sm-8">
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">

                      <input type="text" name="tgl_transaksi" class="form-control datetimepicker-input" data-target="#reservationdate">
                      <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 col-form-label">Keterangan</label>
                  <div class="col-sm-8">
                    <textarea class="form-control" name="keterangan" rows="3" placeholder="Enter ..."></textarea>
                  </div>
                </div>

              </div>
              <!-- /.row -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-success">Simpan</button>
              <button type="reset" class="btn btn-warning">Cancel</button>
            </div>
          </form>
      </div>
    </div>
    <script type="text/javascript">
      function onpiton1() {
        var n = $("#jenis_transaksi").val();
        $(".result").html(null);
        if (n == 1) {
          $.ajax({
            url: '/getForm',
            type: 'get',
            dataType: 'JSON',
            success: function(response) {
              var len = response.length;

              var tr_str = "<div class='form-group row'>";
              tr_str += "<label for='inputEmail3' class='col-sm-4 col-form-label'>Sumber Pemasukan</label>";
              tr_str += "<div class='col-sm-8'>";
              tr_str += "<select name='kode_akun' id='kode_akun' class='custom-select rounded-0' id='exampleSelectRounded0'>";
              for (var i = 0; i < len; i++) {
                var kode_akun = response[i].kode_akun;
                var nama_akun = response[i].nama_akun;
                tr_str += "<option value=" + kode_akun + ">" + nama_akun + "</option>";
              }
              tr_str += "</select>";
              $(".result").append(tr_str);
            }
          });
        } else {
          $.ajax({
            url: '/getForm2',
            type: 'get',
            dataType: 'JSON',
            success: function(response) {
              var len = response.length;

              var tr_str = "<div class='form-group row'>";
              tr_str += "<label for='inputEmail3' class='col-sm-4 col-form-label'>Tujuan Pengeluaran</label>";
              tr_str += "<div class='col-sm-8'>";
              tr_str += "<select name='kode_akun' id='kode_akun' class='custom-select rounded-0' id='exampleSelectRounded0'>";
              for (var i = 0; i < len; i++) {
                var kode_akun = response[i].kode_akun;
                var nama_akun = response[i].nama_akun;
                tr_str += "<option value=" + kode_akun + ">" + nama_akun + "</option>";
              }
              tr_str += "</select>";
              $(".result").append(tr_str);
            }
          });
        }

      }

      var rupiah = document.getElementById('rupiah');
      rupiah.addEventListener('keyup', function(e) {

        rupiah.value = formatRupiah(this.value, 'Rp. ');
      });

      var rupiah2 = document.getElementById('rupiah2');
      rupiah2.addEventListener('keyup', function(e) {

        rupiah2.value = formatRupiah(this.value, 'Rp. ');
      });

      /* Fungsi formatRupiah */
      function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split = number_string.split(','),
          sisa = split[0].length % 3,
          rupiah = split[0].substr(0, sisa),
          ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
      }
    </script>
    @endsection