 @extends('template')
 @section('content')
 @yield('content')
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif

 <div class="row">
   <div class="col-md-12">
     <div class="card card-success">
       <div class="card-header">
         <h3 class="card-title">Form Tambah Detail Aset ({{$data['nama_aset']}})</h3>

         <div class="card-tools">
           <button type="button" class="btn btn-tool" data-card-widget="collapse">
             <i class="fas fa-minus"></i>
           </button>
           <button type="button" class="btn btn-tool" data-card-widget="remove">
             <i class="fas fa-times"></i>
           </button>
         </div>
       </div>
       <form action="{{ route('add_detail_aset_act') }}" method="post" enctype="multipart/form-data">
         <input type="hidden" name="kode_akun" class="form-control" value="{{ $data['kode_akun'] }}">
         @csrf
         <div class="card-body">
           <div class="row">
             <div class="col-md-12">
               <div class="form-group">
                 <label for="exampleInputEmail1">Nama</label>
                 <input type="texs" name="nama" class="form-control">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Nilai</label>
                 <input type="texs" name="nilai" id="rupiah" class="form-control">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">No Inventory</label>
                 <input type="texs" name="no_aset" class="form-control">
               </div>
               <div class="form-group">
                 <label>Deskripsi</label>
                 <textarea class="form-control" name="deskripsi" rows="3" placeholder="Enter ..."></textarea>
               </div>

             </div>
             <!-- /.col -->

           </div>
           <!-- /.row -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Simpan</button>
           <button type="reset" class="btn btn-warning">Cancel</button>
         </div>
       </form>
     </div>
   </div>

 </div>

 <script type="text/javascript">
   var rupiah = document.getElementById('rupiah');
   rupiah.addEventListener('keyup', function(e) {

     rupiah.value = formatRupiah(this.value, 'Rp. ');
   });

   var rupiah2 = document.getElementById('rupiah2');
   rupiah2.addEventListener('keyup', function(e) {

     rupiah2.value = formatRupiah(this.value, 'Rp. ');
   });

   /* Fungsi formatRupiah */
   function formatRupiah(angka, prefix) {
     var number_string = angka.replace(/[^,\d]/g, '').toString(),
       split = number_string.split(','),
       sisa = split[0].length % 3,
       rupiah = split[0].substr(0, sisa),
       ribuan = split[0].substr(sisa).match(/\d{3}/gi);

     // tambahkan titik jika yang di input sudah menjadi angka ribuan
     if (ribuan) {
       separator = sisa ? '.' : '';
       rupiah += separator + ribuan.join('.');
     }

     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
     return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
   }
 </script>

 @endsection