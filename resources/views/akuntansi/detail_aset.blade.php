 @extends('template')
 @section('content')
 @yield('content')
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif

 <div class="row">
   <div class="col-md-12">
     <div class="card">
       <div class="card-header">
         <a href="{{ url('add_detail_aset/'.$data['kode_akun']) }}" class="btn btn-primary float-right">Tambah Aset {{ $data['nama_aset'] }}</a>
         <h3 class="card-title">Daftar Detail Aset {{ $data['nama_aset']}}</h3>
       </div>
       <!-- /.card-header -->
       <div class="card-body">

         <table class="table table-bordered">
           <thead>
             <tr>
               <th style="width: 10px">#</th>
               <th>No Inventory</th>
               <th>Nama</th>
               <th>Nilai</th>

             </tr>
           </thead>
           <tbody>
             <?php $no = 1; ?>
             @foreach($data['detail_aset'] as $row)
             <?php $no++; ?>
             <tr>
               <td>{{ $no}}</td>
               <td>{{ $row->no_aset}}</td>
               <td>{{ $row->nama_detail_akun}}</td>
               <td>{{"Rp " . number_format($row->nilai ,2,',','.')}}</td>
             </tr>

             @endforeach
             <tr>
               <td colspan="3">Total</td>
               <td>Rp.</td>
             </tr>
           </tbody>
         </table>
       </div>
       <!-- /.card-body -->
     </div>
     <!-- /.card -->

     <!-- /.card -->
   </div>

   <!-- /.card -->
   <!-- /.card -->
 </div>
 <!-- /.col -->
 </div>

 @endsection