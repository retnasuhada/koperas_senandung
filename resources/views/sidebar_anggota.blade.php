 <!-- Sidebar -->
 <?php
  if (session_status() !== PHP_SESSION_ACTIVE) session_start();
  // session_start();
  $id_anggota = $_SESSION['id_anggota'];
  $id_jabatan = $_SESSION['id_jabatan'];
  $kd_suplier = $_SESSION['kode_suplier'];
  $foto_profil = $_SESSION['foto'];
  $nama_profil = $_SESSION['nama'];
  ?>
 <div class="sidebar">
   <!-- Sidebar user (optional) -->
   <div class="user-panel mt-3 pb-3 mb-3 d-flex">
     <div class="image">

       <img src="{{ asset('foto/'.$foto_profil)}}" class="img-circle elevation-2" alt="User Image">
     </div>
     <div class="info">
       <a href="{{url('profil_anggota/'.$id_anggota) }}" class="d-block">{{$nama_profil}}</a>
     </div>
   </div>
   <nav class="mt-2">
     <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
       <li class="nav-item <?php if ($data['menu'] == 'simpanan') {
                              echo "menu-open";
                            } ?>">
         <a href="#" class="nav-link">
           <i class="nav-icon fas fa-book"></i>
           <p>
             Simpanan
             <i class="right fas fa-angle-left"></i>
           </p>
         </a>
         <ul class="nav nav-treeview">
           <li class="nav-item">
             <a href="{{ route('agt_sw') }}" class="nav-link <?php if ($data['Active'] == 'list_sw') {
                                                                echo "active";
                                                              } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Simpanan Wajib</p>
             </a>
           </li>
           <li class="nav-item">
             <a href="{{ route('agt_ss') }}" class="nav-link <?php if ($data['Active'] == 'list_ss') {
                                                                echo "active";
                                                              } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Simpanan Sukarela</p>
             </a>
           </li>
           <li class="nav-item">
             <a href="{{ route('agt_sp') }}" class="nav-link <?php if ($data['Active'] == 'list_sp') {
                                                                echo "active";
                                                              } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Simpanan Pokok</p>
             </a>
           </li>
           <li class="nav-item">
             <a href="{{ route('agt_modal') }}" class="nav-link <?php if ($data['Active'] == 'modal_anggota') {
                                                                  echo "active";
                                                                } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Modal Anggota</p>
             </a>
           </li>
           <li class="nav-item">
             <a href="{{ route('agt_hibah') }}" class="nav-link <?php if ($data['Active'] == 'dana_hibah') {
                                                                  echo "active";
                                                                } ?>">
               <i class="far fa-circle nav-icon"></i>
               <p>Dana Hibah</p>
             </a>
           </li>
         </ul>
       </li>
       @if($kd_suplier != '0')
       <li class="nav-item <?php if ($data['menu'] == 'akuntansi') {
                              echo "menu-open";
                            } ?>">
         <a href="{{ url('profil_anggota/'.$id_anggota) }}" class="nav-link">
           <i class="nav-icon fa fa-user"></i>
           <p>
             Profil
             <i class="right fas fa-angle-left"></i>
           </p>
         </a>

       </li>
       <li class="nav-item <?php if ($data['menu'] == 'm_suplier') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-th"></i>
          <p>
            Suplier
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('m_suplier_stok') }}" class="nav-link <?php if ($data['Active'] == 'stok_suplier') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Stok Produk</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('m_penjualan_suplier') }}" class="nav-link <?php if ($data['Active'] == 'm_penjualan_suplier') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Penjualan Ke Koperasi</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('m_pembayaran_suplier') }}" class="nav-link <?php if ($data['Active'] == 'm_pembayaran_suplier') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Pembayaran</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('under_maintenance') }}" class="nav-link <?php if ($data['Active'] == 'jenis_simpanan') {
                                                                      echo "active";
                                                                    } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Penjualan Produk</p>
            </a>
          </li>
          

        </ul>
      </li>

      @endif
       <li class="nav-item <?php if ($data['menu'] == 'e-commerce') {
                            echo "menu-open";
                          } ?>">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-file"></i>
          <p>
            E-Commerce
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('e_comm') }}" class="nav-link <?php if ($data['Active'] == 'e_produk') {
                                                                echo "active";
                                                              } ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Produk</p>
            </a>
          </li>

        </ul>
      </li>
       <li class="nav-item">
         <a href="{{ url('keluar') }}" class="nav-link">
           <i class="nav-icon fas fa-file"></i>
           <p>
             Keluar
             <i class="right fas fa-angle-left"></i>
           </p>
         </a>
       </li>

     </ul>
   </nav>
 </div>