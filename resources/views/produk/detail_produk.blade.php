 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif
     <div class="card-header">
       <h3 class="card-title">Form user Koperasi Senandung 165</h3>

       <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
     </div>
     <!-- /.card-header -->
     @if($data['flag_edit']=='1')
     <form action="{{ route('edit_stok_act') }}" method="post" enctype="multipart/form-data">
       <input type="hidden" name="id_detail_produk" value="{{$data['id_detail_produk']}}">
       @else
       <form action="{{ route('add_stok_produk') }}" method="post" enctype="multipart/form-data">
         @endif

         @csrf
         <div class="card-body">
           <div class="row">
             <div class="col-md-4">
               <div class="form-group">
                 <label>Kode | Nama Produk</label>
                 @if($data['flag_edit']=='1')
                 <input type="text" readonly="" name="nm_produk" class="form-control" value="{{$data['kode_produk']}} | {{$data['nama_produk']}} ">
                 @else
                 <input type="text" readonly="" name="nm_produk" class="form-control" value="{{$data['kode_produk']}} | {{$data['nama_produk']}}">
                 @endif
                 <input type="hidden" readonly="" name="kode_produk" class="form-control" value="{{$data['kode_produk']}}">
               </div>
             </div>
             @if($data['flag_warna']=='1')
             <div class="col-md-4">
               <div class="form-group">
                 <label>Warna</label>
                 @if($data['flag_edit']=='1')
                 <select class="form-control" disabled="" name="kode_warna" style="width: 100%;">
                   @foreach($data['list_warna'] as $row)
                   <option value="{{$row->kode_warna}}" <?php if ($data['kode_warna'] == $row->kode_warna) {
                                                          echo "selected";
                                                        } ?>>{{$row->nm_warna}}</option>
                   @endforeach
                 </select>
                 @else
                 <select class="form-control" name="kode_warna" style="width: 100%;">
                   <option value=""></option>
                   @foreach($data['list_warna'] as $row)

                   <option value="{{$row->kode_warna}}">{{$row->nm_warna}}</option>
                   @endforeach
                 </select>
                 @endif
               </div>
             </div>
             @endif

             <!-- /.col -->

             <div class="col-md-4">

               <div class="form-group">
                 <label for="exampleInputEmail1">Stok / Jumlah Produk</label>
                 @if($data['flag_edit']=='1')
                 <input type="text" name="stok" class="form-control" value="{{ $data['stok'] }}">
                 @else
                 <input type="text" name="stok" class="form-control" value="">
                 @endif
               </div>

             </div>
             <!-- /.col -->
           </div>
           <!-- /.row -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Simpan</button>
           <a href="{{ route('list_produk') }}" class="btn btn-warning">Kembali</a>
           <!-- <button type="reset" class="btn btn-warning">Kembali</button> -->
         </div>
       </form>
   </div>
   <div class="card card-primary">
     <div class="card-header">
       <h3 class="card-title">List Detail Barang Koperasi</h3>
     </div>
     <!-- /.card-header -->
     <div class="card-body">
       <table id="example1" class="table table-bordered table-striped">
         <thead>
           <tr>
             <th>No</th>
             <th>Kode Produk</th>
             <th>Nama Produk</th>
             <th>Warna</th>
             <th>Stok</th>
             <th>Aksi</th>
           </tr>
         </thead>
         <tbody>
           <?php
            $no = 0;
            foreach ($data['list_det_produk'] as $row) {
              $no++;
            ?>

             <tr>
               <td>{{$no}}</td>
               <td>{{$row->kode_produk}}</td>
               <td>{{$row->nama_produk}}</td>
               <td>{{$row->nm_warna}}</td>
               <td>{{$row->stok}}</td>
               <td>

                 <a href="{{ url('edit_det_produk/'.$row->id_detail_produk) }}"><i class="fas fa-edit"></i></a>
                 &nbsp;&nbsp;
                 <a href="{{ url('delete_det_produk/'.$row->id_detail_produk) }}"><i class="fas fa-trash"></i></a>
                 &nbsp;&nbsp;
                 <a href="{{ url('foto_produk/'.$row->id_detail_produk) }}"><i class="fas fa-camera"></i></a>

               </td>
             </tr>

           <?php } ?>
           </tfoot>
       </table>
     </div>
     <!-- /.card-body -->
   </div>
   @endsection