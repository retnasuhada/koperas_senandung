 @extends('template')
 <style type="text/css">
   .title_produk{
      
      font-family: monospace;
      font-weight: bold;
      font-size: 16px;
      text-align: center;
      padding: 0;
   }
   .price{
      color: green;
      font-family: sans-serif;
      font-weight: bold;
      font-size: 10px;
      text-align: center;
      padding: 0;
      margin: 0;
   }
   .title-produk{
      color: black;
      font-family: sans-serif;
      font-weight: bold;
      font-size: 12px;
      text-align: center;
      padding: 0;
      margin: 0;
   }
   .price2{
      color: red;
      font-family: sans-serif;
      font-weight: bold;
      font-size: 10px;
      text-align: center;
      padding: 0;
      margin: 0;
   }
   img {
  width: 500px;
  height: 500px;
   object-fit: cover;
}
 </style>
 @section('content')
 <form action="enhanced-results.html">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <input type="search" class="form-control form-control-lg" placeholder="Cari Produk">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-lg btn-default">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
 <!-- <section class="content pb-3"> -->

<div class="card-footer">
   Produk Terbaru
 </div>
 <br>
<div class="card">
    <div class="card-header bg-primary text-white text-uppercase">
                    <i class="fa fa-star"></i> Produk Terbaru
                </div>
   <div class="row">

     @foreach($data['list_eproduk'] as $list)
     <div class="col-md-2">
       <!-- DIRECT CHAT PRIMARY -->
       <div class="card card-primary card-outline direct-chat direct-chat-primary">

         <div class="card-header">
           <p class="title-produk">{{$list->nama_produk}}</p>
         </div>
         <!-- /.card-header -->
         <div class="card-body">
           <img src="{{ asset('temp/'.$list->foto)}}" class="product-image" alt="white sample">
         </div>
         <div class="text-center">
               <p class="price">Anggota &nbsp; {{"Rp".number_format($list->harga_anggota, 0,',','.')}}</p>
               <p class="price2">Umum &nbsp; {{"Rp".number_format($list->harga_umum, 0,',','.')}}</p>
             </div>
         <!-- /.card-body -->
         
         
         </form>
       </div>
       <!--/.direct-chat -->
     </div>

     @endforeach

    </div>
   </div>
 </section>
 <!-- /.card-body -->
 <div class="card-footer">
   Footer
 </div>
 <!-- /.card-footer-->
 </div>
 @endsection