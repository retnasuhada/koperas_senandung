 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 <!-- /.card-body -->
 <div class="card card-success">

   <div class="card-header">
     <h3 class="card-title">Form Tambah Produk</h3>

     <div class="card-tools">
       <button type="button" class="btn btn-tool" data-card-widget="collapse">
         <i class="fas fa-minus"></i>
       </button>
       <button type="button" class="btn btn-tool" data-card-widget="remove">
         <i class="fas fa-times"></i>
       </button>
     </div>
   </div>
   <!-- /.card-header -->
   @if($data['flag_edit']=='1')
   <form action="{{ route('edit_user_act') }}" method="post" enctype="multipart/form-data">
     <input type="hidden" name="id_produk" value="{{$data['id_produk']}}">
     @else
     <form action="{{ route('add_det_produk') }}" method="post" enctype="multipart/form-data">
       <input type="hidden" name="id_kd_produk" value="{{$data['id_kd_produk']}}">
       @endif

       @csrf
       <div class="card-body">
         <div class="row">
           <div class="col-md-6">

             <div class="form-group">
               <label>Kode Produk</label>
               <input type="text" name="kd_produk" readonly="" class="form-control" value="{{$data['kd_produk']}}">
             </div>
             <div class="form-group">
               <label>Nama Produk</label>
               <input type="text" name="nama_produk" readonly="" class="form-control" value="{{$data['nama_produk']}}">
             </div>
             <div class="form-group">
               <label>Kategori Produk</label>
               <select name="id_kategori" disabled="" class="form-control">
                 @foreach($data['list_kategori'] as $kategori)
                 <option value="{{$kategori->id_kategori}}" <?php if ($data['id_kategori'] == $kategori->id_kategori) {
                                                              echo "selected";
                                                            } ?>>{{$kategori->nm_kategori}}</option>
                 @endforeach
               </select>
             </div>
             <div class="form-group">
               <label>Warna</label>
               <select name="id_warna" disabled="" class="form-control">
                 @foreach($data['list_warna'] as $warna)
                 <option value="{{$warna->id_warna}}" <?php if ($data['id_warna'] == $warna->id_warna) {
                                                        echo "selected";
                                                      } ?>>{{$warna->nm_warna}}</option>
                 @endforeach
               </select>
             </div>
             <div class="form-group">
               <label>Satuan</label>
               <select name="id_satuan" disabled="" class="form-control">

                 @foreach($data['list_satuan'] as $satuan)
                 <option value="{{$satuan->id_satuan}}" <?php if ($data['id_satuan'] == $satuan->id_satuan) {
                                                          echo "selected";
                                                        } ?>>{{$satuan->nm_satuan}}</option>
                 @endforeach
               </select>
             </div>
             <div class="form-group">
               <label>Size (Ukuran)</label>
               <select name="id_size" disabled="" class="form-control">
                 @foreach($data['list_size'] as $size)
                 <option value="{{$size->id_size}}" <?php if ($data['id_size'] == $size->id_size) {
                                                      echo "selected";
                                                    } ?>>{{$size->nm_size}}</option>
                 @endforeach
               </select>
             </div>

           </div>

           <div class="col-md-6">

             <div class="form-group">
               <label>Stok (Jumlah)</label>
               <input type="text" name="stok" readonly="" class="form-control" value="{{$data['stok']}}">
             </div>
             <div class="form-group">
               <label>Berat</label>
               <input type="text" readonly="" name="berat" class="form-control" value="{{$data['berat']}}">
             </div>
             <div class="form-group">
               <label>Volume</label>
               <input type="text" readonly="" name="volume" class="form-control" value="{{$data['volume']}}">
             </div>
             <div class="form-group">
               <label for="exampleInputFile">Deskripsi Produk</label>
               <textarea class="form-control" readonly="" name="deskripsi" rows="4">{{$data['deskripsi']}}</textarea>
             </div>
           </div>
         </div>
         <!-- /.row -->
       </div>
       <div class="card-footer">
         <button type="submit" class="btn btn-success"><i class="fas fa-edit"></i>Edit</button>
         <button type="reset" class="btn btn-warning"><i class="fas fa-times-circle"></i>Cancel</button>
       </div>
     </form>
 </div>


 @endsection