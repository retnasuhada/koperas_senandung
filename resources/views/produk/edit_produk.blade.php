 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->


 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif
     <div class="card-header">
       <h3 class="card-title">Form Produk</h3>

       <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
     </div>
     <!-- /.card-header -->
     @if($data['flag_edit']=='1')
     <form action="{{ route('edit_back_produk_act') }}" method="post" enctype="multipart/form-data">
       <input type="hidden" name="id_kd_produk" value="{{$data['kode_produk']}}">
       @else
       <form action="{{ route('add_back_produk_act') }}" method="post" enctype="multipart/form-data">
         @endif

         @csrf
         <div class="card-body">
           <div class="row">
             <div class="col-md-6">

               <div class="form-group">
                 <label>Kode Produk</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" readonly="" name="kode_produk" class="form-control" value="{{$data['kode_produk']}}">
                 @else
                 <input type="texs" name="kode_produk" class="form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label>Nama Produk</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="nama_produk" class="form-control" value="{{$data['nama_produk']}}">
                 @else
                 <input type="texs" name="nama_produk" class="form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label>Kategori</label>
                 <select onchange="onpiton1()" name="kode_kategori" id="kategori" class="form-control">
                   @if($data['flag_edit']=='1')
                   @foreach($data['list_kategori'] as $kategori)
                   <option value="{{$kategori->kode_kategori}}" <?php if ($data['kode_kategori'] == $kategori->kode_kategori) {
                                                                  echo "selected";
                                                                } ?>>{{$kategori->nama_kategori}}</option>
                   @endforeach
                   @else
                   <option value="">-</option>
                   @foreach($data['list_kategori'] as $kategori)
                   <option value="{{$kategori->kode_kategori}}">{{$kategori->nama_kategori}}</option>
                   @endforeach
                   @endif

                 </select>
               </div>

               <div class="result"></div>
               <div class="result2"></div>

               <div class="form-group">
                 <label>Pilihan Warna</label>
                 @if($data['flag_edit']=='1')
                 <select name="flag_warna" class="form-control">
                   <option value="0" <?php if ($data['flag_warna'] == '0') {
                                        echo "selected";
                                      } ?>>Tidak Ada Pilihan Warna</option>
                   <option value="1" <?php if ($data['flag_warna'] == '1') {
                                        echo "selected";
                                      } ?>>Ada Pilihan Warna</option>
                 </select>
                 @else
                 <select name="flag_warna" class="form-control">
                   <option value="0">Tidak Ada Pilihan Warna</option>
                   <option value="1">Ada Pilihan Warna</option>
                 </select>
                 @endif
               </div>

             </div>
             <div class="col-md-6">
               <div class="form-group">
                 <label>Harga Umum</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="harga_umum" id="rupiah" class="rupiah form-control" value="{{"Rp " . number_format($data['harga_umum'],0,',','.')}}">
                 @else
                 <input type="texs" name="harga_umum" id="rupiah" class="rupiah form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label>Harga Anggota</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="harga_anggota" id="rupiah3" class="rupiah3 form-control" value="{{"Rp " . number_format($data['harga_anggota'],0,',','.')}}">
                 @else
                 <input type="texs" name="harga_anggota" id="rupiah3" class="rupiah3 form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label>Pilihan Grosir</label>
                 @if($data['flag_edit']=='1')
                 <select name="flag_grosir" class="form-control">
                   <option value="0" <?php if ($data['flag_grosir'] == '0') {
                                        echo "selected";
                                      } ?>>Tidak Harga Grosir</option>
                   <option value="1" <?php if ($data['flag_grosir'] == '1') {
                                        echo "selected";
                                      } ?>>Ada Harga Grosir</option>
                 </select>
                 @else
                 <select name="flag_grosir" class="form-control">
                   <option value="0">Tidak Ada Grosir</option>
                   <option value="1">Ada Pilihan Grosir</option>
                 </select>
                 @endif
               </div>
               <div class="form-group">
                 <label>Harga Grosir Umum</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="harga_grosir_umum" id="rupiah2" class="rupiah2 form-control" value="{{"Rp " . number_format($data['harga_grosir_umum'],0,',','.')}}">
                 @else
                 <input type="texs" name="harga_grosir_umum" id="rupiah2" class="rupiah2 form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label>Harga Grosir Umum</label>
                
                 <input type="texs" name="minimum_grosir" class=" form-control" value="{{$data['minimum_grosir']}}">
                
               </div>
               <div class="form-group">
                 <label>Harga Grosir Anggota</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="harga_grosir_anggota" id="rupiah4" class="rupiah4 form-control" value="{{"Rp " . number_format($data['harga_grosir_anggota'],0,',','.')}}">
                 @else
                 <input type="texs" name="harga_grosir_anggota" id="rupiah4" class="rupiah4 form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label>Harga Beli</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="harga_beli" id="rupiah5" class="rupiah5 form-control" value="{{"Rp " . number_format($data['harga_beli'],0,',','.')}}">
                 @else
                 <input type="texs" name="harga_beli" id="rupiah5" class="rupiah5 form-control" value="">
                 @endif
               </div>
               <div class="form-group">
                 <label>Kode_suplier</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="kode_suplier" class="form-control" value="Indomaret">
                 @else
                 <input type="texs" name="kode_suplier" class="form-control" value="">
                 @endif
               </div>
             </div>

           </div>
           <div class="col-md-12">
             <div class="form-group">
               <label>Deskripsi Produk</label>
               @if($data['flag_edit']=='1')
               <textarea class="form-control" name="deskripsi" rows="3">{{ $data['deskripsi'] }}</textarea>
               @else
               <textarea class="form-control" name="deskripsi" rows="3" placeholder="Enter ..."></textarea>
               @endif
             </div>
           </div>
           <!-- /.row -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Simpan</button>
           <button type="reset" class="btn btn-warning">Cancel</button>
         </div>
       </form>
   </div>

   <script type="text/javascript">
     function onpiton1() {
       var n = $("#kategori").val();
       $(".result").html(null);
       $(".result2").html(null);

       $.ajax({
         url: '/getSubKategori/' + n,
         type: 'get',
         dataType: 'JSON',
         success: function(response) {
           var len = response.length;

           var tr_str = "<div class='form-group'>";
           tr_str += "<label for='exampleInputEmail1'>Sub Kategori</label>";

           tr_str += "<select onchange='onpiton2()' id='sub_kategori' name='kode_sub_kategori' class='form-control' style='width: 100%;'>";
           tr_str += "<option value=''>-</option>";
           for (var i = 0; i < len; i++) {
             var kode_akun = response[i].kode_sub_kategori;
             var nama_akun = response[i].nama_sub_kategori;
             tr_str += "<option value=" + kode_akun + ">" + nama_akun + "</option>";
           }
           tr_str += "</select>";
           $(".result").append(tr_str);
         }
       });
     }

     function onpiton2() {
       var n = $("#sub_kategori").val();
       $(".result2").html(null);

       $.ajax({
         url: '/getSubKategoriA/' + n,
         type: 'get',
         dataType: 'JSON',
         success: function(response) {
           var len = response.length;
           var tr_str = "<div class='form-group'>";
           tr_str += "<label for='exampleInputEmail1'>Child Kategori</label>";
           tr_str += "<select name='kode_sub_kategori2' id='kode_sub_kategori' class='form-control' style='width: 100%;'>";
           for (var i = 0; i < len; i++) {
             var kode_akun = response[i].kode_sub_kategori2;
             var nama_akun = response[i].nama_sub_kategori2;
             tr_str += "<option value=" + kode_akun + ">" + nama_akun + "</option>";
           }
           tr_str += "</select>";
           $(".result2").append(tr_str);
         }
       });
     }


     var rupiah = document.getElementById('rupiah');
     rupiah.addEventListener('keyup', function(e) {

       rupiah.value = formatRupiah(this.value, 'Rp. ');
     });

     var rupiah2 = document.getElementById('rupiah2');
     rupiah2.addEventListener('keyup', function(e) {

       rupiah2.value = formatRupiah(this.value, 'Rp. ');
     });
     var rupiah3 = document.getElementById('rupiah3');
     rupiah3.addEventListener('keyup', function(e) {

       rupiah3.value = formatRupiah(this.value, 'Rp. ');
     });
     var rupiah4 = document.getElementById('rupiah4');
     rupiah4.addEventListener('keyup', function(e) {

       rupiah4.value = formatRupiah(this.value, 'Rp. ');
     });

     var rupiah5 = document.getElementById('rupiah5');
     rupiah5.addEventListener('keyup', function(e) {

       rupiah5.value = formatRupiah(this.value, 'Rp. ');
     });

     /* Fungsi formatRupiah */
     function formatRupiah(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 3,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{3}/gi);

       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
     }
   </script>


   @endsection