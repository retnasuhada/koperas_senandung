 @extends('template')
 @section('content')
 @yield('content')
 <!-- Default box -->
 <div class="card">
   <div class="card-header">
     <h3 class="card-title">Produk Detail</h3>

     <div class="card-tools">
       <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
         <i class="fas fa-minus"></i>
       </button>
       <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
         <i class="fas fa-times"></i>
       </button>
     </div>
   </div>
   <div class="card-body">
     <div class="row">
       <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
         <div class="row">
           <div class="col-12 col-sm-4">
             <div class="info-box bg-light">
               <div class="info-box-content">
                 <span class="info-box-text text-center text-muted">Stok Saat Ini</span>
                 <span class="info-box-number text-center text-muted mb-0">{{ $data['stok'] }}</span>
               </div>
             </div>
           </div>
           <div class="col-12 col-sm-4">
             <div class="info-box bg-light">
               <div class="info-box-content">
                 <span class="info-box-text text-center text-muted">Total Produk Di Beli</span>
                 <span class="info-box-number text-center text-muted mb-0">2000</span>
               </div>
             </div>
           </div>
           <div class="col-12 col-sm-4">
             <div class="info-box bg-light">
               <div class="info-box-content">
                 <span class="info-box-text text-center text-muted">Total Pelanggan Pembeli</span>
                 <span class="info-box-number text-center text-muted mb-0">20</span>
               </div>
             </div>
           </div>
         </div>
         <div class="row">
           <div class="col-12">
             <h4>{{$data['nama_produk']}}</h4>
             <div class="post">

               <!-- /.user-block -->
               <table>
                 <tr>
                   <td width="180px;"><b class="d-block">Kode Produk</b></td>
                   <td><b class="d-block">:</b></td>
                   <td>{{$data['kode_produk']}}</td>
                 </tr>
                 <tr>
                   <td><b class="d-block">Kategori</b></td>
                   <td><b class="d-block">:</b></td>
                   <td>{{ $data['kategori'] }}</td>
                 </tr>
                 <tr>
                   <td><b class="d-block">Sub Kategori</b></td>
                   <td><b class="d-block">:</b></td>
                   <td>{{ $data['subkategori'] }}</td>
                 </tr>
                 <tr>
                   <td><b class="d-block">Child Kategori</b></td>
                   <td><b class="d-block">:</b></td>
                   <td>{{ $data['childkategori'] }}</td>
                 </tr>
                 <tr>
                   <td><b class="d-block">Warna</b></td>
                   <td><b class="d-block">:</b></td>
                   @if($data['warna']=='1')
                   <td>{{ $data['warna'] }}</td>
                   @else
                   <td>Tidak Ada Pilihan Warna</td>
                   @endif
                 </tr>

                 <tr>
                   <td><b class="d-block">Harga Anggota</b></td>
                   <td><b class="d-block">:</b></td>
                   <td>{{"Rp " . number_format($data['harga_anggota'] ,2,',','.')}}</td>
                 </tr>
                 <tr>
                   <td><b class="d-block">Harga Umum</b></td>
                   <td><b class="d-block">:</b></td>
                   <td>{{"Rp " . number_format($data['harga_umum'] ,2,',','.')}}</td>
                 </tr>
                 <tr>
                   <td><b class="d-block">Harga Grosir Anggota</b></td>
                   <td><b class="d-block">:</b></td>
                   @if($data['grosir']=='1')
                   <td>{{"Rp " . number_format($data['harga_grosir_anggota'] ,2,',','.')}}</td>
                   @else
                   <td>Tidak Ada Pilihan Grosir</td>
                   @endif
                 </tr>
                 <tr>
                   <td><b class="d-block">Harga Grosir Umum</b></td>
                   <td><b class="d-block">:</b></td>
                   @if($data['grosir']=='1')
                   <td>{{"Rp " . number_format($data['harga_grosir_umum'] ,2,',','.')}}</td>
                   @else
                   <td>Tidak Ada Pilihan Grosir</td>
                   @endif
                 </tr>

                 <tr>
                   <td><b class="d-block">Harga Beli</b></td>
                   <td><b class="d-block">:</b></td>
                   <td>{{"Rp " . number_format($data['harga_beli'] ,2,',','.')}}</td>
                 </tr>
               </table>
             </div>
           </div>
         </div>
       </div>
       <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
         <h3 class="text-primary"><i class="fas fa-paint-brush"></i> {{$data['nama_produk']}}</h3>
         <p class="text-muted">{{$data['deskripsi']}}</p>
         <br>
          <h5 class="mt-5 text-muted">Suplier</h5>
         <table>
         @if(!empty($data['info_suplier']))
         @foreach($data['info_suplier'] as $row_suplier)
         <tr>
             <td width="30px;"><i class="fa fa-user"></i></td>
             <td>{{$row_suplier->nama_suplier}}</td>
           </tr>
         <tr>
             <td width="30px;"><i class="fa fa-map-marker"></i></td>
             <td>{{$row_suplier->alamat}}</td>
           </tr>
           <tr>
             <td width="30px;"><i class="fa fa-phone"></i></td>
             <td>{{$row_suplier->no_hp}}</td>
           </tr>
         @endforeach
         @endif
         </table>


       </div>
     </div>
   </div>
   <!-- /.card-body -->
 </div>
 <!-- /.card -->

 @endsection