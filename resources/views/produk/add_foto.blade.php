 @extends('template')
 @section('content')
 @yield('content')
 <div class="card card-success">
   <form action="{{ route('upload_foto_produk') }}" method="POST" enctype="multipart/form-data">
     <input type="hidden" name="id_kd_produk" value="{{$data['id_produk']}}">
     @csrf
     <div class="card-body">
       <div class="row">
         <div class="col-md-6">
           <div class="form-group">
             <label>Nama Produk</label>
             <input type="text" name="nama_produk" readonly="" class="form-control" value="{{$data['nama_produk']}}">
             <input type="hidden" name="id_produk" readonly="" class="form-control" value="{{$data['id_produk']}}">
           </div>
         </div>
         <div class="col-md-6">
           <div class="form-group">
             <label>Foto</label>
             <div class="custom-file">
               <input type="file" class="custom-file-input" name="foto" id="customFile">
               <label class="custom-file-label" for="customFile"></label>
             </div>
           </div>
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Simpan</button>
           <button type="reset" class="btn btn-warning">Cancel</button>
         </div>
   </form>
 </div>

 @endsection