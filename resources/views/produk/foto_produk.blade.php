 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif
     <div class="card-header">
       <h3 class="card-title">Form Foto Produk Koperasi Senandung 165</h3>

       <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
     </div>
     <!-- /.card-header -->
     @if($data['flag_edit']=='1')
     <form action="{{ route('edit_stok_act') }}" method="post" enctype="multipart/form-data">
       <input type="hidden" name="id_detail_produk" value="{{$data['id_detail_produk']}}">
       @else
       <form action="{{ route('add_foto_produk') }}" method="post" enctype="multipart/form-data">
         @endif

         @csrf
         <div class="card-body">
           <div class="row">
             <div class="col-md-4">
               <div class="form-group">
                 <label for="exampleInputFile">Foto</label>
                 <div class="input-group">
                   <div class="custom-file">
                     <input type="file" name="foto" class="custom-file-input" id="exampleInputFile">
                     <label class="custom-file-label" for="exampleInputFile"></label>
                   </div>
                   <div class="input-group-append">
                     <span class="input-group-text">Upload</span>
                   </div>
                 </div>
               </div>
               <div class="form-check">
                 <input type="checkbox" class="form-check-input" name="foto_utama" id="exampleCheck1">
                 <label class="form-check-label" for="exampleCheck1">Jadikan Foto Utama</label>
                 <input type="hidden" name="kode_produk" value="{{ $data['kode_produk'] }}">
                
               </div>
             </div>

           </div>
           <!-- /.row -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Simpan</button>
           <a href="{{ url('detail_produk/'.$data['kode_produk']) }}" class="btn btn-warning">Kembali</a>
           <!-- <button type="reset" class="btn btn-warning">Kembali</button> -->
         </div>
       </form>
   </div>
   <div class="card card-primary">
     <div class="card-header">
       <h3 class="card-title">List Foto Produk Koperasi</h3>
     </div>
     <!-- /.card-header -->
     <div class="card-body">
       <table id="example1" class="table table-bordered table-striped">
         <thead>
           <tr>
             <th>No</th>
             <th>Foto</th>
             <th>Flag Utama</th>

             <th>Aksi</th>
           </tr>
         </thead>
         <tbody>
           <?php
            $no = 0;
            $link = $data['destinationPath'];
            foreach ($data['list_foto'] as $row) {
              $no++; ?>

             <tr>
               <td>{{$no}}</td>
               <!-- <td>{{$row->foto}}</td> -->
               <td><a href="{{asset('/temp/'.$row->foto)}}">{{$row->foto}}</a></td>
               <td>
                 @if($row->flag_utama=='1')
                 True
                 @else
                 <a href="{{ url('foto_utama_create/'.$row->id_foto_produk) }}" class="btn btn-success">Jadikan Foto Utama</a>
                 @endif

               </td>

               <td>


                 <a href="{{ url('hapus_foto_produk/'.$row->id_foto_produk) }}"><i class="fas fa-trash"></i></a>


               </td>
             </tr>

           <?php } ?>
           </tfoot>
       </table>
     </div>
     <!-- /.card-body -->
   </div>
   @endsection