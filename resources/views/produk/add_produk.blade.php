 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 <!-- /.card-body -->

 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 <div class="card card-primary">
   <div class="card-header">
     <a href="{{url('add_new_detail_produk/'.$data['id_kd_produk'])}}" class="btn btn-primary">
       <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Detail Produk Baru
     </a>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
     <table id="example1" class="table table-bordered table-striped">
       <thead>
         <tr>
           <th>No</th>
           <th>Nama Produk</th>
           <th>Kategori</th>
           <th>Satuan</th>
           <th>Warna</th>
           <th>Stok</th>
           <th>Aksi</th>

         </tr>
       </thead>
       <tbody>

         <?php
          $no = 1;
          foreach ($data['list_det_produk'] as $res) { ?>
           <tr>
             <td>{{$no}}</td>
             <td>{{$res->nama_produk}}</td>
             <td>{{$res->nm_kategori}}</td>
             <td>{{$res->nm_satuan}}</td>
             <td>{{$res->nm_warna}}</td>
             <td>{{$res->stok}}</td>
             <td>
               <a href="{{ url('back_detail_produk/'.$res->id_produk) }}" class="btn btn-primary btn-sm">Detail</a>
             </td>
           </tr>

         <?php $no++;
          } ?>
         </tfoot>
     </table>
   </div>
   <!-- /.card-body -->
 </div>
 <div class="col-12">
   <div class="card card-primary card-outline">
     <div class="card-header">
       <form action="{{ route('add_foto') }}" method="post" enctype="multipart/form-data">
         @csrf
         <input type="hidden" name="id_produk" value="{{$data['id_kd_produk']}}">
         <button type="submit" class="btn btn-primary">Tambah Foto</button>
       </form>
     </div>
     <div class="card-body">
       <div class="row">
         @foreach($data['list_foto'] as $gambar)

         <div class="col-sm-2">
           <a href="{{ asset('temp/'.$gambar->foto) }}" data-toggle="lightbox" data-title="sample 1 - white" data-gallery="gallery">
             <img src="{{ asset('temp/'.$gambar->foto) }}" class="img-fluid mb-2" alt="white sample" />
           </a>
           <a href="#" class="btn btn-primary"><i class="fas fa-edit"></i>Edit</a><a href="#" class="btn btn-danger"><i class="fas fa-trash-alt"></i>Hapus</a>
         </div>
         @endforeach
       </div>
     </div>
   </div>

   @endsection