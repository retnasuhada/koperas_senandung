 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 <!-- /.card-body -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif
     <div class="card-header">
       <h3 class="card-title">Form Tambah Produk</h3>

       <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
     </div>
     <!-- /.card-header -->
     @if($data['flag_edit']=='1')
     <form action="{{ route('edit_user_act') }}" method="post" enctype="multipart/form-data">
       <input type="hidden" name="id_produk" value="{{$data['id_produk']}}">
       @else
       <form action="{{ route('add_det_produk') }}" method="post" enctype="multipart/form-data">
         <input type="hidden" name="id_kd_produk" value="{{$data['id_kd_produk']}}">
         @endif

         @csrf
         <div class="card-body">
           <div class="row">
             <div class="col-md-6">

               <div class="form-group">
                 <label>Kode Produk</label>
                 <input type="texs" name="kd_produk" readonly="" class="form-control" value="{{$data['kd_produk']}}">

               </div>
               <div class="form-group">
                 <label>Nama Produk</label>
                 <input type="texs" name="nama_produk" readonly="" class="form-control" value="{{$data['nama_produk']}}">
               </div>
               <div class="form-group">
                 <label>Kategori Produk</label>
                 <select name="id_kategori" disabled="" class="form-control">
                   @foreach($data['list_kategori'] as $kategori)
                   <option value="{{$kategori->id_kategori}}" <?php if ($data['id_kategori'] == $kategori->id_kategori) {
                                                                echo "selected";
                                                              } ?>>{{$kategori->nm_kategori}}</option>
                   @endforeach
                 </select>
               </div>
               <div class="form-group">
                 <label>Warna</label>
                 <select name="id_warna" class="form-control">
                   <option value="">-</option>
                   @foreach($data['list_warna'] as $warna)
                   <option value="{{$warna->id_warna}}">{{$warna->nm_warna}}</option>
                   @endforeach
                 </select>
               </div>
               <div class="form-group">
                 <label>Satuan</label>
                 <select name="id_satuan" class="form-control">
                   <option value="">-</option>
                   @foreach($data['list_satuan'] as $satuan)
                   <option value="{{$satuan->id_satuan}}">{{$satuan->nm_satuan}}</option>
                   @endforeach
                 </select>
               </div>
               <div class="form-group">
                 <label>Size (Ukuran)</label>
                 <select name="id_size" class="form-control">
                   <option value="">-</option>
                   @foreach($data['list_size'] as $size)
                   <option value="{{$size->id_size}}">{{$size->nm_size}}</option>
                   @endforeach
                 </select>
               </div>

             </div>

             <div class="col-md-6">
               <div class="form-group">
                 <label>Stok (Jumlah)</label>
                 <input type="texs" name="stok" class="form-control">
               </div>
               <div class="form-group">
                 <label>Berat</label>
                 <input type="texs" name="berat" class="form-control">
               </div>
               <div class="form-group">
                 <label>Volume</label>
                 <input type="texs" name="volume" class="form-control">
               </div>
               <div class="form-group">
                 <label for="exampleInputFile">Deskripsi Produk</label>
                 <textarea class="form-control" name="deskripsi" rows="4" placeholder="Enter ..."></textarea>
               </div>

             </div>
           </div>
           <!-- /.row -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Simpan</button>
           <button type="reset" class="btn btn-warning">Cancel</button>
         </div>
       </form>
   </div>

   @endsection