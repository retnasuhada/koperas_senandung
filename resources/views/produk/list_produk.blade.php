 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 <!-- /.card-body -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif

 <div class="card card-primary">
   <div class="card-header">
     <a href="{{route('add_back_produk')}}" class="btn btn-primary">
       <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Produk
     </a>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
     <table id="example1" class="table table-bordered table-striped">
       <thead>
         <tr>
           <th>No</th>
           
           <th>Nama</th>
           
           <th>Harga Umum</th>
           <th>Harga Anggota</th>
           <th>Grosir Umum</th>
           <th>Grosir Anggota</th>
           <th>Min Grosir</th>
           <th>Stok</th>
           <th>Aksi</th>

         </tr>
       </thead>
       <tbody>

         <?php
          $no = 1;
          foreach ($data['list_produk'] as $row) { ?>
           <tr>
             <td>{{$no}}</td>
             
             <td>{{$row->nama_produk}}</td>
             <td>{{"Rp " . number_format($row->harga_umum ,2,',','.')}}</td>
             <td>{{"Rp " . number_format($row->harga_anggota ,2,',','.')}}</td>
             <td>{{"Rp " . number_format($row->harga_grosir_umum ,2,',','.')}}</td>
             <td>{{"Rp " . number_format($row->harga_grosir_anggota ,2,',','.')}}</td>
             <td>{{$row->minimum_grosir}} Pcs</td>
             <td><a href="{{ url('detail_produk/'.$row->kode_produk) }}">{{$row->stok}}</a></td>

             <td>

               <a href="{{ url('detail_det_produk/'.$row->kode_produk) }}"><i class="fas fa-eye"></i>&nbsp;</a>
               <a href="{{ url('edit_produk/'.$row->kode_produk) }}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                &nbsp;
                 <a href="{{ url('foto_produk/'.$row->kode_produk) }}"><i class="fas fa-camera"></i></a>

             </td>
           </tr>

         <?php $no++;
          } ?>
         </tfoot>
     </table>
   </div>

   @endsection