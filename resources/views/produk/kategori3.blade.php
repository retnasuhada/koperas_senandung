 @extends('template')
 @section('content')
 @yield('content')

 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @elseif ($data['save']=='2')
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Gagal !</h5>
   Internal Server Error !
 </div>
 @endif
 <div class="row">

   <div class="col-md-4">
     <!-- general form elements -->
     @if($data['flag_edit']=='1')
     <div class="card card-warning">
       <div class="card-header">
         <h3 class="card-title">Edit Sub Kategori</h3>
       </div>
       @else
       <div class="card card-success">
         <div class="card-header">
           <h3 class="card-title">Tambah Sub Kategori</h3>
         </div>
         @endif

         <!-- /.card-header -->
         <!-- form start -->
         @if($data['flag_edit']=='1')
         <form action="{{ route('edit_child_kategori_act') }}" method="post">
           @else
           <form action="{{ route('add_child_kategori') }}" method="post">
             @endif

             @csrf

             <div class="card-body">
               <div class="form-group">
                 <label for="exampleInputEmail1">Kode Child Sub Kategori</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" readonly="" name="kode_sub_kategori2" class="form-control" value="{{ $data['kode_sub_kategori2'] }}">
                 <input type="hidden" name="id_sub_kategori2" class="form-control" value="{{ $data['id_sub_kategori2'] }}">
                 @else
                 <input type="texs" name="kode_sub_kategori2" class="form-control" value="{{$data['kode_child_kategori']}}">
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Kategori</label>
                 <select class="form-control" id="kategori" name="kode_kategori" onchange="onpiton1()" style="width: 100%;">
                   @if($data['flag_edit']=='1')
                   @foreach($data['list_kategori'] as $list)
                   <option value="{{$list->kode_kategori}}" <?php if ($data['kode_kategori'] == $list->kode_kategori) {
                                                              echo "selected";
                                                            } ?>>{{$list->nama_kategori}}</option>
                   @endforeach
                   @else
                   <option value=""></option>
                   @foreach($data['list_kategori'] as $list)
                   
                   <option value="{{$list->kode_kategori}}">{{$list->nama_kategori}}</option>
                   @endforeach
                   @endif
                 </select>
               </div>

               <div class="result"></div>


               <div class="form-group">
                 <label for="exampleInputEmail1">Nama child Kategori</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="nama_sub_kategori2" class="form-control" value="{{ $data['nama_sub_kategori2'] }}">
                 @else
                 <input type="texs" name="nama_sub_kategori2" class="form-control" placeholder="Masukan Sub  Kategori">
                 @endif

               </div>

             </div>
             <!-- /.card-body -->

             <div class="card-footer">
               <button type="submit" class="btn btn-primary">Simpan</button>
             </div>
           </form>
       </div>
     </div>
     <div class="col-md-8">

       <div class="card">
         <div class="card-header">
           <h3 class="card-title">List kategori</h3>
         </div>
         <!-- /.card-header -->
         <div class="card-body">
           <table class="table table-bordered">
             <thead>
               <tr>
                 <th style="width: 10px">#</th>
                 <th>Kode</th>
                 <th>Kategori</th>
                 <th>Sub Kategori</th>
                 <th>Child Sub Kategori</th>

                 <th style="width: 160px">Aksi</th>
               </tr>
             </thead>
             <tbody>
               <?php
                $no = 1;
                foreach ($data['list_kategori3'] as $row) { ?>
                 <tr>
                   <td>{{$no}}</td>
                   <td>{{$row->kode_sub_kategori}}</td>
                   <td>{{$row->nama_kategori}}</td>
                   <td>{{$row->nama_sub_kategori}}</td>
                   <td>{{$row->nama_sub_kategori2}}</td>
                   <td>

                     <a href="{{ url('edit_sub_kategori2/'.$row->id_sub_kategori2) }}"><i class="fas fa-edit"></i></a>
                     &nbsp;&nbsp;
                     <a href="{{ url('hapus_sub_kategori2/'.$row->id_sub_kategori2) }}"><i class="fas fa-trash"></i></a>

                   </td>
                 </tr>

               <?php $no++;
                } ?>



             </tbody>
           </table>
         </div>
         <!-- /.card-body -->

       </div>
     </div>
   </div>
   <script type="text/javascript">
     function onpiton1() {
       var n = $("#kategori").val();
       $(".result").html(null);

       $.ajax({
         url: '/getSubKategori/' + n,
         type: 'get',
         dataType: 'JSON',
         success: function(response) {
           var len = response.length;

           var tr_str = "<div class='form-group'>";
           tr_str += "<label for='exampleInputEmail1'>Sub Kategori</label>";

           tr_str += "<select name='kode_sub_kategori' id='kode_sub_kategori' class='form-control' style='width: 100%;'>";
           for (var i = 0; i < len; i++) {
             var kode_akun = response[i].kode_sub_kategori;
             var nama_akun = response[i].nama_sub_kategori;
             tr_str += "<option value=" + kode_akun + ">" + nama_akun + "</option>";
           }
           tr_str += "</select>";
           $(".result").append(tr_str);
         }
       });


     }

     var rupiah = document.getElementById('rupiah');
     rupiah.addEventListener('keyup', function(e) {

       rupiah.value = formatRupiah(this.value, 'Rp. ');
     });

     var rupiah2 = document.getElementById('rupiah2');
     rupiah2.addEventListener('keyup', function(e) {

       rupiah2.value = formatRupiah(this.value, 'Rp. ');
     });

     /* Fungsi formatRupiah */
     function formatRupiah(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 3,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{3}/gi);

       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
     }
   </script>
   @endsection