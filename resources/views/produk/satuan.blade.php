 @extends('template')
 @section('content')
 @yield('content')

 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @elseif ($data['save']=='2')
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Gagal !</h5>
   Internal Server Error !
 </div>
 @endif
 <div class="row">

   <div class="col-md-6">
     <!-- general form elements -->
     @if($data['flag_edit']=='1')
     <div class="card card-warning">
       <div class="card-header">
         <h3 class="card-title">Edit Warna</h3>
       </div>
       @else
       <div class="card card-success">
         <div class="card-header">
           <h3 class="card-title">Tambah Warna</h3>
         </div>
         @endif

         <!-- /.card-header -->
         <!-- form start -->
         @if($data['flag_edit']=='1')
         <form action="{{ route('edit_satuan_act') }}" method="post">
           @else
           <form action="{{ route('add_satuan') }}" method="post">
             @endif

             @csrf

             <div class="card-body">
               <div class="form-group">
                 <label for="exampleInputEmail1">Satuan</label>
                 @if($data['flag_edit']=='1')
                 <input type="text" name="nm_satuan" class="form-control" value="{{$data['nm_satuan']}}">
                 <input type="hidden" name="id_satuan" class="form-control" value="{{$data['id_satuan']}}">
                 @else
                 <input type="texs" name="nm_satuan" class="form-control" placeholder="Masukan Satuan">
                 @endif
               </div>


             </div>
             <!-- /.card-body -->

             <div class="card-footer">
               <button type="submit" class="btn btn-primary">Simpan</button>
             </div>
           </form>
       </div>
     </div>
     <div class="col-md-6">

       <div class="card">
         <div class="card-header">
           <h3 class="card-title">List Satuan</h3>
         </div>
         <!-- /.card-header -->
         <div class="card-body">
           <table class="table table-bordered">
             <thead>
               <tr>
                 <th style="width: 10px">#</th>
                 <th>satuan</th>

                 <th style="width: 160px">Aksi</th>
               </tr>
             </thead>
             <tbody>
               <?php
                $no = 1;
                foreach ($data['list_satuan'] as $row) { ?>
                 <tr>
                   <td>{{$no}}</td>
                   <td>{{$row->nm_satuan}}</td>


                   <td>
                     <a href="{{ url('edit_satuan/'.$row->id_satuan) }}" class="btn btn-warning btn-sm">Edit</a>

                     <a href="{{ url('hapus_petugas/'.$row->id_satuan) }}" class="btn btn-danger btn-sm">Hapus</a>

                   </td>
                 </tr>

               <?php $no++;
                } ?>



             </tbody>
           </table>
         </div>
         <!-- /.card-body -->

       </div>
     </div>
   </div>

   <script type="text/javascript">
     var rupiah = document.getElementById('rupiah');
     rupiah.addEventListener('keyup', function(e) {

       rupiah.value = formatRupiah(this.value, 'Rp. ');
     });

     var rupiah2 = document.getElementById('rupiah2');
     rupiah2.addEventListener('keyup', function(e) {

       rupiah2.value = formatRupiah(this.value, 'Rp. ');
     });

     /* Fungsi formatRupiah */
     function formatRupiah(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 3,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{3}/gi);

       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
     }
   </script>
   @endsection