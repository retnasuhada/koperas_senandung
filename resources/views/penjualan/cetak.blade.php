<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <style>
    @page {
      size: 8.5cm 5cm;
      margin: 0;
      padding: 0;
    }

    body {
      background-image: url('background.png');
      background-repeat: no-repeat;
      background-position: center;
      margin: 0;
      padding: 0;

    }

    .nm {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 9px;
    }

    .penggunaan {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 7px;


    }
  </style>
</head>

<body>
  <table width="100%" cellspacing="0" cellpadding="0">
    <tr>
      <td colspan="5" align="center">
        <p style="font-size: 12px;  font-weight: bold; font-family: Arial, Helvetica, sans-serif;">KARTU ANGGOTA KOPERASI</p>
      </td>

    </tr>
    <tr>
      <td width="5%"></td>
      <td width="25%" class="nm">No Anggota</td>
      <td width="2%" class="nm">:</td>
      <td class="nm">SN165-AGT-0721-0001</td>
      <td rowspan="4" class="nm"><img style="height: 70px;
     width: 70px;" src="{{public_path().'/user55.jpg'}}"></td>
    </tr>
    <tr>
      <td width="5%"></td>
      <td width="25%" class="nm">Nama</td>
      <td width="2%" class="nm">:</td>
      <td class="nm">Retna Suhada</td>
    </tr>
    <tr>
      <td width="5%"></td>
      <td width="25%" class="nm">NIK</td>
      <td width="2%" class="nm">:</td>
      <td class="nm">33011501007990001</td>
    </tr>

    <tr>
      <td colspan="5"></td>
    </tr>
    <tr>
      <td colspan="4"></td>
      <td class="penggunaan">Tangerang, 1-Juni-2021</td>
    </tr>
    <tr>
      <td colspan="5"></td>
    </tr>


  </table>
</body>

</html>