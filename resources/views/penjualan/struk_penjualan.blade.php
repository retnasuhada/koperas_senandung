<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <style>
    @page { 
      size: 8.2in 11.6in;
      
       }
    table {
        border-collapse: collapse;
        width: 100%;
    }
  
    .th {
        padding: 2px 2px;
        border:1px solid #000000;
        text-align: center;
    }
  
    .td {
        padding: 1px 1px;
        border:1px solid #000000;
    }
    .td2 {
        padding: 1px 1px;
       
    }
    .text-center {
        text-align: center;
    }

    .title {
       padding: 0px;
       margin: 0px;
       font-size: 10px;
       font-family: sans-serif;
       text-align: center;

    }
    .struk {
       padding: 0px;
       margin: 0px;
       font-size: 10px;
       font-family: sans-serif;
       text-align: left;
    }
    .struk-kanan {
       padding: 0px;
       margin: 0px;
       font-size: 10px;
       font-family: sans-serif;
       text-align: right;
    }
    .struk-center {
       padding: 0px;
       margin: 0px;
       font-size: 10px;
       font-family: sans-serif;
       text-align: center;
    }
    .total {
       padding: 0px;
       margin: 0px;
       font-size: 10px;
       font-family: sans-serif;
       text-align: left;
       font-weight: bold;
    }
</style>
</head>

<body class="A4">
    <section class="sheet padding-10mm">
        <!-- <h1>10 UNIVERSITAS FAVORIT DI INDONESIA</h1> -->
        
        <p class="title">KOPERASI SENANDUNG 165</p>
        <p class="title">Taman Argosubur, S11/11. Desa Pasanggrahan</p>
        <p class="title">Kecamatan Solear, Kabupaten Tangerang </p>
        <br>
        <table style="
        width: 100%; border: 1px solid #000000;">
            <thead>
                <tr>
                    <th class="th"><p class="title">Nama Barang</p></th>
                    <th class="th"><p class="title">QTY</p></th>
                    <th class="th"><p class="title">Harga</p></th>
                    <th class="th"><p class="title">Total</p></th>
                </tr>
            </thead>
            <tbody>
              <?php
              $t_jumlah = 0;
              $t_harga = 0;
              foreach($data['detail_pembelian'] as $row){
                $total_produk = $row->jumlah * $row->harga_produk;
                $t_jumlah = $t_jumlah + $row->jumlah;
                $t_harga = $t_harga + $total_produk;
               ?>

                <tr>
                    <td class="td"><p class="struk">{{$row->nama_produk}}</p></td>
                    <td class="td"><p class="struk-center">{{$row->jumlah}}</p></td>
                    <td class="td"><p class="struk-kanan">{{"Rp " . number_format($row->harga_produk ,2,',','.')}}</p></td>
                    <td class="td"><p class="struk-kanan">{{"Rp " . number_format($total_produk ,2,',','.')}}</p></td>
                </tr>
              <?php  }
               ?>
                <tr>
                    <td class="td"><p class="total">Total</p></td>
                    <td class="td"><p class="struk-center ">{{$t_jumlah}}</p></td>
                    <td class="td"></td>
                    <td class="td"><p class="struk-kanan">{{"Rp " . number_format($t_harga ,2,',','.')}}</p></td>
                </tr>
                
            </tbody>
        </table>
        <br>
        <table>
                <tr>
                    <td width="12%;" class="struk"><p class="struk">No Anggota</p></td>
                    <td  width="48%;" class="struk"><p class="struk">{{$data['no_anggota']}}</p></td>
                    <td  width="20%;" class="td2"><p class="struk-kanan">Sub Total</p></td>
                    <td class="td2" width="20%;"><p class="struk-kanan">{{"Rp " . number_format($t_harga ,2,',','.')}}</p></td>
                </tr>
                <tr>
                    <td  width="12%;" class="td2"><p class="struk">Nama Anggota</p></td>
                    <td  width="48%;" class="td2"><p class="struk">{{$data['nama_anggota']}}</p></td>
                    <td width="20%;" class="td2"><p class="struk-kanan">PPN</p></td>
                    <td class="td2" width="20%;"><p class="struk-kanan">{{"Rp " . number_format($data['ppn'] ,2,',','.')}}</p></td>
                </tr>
                <tr>
                    <td colspan="3" width="80%;" class="td2"><p class="struk-kanan">Ext Tambahan</p></td>
                    <td class="td2" width="20%;"><p class="struk-kanan">{{"Rp " . number_format($data['biaya_tambahan'] ,2,',','.')}}</p></td>
                </tr>
                <tr>
                    <td  width="12%;" class="td2"><p class="struk">Tanggal Trx</p></td>
                    <td  width="48%;" class="td2"><p class="struk">{{$data['created_at']}}</p></td>
                    <td  width="20%;" class="td2"><p class="struk-kanan">Potongan</p></td>
                    <td class="td2" width="20%;"><p class="struk-kanan">{{"Rp " . number_format(0 ,2,',','.')}}</p></td>
                </tr>
                <tr>
                  <?php $t_total = $t_harga + $data['ppn'] + $data['biaya_tambahan']; ?>
                    <td colspan="3" width="80%" class="td2"><p class="struk-kanan">Total</p></td>
                    <td class="td2" width="20%;"><p class="struk-kanan">{{"Rp " . number_format($t_total ,2,',','.')}}</p></td>
                </tr>
        </table>
        <br>
       
    </section>
</body>

</html>