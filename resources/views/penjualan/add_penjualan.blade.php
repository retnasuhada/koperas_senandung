 @extends('template')
 @section('content')
 @yield('content')
 <div class="row">

   <div class="col-md-4">
     <!-- general form elements -->


     @if ($data['error']=='1')
     <div class="alert alert-danger alert-dismissible">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
       <h5><i class="icon fas fa-check"></i> Error</h5>
       {{$data['message']}}
     </div>

     @endif
     @if($data['flag_edit']=='1')
     <div class="card card-warning">
       <div class="card-header">
         <h3 class="card-title">Edit Detail Penjualan</h3>
       </div>
       <form action="{{ route('edit_det_penjualan_act') }}" method="post">
         @else
         <div class="card card-success">
           <div class="card-header">
             <h3 class="card-title">Detail Penjualan</h3>
           </div>
           <form action="{{ route('add_det_penjualan') }}" method="post">
             @endif

             @csrf

             <div class="card-body">
               <div class="form-group">
                 <label for="exampleInputEmail1">Nama Produk</label>
                 <input type="hidden" name="kode_penjualan" value="{{$data['kode_penjualan']}}">
                 @if($data['flag_edit']=='1')
                 <input type="hidden" name="id_detail_penjualan" value="{{ $data['id_detail_penjualan'] }}">
                 <select class="form-control select2bs4" name="kode_produk">
                   @foreach($data['list_produk'] as $row)
                   <option value="{{$row->kode_produk}}" <?php if ($data['kode_produk'] == $row->kode_produk) {
                                                            echo "selected";
                                                          } ?>>{{$row->nama_produk}}</option>
                   @endforeach
                 </select>
                 @else
                 <select class="form-control select2bs4" name="kode_produk">
                   <option value=""></option>
                   @foreach($data['list_produk'] as $row)

                   <option value="{{$row->kode_produk}}">{{$row->nama_produk}}</option>
                   @endforeach
                 </select>
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Jumlah</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="jumlah" class="form-control" value="{{$data['jumlah']}}">
                 @else
                 <input type="texs" name="jumlah" class="form-control" placeholder="Masukan Nama Jabatan">
                 @endif
               </div>
             </div>
             <!-- /.card-body -->

             <div class="card-footer">
               @if($data['flag_edit']=='1')
               <button type="submit" class="btn btn-warning">Simpan</button>
               @else
               <button type="submit" class="btn btn-primary">Tambahkan</button>
               @endif

             </div>
           </form>
         </div>
     </div>
     <div class="col-md-8">

       <div class="card">
         <div class="card-header">
           <h3 class="card-title">List Detail Penjualan</h3>
         </div>
         <!-- /.card-header -->
         <div class="card-body">
           <table class="table table-bordered">
             <thead>
               <tr>
                 <th style="width: 10px">#</th>
                 <th>Nama Produk</th>
                 <th>Jumlah</th>
                 <th>Harga Anggota</th>
                 <th>Harga Umum</th>
                 <th style="width: 100px">Aksi</th>
               </tr>
             </thead>
             <tbody>
               <?php
                $no = 1;
                $total = 0;
                $total_harga_anggota = 0;
                $total_harga_umum = 0;

                foreach ($data['list_penjualan_det'] as $row) {
                  $min_grosir           = $row->minimum_grosir;
                  if ($row->jumlah < $min_grosir) {
                      $t_harga_anggota      = $row->jumlah * $row->harga_grosir_anggota;
                      $t_harga_umum         = $row->jumlah * $row->harga_grosir_umum;
                  }else{
                    $t_harga_anggota      = $row->jumlah * $row->harga_anggota;
                    $t_harga_umum         = $row->jumlah * $row->harga_umum;
                  }
                  $total                = $total + $row->jumlah;
                  $total_harga_anggota  = $total_harga_anggota + $t_harga_anggota;
                  $total_harga_umum     = $total_harga_umum + $t_harga_umum;
                ?>
                 <tr>
                   <td>{{$no}}</td>
                   <td>{{$row->nama_produk}}</td>
                   <td>{{$row->jumlah}}</td>
                   <td> {{"Rp " . number_format($t_harga_anggota ,2,',','.')}}</td>
                   <td> {{"Rp " . number_format($t_harga_umum  ,2,',','.')}}</td>
                   <input type="hidden" name="t_harga_anggota" id="t_harga_anggota" value="{{$t_harga_anggota}}">
                   <input type="hidden" name="t_harga_umum" id="t_harga_umum" value="{{$t_harga_umum}}">
                   <td>
                     <a href="{{ url('edit_detail_penjualan/'.$row->id_detail_penjualan) }}"><i class="fas fa-edit"></i></a>
                     &nbsp;&nbsp;
                     <a href="{{ url('hapus_detail_penjualan/'.$row->id_detail_penjualan) }}"><i class="fas fa-trash"></i></a>

                   </td>
                 </tr>

               <?php $no++;
                }
                ?>
               <tr>
                 <td colspan="2" align="center"><b>TOTAL</b></td>

                 <td><b>{{$total}}</b></td>
                 <td><b>{{"Rp " . number_format($total_harga_anggota ,2,',','.')}}</b></td>
                 <td><b>{{"Rp " . number_format($total_harga_umum  ,2,',','.')}}</b></td>

                 <td>

                 </td>
               </tr>


             </tbody>
           </table>
         </div>
         <!-- /.card-body -->

       </div>
     </div>
   </div>




   <div class="row">

     <div class="col-md-12">
       <!-- general form elements -->
       <div class="card card-success">
         <div class="card-header">
           <h3 class="card-title">Penjualan</h3>
         </div>

         <form action="{{ route('save_penjualan_act') }}" method="post">

           @csrf

           <div class="card-body">

             <div class="form-group">
               <label for="exampleInputEmail1">Kode Penjualan</label>
               <input type="text" name="kode_penjualan" class="form-control" value="{{ $data['kode_penjualan'] }}">

             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Tanggal Penjualan</label>
               <div class="input-group date" id="reservationdate" data-target-input="nearest">

                 <input type="text" name="tgl_penjualan" class="form-control datetimepicker-input" data-target="#reservationdate">
                 <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                   <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                 </div>

               </div>
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Nama Anggota</label>

               <select class="form-control select2bs4" id="no_anggota" name="no_anggota">
                 <option value=""></option>
                 @foreach($data['list_anggota'] as $row)

                 <option value="{{$row->no_anggota}}">{{$row->nama_anggota}}</option>
                 @endforeach
               </select>

             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Kode Suplier</label>

               <select class="form-control select2bs4" id="kode_suplier" name="kode_suplier">
                 <option value=""></option>
                 @foreach($data['list_suplier'] as $row_sp)
                 <option value="{{$row_sp->kode_suplier}}">{{$row_sp->nama_suplier}}</option>
                 @endforeach
               </select>
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">PPN</label>
               <input type="text" onkeyup="hitung()" id="persen" name="ppn" class="persen form-control">
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Biaya Lain-lain</label>
               <input type="text" onkeyup="hitung()" name="biaya_tambahan" id="rupiah" class="rupiah form-control" value="0">
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Total</label>
               <input type="hidden" name="row-total">
               <input type="text" name="total" id="rupiah3" class="rupiah3 form-control">
             </div>
             <!-- <div class="form-group">
               <label for="exampleInputEmail1">Dibayar</label>
               <input type="hidden" name="row-total">
               <input type="text" name="total" id="rupiah4" onkeyup="dibayar()" class="rupiah3 form-control">
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Kembalian</label>
               <input type="hidden" name="row-total">
               <input type="text" name="total" id="kembalian" onkeyup="dibayar()" class="rupiah3 form-control">
             </div> -->
           </div>
           <!-- /.card-body -->

           <div class="card-footer">
             <button type="submit" class="btn btn-primary">Simpan</button>
           </div>
         </form>
       </div>
     </div>

   </div>

   <script type="text/javascript">
     var rupiah = document.getElementById('rupiah');
     rupiah.addEventListener('keyup', function(e) {

       rupiah.value = formatRupiah(this.value, 'Rp. ');
     });
     var rupiah4 = document.getElementById('rupiah4');
     rupiah4.addEventListener('keyup', function(e) {

       rupiah4.value = formatRupiah(this.value, 'Rp. ');
     });

     var rupiah3 = document.getElementById('rupiah3');
     rupiah3.addEventListener('keyup', function(e) {

       rupiah3.value = formatRupiah(this.value, '');
     });

     /* Fungsi formatRupiah */
     function formatRupiah(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 3,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{3}/gi);

       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
     }
   </script>

   <script type="text/javascript">
     var persen = document.getElementById('persen');
     persen.addEventListener('keyup', function(e) {

       persen.value = formatPersen(this.value, '%. ');
     });
     /* Fungsi formatRupiah */
     function formatPersen(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 2,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{2}/gi);
       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '' : '';
         rupiah += separator + ribuan.join(',');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
     }
   </script>

   <script type="text/javascript">
     function hitung() {
       var x = document.getElementById("no_anggota");
       var total_belanja = 0;
       var ppn = parseInt($("#persen").val());
       var add = parseInt($("#rupiah").val());
       var total = 0;
       if (x.value != '110') {
         total_belanja = parseInt($("#t_harga_anggota").val());
       } else {
         total_belanja = parseInt($("#t_harga_umum").val());
       }
       total_a = (total_belanja * ppn / 100) + total_belanja + add;
       // total = total_a + add;
       $("#rupiah3").val(total_a);

       // alert(total_belanja);
     }

     function dibayar() {

       var total = $("#rupiah3").val();
       total = Number(total.replace(/[^0-9.-]+/g, ""));
       total = parseFloat(total);
       var dibayar = $("#rupiah4").val();
       number = parseInt(dibayar.replace(/[^0-9.-]+/g, ""));
       var kembalian = 0;

       kembalian = dibayar - total;
       // total = total_a + add;
       $("#kembalian").val(number);

       // alert(total_belanja);
     }
   </script>


   @endsection