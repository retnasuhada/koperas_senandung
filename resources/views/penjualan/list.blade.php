 @extends('template')
 @section('content')
 @yield('content')
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif


 <div class="card">
   <div class="card-header">
     <a href="{{route('add_penjualan')}}" class="btn btn-primary">
       <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Penjualan
     </a>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
     <table id="example1" class="table table-bordered table-striped">
       <thead>

         <tr>
           <th>No</th>
           <th>Tanggal</th>
           <th>No Anggota</th>

           <th>Total Harga (+ppn +lain2)</th>

           <th width="50px;">Aksi</th>
         </tr>
       </thead>
       <tbody>
         <?php
          $no = 1;
          foreach ($data['list_penjualan'] as $row) {
            $total = $row->total + $row->biaya_tambahan;
          ?>
           <tr>
             <td>{{$no}}</td>
             <td>{{$row->tanggal_penjualan}}</td>
             <td>{{$row->nama_anggota}}</td>
             <td>{{"Rp " . number_format($row->total ,2,',','.')}}</td>

             <td>
               <a title="Infor Lebih Detail" href="{{ url('info_penjualan/'.$row->kode_penjualan) }}"><i class="fa fa-info"></i></a>
               &nbsp;&nbsp;
               <a title="Cetak Struk Penjualan" target="_blank" href="{{ url('cetak_struk_penjualan/'.$row->kode_penjualan) }}"><i class="fa fa-print"></i></a>

             </td>
           </tr>

         <?php $no++;
          } ?>

         </tfoot>
     </table>
   </div>
   <!-- /.card-body -->
 </div>
 @endsection