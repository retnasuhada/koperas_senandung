 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 <div class="card card-success">
   <div class="card-header">
     <h3 class="card-title">Detail Simpanan {{$data['jenis_simpanan']}}</h3>
     <div class="card-tools">
       <button type="button" class="btn btn-tool" data-card-widget="collapse">
         <i class="fas fa-minus"></i>
       </button>
       <button type="button" class="btn btn-tool" data-card-widget="remove">
         <i class="fas fa-times"></i>
       </button>
     </div>
   </div>
   <!-- /.card-header -->

   <div class="card-body">
     <div class="row">
       <div class="col-6">
         <div class="table-responsive">
           <table class="table">
             <tbody>
               <tr>
                 <th style="width:50%">No Anggota:</th>
                 <td>{{ $data['no_anggota'] }}</td>
               </tr>
               <tr>
                 <th>Nama Anggota</th>
                 <td>{{ $data['nama_anggota'] }}</td>
               </tr>
               <tr>
                 <th>Jenis Simpanan</th>
                 <td>{{ $data['nama_jenis_simpanan'] }}</td>
               </tr>
               <tr>
                 <th>Periode Simpanan</th>
                 <td>{{ $data['periode_simpanan'] }}</td>
               </tr>
             </tbody>
           </table>
         </div>
       </div>
       <div class="col-6">
         <div class="table-responsive">
           <table class="table">
             <tbody>
               <tr>
                 <th style="width:50%">Tanggal Simpanan</th>
                 <td>{{$data['tgl_simpanan']}}</td>
               </tr>
               <tr>
                 <th>Jumlah Simpanan</th>
                 <td>{{$data['jumlah_simpanan']}}</td>
               </tr>
               <tr>
                 <th>Jenis Penerimaan</th>
                 <td>{{$data['nama_jenis_penerimaan']}}</td>
               </tr>

             </tbody>
           </table>
         </div>
       </div>
       <div class="footer">
         <?php if ($data['jenis_simpanan'] == 'Pokok') { ?>
           <a href="{{route('simpanan_pokok_list')}}" class="btn btn-primary">Kembali</a>

         <?php }
          if ($data['jenis_simpanan'] == 'Wajib') { ?>
           <a href="{{route('simpanan_wajib_list')}}" class="btn btn-primary">Kembali</a>
         <?php }
          if ($data['jenis_simpanan'] == 'Sukarela') { ?>
           <a href="{{route('simpanan_pokok_list')}}" class="btn btn-primary">Kembali</a>
         <?php } ?>

       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
   </div>
   <!-- /.card-body -->

 </div>
 <script type="text/javascript">
   var rupiah = document.getElementById('rupiah');
   rupiah.addEventListener('keyup', function(e) {

     rupiah.value = formatRupiah(this.value, 'Rp. ');
   });

   var rupiah2 = document.getElementById('rupiah2');
   rupiah2.addEventListener('keyup', function(e) {

     rupiah2.value = formatRupiah(this.value, 'Rp. ');
   });

   /* Fungsi formatRupiah */
   function formatRupiah(angka, prefix) {
     var number_string = angka.replace(/[^,\d]/g, '').toString(),
       split = number_string.split(','),
       sisa = split[0].length % 3,
       rupiah = split[0].substr(0, sisa),
       ribuan = split[0].substr(sisa).match(/\d{3}/gi);

     // tambahkan titik jika yang di input sudah menjadi angka ribuan
     if (ribuan) {
       separator = sisa ? '.' : '';
       rupiah += separator + ribuan.join('.');
     }

     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
     return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
   }
 </script>
 @endsection