 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   <div class="card-header">
     <h3 class="card-title">Form Edit Modal Anggota</h3>
     @else
     <div class="card card-success">
       <div class="card-header">
         <h3 class="card-title">Form Tambah Modal Anggota</h3>
         @endif


         <div class="card-tools">
           <button type="button" class="btn btn-tool" data-card-widget="collapse">
             <i class="fas fa-minus"></i>
           </button>
           <button type="button" class="btn btn-tool" data-card-widget="remove">
             <i class="fas fa-times"></i>
           </button>
         </div>
       </div>
       <!-- /.card-header -->
       @if($data['flag_edit']=='1')
       <form action="{{ route('edit_modal_anggota_act') }}" method="post" enctype="multipart/form-data">
         <input type="hidden" name="id_simpanan" value="{{$data['id_simpanan']}}">
         <input type="hidden" name="unik_kode" value="{{$data['unik_kode']}}">
         @else
         <form action="{{ route('add_modal_act') }}" method="post" enctype="multipart/form-data">
           @endif

           @csrf
           <div class="card-body">
             <div class="row">
               <div class="col-md-6">
                 <div class="form-group">
                   <label for="exampleInputEmail1">Nama Anggota</label>
                   <select class="form-control select2" name="id_anggota" style="width: 100%;">
                     @if($data['flag_edit']=='1')
                     @foreach($data['list_anggota'] as $row)
                     <option value="{{$row->id_anggota}}" <?php if ($data['id_anggota'] == $row->id_anggota) {
                                                            echo "selected";
                                                          } ?>>{{$row->nama_anggota}}</option>
                     @endforeach
                     @else
                     @foreach($data['list_anggota'] as $row)
                     <option value="{{$row->id_anggota}}">{{$row->nama_anggota}}</option>
                     @endforeach
                     @endif

                   </select>
                 </div>
                 <div class="form-group">
                   <label for="exampleInputEmail1">Tanggal Simpanan</label>
                   <div class="input-group date" id="reservationdate" data-target-input="nearest">

                     <input type="text" name="tgl_simpanan" class="form-control datetimepicker-input" data-target="#reservationdate">
                     <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                       <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                     </div>

                   </div>
                 </div>
                 <div class="form-group">
                   <label for="exampleInputEmail1">Jumlah Simpanan Modal</label>
                   @if($data['flag_edit']=='1')

                   <input type="text" id="rupiah" name="jumlah_simpanan" class="rupiah form-control" value="{{ $data['jumlah_simpanan'] }}">
                   @else
                   <input type="text" id="rupiah" name="jumlah_simpanan" class="rupiah form-control">
                   @endif
                 </div>
                 <div class="form-group">
                   <label>Jenis Penerimaan</label>
                   <select class="custom-select" name="jenis_penerimaan" style="width: 100%;">
                     @if($data['flag_edit'])
                     @foreach($data['list_akun'] as $row_akun)
                     <option value="{{$row_akun->kode_akun}}" <?php if ($data['kode_akun'] == $row_akun->kode_akun) {
                                                                echo "selected";
                                                              } ?>>{{$row_akun->nama_akun}}</option>
                     @endforeach
                     @else
                     <option value=""></option>
                     @foreach($data['list_akun'] as $row_akun)
                     <option value="{{$row_akun->kode_akun}}">{{$row_akun->nama_akun}}</option>
                     @endforeach
                     @endif
                   </select>
                 </div>
               </div>
             </div>
             <!-- /.col -->
           </div>
           <!-- /.row -->
     </div>
     <!-- /.card-body -->
     <div class="card-footer">
       <button type="submit" class="btn btn-success">Simpan</button>
       <button type="reset" class="btn btn-warning">Cancel</button>
     </div>
     </form>
   </div>
   <script type="text/javascript">
     var rupiah = document.getElementById('rupiah');
     rupiah.addEventListener('keyup', function(e) {

       rupiah.value = formatRupiah(this.value, 'Rp. ');
     });

     var rupiah2 = document.getElementById('rupiah2');
     rupiah2.addEventListener('keyup', function(e) {

       rupiah2.value = formatRupiah(this.value, 'Rp. ');
     });

     /* Fungsi formatRupiah */
     function formatRupiah(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 3,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{3}/gi);

       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
     }
   </script>
   @endsection