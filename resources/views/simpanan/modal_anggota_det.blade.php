 @extends('template')
 @section('content')
 @yield('content')
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif

 <div class="card">
   <div class="card-header">
     <a href="{{route('add_modal_anggota')}}" class="btn btn-primary">
       <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Modal Anggota
     </a>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
     <table id="example1" class="table table-bordered table-striped">
       <thead>
         <tr>
           <th>No</th>
           <th>Tanggal</th>
           <th>Nama Anggota</th>
           <th>Nama Akun</th>
           <th>Periode Simpanan</th>
           <th>Jumlah Simpanan</th>
           <th width="50px;">Aksi</th>
         </tr>
       </thead>
       <tbody>

         <?php
          $no = 1;
          foreach ($data['simpanan'] as $row) {
          ?>
           <tr>
             <td>{{$no}}</td>
             <td>{{$row->tgl_simpanan}}</td>
             <td>{{$row->nama_anggota}}</td>
             <td>{{$row->nama_akun}}</td>
             <td>{{$row->tgl_simpanan}}</td>
             <td>{{"Rp " . number_format($row->jumlah_simpanan ,2,',','.')}}</td>
             <td>
               <a href="{{ url('edit_modal_anggota/'.$row->id_simpanan) }}"><i class="fas fa-edit"></i></a>
               &nbsp;&nbsp;
               <a href="{{ url('detail_simpanan_pokok/'.$row->id_simpanan) }}"><i class="fas fa-eye"></i></a>

             </td>
           </tr>
         <?php }
          ?>
         </tfoot>
     </table>
   </div>
   <!-- /.card-body -->
 </div>
 @endsection