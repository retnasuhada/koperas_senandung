 @extends('template')
 @section('content')
 @yield('content')
 <div class="row">

   <div class="col-md-6">
     <!-- general form elements -->
     <div class="card card-primary">
       <div class="card-header">
         <h3 class="card-title">Tambah Jenis Simpanan</h3>
       </div>
       <!-- /.card-header -->
       <!-- form start -->
       <form action="{{ route('add_jenis_simpanan') }}" method="post">
         @csrf

         <div class="card-body">
           <div class="form-group">
             <label for="exampleInputEmail1">Jenis Simpanan</label>
             @if($data['flag_edit']=='1')
             <input type="texs" name="nama_simpanan" class="form-control" value="{{$data['nama_jenis_simpanan']}}">
             @else
             <input type="texs" name="nama_simpanan" class="form-control" placeholder="Masukan Nama Jenis Simpanan">
             @endif
           </div>
           <div class="form-group">
             <label for="exampleInputPassword1">Status</label>
             <select class="form-control" name="status">
               <option value="1" <?php if ($data['status'] == '1') {
                                    echo 'selected';
                                  } ?>>Aktif</option>
               <option value="0" <?php if ($data['status'] == '0') {
                                    echo 'selected';
                                  } ?>>Tidak Aktif</option>
             </select>
           </div>

         </div>
         <!-- /.card-body -->

         <div class="card-footer">
           <button type="submit" class="btn btn-primary">Simpan</button>
         </div>
       </form>
     </div>
   </div>
   <div class="col-md-6">

     <div class="card">
       <div class="card-header">
         <h3 class="card-title">List Jenis Simpanan</h3>
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table class="table table-bordered">
           <thead>
             <tr>
               <th style="width: 10px">#</th>
               <th>Nama Jenis Simpanan</th>
               <th>Status</th>
               <th style="width: 100px">Aksi</th>
             </tr>
           </thead>
           <tbody>
             <?php
              $no = 1;
              foreach ($data['jenis_simpanan'] as $row) { ?>
               <tr>
                 <td>{{$no}}</td>
                 <td>{{$row->nama_jenis_simpanan}}</td>
                 <td>{{$row->status}}</td>
                 <td>
                   <a href="{{ url('edit_jenis_simpanan/'.$row->id_jenis_simpanan) }}"><i class="fas fa-edit"></i></a>
                   &nbsp;&nbsp;
                   <a href="#"><i class="fas fa-trash"></i></a>

                 </td>
               </tr>

             <?php $no++;
              } ?>



           </tbody>
         </table>
       </div>
       <!-- /.card-body -->

     </div>
   </div>
 </div>
 @endsection