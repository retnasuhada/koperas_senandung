@extends('template')
@section('content')
@yield('content')



<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <!-- Profile Image -->
        <div class="card card-primary card-outline">
          <div class="card-body box-profile">
            <div class="text-center">
              <img class="profile-user-img img-fluid img-circle" src="{{ asset('foto/'.$data['foto'])}}" alt="User profile picture">

            </div>

            <h3 class="profile-username text-center">{{$data['nama_anggota']}}</h3>

            <p class="text-muted text-center">{{$data['jabatan']}}</p>

            <strong><i class="fas fa-book mr-1"></i> Phone</strong>

            <p class="text-muted">
              {{$data['no_hp']}}
            </p>

            <hr>

            <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

            <p class="text-muted">{{$data['alamat']}}</p>

            <hr>

            <a href="{{ url('cetak_kartu/'.$data['no_anggota']) }}" class="btn btn-primary btn-block"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;&nbsp;<b>Cetak Kartu</b></a>
            <a href="{{url('edit_anggota/'.$data['id_anggota']) }}" class="btn btn-danger btn-block"><i class="fas fa-edit"></i>&nbsp;&nbsp;<b>Edit Data</b></a></a>
            <a href="{{url('edit_user_agt/'.$data['id_anggota']) }}" class="btn btn-success btn-block"><i class="fas fa-edit"></i>&nbsp;&nbsp;<b>Edit User</b></a></a>
          </div>


          <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <!-- About Me Box -->

      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Bio Data</a></li>
              <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Simpanan</a></li>
              <li class="nav-item"><a class="nav-link" href="#transaksi" data-toggle="tab">Pembelanjaan</a></li>
              <li class="nav-item"><a class="nav-link" href="#pinjaman" data-toggle="tab">Pinjaman</a></li>
              <li class="nav-item"><a class="nav-link" href="#shu" data-toggle="tab">SHU</a></li>

            </ul>
          </div><!-- /.card-header -->
          <div class="card-body">
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="post">
                  <div class="row invoice-info">
                    <div class="col-sm-12 invoice-col">
                      <table>
                        <tr>
                          <td width="150px;"><b>Nama Anggota</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{$data['nama_anggota']}}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>ID Anggota</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{$data['no_anggota']}}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Tanggal Bergabung</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{$data['join_date']}}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>NIK</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{$data['nik']}}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Alamat</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{$data['alamat']}}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Jenis Kelamin</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{$data['jenis_kelamin']}}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Status Anggota</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{$data['status']}}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>NPWP</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{$data['npwp']}}</td>
                        </tr>
                      </table>

                    </div>
                    <!-- /.col -->

                    <!-- /.col -->
                  </div>
                </div>
                <!-- /.post -->


              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <th style="width:50%"> Simpanan Pokok</th>
                        <td>{{"Rp " . number_format($data['n_pokok'] ,2,',','.')}}</td>
                      </tr>
                      <tr>
                        <th>Simpanan Wajib</th>
                        <td>{{"Rp " . number_format($data['n_wajib'] ,2,',','.')}}</td>
                      </tr>
                      <tr>
                        <th>Simpanan Sukarela</th>
                        <td>{{"Rp " . number_format($data['n_sukarela'] ,2,',','.')}}</td>
                      </tr>
                      <tr>
                        <th>Modal Anggota</th>
                        <td>{{"Rp " . number_format($data['n_modal'] ,2,',','.')}}</td>
                      </tr>
                      <tr>
                        <th>Total</th>
                        <td>{{"Rp " . number_format($data['n_total'] ,2,',','.')}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="tab-pane" id="transaksi">
                <!-- The transaksi -->
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <td><b> Bulan</b></td>
                        <td><b>Total Transaksi</b></td>
                      </tr>
                      <?php
                        $total = 0;
                        foreach($data['pembelanjaan'] as $row_pembelanjaan){
                          $total = $total + $row_pembelanjaan->total;
                       ?>
                       <tr>
                        <td>{{ getBulan($row_pembelanjaan->bulan)}}</td>
                        <td>{{"Rp " . number_format($row_pembelanjaan->total ,2,',','.')}}</td>
                      </tr>
                    <?php } ?>
                      <tr>
                        <td><b> Total </b></td>
                        <td><b>{{"Rp " . number_format($total ,2,',','.')}}</b></td>
                      </tr>

                     
                    </tbody>
                  </table>
                </div>
              </div>




              <div class="tab-pane" id="pinjaman">
                <!-- The transaksi -->
                <div class="table-responsive">
                  <b>On Progress Development</b>

                </div>
              </div>



              <div class="tab-pane" id="shu">
                <!-- The transaksi -->
                <div class="table-responsive">
                  <b>On Progress Development</b>

                </div>
              </div>
              <!-- /.tab-pane -->


              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

@endsection