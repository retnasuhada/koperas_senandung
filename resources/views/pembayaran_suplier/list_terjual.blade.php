 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif


   <div class="card card-success">
  
     <div class="card card-primary">
       <div class="card-header">
        
          Penjualan Suplier a/n {{ $data['nama_suplier']}}
      
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table id="example1" class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>#</th>
               <th>Tanggal Penjualan</th>
               <th>Nama Produk</th>
               <th>Harga Suplier</th>
               <th>Jumlah</th>
               <th>Total</th>
               <th>Opsi</th>

             </tr>
           </thead>
           <tbody>
           @php
           $no=0;
           @endphp
             @foreach($data['data_penjualan'] as $row) 
          @php
           $no++;
           @endphp
              <tr>
                 <td>{{$no}}</td>
                 <td>{{$row->tanggal_penjualan}}</td>
                 <td>{{$row->nama_produk}}</td>
                 <td>{{"Rp " . number_format($row->harga_suplier ,2,',','.')}}</td>
                 <td>{{$row->jumlah}}</td>
                 <td>{{"Rp " . number_format($row->total ,2,',','.')}}</td>
                 <td>
                   <a title="edit" href="{{ url('add_prd_bayar/'.$row->id_tr_det_penjualan) }}" class="btn btn-primary">Add Pay</a>
                  
                 

                 </td>
               </tr>
           @endforeach
           
             </tfoot>
         </table>
       </div>

       <!-- /.card-body -->
     </div>
 </div>
     <div class="card card-success">
  
     <div class="card card-success">
       <div class="card-header">
        
         LIST PEMBAYARAN
      
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table id="example1" class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>#</th>
              
               <th>Nama Produk</th>
               <th>Harga</th>
               <th>Jumlah</th>
               <th>Total</th>
               <th>Opsi</th>

             </tr>
           </thead>
           <tbody>
        
          @foreach($data['data_pembayaran'] as $res)
                <tr>
                   <td></td>
                   <td>{{$res->nama_produk}}</td>
                   
                   <td>{{$res->harga}}</td>
                   <td>{{$res->jumlah}}</td>
                   <td>{{$res->total}}</td>
                   <td>
                    <a href="{{ url('edit_pembelian/'.$res->id_detail_pembayaran_suplier) }}"><i class="fas fa-trash"></i></a>
              
                   </td>
                 </tr>
          @endforeach
        
             </tfoot>
         </table>
      
       <!-- /.card-body -->
     </div>
     <a href="{{ url('act_bayar_suplier/'.$data['KodePembayaran'].'/'.$data['kode_suplier']) }}" class="btn btn-primary">Bayar</a>
     @endsection