 @extends('template')
 @section('content')
 @yield('content')
 <div class="row">

   <div class="col-md-4">
     <!-- general form elements -->


     
         <div class="card card-success">
           <div class="card-header">
             <h3 class="card-title">Pembayaran Suplier</h3>
           </div>
           <form action="{{ route('cek_penjualan') }}" method="post">
             @csrf
             <div class="card-body">
               <div class="form-group">
                 <label for="exampleInputEmail1">Nama Suplier</label>
                 <select class="form-control select2bs4" name="kode_suplier">
                   <option value=""></option>
                   @foreach($data['list_suplier'] as $row)
                   <option value="{{$row->kode_suplier}}">{{$row->nama_suplier}}</option>
                   @endforeach
                 </select>
                
               </div>
             </div>

             <div class="card-footer">
               <button type="submit" class="btn btn-primary">Lanjut</button>
               <button type="clear" class="btn btn-warning">Batal</button>
             </div>
           </form>
         </div>
     </div>


   <script type="text/javascript">
    function option() {
        $(".result").html(null);
       var n = $("#jenis_invoice").val();
       if(n==1){
          var html = "<label for='exampleInputEmail1'>No Invoice</label>";
            
            html += "<input type='text' id='invoice' name='no_invoice' class='form-control' value='{{KodeInvoice()}}' >";
            
       }else{
          var html = "<label for='exampleInputEmail1'>No Invoice</label>";
            html += "@if($data['flag_edit']=='1')";
            html += "<input type='text' name='no_invoice' class='form-control' value='{{$data['jumlah']}}'>";
            html += "@else";
            html += "<input type='text' id='invoice' required=' name='no_invoice' class='form-control' placeholder='Masukan Nomor Invoice'>";
            html += "@endif";
       }
       
        $(".result").append(html);

      
     }

     var rupiah = document.getElementById('rupiah');
     rupiah.addEventListener('keyup', function(e) {

       rupiah.value = formatRupiah(this.value, 'Rp. ');
     });

     var rupiah2 = document.getElementById('rupiah2');
     rupiah2.addEventListener('keyup', function(e) {

       rupiah2.value = formatRupiah(this.value, 'Rp. ');
     });
     var rupiah3 = document.getElementById('rupiah3');
     rupiah3.addEventListener('keyup', function(e) {

       rupiah3.value = formatRupiah(this.value, 'Rp. ');
     });

     /* Fungsi formatRupiah */
     function formatRupiah(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 3,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{3}/gi);

       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
     }

     

   </script>


   @endsection