 @extends('template')
 @section('content')
 @yield('content')
 <div class="card">
   <div class="row">
     <div class="col-md-6">

       <table style="margin: 0; padding: 0;">

         <tr>
           <td colspan="3">Pembayaran Koperasi Ke Suplier</td>
           
         </tr>
         <tr>
           <td width="200px;">No Pembayaran</td>
           <td>:</td>
           <td>{{$data['no_pembayaran']}}</td>
         </tr>
         <tr>
           <td width="200px;">Tanggal Pembayaran</td>
           <td>:</td>
           <td>{{$data['tgl_pembayaran']}}</td>
         </tr>
        
         <tr>
           <td width="200px;">Nama Suplier</td>
           <td>:</td>
           <td>{{$data['nama_suplier']}}</td>
         </tr>

       </table>
     </div>
   </div>
 </div>
 <div class="row">


   <div class="col-md-12">

     <div class="card">

       <div class="card-header">
         <h3 class="card-title"></h3>
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table class="table table-bordered">
           <thead>
             <tr>
               <th style="width: 10px">#</th>
               <th>No Pembelian</th>
               <th>Kode Produk</th>
               <th>Nama Produk</th>
               <th>Harga</th>
               <th>Jumlah</th>
               <th>Total</th>
               

             </tr>
           </thead>
           <tbody>
        <?php
        $no=0;
        $jml = 0;
        $sub_total = 0;
         foreach($data['list_pembayaran_suplier'] as $res) { 
          $no++;
          $jml += $jml + $res->jumlah;
          $sub_total += $sub_total + $res->total;
          ?>
           <tr>
                 <td>{{$no}}</td>
                 <td>{{$res->kode_pembelian}}</td>
                 <td>{{$res->kode_produk}}</td>
                 <td>{{$res->nama_produk}}</td>
                 <td>{{"Rp " . number_format($res->harga ,2,',','.')}} </td>
                 <td>{{$res->jumlah}}</td>
                 <td>{{"Rp " . number_format($res->total ,2,',','.')}}</td>
                
               
               </tr>
       <?php } ?>
          <tr>
               <td colspan="6" align="center"><b>TOTAL</b></td>

               
               
               <td><b>{{"Rp " . number_format($data['sub_total'] ,2,',','.')}}</b></td>
               
               
             

             </tr> 


           </tbody>
         </table>
         <br>

       </div>
       <!-- /.card-body -->

     </div>
   </div>

 </div>

 <div class="card">
   <div class="row">
     <!-- <div class="card"> -->
     <div class="col-md-6">

       <img src="{{ asset('Admin/dist/img/credit/bca.png')}}" alt="Visa">
       <img src="{{ asset('Admin/dist/img/credit/bni.jpg')}}" alt="Mastercard">
       <img src="{{ asset('Admin/dist/img/credit/bri.png')}}" alt="American Express">
       <img src="{{ asset('Admin/dist/img/credit/mandiri.png')}}" alt="Paypal">

       <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
         Dapatkan Nomor Rekening dan tunjukan bukti pembayaran kepada kasir (admin) toko Koperasi Senandung 165
       </p>
     </div>
     <div class="col-md-6">
       <div class="table-responsive">
         <table>
           <tr>
             <th width="200px">Subtotal </th width="200px">
             <td><b>{{"Rp " . number_format($data['sub_total'] ,2,',','.')}}</b></td>
           </tr>
           <tr>
             <th width="200px">Biaya Tambahan</th width="200px">
             <td><b></b></td>
           </tr>
           <tr>
             <th width="200px">Diskon</th width="200px">
             <td><b></b></td>
           </tr>

           <tr>
             <th width="200px">Total:</th width="200px">
             
             <td><b>{{"Rp " . number_format($data['sub_total'] ,2,',','.')}}</b></td>
           </tr>
           <tr>
             <th width="200px">Keterangan:</th width="200px">
             
             <td><b>Sudah Dibayar</b></td>
           </tr>
          
         </table>
       </div>
     </div>
   </div>
 </div>




 <script type="text/javascript">
   var rupiah = document.getElementById('rupiah');
   rupiah.addEventListener('keyup', function(e) {

     rupiah.value = formatRupiah(this.value, 'Rp. ');
   });
   var rupiah4 = document.getElementById('rupiah4');
   rupiah4.addEventListener('keyup', function(e) {

     rupiah4.value = formatRupiah(this.value, 'Rp. ');
   });

   var rupiah3 = document.getElementById('rupiah3');
   rupiah3.addEventListener('keyup', function(e) {

     rupiah3.value = formatRupiah(this.value, '');
   });

   /* Fungsi formatRupiah */
   function formatRupiah(angka, prefix) {
     var number_string = angka.replace(/[^,\d]/g, '').toString(),
       split = number_string.split(','),
       sisa = split[0].length % 3,
       rupiah = split[0].substr(0, sisa),
       ribuan = split[0].substr(sisa).match(/\d{3}/gi);

     // tambahkan titik jika yang di input sudah menjadi angka ribuan
     if (ribuan) {
       separator = sisa ? '.' : '';
       rupiah += separator + ribuan.join('.');
     }

     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
     return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
   }
 </script>

 <script type="text/javascript">
   var persen = document.getElementById('persen');
   persen.addEventListener('keyup', function(e) {

     persen.value = formatPersen(this.value, '%. ');
   });
   /* Fungsi formatRupiah */
   function formatPersen(angka, prefix) {
     var number_string = angka.replace(/[^,\d]/g, '').toString(),
       split = number_string.split(','),
       sisa = split[0].length % 2,
       rupiah = split[0].substr(0, sisa),
       ribuan = split[0].substr(sisa).match(/\d{2}/gi);
     // tambahkan titik jika yang di input sudah menjadi angka ribuan
     if (ribuan) {
       separator = sisa ? '' : '';
       rupiah += separator + ribuan.join(',');
     }

     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
     return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
   }
 </script>

 <script type="text/javascript">
   function hitung() {
     var x = document.getElementById("no_anggota");
     var total_belanja = 0;
     var ppn = parseInt($("#persen").val());
     var add = parseInt($("#rupiah").val());
     var total = 0;
     if (x.value != '110') {
       total_belanja = parseInt($("#t_harga_anggota").val());
     } else {
       total_belanja = parseInt($("#t_harga_umum").val());
     }
     total_a = (total_belanja * ppn / 100) + total_belanja + add;
     // total = total_a + add;
     $("#rupiah3").val(total_a);

     // alert(total_belanja);
   }

   function dibayar() {

     var total = $("#rupiah3").val();
     total = Number(total.replace(/[^0-9.-]+/g, ""));
     total = parseFloat(total);
     var dibayar = $("#rupiah4").val();
     number = parseInt(dibayar.replace(/[^0-9.-]+/g, ""));
     var kembalian = 0;

     kembalian = dibayar - total;
     // total = total_a + add;
     $("#kembalian").val(number);

     // alert(total_belanja);
   }
 </script>


 @endsection