@extends('ecomm/template')
@section('content')
@yield('content')

<div class="well well-small">
	<h3>New Products </h3>
	<hr class="soften"/>
		<div class="row-fluid">
		<div id="newProductCar" class="carousel slide">
            <div class="carousel-inner">
			<div class="item active">
			  <ul class="thumbnails">
			  	<?php
			  	$Count = 0;
				foreach($data['new_produk'] as $Key => $Value){ ?>
				<li class="span3">
				<div class="thumbnail">
					<a class="zoomTool" href="{{ url('e_ecom_detail/'.$Value->kode_produk) }}" title="add to cart"><span class="icon-search"></span> QUICK VIEW</a>
					<a href="#" class="tag"></a>
					<a href="product_details.html"><img src="{{ asset('temp/'.$Value->foto)}}" alt="bootstrap-ring"></a>
				</div>
				</li>
				
				   
				<?php    $Count++;
				    if ($Count == 4){
				        break; //stop foreach loop after 4th loop
				    }
				}
				?>	
			  </ul>
			</div>
			@if($data['count_new_produk'] > 4)
			<div class="item">
			  <ul class="thumbnails">
			  	<?php
			  	$arr = $data['new_produk'];
			  	for($i=3; $i < 5; $i++){
			  		$foto = $arr[$i]->foto;
			  	
			  	?>

				<li class="span3">
				<div class="thumbnail">
					<a class="zoomTool" href="{{ url('e_ecom_detail/'.$arr[$i]->kode_produk) }}" title="add to cart"><span class="icon-search"></span> QUICK VIEW</a>
					<a href="#" class="tag"></a>
					<a href="{{ url('e_ecom_detail/'.$arr[$i]->kode_produk) }}"><img src="{{ asset('temp/'.$foto)}}" alt="bootstrap-ring"></a>
				</div>
				</li>
				<?php  }
				?>	
			  </ul>
			</div>
			@endif
		   
		   </div>
		  <a class="left carousel-control" href="#newProductCar" data-slide="prev">&lsaquo;</a>
            <a class="right carousel-control" href="#newProductCar" data-slide="next">&rsaquo;</a>
		  </div>
		  </div>
		
	</div>
	<div class="well well-small">
		  <h3><a class="btn btn-mini pull-right" href="products.html" title="View more">VIew More<span class="icon-plus"></span></a> Semua Produk  </h3>
		  <hr class="soften"/>
		  <div class="row-fluid">
		  <ul class="thumbnails">
		  	<?php
			$jml = 0;
			$limit = 3;
			foreach($data['list_produk'] as $Key => $Value){ ?>
			<li class="span4">
			  <div class="thumbnail"> 
				<a class="zoomTool" href="{{ url('e_ecom_detail/'.$Value->kode_produk) }}" title="add to cart"><span class="icon-search"></span> Lihat Detail</a>
				<a href="{{ url('e_ecom_detail/'.$Value->kode_produk) }}"><img src="{{ asset('temp/'.$Value->foto)}}" alt=""></a>
				<div class="caption cntr">
					<p>{{ $Value->nama_produk }}</p>
					<p class="price"><strong>Anggota : {{"Rp".number_format($Value->harga_anggota, 0,',','.')}}</strong></p>
					<p class="price2"><strong>Umum : {{"Rp".number_format($Value->harga_umum, 0,',','.')}}</strong></p>
					<h4><a class="shopBtn" href="#" title="add to cart"> Add to cart </a></h4>
					
					<br class="clr">
				</div>
			  </div>
			</li>
			<?php   
			$jml++;
			if ($jml >= $limit ){
				$jml =0; ?>  
					</ul>
					</div>
					<div class="row-fluid">
		  			<ul class="thumbnails">
				       
				    <?php }
				}
				?>	
			
		  </ul>
		</div>
	</div>
	<div class="well well-small">
		  <h3><a class="btn btn-mini pull-right" href="products.html" title="View more">VIew More<span class="icon-plus"></span></a> Produk Terlaris </h3>
		  <hr class="soften"/>
		  <div class="row-fluid">
		  <ul class="thumbnails">
			<li class="span4">
			  <div class="thumbnail">
				<a class="zoomTool" href="product_details.html" title="add to cart"><span class="icon-search"></span> QUICK VIEW</a>
				<a  href="{{ asset('Admin/template/product_details.html')}}"><img src="{{ asset('Admin/template/assets/img/d.jpg')}}" alt=""></a>
				<div class="caption">
				  <h5>Manicure & Pedicure</h5>
				  <h4>
					  <a class="defaultBtn" href="product_details.html" title="Click to view"><span class="icon-zoom-in"></span></a>
					  <a class="shopBtn" href="#" title="add to cart"><span class="icon-plus"></span></a>
					  <span class="pull-right">$22.00</span>
				  </h4>
				</div>
			  </div>
			</li>
			<li class="span4">
			  <div class="thumbnail">
				<a class="zoomTool" href="product_details.html" title="add to cart"><span class="icon-search"></span> QUICK VIEW</a>
				<a  href="product_details.html"><img src="{{ asset('Admin/template/assets/img/e.jpg')}}" alt=""></a>
				<div class="caption">
				  <h5>Manicure & Pedicure</h5>
				  <h4>
					  <a class="defaultBtn" href="product_details.html" title="Click to view"><span class="icon-zoom-in"></span></a>
					  <a class="shopBtn" href="#" title="add to cart"><span class="icon-plus"></span></a>
					  <span class="pull-right">$22.00</span>
				  </h4>
				</div>
			  </div>
			</li>
			<li class="span4">
			  <div class="thumbnail">
				<a class="zoomTool" href="product_details.html" title="add to cart"><span class="icon-search"></span> QUICK VIEW</a>
				<a  href="product_details.html"><img src="{{ asset('Admin/template/assets/img/f.jpg')}}" alt=""/></a>
				<div class="caption">
				  <h5>Manicure & Pedicure</h5>
				  <h4>
					  <a class="defaultBtn" href="product_details.html" title="Click to view"><span class="icon-zoom-in"></span></a>
					  <a class="shopBtn" href="#" title="add to cart"><span class="icon-plus"></span></a>
					  <span class="pull-right">$22.00</span>
				  </h4>
				</div>
			  </div>
			</li>
		  </ul>	
	</div>

	</div>
	 @endsection