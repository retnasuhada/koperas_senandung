<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Koperasi Senandung 165</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Bootstrap styles -->
    <link href="{{ asset('/Admin/template/assets/css/bootstrap.css') }}" type="text/css" rel="stylesheet"/>
    <!-- Customize styles -->
    <link href="{{ asset('/Admin/template/style.css')}}" type="text/css" rel="stylesheet"/>
    <!-- font awesome styles -->
	<link href="{{ asset('/Admin/template/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css">
		
    <link rel="shortcut icon" type="text/css" href="{{ asset('/Admin/template/assets/ico/ksico.ico') }}">
<style type="text/css">
	.price{
      color: green;
      font-family: sans-serif;
      font-weight: bold;
      font-size: 14px;
      text-align: center;
      padding: 0;
      margin: 0;
   }
   .price2{
      color: red;
      font-family: sans-serif;
      font-weight: bold;
      font-size: 14px;
      text-align: center;
      padding: 0;
      margin: 0;
   }
</style>
  </head>
<body>
<!-- 
	Upper Header Section 
-->
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="topNav">
		<div class="container">
			<div class="alignR">
			</div>
			
				<a class="active" href="index.html"> <span class="icon-home"></span> Home</a>
				<?php 
				if(isset($_SESSION['id_anggota'])){ ?>
					<a href="#"><span class="icon-user"></span> My Account</a> 
        <?php }else{ ?>
        	<a href="{{url('register_ecomm') }}"><span class="icon-edit"></span> Register </a> 
        <?php }
        ?> 
				
				<a href="{{url('register_ecomm') }}"><span class="icon-edit"></span> About us </a> 
				<a href="contact.html"><span class="icon-envelope"></span> Contact us</a>
				<a href="{{url('checkout') }}"><span class="icon-shopping-cart"></span> {{$data['item']}} Item(s) - <span class="badge badge-warning">{{"Rp".number_format($data['harga_total'], 0,',','.')}}</span></a>
			</div>
		</div>
	</div>
</div>

<!--
Lower Header Section 
-->
<div class="container">
<div id="gototop"> </div>
<header id="header">
<div class="row">
	<div class="span4">
	<h1>
	<a class="logo" href="index.html"><span>Koperasi Senandung 165</span> 
		<img src="{{ asset('Admin/template/assets/img/ksu165.png')}}" alt="bootstrap sexy shop">
	</a>
	</h1>
	</div>
	
	<div class="span8 alignR">
	<p><br> <strong> Support  :  0821 1243 3114 </strong><br><br></p>
	<span class="btn btn-mini">[ {{$data['item']}} ] <span class="icon-shopping-cart"></span></span>
	<span class="btn btn-warning btn-mini">{{"Rp".number_format($data['harga_total'], 0,',','.')}}</span>
	
	</div>
</div>
</header>

<!--
Navigation Bar Section 
-->
<div class="navbar">
	  <div class="navbar-inner">
		<div class="container">
		  <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </a>
		  <div class="nav-collapse">
			<ul class="nav">
			  <li class="active"><a href="#">Home	</a></li>
			  <li class=""><a href="#">Produk Terbaru</a></li>
			  <li class=""><a href="#">PPOB</a></li>
			  <li class=""><a href="#">Promo</a></li>
			  <li class=""><a href="#">Produk Terlaris</a></li>
			  
			</ul>
			<form action="#" class="navbar-search pull-left">
			  <input type="text" placeholder="Search" class="search-query span2">
			</form>
			<ul class="nav pull-right">
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="icon-lock"></span> Login <b class="caret"></b></a>
				<div class="dropdown-menu">
					<form action="{{ route('login') }}" method="post" class="form-horizontal loginFrm">
         @csrf

				  <div class="control-group">
					<input type="text" name="username" class="span2" id="inputEmail" placeholder="Email">
				  </div>
				  <div class="control-group">
					<input type="password" name="password" class="span2" id="inputPassword" placeholder="Password">
				  </div>
				  <div class="control-group">
					<label class="checkbox">
					<input name="anggota" type="checkbox"> Anggota Koperasi
					</label>
					<button type="submit" class="shopBtn btn-block">Sign in</button>
				  </div>
				</form>
				</div>
			</li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
<!-- 
Body Section 
-->
	<div class="row">
<div id="sidebar" class="span3">
<div class="well well-small">
	<ul class="nav nav-list">
		@foreach($data['list_kategori'] as $kategori)
			<li><a href="products.html"><span class="icon-chevron-right"></span>{{$kategori->nama_kategori}}</a></li>
		@endforeach
		<li style="border:0"> &nbsp;</li>
		<li> <a class="totalInCart" href="#"><strong>Total Belanja  <span class="badge badge-warning pull-right" style="line-height:18px;">{{"Rp".number_format($data['harga_total'], 0,',','.')}}</span></strong></a></li>
	</ul>
</div>
<div class="well well-small alert alert-warning cntr">
				  <h2>50% Discount</h2>
				  <p> 
					 only valid for online order. <br><br><a class="defaultBtn" href="#">Comming Soon </a>
				  </p>
			  </div>
			  <div class="well well-small" ><a href="#"><img src="{{ asset('Admin/template/assets/img/paypal.jpg')}}" alt="payment method paypal"></a></div>
			
			<a class="shopBtn btn-block" href="#">Upcoming products <br><small>Click to view</small></a>
			<br>
			<br>
			<ul class="nav nav-list promowrapper">
				@foreach($data['side_produk'] as $side)
				
			<li>
			  <div class="thumbnail">
				<a class="zoomTool" href="#" title="add to cart"><span class="icon-search"></span> QUICK VIEW</a>
				<img src="{{ asset('temp/'.$side->foto)}}" alt="bootstrap ecommerce templates">
				<div class="caption cntr">
					
					<p class="price"><strong>Anggota : {{"Rp".number_format($Value->harga_umum, 0,',','.')}}</strong></p>
					<p class="price2"><strong>Umum : {{"Rp".number_format($Value->harga_umum, 0,',','.')}}</strong></p>
				</div>
			  </div>
			</li>
			<li style="border:0"> &nbsp;</li>
			@endforeach
			
		  </ul>

	</div>
	<div class="span9">
	<div class="well np">
		
<!--
New Products
-->
	@yield('content')
	<!--
	Featured Products
	-->
		
	

	</div>
<!-- 
Clients 
-->


<!--
Footer
-->

</div><!-- /container -->

<div class="copyright">
<div class="container">
	
	<span>Retna Suhada Dev &copy; 2021 || Koperasi Senandun 165 V.1.0</span>
</div>
</div>
<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('Admin/template/assets/js/jquery.js')}}"></script>
	<script src="{{ asset('Admin/template/assets/js/bootstrap.min.js')}}"></script>
	<script src="{{ asset('Admin/template/assets/js/jquery.easing-1.3.min.js')}}"></script>
    <script src="{{ asset('Admin/template/assets/js/jquery.scrollTo-1.4.3.1-min.js')}}"></script>
    <script src="{{ asset('Admin/template/assets/js/shop.js')}}"></script>
  </body>
</html>