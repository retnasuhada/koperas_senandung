@extends('ecomm/template')
@section('content')
@yield('content')
   
    <div class="well well-small">
        <h3>Check Out <small class="pull-right"> 2 Items are in the cart </small></h3>
    <hr class="soften"/>    

    <table class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Deskripdi</th>  
                  <th>Harga </th>
                  <th>Qty </th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><img width="100" src="assets/img/e.jpg" alt=""></td>
                  <td>Items name here<br>Carate : 22<br>Model : n/a</td>
                  
                  
                  <td>$50.00</td>
                  <td>
                    <input class="span1" style="max-width:34px" placeholder="1" id="appendedInputButtons" size="16" type="text" value="2">
                  <div class="input-append">
                    <button class="btn btn-mini btn-danger" type="button"><span class="icon-remove"></span></button>
                </div>
                </td>
                  <td>$100.00</td>
                </tr>
                
                <tr>
                  <td colspan="4" class="alignR">Sub Total:    </td>
                  <td> $448.42</td>
                </tr>
                
                </tbody>
            </table><br/>
        
        
            <table class="table table-bordered">
            <tbody>
                 <tr>
                  <td> 
                <form class="form-inline">
                <div class="control-group">
                <label style="min-width:159px"> Pilih Pengiriman: </label> 
                <select class="form-control">
                    <option value="">-</option>
                    <option value="1">Reguler</option>
                    <option value="2">Kilat</option>
                    <option value="3">Ambil Di Toko</option>
                </select>
                </div>
                <div class="control-group">
                <label style="min-width:159px"> Pembayaran: </label> 
                <select class="form-control">
                    <option value="">-</option>
                    <option value="1">COD</option>
                    <!-- <option value="2">M-KS165</option>
                    <option value="3">Dana</option>
                    <option value="3">Ovo</option> -->
                </select>
                </div>
                </form>
                </td>
                </tr>
                
            </tbody>
                </table>
                   
    <a href="products.html" class="shopBtn btn-large"><span class="icon-arrow-left"></span> Continue Shopping </a>
    <a href="login.html" class="shopBtn btn-large pull-right">Next <span class="icon-arrow-right"></span></a>

</div>
</div>
</div>
 @endsection