@extends('ecomm/template')
@section('content')
@yield('content')
 

    
    <div class="well">
        <form class="form-horizontal" action="{{ route('ecom_regist') }}" method="post" enctype="multipart/form-data">
         @csrf
    
        <h3>Daftarkan Akun Anda</h3>
        <div class="control-group">
            <label class="control-label" for="inputFname">Nama Lengkap <sup>*</sup></label>
            <div class="controls">
              <input type="text" id="inputFname" placeholder="Nama Lengkap" name="nama">
            </div>
         </div>
        <div class="control-group">
        <label class="control-label">Jenis Kelamin <sup>*</sup></label>
        <div class="controls">
        <select class="span3" name="jenis_kelamin">
            <option value="">-</option>
            <option value="1">Laki-laki</option>
            <option value="2">Perempuan</option>
          
        </select>
        </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputFname">Alamat Rumah<sup>*</sup></label>
            <div class="controls">
              <textarea name="alamat"></textarea>
            </div>
         </div>
         <div class="control-group">
            <label class="control-label" for="inputLname">Email</label>
            <div class="controls">
              <input type="text" id="inputLname" placeholder="Email" name="email">
            </div>
         </div>
        <div class="control-group">
        <label class="control-label" for="inputEmail">No Hp <sup>*</sup></label>
        <div class="controls">
          <input type="text" placeholder="Nomoh Handphone" name="no_hp">
        </div>
        </div>
        <div class="control-group">
        <label class="control-label" for="inputEmail">Username <sup>*</sup></label>
        <div class="controls">
          <input type="text" placeholder="username" autocomplete="off" name="username">
        </div>
      </div>      
        <div class="control-group">
        <label class="control-label">Password <sup>*</sup></label>
        <div class="controls">
          <input type="password" placeholder="Password" autocomplete="off" name="password">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Ulangi Password <sup>*</sup></label>
        <div class="controls">
          <input type="password" placeholder="Password" autocomplete="off" name="password2">
        </div>
      </div>
        
    <div class="control-group">
        <div class="controls">
         <input type="submit" name="submitAccount" value="Daftar" class="exclusive shopBtn">
        </div>
    </div>
    </form>
</div>
 @endsection