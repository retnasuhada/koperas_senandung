 @extends('template')
 @section('content')
 @yield('content')

 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @elseif ($data['save']=='2')
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Gagal !</h5>
   Internal Server Error !
 </div>
 @endif
 <div class="row">

   <div class="col-md-6">
     <!-- general form elements -->
     @if($data['flag_edit']=='1')
     <div class="card card-warning">
       <div class="card-header">
         <h3 class="card-title">Edit Jenis Pinjaman</h3>
       </div>
       @else
       <div class="card card-success">
         <div class="card-header">
           <h3 class="card-title">Tambah Jenis Pinjaman</h3>
         </div>
         @endif

         <!-- /.card-header -->
         <!-- form start -->
         @if($data['flag_edit']=='1')
         <form action="{{ route('edit_jenis_pinjaman_act') }}" method="post">
           @else
           <form action="{{ route('add_jenis_pinjaman') }}" method="post">
             @endif

             @csrf

             <div class="card-body">
               <div class="form-group">
                 <label for="exampleInputEmail1">Jenis Pinjaman</label>
                 @if($data['flag_edit']=='1')
                 <input type="text" name="nama_pinjaman" class="form-control" value="{{$data['nama_jenis_pinjaman']}}">
                 <input type="hidden" name="id_jenis_pinjaman" class="form-control" value="{{$data['id_jenis_pinjaman']}}">
                 @else
                 <input type="texs" name="nama_pinjaman" class="form-control" placeholder="Masukan Nama Jenis Pinjaman">
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Margin</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="margin" class="decimal form-control" value="{{$data['margin']}}">
                 @else
                 <input type="texs" name="margin" class="decimal form-control" placeholder="Masukan Persentase Pargin">
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Max Pinjaman</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="max_pinjaman" id="rupiah" class="rupiah form-control" value="{{$data['max_pinjaman']}}">
                 @else
                 <input type="texs" name="max_pinjaman" id="rupiah" class="rupiah form-control" placeholder="Masukan Persentase Pargin">
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Max Tenor (* bulan)</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="max_tenor" onkeypress="return inputAngka(event)" class="form-control" value="{{$data['max_tenor']}}">
                 @else
                 <input type="texs" name="max_tenor" onkeypress="return inputAngka(event)" class="form-control" placeholder="Masukan Persentase Pargin">
                 @endif
               </div>

               <div class="form-group">
                 <label for="exampleInputPassword1">Status</label>
                 <select class="form-control" name="status">
                   @if($data['flag_edit']=='1')
                   <option value="1" <?php if ($data['status'] == '1') {
                                        echo 'selected';
                                      } ?>>Aktif</option>
                   <option value="0" <?php if ($data['status'] == '0') {
                                        echo 'selected';
                                      } ?>>Tidak Aktif</option>
                   @else
                   <option value="1">Aktif</option>
                   <option value="0">Tidak Aktif</option>
                   @endif
                 </select>
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Keterangan</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="keterangan" class="form-control" value="{{$data['keterangan']}}">
                 @else
                 <input type="texs" name="keterangan" class="form-control" placeholder="Masukan Keterangan">
                 @endif
               </div>

             </div>
             <!-- /.card-body -->

             <div class="card-footer">
               <button type="submit" class="btn btn-primary">Simpan</button>
             </div>
           </form>
       </div>
     </div>
     <div class="col-md-6">

       <div class="card">
         <div class="card-header">
           <h3 class="card-title">List Jenis Pinjaman</h3>
         </div>
         <!-- /.card-header -->
         <div class="card-body">
           <table class="table table-bordered">
             <thead>
               <tr>
                 <th style="width: 10px">#</th>
                 <th>Nama Jenis Pinjaman</th>
                 <th>Margin %</th>
                 <th>Status</th>
                 <th style="width: 100px">Aksi</th>
               </tr>
             </thead>
             <tbody>
               <?php
                $no = 1;
                foreach ($data['jenis_pinjaman'] as $row) { ?>
                 <tr>
                   <td>{{$no}}</td>
                   <td>{{$row->nama_jenis_pinjaman}}</td>
                   <td>{{$row->margin}}</td>
                   <td>{{$row->status}}</td>
                   <td>
                     <a href="{{ url('edit_jenis_pinjaman/'.$row->id_jenis_pinjaman) }}"><i class="fas fa-edit"></i></a>
                     &nbsp;&nbsp;
                     <a href="{{ url('hapus_jenis_pinjaman/'.$row->id_jenis_pinjaman) }}"><i class="fas fa-trash"></i></a>

                   </td>
                 </tr>

               <?php $no++;
                } ?>



             </tbody>
           </table>
         </div>
         <!-- /.card-body -->

       </div>
     </div>
   </div>

   <script type="text/javascript">
     var rupiah = document.getElementById('rupiah');
     rupiah.addEventListener('keyup', function(e) {

       rupiah.value = formatRupiah(this.value, 'Rp. ');
     });

     var rupiah2 = document.getElementById('rupiah2');
     rupiah2.addEventListener('keyup', function(e) {

       rupiah2.value = formatRupiah(this.value, 'Rp. ');
     });

     /* Fungsi formatRupiah */
     function formatRupiah(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 3,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{3}/gi);

       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
     }
   </script>
   @endsection