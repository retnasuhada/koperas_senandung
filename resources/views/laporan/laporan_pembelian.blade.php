 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->


   <div class="card card-success">

     <div class="card-header">
       <h3 class="card-title">Laporan Pembelian</h3>

       <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
     </div>
     <!-- /.card-header -->
    
       <form action="{{ route('cetak_lap_pembelian') }}" method="post" enctype="multipart/form-data">
        
         @csrf
         <div class="card-body">
           <div class="row">
             <div class="col-md-4">

               <div class="form-group">
                 <label for="exampleInputEmail1">Jenis Pembelian</label>
                 
                 <select class="form-control" name="jenis_laporan" style="width: 100%;">
                   
                   <option value="All">Semua</option>
                   <option value="1">Pembelian Cash</option>
                   <option value="2">Titip Barang</option>
                   <option value="3">Pembelian Tempo</option>
                   
                 </select>
               </div>

               <!-- /.form-group -->
             </div>
             <!-- /.col -->
             <div class="col-md-4">
               <div class="form-group">
                 <label>Start Date</label>
                 <input type="text" name="start" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask="" inputmode="numeric">
               </div>
             </div>

             <div class="col-md-4">

               <div class="form-group">
                 <label for="exampleInputEmail1">End Date</label>
                <input type="text" name="end" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask="" inputmode="numeric">
                 
               </div>

             </div>
             <!-- /.col -->
           </div>
           <!-- /.row -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Tampilkan</button>
         </div>
       </form>
   
     <!-- /.card-header -->
     <div class="card-body">
       <table id="example1" class="table table-bordered table-striped">
         <thead>

           <tr>
             <th>#</th>
             <th>Tanggal Pembelian</th>
             <th>No Pembelian</th>
             <th>Suplier</th>
             <th>Total Harga</th>
             <th>Jenis Pembelian</th>
           </tr>
         </thead>
         <tbody>
          @if($data['cetak']=='1')
          @php $no=1; @endphp
          @foreach($data['lap_pembelian'] as $row)
             <tr>
               <td>{{$no}}</td>
               <td>{{$row->tgl_pembelian}}</td>
               <td>{{$row->kode_pembelian}}</td>
               <td>{{$row->nama_suplier}}</td>
               <td>{{"Rp " . number_format($row->total ,2,',','.')}}</td>
               <td>{{$row->jenis}}</td>
             </tr>
             @php $no++; @endphp
          @endforeach
          @endif
           </tfoot>
       </table>
     </div>
     <!-- /.card-body -->
   </div>
   @endsection