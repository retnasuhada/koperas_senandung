 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif

 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif

     <div class="card card-primary">
       <div class="card-header">
         <!-- <a href="{{route('add_frm_suplier')}}" class="btn btn-primary"> -->
           <!-- <i class="fas fa-plus"></i> -->
            &nbsp; &nbsp;Stok Produk Suplier
         <!-- </a> -->
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table id="example1" class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>No</th>
               <th>Nama Produk</th>
               <th>Stok</th>
               <th>Aksi</th>
               
              

             </tr>
           </thead>
           <tbody>

             <?php
              $no = 1;
              foreach ($data['list_produk'] as $row) { ?>
               <tr>
                 <td>{{$no}}</td>
                
                 <td>{{$row->nama_produk}}</td>
                 <td>{{$row->stok}}</td>
                <td>
                <a href="{{ url('edit_produkspl/'.$row->kode_produk) }}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                <a href="{{ url('foto_produkspl/'.$row->kode_produk) }}"><i class="fas fa-camera"></i></a></td>
               </tr>

             <?php $no++;
              } ?>
             </tfoot>
         </table>
       </div>
       <!-- /.card-body -->
     </div>
     @endsection