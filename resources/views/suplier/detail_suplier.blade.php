@extends('template')
@section('content')
@yield('content')



<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <!-- Profile Image -->
        <div class="card card-primary card-outline">
          <div class="card-body box-profile">
           

            <h3 class="profile-username text-center">{{$data['nama_suplier']}}</h3>

            <p class="text-muted text-center">{{$data['kode_suplier']}}</p>

            <strong><i class="fas fa-book mr-1"></i> Phone</strong>

            <p class="text-muted">
              {{$data['no_hp']}}
            </p>

            <hr>

            <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

            <p class="text-muted">{{$data['alamat']}}</p>

            <hr>

           
            <a href="{{url('edit_suplier/'.$data['kode_suplier']) }}" class="btn btn-primary btn-block"><i class="fas fa-edit"></i>&nbsp;&nbsp;<b>Edit Data</b></a></a>
          </div>


          <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <!-- About Me Box -->

      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Bio Data</a></li>
              <li class="nav-item"><a class="nav-link" href="#produk" data-toggle="tab">Produk</a></li>
              <li class="nav-item"><a class="nav-link" href="#invoice" data-toggle="tab">Invoice</a></li>
              

            </ul>
          </div><!-- /.card-header -->
          <div class="card-body">
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="post">
                  <div class="row invoice-info">
                    <div class="col-sm-12 invoice-col">
                      <table>
                        <tr>
                          <td width="150px;"><b>Nama Suplier</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{ $data['nama_suplier'] }}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Kode Suplier</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{ $data['kode_suplier'] }}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Alamat</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{ $data['alamat'] }}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>No Hp</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{ $data['no_hp'] }}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>No Telephone</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{ $data['telp'] }}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Anggota</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{ $data['no_anggota'] }}</td>
                        </tr>
                        <tr>
                          <td width="150px;"><b>Deskripsi</b></td>
                          <td>:&nbsp;&nbsp;</td>
                          <td>{{ $data['deskripsi'] }}</td>
                        </tr>
                        
                      </table>

                    </div>
                    <!-- /.col -->

                    <!-- /.col -->
                  </div>
                </div>
                <!-- /.post -->


              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="produk">
                <!-- The timeline -->
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <td style="width:30%"> Kode Produk</td>
                        <td style="width:70%"> Nama Produk</td>
                      </tr>
                      @foreach($data['produk_suplier'] as $row)
                      <tr>
                        <td style="width:30%"> {{$row->kode_produk}}</td>
                        <td style="width:70%"> {{$row->nama_produk}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>

                </div>
              </div>



              <div class="tab-pane" id="invoice">
                <!-- The invoice -->
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <td style="width:30%"> No Invoice</td>
                        <td> Tanggal Invoice</td>
                        <td> Total</td>
                        <td> Dibayar</td>
                        <td> Status</td>
                      </tr>
                      @foreach($data['invoice_suplier'] as $row)
                      <tr>
                        <td style="width:30%"> {{$row->no_invoice}}</td>
                        <td> {{$row->tgl_invoice}}</td>
                        <td> {{"Rp " . number_format($row->total ,2,',','.')}}</td>
                        <td> {{"Rp " . number_format($row->total_pembayaran ,2,',','.')}}</td>
                        <td> {{$row->lunas}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>

                </div>
              </div>


              <!-- /.tab-pane -->


              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

@endsection