 @extends('template')
 @section('content')
 @yield('content')
 <div class="row">

   <div class="col-md-4">
     <!-- general form elements -->


     @if($data['flag_edit']=='1')
     <div class="card card-warning">
       <div class="card-header">
         <h3 class="card-title">Edit Detail Pembelian</h3>
       </div>
       <form action="{{ route('edit_pembelian_act') }}" method="post">
         @else
         <div class="card card-success">
           <div class="card-header">
             <h3 class="card-title">Detail Pembelian</h3>
           </div>
           <form action="{{ route('add_penjualanspl_act') }}" method="post">
             @endif

             @csrf

             <div class="card-body">
               <div class="form-group">
                 <label for="exampleInputEmail1">Nama Produk</label>
                 <input type="hidden" name="kode_pembelian" value="{{$data['kode_pembelian']}}">
                 @if($data['flag_edit']=='1')
                 <input type="hidden" name="id_detail_pembelian" value="{{ $data['id_detail_pembelian'] }}">
                 <select class="form-control select2bs4" name="kode_produk">
                   @foreach($data['list_produk'] as $row)
                   <option value="{{$row->kode_produk}}" <?php if ($data['kode_produk'] == $row->kode_produk) {
                                                            echo "selected";
                                                          } ?>>{{$row->nama_produk}}</option>
                   @endforeach
                 </select>
                 @else
                 <select class="form-control select2bs4" name="kode_produk">
                   <option value=""></option>
                   @foreach($data['list_produk'] as $row)

                   <option value="{{$row->kode_produk}}">{{$row->nama_produk}}</option>
                   @endforeach
                 </select>
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Jumlah</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="jumlah" class="form-control" value="{{$data['jumlah']}}">
                 @else
                 <input type="texs" name="jumlah" class="form-control" placeholder="Masukan Nama Jabatan">
                 @endif
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Harga</label>
                 @if($data['flag_edit']=='1')
                 <input type="texs" name="harga" id="rupiah" class="rupiah form-control" value="{{"Rp " . number_format($data['harga'] ,0,',','.')}}">
                 @else
                 <input type="texs" name="harga" id="rupiah" class="rupiah form-control" placeholder="Masukan Total Harga barang ini">
                 @endif
               </div>


             </div>
             <!-- /.card-body -->

             <div class="card-footer">
               <button type="submit" class="btn btn-primary">Tambahkan</button>
             </div>
           </form>
         </div>
     </div>
     <div class="col-md-8">

       <div class="card">
         <div class="card-header">
           <h3 class="card-title">List Detail Pembelian</h3>
         </div>
         <!-- /.card-header -->
         <div class="card-body">
           <table class="table table-bordered">
             <thead>
               <tr>
                 <th style="width: 10px">#</th>
                 <th>Nama Produk</th>
                 <th>Jumlah</th>
                 <th>Harga</th>
                 <th style="width: 100px">Aksi</th>
               </tr>
             </thead>
             <tbody>
               <?php
                $no = 1;
                foreach ($data['list_pembelian'] as $row) { ?>
                 <tr>
                   <td>{{$no}}</td>
                   <td>{{$row->nama_produk}}</td>
                   <td>{{$row->jumlah}}</td>
                   <td> {{"Rp " . number_format($row->harga_semua ,2,',','.')}}</td>

                   <td>
                     <a href="{{ url('edit_detail_pembelian/'.$row->id_detail_pembelian) }}"><i class="fas fa-edit"></i></a>
                     &nbsp;&nbsp;
                     <a href="{{ url('hapus_detail_pembelian/'.$row->id_detail_pembelian) }}"><i class="fas fa-trash"></i></a>

                   </td>
                 </tr>

               <?php $no++;
                } ?>



             </tbody>
           </table>
         </div>
         <!-- /.card-body -->

       </div>
     </div>
   </div>




   <div class="row">

     <div class="col-md-12">
       <!-- general form elements -->
       <div class="card card-success">
         <div class="card-header">
           <h3 class="card-title">Pembelian</h3>
         </div>

         <form action="{{ route('save_penjualanspl_act') }}" method="post">

           @csrf

           <div class="card-body">

             <div class="form-group">
               <label for="exampleInputEmail1">Kode Pembelian</label>
               @if($data['flag_edit']=='1')
               <input type="text" name="kode_pembelian" class="form-control" value="{{$data['jumlah']}}">
               @else
               <input type="text" readonly="" name="kode_pembelian" class="form-control" value="{{$data['kode_pembelian']}}">
               @endif
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Tanggal Pembelian</label>
               <div class="input-group date" id="reservationdate" data-target-input="nearest">

                 <input type="text" name="tgl_pembelian" class="form-control datetimepicker-input" data-target="#reservationdate">
                 <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                   <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                 </div>

               </div>
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Jenis Invoice</label>
               <select onchange="option()" id="jenis_invoice" class="form-control" name="jenis_invoice" required="">
                 <option value="">-- Pilih --</option>
                 <option value="1">Generate Otomatis</option>
                 <option value="2">Input Manual</option>
               </select>
             </div>
             
             <div class="result form-group">
               
             </div>
            

             <div class="form-group">
               <label for="exampleInputEmail1">Biaya Tambahan</label>
               @if($data['flag_edit']=='1')
               <input type="text" name="biaya_tambahan" id="rupiah2" class="rupiah2 form-control" value="{{$data['harga']}}">
               @else
               <input type="text" name="biaya_tambahan" id="rupiah2" class="rupiah2 form-control" placeholder="Masukan Biaya Tambahan">
               @endif
             </div>
             <div class="form-group">
               <label for="exampleInputEmail1">Potongan / Diskon</label>
               @if($data['flag_edit']=='1')
               <input type="text" name="diskon" id="rupiah3" class="rupiah3 form-control" value="{{$data['diskon']}}">
               @else
               <input type="text" name="diskon" id="rupiah3" class="rupiah3 form-control" placeholder="Masukan Potongan Pembayaran / Diskon">
               @endif
             </div>

             <div class="form-group">
               <label for="exampleInputEmail1">Jenis Pembelian</label>
               <select class="form-control" name="jenis_pembelian">
                 <option value="1">Pembelian Cash</option>
                 <option value="2">Titip Barang</option>
                 <option value="3">Pembayarn Tempo</option>
               </select>
             </div>
           </div>
           <!-- /.card-body -->

           <div class="card-footer">
             <button type="submit" class="btn btn-primary">Simpan</button>
           </div>
         </form>
       </div>
     </div>

   </div>
   

   <script type="text/javascript">
    function option() {
        $(".result").html(null);
       var n = $("#jenis_invoice").val();
       if(n==1){
          var html = "<label for='exampleInputEmail1'>No Invoice</label>";
            
            html += "<input type='text' id='invoice' name='no_invoice' class='form-control' value='{{KodeInvoice()}}' >";
            
       }else{
          var html = "<label for='exampleInputEmail1'>No Invoice</label>";
            html += "@if($data['flag_edit']=='1')";
            html += "<input type='text' name='no_invoice' class='form-control' value='{{$data['jumlah']}}'>";
            html += "@else";
            html += "<input type='text' id='invoice' required=' name='no_invoice' class='form-control' placeholder='Masukan Nomor Invoice'>";
            html += "@endif";
       }
       
        $(".result").append(html);

      
     }

     var rupiah = document.getElementById('rupiah');
     rupiah.addEventListener('keyup', function(e) {

       rupiah.value = formatRupiah(this.value, 'Rp. ');
     });

     var rupiah2 = document.getElementById('rupiah2');
     rupiah2.addEventListener('keyup', function(e) {

       rupiah2.value = formatRupiah(this.value, 'Rp. ');
     });
     var rupiah3 = document.getElementById('rupiah3');
     rupiah3.addEventListener('keyup', function(e) {

       rupiah3.value = formatRupiah(this.value, 'Rp. ');
     });

     /* Fungsi formatRupiah */
     function formatRupiah(angka, prefix) {
       var number_string = angka.replace(/[^,\d]/g, '').toString(),
         split = number_string.split(','),
         sisa = split[0].length % 3,
         rupiah = split[0].substr(0, sisa),
         ribuan = split[0].substr(sisa).match(/\d{3}/gi);

       // tambahkan titik jika yang di input sudah menjadi angka ribuan
       if (ribuan) {
         separator = sisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
       }

       rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
       return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
     }

     

   </script>


   @endsection