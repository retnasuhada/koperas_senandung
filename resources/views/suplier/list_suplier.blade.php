 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif

     <div class="card card-primary">
       <div class="card-header">
         <a href="{{route('add_frm_suplier')}}" class="btn btn-primary">
           <i class="fas fa-plus"></i> &nbsp; &nbsp;Tambah Suplier
         </a>
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table id="example1" class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>No</th>
               <th>Kode</th>
               <th>Nama</th>
               <th>Join Date</th>
               <th>Rating</th>
               <th>Status</th>
               <th>Opsi</th>

             </tr>
           </thead>
           <tbody>

             <?php
              $no = 1;
              foreach ($data['list_suplier'] as $row) { ?>
               <tr>
                 <td>{{$no}}</td>
                 <td>{{$row->kode_suplier}}</td>
                 <td>{{$row->nama_suplier}}</td>
                 <td>{{$row->join_date}}</td>
                 <td>{{$row->rating}}</td>
                 <td>{{$row->status}}</td>

                 <td>
                   <a href="{{ url('edit_suplier/'.$row->kode_suplier) }}"><i class="fas fa-edit"></i></a>
                   &nbsp;&nbsp;
                   <a href="{{ url('detail_suplier/'.$row->kode_suplier) }}"><i class="far fa-eye"></i></a>

                 </td>
               </tr>

             <?php $no++;
              } ?>
             </tfoot>
         </table>
       </div>
       <!-- /.card-body -->
     </div>
     @endsection