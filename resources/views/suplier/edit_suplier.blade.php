 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 @if ($data['save']=='1')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Disimipan
 </div>
 @elseif ($data['save']=='3')
 <div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-check"></i> Berhasil !</h5>
   Data Berhasil Dihapus
 </div>
 @endif
 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif
     <div class="card-header">
       <h3 class="card-title">Form Suplier Koperasi Senandung 165</h3>

       <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
     </div>
     <!-- /.card-header -->
     
       <form action="{{ route('edit_suplier_act') }}" method="post" enctype="multipart/form-data">
         
         @csrf
         <div class="card-body">
           <div class="row">
             <div class="col-md-6">
               <div class="form-group">
                 <label for="exampleInputEmail1">Kode Suplier</label>
                 <input type="text" name="kode_suplier" readonly="" class="form-control" value="{{ $data['kode_suplier']}}">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Nama Suplier</label>
                 <input type="text" name="nama_suplier" class="form-control" value="{{ $data['nama_suplier']}}">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">No HP</label>
                 <input type="text" name="no_hp" class="form-control" value="{{ $data['no_hp']}}">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">No Telp</label>
                 <input type="text" name="no_telp" class="form-control" value="{{ $data['telp']}}">
               </div>
               <div class="form-group">
                 <label>Alamat</label>
                 <textarea class="form-control" name="alamat" rows="2" placeholder="Enter ...">{{ $data['alamat']}}</textarea>
               </div>
               <!-- /.form-group -->
             </div>
             <!-- /.col -->
             <div class="col-md-6">
              <div class="form-group">
                 <label>Nama Anggota</label>
                 <select name="no_anggota" class="form-control">
                  @foreach($data['list_anggota'] as $row)
                   <option value="{{$row->no_anggota}}" {{$row->no_anggota}}" <?php if ($data['no_anggota'] == $row->no_anggota) {
                                                                  echo "selected";} ?> >{{$row->nama_anggota}}</option>
                  @endforeach
                 </select>
                 
               </div>
               <div class="form-group">
                 <label>Status</label>
                 <select name="status" class="form-control">
                   <option value="1">Aktif</option>
                   <option value="0">Tidak Aktif</option>
                 </select>
               </div>
               
               <div class="form-group">
                 <label>Deskripsi</label>

                 <textarea class="form-control" name="deskripsi" rows="3" placeholder="Enter ...">{{ $data['kode_suplier']}}</textarea>

               </div>
             </div>
             <!-- /.col -->
           </div>
           <!-- /.row -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-success">Simpan</button>
           <button type="reset" class="btn btn-warning">Cancel</button>
         </div>
       </form>
   </div>

   @endsection