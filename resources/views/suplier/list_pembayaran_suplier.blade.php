 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif

 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif

     <div class="card card-primary">
       <div class="card-header">
         <!-- <a href="{{route('add_frm_suplier')}}" class="btn btn-primary"> -->
           <!-- <i class="fas fa-plus"></i> -->
            &nbsp; &nbsp;Pembayaran Koperas Ke {{$data['nama_suplier']}}
         <!-- </a> -->
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table id="example1" class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>No</th>
               <th>Tanggal Pembayaran</th>
               <th>No Pembayaran</th>
               <th>Total Pembayaran</th>
               <th>Detail</th>
               
              

             </tr>
           </thead>
           <tbody>

             <?php
              $no = 1;
             
              foreach ($data['pembayaran_suplier'] as $row) {
              
               ?>
               <tr>
                 <td>{{$no}}</td>
                 <td>{{$row->tgl_pembayaran}}</td>
                 <td>{{$row->kode_pembayaran_suplier}}</td>                 
                 <td>{{"Rp " . number_format($row->total ,2,',','.')}}</td>
                 <td>
                   <a class="btn btn-success" href="{{ url('m_detail_pembayaran_suplier/'.$row->kode_pembayaran_suplier) }}">Detail</a>
                 </td>
               </tr>

             <?php $no++;
              } ?>

             </tfoot>
         </table>
       </div>
       <!-- /.card-body -->
     </div>
     @endsection