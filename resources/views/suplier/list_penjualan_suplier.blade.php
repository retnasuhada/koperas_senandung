 @extends('template')
 @section('content')
 @yield('content')
 <!-- SELECT2 EXAMPLE -->
 @if (count($errors) > 0)
 <div class="alert alert-danger alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif

 @if($data['flag_edit']=='1')
 <div class="card card-warning">
   @else
   <div class="card card-success">
     @endif

     <div class="card card-primary">
       <div class="card-header">
         <a href="{{route('add_m_penjualan_suplier')}}" class="btn btn-primary"> 
           <i class="fas fa-plus"></i>
            &nbsp; &nbsp;Tambah Penjualan Ke Koperasi
        </a> 
       </div>
       <!-- /.card-header -->
       <div class="card-body">
         <table id="example1" class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>No</th>
               <th>Tanggal Penjualan</th>
               <th>No Penjualan</th>
               <th>Jenis Penjualan</th>
               <th>Total</th>
               <th>Status</th>
               <th>Detail</th>
               
              

             </tr>
           </thead>
           <tbody>

             <?php
              $no = 1;
             
              foreach ($data['penjualan_suplier'] as $row) {
              if($row->jenis_pembelian=='1'){
                  $jenis_pembelian = 'cash';
              }
              if($row->jenis_pembelian=='2'){
                  $jenis_pembelian = 'Titip';
              }
              else{
                  $jenis_pembelian = 'Tempo';
              }
              
               ?>
               <tr>
                 <td>{{$no}}</td>
                
                 <td>{{$row->tgl_pembelian}}</td>
                 <td>{{$row->kode_pembelian}}</td>
                 <td>{{$jenis_pembelian}}</td>

                 
                 <td>{{"Rp " . number_format($row->total ,2,',','.')}}</td>
                 <td><?php if($row->verified=='0'){ ?>
                  <a href="#" class="btn btn-block btn-outline-warning btn-xs">Belum Di verifikasi</a>
                <?php } else {?>
                  <a href="#" class="btn btn-block btn-outline-success btn-xs">Verified</a> <?php } ?>
                </td>
                 <td>
                   <a class="btn btn-success" href="{{ url('detail_pembelian/'.$row->kode_pembelian) }}">Detail</a>
                 </td>
               </tr>

             <?php $no++;
              } ?>

             </tfoot>
         </table>
       </div>
       <!-- /.card-body -->
     </div>
     @endsection