<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Image;
use Illuminate\Support\Facades\DB;
use App\Models\Produk;
use Illuminate\Support\Facades\Storage;

class ProdukController extends Controller
{
    public function __construct()
    {
        $this->produk = new Produk();
        // $this->anggota = new Anggota();

    }



    public function list_produk()
    {

        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_produk']      = $this->produk->list_produk();
        $data['list_kategori']      = $this->produk->list_kategori();
        $data['flag_edit']      = '0';
        $data['title']          = 'Managemen Produk';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $data['menu']           = 'toko';

        return view("produk.list_produk", ["data" => $data]);
    }

    public function add_back_produk()
    {
        $data['kode_produk']    = kodeProduk();
        $data['save']           = '0';
        $data['list_kategori']      = $this->produk->list_kategori();
        $data['flag_edit']      = '0';
        $data['title']          = 'Managemen Produk';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $data['menu']           = 'toko';

        return view("produk.add_back_produk", ["data" => $data]);
    }

    public function edit_produk($id)
    {
        $old = $this->produk->getInfoProduk($id);
        foreach ($old as $row) {
            $data['kode_produk']            = $row->kode_produk;
            $data['nama_produk']            = $row->nama_produk;
            $data['kode_kategori']          = $row->kode_kategori;
            $data['flag_warna']             = $row->flag_warna;
            $data['harga_umum']             = $row->harga_umum;
            $data['harga_anggota']          = $row->harga_anggota;
            $data['flag_grosir']            = $row->flag_grosir;
            $data['harga_grosir_umum']      = $row->harga_grosir_umum;
            $data['harga_grosir_anggota']   = $row->harga_grosir_anggota;
            $data['harga_beli']             = $row->harga_beli;
            $data['deskripsi']              = $row->deskripsi;
            $data['minimum_grosir']         = $row->minimum_grosir;
        }
        $data['save']           = '0';
        $data['list_kategori']  = $this->produk->list_kategori();
        $data['flag_edit']      = '1';
        $data['title']          = 'Managemen Produk';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $data['menu']           = 'toko';

        return view("produk.edit_produk", ["data" => $data]);
    }

    public function edit_produkspl($id)
    {
        $old = $this->produk->getInfoProduk($id);
        foreach ($old as $row) {
            $data['kode_produk']            = $row->kode_produk;
            $data['nama_produk']            = $row->nama_produk;
            $data['kode_kategori']          = $row->kode_kategori;
            $data['flag_warna']             = $row->flag_warna;
            $data['harga_umum']             = $row->harga_umum;
            $data['harga_anggota']          = $row->harga_anggota;
            $data['flag_grosir']            = $row->flag_grosir;
            $data['harga_grosir_umum']      = $row->harga_grosir_umum;
            $data['harga_grosir_anggota']   = $row->harga_grosir_anggota;
            $data['harga_beli']             = $row->harga_beli;
            $data['deskripsi']              = $row->deskripsi;
            $data['minimum_grosir']         = $row->minimum_grosir;
        }
        $data['save']           = '0';
        $data['list_kategori']  = $this->produk->list_kategori();
        $data['flag_edit']      = '1';
        $data['title']          = 'Managemen Produk';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $data['menu']           = 'toko';

        return view("produk.edit_produkspl", ["data" => $data]);
    }


    public function add_detail_produk($id)
    {

        $data['save']           = '0';
        $data['list_kategori']      = $this->produk->list_kategori();
        $data['flag_edit']      = '0';
        $data['title']          = 'Managemen Produk';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $data['menu']           = 'toko';

        return view("produk.add_det_produk", ["data" => $data]);
    }

    public function edit_back_produk($id)
    {

        $data['save']           = '0';
        $data['list_kategori']      = $this->produk->list_kategori();
        $data['flag_edit']      = '1';
        $data['title']          = 'Managemen Produk';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $data['menu']           = 'toko';

        $data['old']            = $this->produk->getKdProduk($id);

        foreach ($data['old'] as $row) {
            $data['id_kd_produk']   = $row->id_kd_produk;
            $data['id_kategori']    = $row->id_kategori;
            $data['kd_produk']      = $row->kd_produk;
            $data['nama_produk']    = $row->nama_produk;
            $data['id_toko']        = $row->id_toko;
            $data['foto_utama']     = $row->foto_utama;
            $data['harga_satuan']   = "Rp" . number_format($row->harga_satuan, 0, ',', '.');
            $data['harga_grosir']   = "Rp" . number_format($row->harga_grosir, 0, ',', '.');
            $data['flag_warna']     = $row->flag_warna;
            $data['flag_size']      = $row->flag_size;
            $data['deskripsi']      = $row->deskripsi;
            $data['foto_utama']     = $row->foto_utama;
        }

        return view("produk.add_back_produk", ["data" => $data]);
    }

    public function list_size()
    {

        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_size']      = $this->produk->list_size();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Produk';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Size';
        $data['Active']         = 'list_size';
        $data['menu']           = 'm_produk';
        return view("produk.size", ["data" => $data]);
    }

    public function add_foto(Request $request)
    {
        $data['data_produk']    = $this->produk->getDetProduk($request->id_produk);
        foreach ($data['data_produk'] as $row) {
            $data['nama_produk'] = $row->nama_produk;
        }
        $data['id_produk']      = $request->post('id_produk');
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Foto Produk';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Foto Produk';
        $data['Active']         = 'list_produk';
        $data['menu']           = 'm_produk';
        return view("produk.add_foto", ["data" => $data]);
    }

    public function upload_foto_produk(Request $request)
    {
        $this->validate($request, [
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $id                 = $request->id_produk;
        $image              = $request->file('foto');
        $size               = $image->getSize();
        $input['imagename'] = time() . '.' . $image->extension();
        $destinationPath    = public_path('/temp');
        $img                = Image::make($image->path());

        $img->resize(276, 357, function ($constraint) {

            $constraint->aspectRatio();
        })->save($destinationPath . '/' . $input['imagename']);

        $foto_nama          = $input['imagename'];
        $save               = DB::insert('insert into foto_produk (id_kd_produk,foto) values (?, ?)', [$id, $foto_nama]);
        $data['destinationPath'] = $destinationPath;
        $data['list_size']      = $this->produk->list_size();
        $data['list_warna']     = $this->produk->list_warna();
        $data['list_satuan']    = $this->produk->list_satuan();
        $data['list_kategori']  = $this->produk->list_kategori();
        $data['list_det_produk']  = $this->produk->list_det_produk();
        // $data['det_produk']     = $this->produk->getKdProduk($id);
        $data['old']            = $this->produk->getDetProduk($id);
        $data['list_foto']           = $this->produk->fotoProduk($id);
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Foto Produk';
        $data['Active']         = 'list_produk';
        $data['menu']           = 'm_produk';
        foreach ($data['old'] as $row) {
            $data['id_kd_produk']   = $row->id_kd_produk;
            $data['id_produk']      = $row->id_produk;
            $data['kd_produk']      = $row->kd_produk;
            $data['nama_produk']    = $row->nama_produk;
            $data['id_kategori']    = $row->id_kategori;
            $data['id_warna']       = $row->id_warna;
            $data['id_satuan']      = $row->id_satuan;
            $data['id_size']        = $row->id_size;
            $data['berat']          = $row->berat;
            $data['volume']         = $row->volume;
            $data['harga_satuan']   = $row->harga_satuan;
            $data['harga_grosir']   = $row->harga_grosir;
            $data['min_grosir']     = $row->min_grosir;
            $data['deskripsi']      = $row->deskripsi;
            $data['stok']           = $row->stok;
        }


        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Detail Produk';
        $data['edit']           = '0';
        $data['save']           = '0';

        return view("produk.back_detail_produk", ["data" => $data]);
    }

    public function list_kategori()
    {

        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['kode_kategori']   = KodeKategori();

        $data['list_kategori']      = $this->produk->list_kategori();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Katgori';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Kategori';
        $data['Active']         = 'list_kategori';
        $data['menu']           = 'm_produk';
        return view("produk.kategori", ["data" => $data]);
    }

    public function list_kategori2()
    {

        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['kode_sub_kategori'] = KodeSubKategori();
        $data['list_kategori']      = $this->produk->list_kategori();
        $data['list_kategori2']      = $this->produk->list_kategori2();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Sub Kategori';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Kategori';
        $data['Active']         = 'list_kategori2';
        $data['menu']           = 'm_produk';
        return view("produk.kategori2", ["data" => $data]);
    }

    public function getSubKategori($id)
    {
        $data = $this->produk->subkategori($id);

        return response()->json($data);
    }

    public function getSubKategoriA($id)
    {
        $data = $this->produk->subkategoriA($id);

        return response()->json($data);
    }


    public function list_kategori3()
    {

        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['kode_child_kategori']    = KodeChildKategori();
        $data['list_kategori']  = $this->produk->list_kategori();
        $data['list_kategori2'] = $this->produk->list_kategori2();
        $data['list_kategori3'] = $this->produk->list_kategori3();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Child Sub Kategori';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Kategori';
        $data['Active']         = 'list_kategori3';
        $data['menu']           = 'm_produk';
        return view("produk.kategori3", ["data" => $data]);
    }



    public function list_satuan()
    {

        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_satuan']      = $this->produk->list_satuan();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Satuan';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Satuan';
        $data['Active']         = 'list_satuan';
        $data['menu']           = 'm_produk';
        return view("produk.satuan", ["data" => $data]);
    }

    public function list_warna()
    {

        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['kode_warna']      = KodeWarna();
        $data['list_warna']      = $this->produk->list_warna();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Warna';
        $data['edit']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Warna';
        $data['Active']         = 'list_warna';
        $data['menu']           = 'm_produk';
        return view("produk.warna", ["data" => $data]);
    }




    public function edit_size($id)
    {
        // $data['list_user']      = $this->user->list_user();
        $data['list_size']      = $this->produk->list_size();
        $data['old']            = $this->produk->getSize($id);

        foreach ($data['old'] as $row) {
            $data['id_size']     = $row->id_size;
            $data['nm_size']     = $row->nm_size;
            $data['flag_size']   = $row->flag_size;
        }
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Size';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Size';
        $data['Active']         = 'list_size';
        $data['menu']           = 'm_produk';
        return view("produk.size", ["data" => $data]);
    }

    public function detail_back_produk($id)
    {
        // $data['list_user']      = $this->user->list_user();
        $data['list_size']      = $this->produk->list_size();
        $data['list_warna']     = $this->produk->list_warna();
        $data['list_satuan']    = $this->produk->list_satuan();
        $data['list_kategori']  = $this->produk->list_kategori();
        $data['list_foto']      = $this->produk->fotoProduk($id);
        $data['list_det_produk']  = $this->produk->list_det_produk();
        $data['det_produk']     = $this->produk->getKdProduk($id);

        foreach ($data['det_produk'] as $row) {
            $data['id_kd_produk']   = $row->id_kd_produk;
            $data['kd_produk']      = $row->kd_produk;
            $data['nama_produk']    = $row->nama_produk;
            $data['id_kategori']    = $row->id_kategori;
        }
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Detail Produk';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $data['menu']           = 'toko';
        return view("produk.add_produk", ["data" => $data]);
    }

    public function add_new_detail_produk($id)
    {
        // $data['list_user']      = $this->user->list_user();
        $data['list_size']      = $this->produk->list_size();
        $data['list_warna']     = $this->produk->list_warna();
        $data['list_satuan']    = $this->produk->list_satuan();
        $data['list_kategori']  = $this->produk->list_kategori();
        $data['list_det_produk']  = $this->produk->list_det_produk();
        $data['det_produk']     = $this->produk->getKdProduk($id);

        foreach ($data['det_produk'] as $row) {
            $data['id_kd_produk']   = $row->id_kd_produk;
            $data['kd_produk']      = $row->kd_produk;
            $data['nama_produk']    = $row->nama_produk;
            $data['id_kategori']    = $row->id_kategori;
        }
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Detail Produk';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $data['menu']           = 'toko';
        return view("produk.add_new_detail_produk", ["data" => $data]);
    }

    public function back_detail_produk($id)
    {
        // $data['list_user']      = $this->user->list_user();
        $data['list_size']      = $this->produk->list_size();
        $data['list_warna']     = $this->produk->list_warna();
        $data['list_satuan']    = $this->produk->list_satuan();
        $data['list_kategori']  = $this->produk->list_kategori();
        $data['list_det_produk'] = $this->produk->list_det_produk();
        // $data['det_produk']     = $this->produk->getKdProduk($id);
        $data['old']            = $this->produk->getDetProduk($id);
        $data['list_foto']      = $this->produk->fotoProduk($id);
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';

        $destinationPath        = public_path('/temp');
        $data['destinationPath'] = $destinationPath;
        foreach ($data['old'] as $row) {
            $data['id_kd_produk']   = $row->id_kd_produk;
            $data['id_produk']      = $row->id_produk;
            $data['kd_produk']      = $row->kd_produk;
            $data['nama_produk']    = $row->nama_produk;
            $data['id_kategori']    = $row->id_kategori;
            $data['id_warna']       = $row->id_warna;
            $data['id_satuan']      = $row->id_satuan;
            $data['id_size']        = $row->id_size;
            $data['berat']          = $row->berat;
            $data['volume']         = $row->volume;
            $data['deskripsi']      = $row->deskripsi;
            $data['stok']           = $row->stok;
        }


        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Detail Produk';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['menu']           = 'toko';
        return view("produk.back_detail_produk", ["data" => $data]);
    }

    public function detail_produk($id)
    {
        $data_produk = $this->produk->getProduk($id);
        foreach ($data_produk as $row) {
            $data['flag_warna']     = $row->flag_warna;
            $data['kode_produk']     = $row->kode_produk;
            $data['nama_produk']     = $row->nama_produk;
        }
        $data['list_det_produk']     = $this->produk->list_det_produk($id);

        // $data['flag_warna']     = $flag_warna;
        $data['list_warna']     = $this->produk->list_warna();
        $data['list_foto']      = $this->produk->fotoProduk($id);
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';

        $destinationPath        = public_path('/temp');
        $data['destinationPath'] = $destinationPath;
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Detail Produk';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['menu']           = 'toko';
        return view("produk.detail_produk", ["data" => $data]);
    }

    public function edit_det_produk($id)
    {

        $old = $this->produk->getDetProduk($id);
        foreach ($old as $row_old) {
            $data['kode_warna']         = $row_old->kode_warna;
            $data['id_detail_produk']   = $row_old->id_detail_produk;
            $data['stok']               = $row_old->stok;
            $data['kode_produk']        = $row_old->kode_produk;
            $data['flag_warna']         = $row_old->flag_warna;
            $data['nama_produk']        = $row_old->nama_produk;
        }

        $data['list_det_produk']     = $this->produk->list_det_produk($row_old->kode_produk);

        // $data['flag_warna']     = $flag_warna;
        $data['list_warna']     = $this->produk->list_warna();
        $data['list_foto']      = $this->produk->fotoProduk($row_old->kode_produk);
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';

        $destinationPath        = public_path('/temp');
        $data['destinationPath'] = $destinationPath;
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Detail Produk';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['menu']           = 'toko';
        return view("produk.detail_produk", ["data" => $data]);
    }

    public function detail_det_produk($id)
    {

        $list_produk = $this->produk->getInfoProduk($id);

        foreach ($list_produk as $row) {

            $data['deskripsi']               =  $row->deskripsi;
            $data['nama_produk']             =  $row->nama_produk;
            $data['kode_produk']             =  $row->kode_produk;
            $data['kategori']                =  $row->nama_kategori;
            $data['subkategori']             =  $row->nama_sub_kategori;
            $data['childkategori']           =  $row->nama_sub_kategori2;
            $data['warna']                   =  $row->flag_warna;
            $data['grosir']                  =  $row->flag_grosir;
            $data['harga_anggota']           =  $row->harga_anggota;
            $data['harga_umum']              =  $row->harga_umum;
            $data['harga_grosir_anggota']    =  $row->harga_grosir_anggota;
            $data['harga_grosir_umum']       =  $row->harga_grosir_umum;
            $data['harga_beli']              =  $row->harga_beli;
            $data['stok']                    =  $row->stok;
        }
        $det_produk = $this->produk->getDetInfoProduk($id);
        // print_r($det_produk);
        // die();
        $data['info_suplier']   = $det_produk;
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Detail Produk';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['menu']           = 'toko';
        return view("produk.detail_produk_det", ["data" => $data]);
    }


    public function foto_produk($id)
    {
        
            $data['kode_produk']        = $id;
       

        $data['list_foto']      = $this->produk->fotoProduk($data['kode_produk']);
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $destinationPath        = public_path('/temp');
        $data['destinationPath'] = $destinationPath;



        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Detail Produk';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['menu']           = 'toko';
        return view("produk.foto_produk", ["data" => $data]);
    }

    public function foto_produkspl($id)
    {
        
            $data['kode_produk']        = $id;
       

        $data['list_foto']      = $this->produk->fotoProduk($data['kode_produk']);
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Produk';
        $data['Active']         = 'list_produk';
        $destinationPath        = public_path('/temp');
        $data['destinationPath'] = $destinationPath;



        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Detail Produk';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['menu']           = 'toko';
        return view("produk.foto_produkspl", ["data" => $data]);
    }


    public function edit_kategori($id)
    {
        // $data['list_user']      = $this->user->list_user();
        $data['list_kategori']      = $this->produk->list_kategori();
        $data['old']            = $this->produk->getKategori($id);

        foreach ($data['old'] as $row) {
            $data['id_kategori']     = $row->id_kategori;
            $data['kode_kategori']     = $row->kode_kategori;
            $data['nama_kategori']     = $row->nama_kategori;
        }
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman kategori';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Kategori';
        $data['Active']         = 'list_kategori';
        $data['menu']           = 'm_produk';
        return view("produk.kategori", ["data" => $data]);
    }

    public function edit_sub_kategori($id)
    {

        $data['list_kategori']      = $this->produk->list_kategori();
        $data['list_kategori2']      = $this->produk->list_kategori2();
        $data['old']                = $this->produk->getSubkategori($id);

        foreach ($data['old'] as $row) {
            $data['id_sub_kategori']    = $row->id_sub_kategori;
            $data['kode_kategori']      = $row->kode_kategori;
            $data['nama_kategori']      = $row->nama_kategori;
            $data['kode_sub_kategori']  = $row->kode_sub_kategori;
            $data['nama_sub_kategori']  = $row->nama_sub_kategori;
        }
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Sub Kategori';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Kategori';
        $data['Active']         = 'list_kategori2';
        $data['menu']           = 'm_produk';
        return view("produk.kategori2", ["data" => $data]);
    }

    public function edit_sub_kategori2($id)
    {

        $data['list_kategori']      = $this->produk->list_kategori();
        $data['list_kategori2']     = $this->produk->list_kategori2();
        $data['list_kategori3']     = $this->produk->list_kategori3();
        $data['old']                = $this->produk->getChildkategori($id);

        foreach ($data['old'] as $row) {
            $data['kode_sub_kategori2'] = $row->kode_sub_kategori2;
            $data['id_sub_kategori2']   = $row->id_sub_kategori2;
            $data['kode_kategori']      = $row->kode_kategori;
            $data['nama_sub_kategori2'] = $row->nama_sub_kategori2;
        }
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Sub Kategori';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Kategori';
        $data['Active']         = 'list_kategori2';
        $data['menu']           = 'm_produk';
        return view("produk.kategori3", ["data" => $data]);
    }



    public function edit_satuan($id)
    {
        // $data['list_user']      = $this->user->list_user();
        $data['list_satuan']      = $this->produk->list_satuan();
        $data['old']            = $this->produk->getSatuan($id);

        foreach ($data['old'] as $row) {
            $data['id_satuan']     = $row->id_satuan;
            $data['nm_satuan']     = $row->nm_satuan;
        }
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Satuan';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Satuan';
        $data['Active']         = 'list_satuan';
        $data['menu']           = 'm_produk';
        return view("produk.satuan", ["data" => $data]);
    }

    public function edit_warna($id)
    {
        // $data['list_user']      = $this->user->list_user();
        $data['list_warna']      = $this->produk->list_warna();
        $data['old']            = $this->produk->getWarna($id);

        foreach ($data['old'] as $row) {
            $data['id_warna']     = $row->id_warna;
            $data['nm_warna']     = $row->nm_warna;
            $data['kode_warna']     = $row->kode_warna;
        }
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Warna';
        $data['edit']           = '0';
        $data['save']           = '0';
        $data['Halaman']        = 'Managemen Produk';
        $data['Sub_Halaman']    = 'Warna';
        $data['Active']         = 'list_warna';
        $data['menu']           = 'm_produk';

        return view("produk.warna", ["data" => $data]);
    }



    public function edit_size_act(Request $request)
    {

        $this->validate($request, [
            'id_size'   => 'required',
            'nm_size'   => 'required',
            'flag_size'   => 'required'
        ]);
        $id_size    = $request->post('id_size');
        $nm_size    = $request->post('nm_size');
        $flag_size  = $request->post('flag_size');

        try {
            DB::update(
                'UPDATE size set 
                                        nm_size = ?, 
                                        flag_size = ?
                                      WHERE id_size =?',
                [
                    $nm_size,
                    $flag_size,
                    $id_size
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            DB::rollback();
            $data          = '2';
        }

        return redirect()->route('size')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function edit_back_produk_act(Request $request)
    {
        $this->validate($request, [
            'kode_produk' => 'required',
            'nama_produk' => 'required',
            'flag_warna' => 'required',
            'harga_umum' => 'required',
            'harga_anggota' => 'required',
            'flag_grosir' => 'required',
            'harga_grosir_umum' => 'required',
            'harga_grosir_anggota' => 'required',
            'deskripsi' => 'required'
        ]);

        $kode_produk            = $request->post('kode_produk');
        $nama_produk            = $request->post('nama_produk');
        $kode_kategori          = $request->post('kode_kategori');
        $kode_sub_kategori      = $request->post('kode_sub_kategori');
        $kode_sub_kategori2     = $request->post('kode_sub_kategori2');
        $flag_warna             = $request->post('flag_warna');
        $harga_umum             = InsertRupiah($request->post('harga_umum'));
        $harga_anggota          = InsertRupiah($request->post('harga_anggota'));
        $flag_grosir            = $request->post('flag_grosir');
        $harga_grosir_umum      = InsertRupiah($request->post('harga_grosir_umum'));
        $harga_grosir_anggota   = InsertRupiah($request->post('harga_grosir_anggota'));
        $minimum_grosir        = $request->post('minimum_grosir');
        // $kode_suplier           = $request->post('kode_suplier');
        $deskripsi              = $request->post('deskripsi');


        try {
            DB::update(
                'UPDATE produk set 
                                        kode_kategori = ?,
                                        nama_produk = ?, 
                                        kode_sub_kategori = ?,
                                        kode_sub_kategori2 = ?, 
                                        flag_warna = ?,
                                        flag_grosir = ?, 
                                        deskripsi = ?,
                                        harga_anggota = ?, 
                                        harga_umum = ?,
                                        harga_grosir_anggota = ?, 
                                        harga_grosir_umum = ?,
                                        minimum_grosir = ?
                                      WHERE kode_produk =?',
                [
                    $kode_kategori,
                    $nama_produk,
                    $kode_sub_kategori,
                    $kode_sub_kategori2,
                    $flag_warna,
                    $flag_grosir,
                    $deskripsi,
                    $harga_anggota,
                    $harga_umum,
                    $harga_grosir_anggota,
                    $harga_grosir_umum,
                    $minimum_grosir,
                    $kode_produk
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
            $data          = '2';
        }

        return redirect()->route('list_produk')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function edit_back_produkspl_act(Request $request)
    {
        $this->validate($request, [
            
            'nama_produk' => 'required',
            'deskripsi' => 'required'
        ]);

        $kode_produk            = $request->post('kode_produk');
        $nama_produk            = $request->post('nama_produk');
        $kode_kategori          = $request->post('kode_kategori');
        $kode_sub_kategori      = $request->post('kode_sub_kategori');
        $kode_sub_kategori2     = $request->post('kode_sub_kategori2');
        $deskripsi              = $request->post('deskripsi');


        try {
            DB::update(
                'UPDATE produk set 
                                        kode_kategori = ?,
                                        nama_produk = ?, 
                                        kode_sub_kategori = ?,
                                        kode_sub_kategori2 = ?, 
                                        deskripsi = ?
                                      WHERE kode_produk =?',
                [
                    $kode_kategori,
                    $nama_produk,
                    $kode_sub_kategori,
                    $kode_sub_kategori2,
                   
                    $deskripsi,
                    
                    $kode_produk
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
            $data          = '2';
        }

        return redirect()->route('m_suplier_stok')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function edit_kategori_act(Request $request)
    {

        $this->validate($request, [
            'id_kategori'   => 'required',
            'kode_kategori'   => 'required',
            'nama_kategori'   => 'required'

        ]);
        $kode_kategori    = $request->post('kode_kategori');
        $nama_kategori    = $request->post('nama_kategori');
        try {
            DB::update(
                'UPDATE kategori set 
                                        nama_kategori = ?
                                      WHERE kode_kategori =?',
                [
                    $nama_kategori,
                    $kode_kategori
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            DB::rollback();
            $data          = '2';
        }

        return redirect()->route('kategori')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function edit_stok_act(Request $request)
    {

        $this->validate($request, [
            'id_detail_produk'   => 'required',
            'kode_produk'        => 'required',
            'stok'               => 'required',


        ]);
        $id_detail_produk   = $request->post('id_detail_produk');
        $kode_produk        = $request->post('kode_produk');
        $stok               = $request->post('stok');
        $kode_warna         = $request->post('kode_warna');
        try {
            DB::update(
                'UPDATE detail_produk set 
                                        stok = ?,
                                        kode_warna = ?
                                      WHERE id_detail_produk = ?',
                [
                    $stok,
                    $kode_warna,
                    $id_detail_produk
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
            $data          = '2';
        }

        return $this->detail_produk($kode_produk);
    }

    public function edit_sub_kategori_act(Request $request)
    {

        $this->validate($request, [
            'kode_sub_kategori'  => 'required',
            'kode_kategori'      => 'required',
            'nama_sub_kategori'  => 'required'
        ]);
        // for input database

        $id_sub_kategori        = $request->post('id_sub_kategori');
        $kode_sub_kategori     = $request->post('kode_sub_kategori');
        $nama_sub_kategori     = $request->post('nama_sub_kategori');
        $kode_kategori         = $request->post('kode_kategori');
        try {
            DB::update(
                'UPDATE sub_kategori set 
                                        nama_sub_kategori = ?,
                                        kode_kategori = ?
                                      WHERE id_sub_kategori =?',
                [
                    $nama_sub_kategori,
                    $kode_kategori, $id_sub_kategori
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            DB::rollback();
            $data          = '2';
        }

        return redirect()->route('kategori2')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function edit_child_kategori_act(Request $request)
    {

        $this->validate($request, [
            'nama_sub_kategori2'  => 'required',
            'id_sub_kategori2'      => 'required',
            'kode_sub_kategori'      => 'required',
            'kode_kategori'  => 'required'
        ]);
        // for input database
        $nama_sub_kategori2  = $request->post('nama_sub_kategori2');
        $kode_sub_kategori  = $request->post('kode_sub_kategori');
        $kode_kategori      = $request->post('kode_kategori');
        $id_sub_kategori2   = $request->post('id_sub_kategori2');
        try {
            DB::update(
                'UPDATE sub_kategori2 set 
                                        nama_sub_kategori2 = ?,
                                        kode_kategori = ?
                                      WHERE id_sub_kategori2 =?',
                [
                    $nama_sub_kategori2,
                    $kode_kategori, $id_sub_kategori2
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
            $data          = '2';
        }

        return redirect()->route('kategori3')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }


    public function hapus_kategori($id)
    {
        try {
            DB::delete('DELETE FROM kategori WHERE id_kategori =?', [$id]);

            DB::commit();
            $data          = '3';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data           = '2';
            // something went wrong
        }

        return redirect()->route('kategori')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function hapus_warna($id)
    {
        try {
            DB::delete('DELETE FROM warna WHERE id_warna =?', [$id]);

            DB::commit();
            $data          = '3';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data           = '2';
            // something went wrong
        }

        return redirect()->route('warna')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function delete_det_produk($id)
    {
        $old = $this->produk->getDetProduk($id);
        foreach ($old as $row_old) {
            $kode_produk        = $row_old->kode_produk;
        }

        try {
            DB::delete('DELETE FROM detail_produk WHERE id_detail_produk =?', [$id]);

            DB::commit();
            $data          = '3';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data           = '2';
            // something went wrong
        }

        return $this->detail_produk($kode_produk);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function hapus_foto_produk($id)
    {
        $cek_produk = $this->produk->GetFotoInfo($id);

        foreach ($cek_produk as $row_cek) {
            $kode_produk = $row_cek->kode_produk;
        }

        $cek_detail_produk = $this->produk->get1detproduk($kode_produk);
        foreach ($cek_detail_produk as $res) {
            $id_detail_produk = $res->id_detail_produk;
        }

            try {
                DB::delete('DELETE FROM foto_produk WHERE id_foto_produk =?', [$id]);
                DB::commit();
                $data['save']           = '3';
            } catch (\Exception $e) {
                DB::rollback();
                $data['save']           = '2';
            }
            // return redirect()->route('foto_produk/' . $kode_produk);

        return $this->foto_produk($kode_produk);
    }


    public function hapus_sub_kategori($id)
    {
        try {
            DB::delete('DELETE FROM sub_kategori WHERE id_sub_kategori =?', [$id]);

            DB::commit();
            $data          = '3';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data           = '2';
            // something went wrong
        }

        return redirect()->route('kategori2')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function hapus_sub_kategori2($id)
    {
        try {
            DB::delete('DELETE FROM sub_kategori2 WHERE id_sub_kategori2 =?', [$id]);

            DB::commit();
            $data          = '3';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data           = '2';
            // something went wrong
        }

        return redirect()->route('kategori3')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function edit_warna_act(Request $request)
    {

        $this->validate($request, [
            'id_warna'   => 'required',
            'warna'      => 'required'
        ]);
        // data from edit
        $id_warna       = $request->post('id_warna');
        $warna          = $request->post('warna');
        $kode_warna    =     $request->post('kode_warna');


        try {
            DB::update(
                'UPDATE warna set 
                                        nm_warna = ?, 
                                        kode_warna = ?
                                      WHERE id_warna =?',
                [
                    $warna,
                    $kode_warna,
                    $id_warna
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            DB::rollback();
            $data          = '2';
        }

        return redirect()->route('warna')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function edit_satuan_act(Request $request)
    {

        $this->validate($request, [
            'id_satuan'   => 'required',
            'nm_satuan'   => 'required'
        ]);
        // data from edit
        $id_satuan    = $request->post('id_satuan');
        $nm_satuan    = $request->post('nm_satuan');


        try {
            DB::update(
                'UPDATE satuan set 
                                        nm_satuan = ?
                                        WHERE id_satuan =?',
                [
                    $nm_satuan,
                    $id_satuan
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            DB::rollback();
            $data          = '2';
        }

        return redirect()->route('satuan')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }



    public function add_user(Request $request)
    {
        $this->validate($request, [
            'id_anggota' => 'required',
            'username'   => 'required',
            'password'   => 'required'
        ]);


        // for input database
        $id_anggota     = $request->post('id_anggota');
        $username       = $request->post('username');
        $password       = Enkrip($request->post('password'));
        $save = DB::insert('insert into user (id_anggota,username,password) values (?, ?, ?)', [$id_anggota, $username, $password]);
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }
        // die($data['save']);


        // return view("user.list_user", ["data"=>$data]);
        return redirect()->route('user')->with(['data' => $data]);

        // return redirect()->route('home');

    }

    public function add_size(Request $request)
    {
        $this->validate($request, [
            'nm_size' => 'required',
            'flag_size'   => 'required'

        ]);


        // for input database
        $nm_size     = $request->post('nm_size');
        $flag_size       = $request->post('flag_size');
        $save = DB::insert('insert into size (nm_size,flag_size) values (?, ?)', [$nm_size, $flag_size]);
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return redirect()->route('size')->with(['data' => $data]);
    }

    public function add_produk(Request $request)
    {
        $this->validate($request, [
            'kd_produk' => 'required',
            'nama_produk'   => 'required',
            'id_kategori' => 'required'
        ]);
        // for input database
        $kd_produk     = $request->post('kd_produk');
        $nama_produk       = $request->post('nama_produk');
        $id_kategori       = $request->post('id_kategori');
        $save = DB::insert('insert into kd_produk (kd_produk,nama_produk,id_kategori,id_toko) values (?, ?, ?, ?)', [$kd_produk, $nama_produk, $id_kategori, '1']);
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return redirect()->route('list_produk')->with(['data' => $data]);
    }

    public function add_back_produk_act(Request $request)
    {


        $this->validate($request, [
            'kode_produk'    => 'required',
            'nama_produk'    => 'required',
            'flag_warna'     => 'required',
            'harga_umum'     => 'required',
            'harga_anggota'  => 'required',
            'flag_grosir'    => 'required',
            'deskripsi'      => 'required',
            'harga_anggota'  => 'required',
        ]);

        // for input database    
        $kode_produk            = $request->post('kode_produk');
        $nama_produk            = $request->post('nama_produk');
        $kode_kategori          = $request->post('kode_kategori');
        $flag_warna             = $request->post('flag_warna');
        $harga_umum             = InsertRupiah($request->post('harga_umum'));
        $harga_anggota          = InsertRupiah($request->post('harga_anggota'));
        $flag_grosir            = $request->post('flag_grosir');
        $harga_grosir_umum      = InsertRupiah($request->post('harga_grosir_umum'));
        $harga_grosir_anggota   = InsertRupiah($request->post('harga_grosir_anggota'));
        // $kode_suplier           = $request->post('kode_suplier');
        $deskripsi              = $request->post('deskripsi');
        $kode_sub_kategori      = $request->post('kode_sub_kategori');
        $kode_sub_kategori2     = $request->post('kode_sub_kategori2');
        $minimum_grosir         = $request->post('minimum_grosir');

        //FOR UPLOAD FOTO
        // $image              = $request->file('foto');
        // $size               = $image->getSize();
        // $input['imagename'] = time().'.'.$image->extension();
        // $destinationPath    = public_path('/temp');
        // $img                = Image::make($image->path());

        // $img->resize(575, 521, function ($constraint) {

        //     $constraint->aspectRatio();

        // })->save($destinationPath.'/'.$input['imagename']);
        // $foto_nama          = $input['imagename'];

        $save = DB::insert(
            'insert into produk 
                                (kode_produk,
                                nama_produk,
                                kode_kategori,
                                kode_sub_kategori,
                                kode_sub_kategori2,
                                flag_warna,
                                flag_grosir,
                                deskripsi, 
                                harga_anggota, 
                                harga_umum, 
                                harga_grosir_anggota, 
                                minimum_grosir, 
                                harga_grosir_umum) 
                            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            [
                $kode_produk,
                $nama_produk,
                $kode_kategori,
                $kode_sub_kategori,
                $kode_sub_kategori2,
                $flag_warna,
                $flag_grosir,
                $deskripsi,
                $harga_anggota,
                $harga_umum,
                $harga_grosir_anggota,
                $minimum_grosir,
                $harga_grosir_umum
            ]
        );

        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }
        return redirect()->route('list_produk')->with(['data' => $data]);
    }
    public function cek_foto_utama($kode_produk)
    {
        $data_cek = $this->produk->foto_utama($kode_produk);
        if ($data_cek != null || !empty($data_cek)) {
            foreach ($data_cek as $row) {
                $id_foto_produk = $row->id_foto_produk;
                try {
                    DB::update(
                        'UPDATE foto_produk set 
                                                flag_utama = ?
                                              WHERE id_foto_produk =?',
                        [
                            '0',
                            $id_foto_produk
                        ]
                    );

                    DB::commit();
                } catch (\Exception $e) {
                    // DB::rollback();
                    throw $e;
                }
            }
        }
    }

    public function foto_utama_create($id)
    {
        $data_cek = $this->produk->GetFotoInfo($id);
        foreach ($data_cek as $row) {
            $kode_produk = $row->kode_produk;
            $this->cek_foto_utama($kode_produk);
        }

        $cek_detail_produk = $this->produk->get1detproduk($kode_produk);
        foreach ($cek_detail_produk as $res) {
            $id_detail_produk = $res->id_detail_produk;
        }

        try {
            DB::update(
                'UPDATE foto_produk set 
                                                flag_utama = ?
                                              WHERE id_foto_produk =?',
                [
                    '1',
                    $id
                ]
            );

            DB::commit();
        } catch (\Exception $e) {
            // DB::rollback();
            throw $e;
        }

        return $this->foto_produk($kode_produk);
    }

    public function add_foto_produk(Request $request)
    {

        // for input database    
        $foto               = $request->file('foto');
        $foto_utama         = $request->post('foto_utama');
        $kode_produk        = $request->post('kode_produk');
        

        if ($foto_utama == 'on') {
            $this->cek_foto_utama($kode_produk);
            $flag_utama = '1';
        } else {
            $this->cek_foto_utama($kode_produk);
            $flag_utama = '0';
        }
        // FOR UPLOAD FOTO
        $image              = $request->file('foto');
        $size               = $image->getSize();
        $input['imagename'] = time() . '.' . $image->extension();
        $destinationPath    = public_path('/temp');
        $img                = Image::make($image->path());

        $img->resize(276, 357, function ($constraint) {

            $constraint->aspectRatio();
        })->save($destinationPath . '/' . $input['imagename']);
        $foto_nama  = $input['imagename'];

        $save = DB::insert(
            'insert into foto_produk 
                                (kode_produk, 
                                foto, flag_utama) 
                            values (?, ?, ?)',
            [
                $kode_produk,
                $foto_nama,
                $flag_utama
            ]
        );

        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return $this->foto_produkspl($kode_produk);
      

    }

    public function add_foto_produk_spl(Request $request)
    {

        // for input database    
        $foto               = $request->file('foto');
        $foto_utama         = $request->post('foto_utama');
        $kode_produk        = $request->post('kode_produk');
        

        if ($foto_utama == 'on') {
            $this->cek_foto_utama($kode_produk);
            $flag_utama = '1';
        } else {
            $this->cek_foto_utama($kode_produk);
            $flag_utama = '0';
        }
        // FOR UPLOAD FOTO
        $image              = $request->file('foto');
        $size               = $image->getSize();
        $input['imagename'] = time() . '.' . $image->extension();
        $destinationPath    = public_path('/temp');
        $img                = Image::make($image->path());

        $img->resize(276, 357, function ($constraint) {

            $constraint->aspectRatio();
        })->save($destinationPath . '/' . $input['imagename']);
        $foto_nama  = $input['imagename'];

        $save = DB::insert(
            'insert into foto_produk 
                                (kode_produk, 
                                foto, flag_utama) 
                            values (?, ?, ?)',
            [
                $kode_produk,
                $foto_nama,
                $flag_utama
            ]
        );

        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return $this->foto_produk($kode_produk);
       

    }


    public function add_stok_produk(Request $request)
    {


        $this->validate($request, [
            'kode_produk'    => 'required',
            'stok'     => 'required'
        ]);

        // for input database    
        $kode_produk    = $request->post('kode_produk');
        $kode_warna     = $request->post('kode_warna');
        $stok           = $request->post('stok');

        $save = DB::insert(
            'insert into detail_produk 
                                (kode_produk,
                                kode_warna,
                                stok) 
                            values (?, ?, ?)',
            [
                $kode_produk,
                $kode_warna,
                $stok
            ]
        );

        if ($save == TRUE) {
            $data['data_save']  = '1';
        } else {
            $data['data_save']  = '2';
        }

        return $this->detail_produk($kode_produk);
    }

    public function add_det_produk(Request $request)
    {
        $this->validate($request, [
            'id_satuan'   => 'required'
        ]);
        // for input database
        $id_kd_produk   = $request->post('id_kd_produk');
        $nama_produk    = $request->post('nama_produk');
        $id_kategori    = $request->post('id_kategori');
        $id_warna       = $request->post('id_warna');
        $id_satuan      = $request->post('id_satuan');
        $id_size        = $request->post('id_size');
        $berat          = $request->post('berat');
        $volume         = $request->post('volume');
        $deskripsi      = $request->post('deskripsi');
        $stok           = $request->post('stok');



        $save = DB::insert(
            'insert into produk (
                                id_kd_produk,
                                id_warna,
                                id_satuan,
                                id_size,
                                berat,
                                volume,
                                stok,
                                deskripsi)
                         values (?, ?, ?, ?, ?, ?, ?, ?)',
            [
                $id_kd_produk,
                $id_warna,
                $id_satuan,
                $id_size,
                $berat,
                $volume,
                $stok,
                $deskripsi
            ]
        );
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return redirect()->to('detail_back_produk/' . $id_kd_produk);
    }

    public function add_satuan(Request $request)
    {
        $this->validate($request, [
            'nm_satuan' => 'required',
        ]);


        // for input database
        $nm_satuan     = $request->post('nm_satuan');
        $save = DB::insert('insert into satuan (nm_satuan) values (?)', [$nm_satuan]);
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return redirect()->route('satuan')->with(['data' => $data]);
    }

    public function add_kategori(Request $request)
    {
        $this->validate($request, [
            'kode_kategori' => 'required',
            'nama_kategori' => 'required'
        ]);
        // for input database
        $nama_kategori     = $request->post('nama_kategori');
        $kode_kategori     = $request->post('kode_kategori');

        $save = DB::insert('insert into kategori (nama_kategori, kode_kategori) values (?, ?)', [$nama_kategori, $kode_kategori]);
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return redirect()->route('kategori')->with(['data' => $data]);
    }

    public function add_sub_kategori(Request $request)
    {
        $this->validate($request, [
            'kode_sub_kategori' => 'required',
            'kode_kategori' => 'required',
            'nama_sub_kategori' => 'required'
        ]);
        // for input database
        $kode_sub_kategori     = $request->post('kode_sub_kategori');
        $nama_sub_kategori     = $request->post('nama_sub_kategori');
        $kode_kategori         = $request->post('kode_kategori');

        $save = DB::insert('insert into sub_kategori (kode_sub_kategori,kode_kategori, nama_sub_kategori) values (?, ?, ?)', [$kode_sub_kategori, $kode_kategori, $nama_sub_kategori]);
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return redirect()->route('kategori2')->with(['data' => $data]);
    }

    public function add_child_kategori(Request $request)
    {
        $this->validate($request, [
            'kode_sub_kategori2' => 'required',
            'kode_kategori'      => 'required',
            'kode_sub_kategori'  => 'required',
            'nama_sub_kategori2' => 'required'
        ]);
        // for input database
        $kode_sub_kategori2     = $request->post('kode_sub_kategori2');
        $kode_kategori          = $request->post('kode_kategori');
        $kode_sub_kategori      = $request->post('kode_sub_kategori');
        $nama_sub_kategori2     = $request->post('nama_sub_kategori2');

        $save = DB::insert('insert into sub_kategori2 (kode_sub_kategori2, kode_sub_kategori,kode_kategori, nama_sub_kategori2) values (?, ?, ?, ?)', [$kode_sub_kategori2, $kode_sub_kategori, $kode_kategori, $nama_sub_kategori2]);
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return redirect()->route('kategori3')->with(['data' => $data]);
    }


    public function add_warna(Request $request)
    {
        $this->validate($request, [
            'warna' => 'required'
        ]);


        // for input database
        $warna     = $request->post('warna');
        $kode_warna     = $request->post('kode_warna');
        $save = DB::insert('insert into warna (nm_warna,kode_warna) values (?,?)', [$warna, $kode_warna]);
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return redirect()->route('warna')->with(['data' => $data]);
    }
}
