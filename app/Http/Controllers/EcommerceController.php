<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Image;
use Illuminate\Support\Facades\DB;
use App\Models\Produk;
use App\Models\Eproduk;


use Illuminate\Support\Facades\Storage;




class EcommerceController extends Controller
{
   public function __construct()
   {
      $this->produk = new Produk();
      $this->eproduk = new Eproduk();

  if (session_status() !== PHP_SESSION_ACTIVE) session_start();
        if(isset($_SESSION['id_anggota'])){
          $this->id_anggota = $_SESSION['id_anggota'];
          $this->id_jabatan = $_SESSION['id_jabatan'];
        }else{
          $this->id_anggota = 'xx';
          $this->id_jabatan = 'xx';
        }
   }

   public function e_produk()
   {
      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      $data['list_eproduk']      = $this->eproduk->list_produk();
      
      $data['flag_edit']      = '0';
      $data['title']          = 'Managemen E-Commerce';
      $data['edit']           = '0';
      $data['Halaman']        = 'E-Commerce';
      $data['Sub_Halaman']    = 'Produk';
      $data['Active']         = 'e_produk';
      $data['menu']           = 'e-commerce';

      return view("produk.produk", ["data" => $data]);
   }

  

   public function eproduk2()
   {
      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      $data['list_eproduk']      = $this->eproduk->list_produk();
      
      $data['flag_edit']      = '0';
      $data['title']          = 'Managemen E-Commerce';
      $data['edit']           = '0';
      $data['Halaman']        = 'E-Commerce';
      $data['Sub_Halaman']    = 'Produk';
      $data['Active']         = 'e_produk';
      $data['menu']           = 'e-commerce';

      return view("welcome", ["data" => $data]);
   }

   public function dashboard()
   {
      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      $data['list_eproduk']      = $this->eproduk->list_produk();
      
      $data['flag_edit']      = '0';
      $data['title']          = 'Managemen E-Commerce';
      $data['edit']           = '0';
      $data['Halaman']        = 'E-Commerce';
      $data['Sub_Halaman']    = 'Produk';
      $data['Active']         = 'e_produk';
      $data['menu']           = 'e-commerce';

      return view("ecomm.dashboard", ["data" => $data]);
   }


   public function buy_produk($id)
   {
      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      $data['list_eproduk']      = $this->eproduk->list_produk();

      $data['flag_edit']      = '0';
      $data['title']          = 'Managemen E-Commerce';
      $data['edit']           = '0';
      $data['Halaman']        = 'E-Commerce';
      $data['Sub_Halaman']    = 'Produk';
      $data['Active']         = 'e_produk';
      $data['menu']           = 'e-commerce';

      return view("ecomm.detail_produk", ["data" => $data]);
   }

   public function register_ecomm()
   {
      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      $data['list_eproduk']      = $this->eproduk->list_produk();

      $data['flag_edit']      = '0';
      $data['title']          = 'Managemen E-Commerce';
      $data['edit']           = '0';
      $data['Halaman']        = 'E-Commerce';
      $data['Sub_Halaman']    = 'Produk';
      $data['Active']         = 'e_produk';
      $data['menu']           = 'e-commerce';
      $data['harga_total']    = 0;
      $data['item']      = 0;
      $data['list_kategori'] = $this->produk->list_kategori();
        
        $data['side_produk'] = $this->eproduk->side_produk();

      return view("ecomm.register", ["data" => $data]);
   }

   public function e_comm()
    {
      if(isset($_SESSION['id_anggota'])){
         $keranjang    = $this->eproduk->getKeranjang($this->id_anggota);
         $harga_total   = 0;
         $item     = 0;
         if($keranjang){
            foreach ($keranjang as $row) {
               $status_anggota   = $row->status_anggota;
               $flag_grosir      = $row->flag_grosir;

                  if($status_anggota == '1'){
                     $harga = $row->harga_anggota;
                  }else{
                     $harga = $row->harga_umum;
                  }

               $sub_total = $harga * $row->qty;
               $harga_total += $harga_total + $sub_total;
               $item ++; 

            }
            $data['harga_total']    = $harga_total;
            $data['item']           = $item;

         }
         $data['harga_total']    = $harga_total;
         $data['item']      = $item; 
         
      }else{
         $data['harga_total']    = 0;
         $data['item']      = 0; 
      }
        $data['list_kategori'] = $this->produk->list_kategori();
        $data['new_produk'] = $this->eproduk->new_produk();
        $data['count_new_produk'] = count($this->eproduk->new_produk());
        $data['list_produk'] = $this->eproduk->list_produk();
        $data['side_produk'] = $this->eproduk->side_produk();

        $data['produk'] = DB::select("SELECT a.kode_produk, a.nama_produk, a.kode_kategori, a.kode_sub_kategori, 
                     a.kode_sub_kategori2,e.stok, f.foto, a.harga_umum, a.harga_anggota, 
                     a.harga_grosir_anggota, a.harga_grosir_umum, a.flag_grosir, 
                     a.minimum_grosir
                               FROM produk AS a
                     JOIN kategori AS b ON a.kode_kategori = b.kode_kategori
                     LEFT JOIN sub_kategori AS c ON a.kode_sub_kategori = c.kode_sub_kategori
                     LEFT JOIN sub_kategori2 AS d ON a.kode_sub_kategori2 = d.kode_sub_kategori2
                     LEFT JOIN (
                        SELECT SUM(aa.stok) AS stok, aa.kode_produk FROM detail_produk AS aa 
                        JOIN produk AS bb ON aa.kode_produk = bb.kode_produk 
                        GROUP BY aa.kode_produk
                     ) AS e ON a.kode_produk = e.kode_produk
                     LEFT JOIN (
                        SELECT ab.foto, ab.kode_produk FROM foto_produk AS ab
                        JOIN produk AS ac ON ab.kode_produk = ac.kode_produk
                        WHERE ab.flag_utama = '1' 
                        GROUP BY ab.kode_produk, ab.foto
                     ) AS f ON a.kode_produk = f.kode_produk
                     
                     ");

        return view('ecomm.produk', ["data" => $data]);
        // return view('ecomm.detail', ["data" => $data]);
    }

    public function add_to_cart(Request $request)
    {
      
      $qty           = $request->qty;
      $kode_produk   = $request->kode_produk;
      $no_agt    = $this->eproduk->getAnggota($this->id_anggota);
      foreach ($no_agt as $r_agt) {
         $no_anggota = $r_agt->no_anggota;  
      }
      try {
             DB::insert('insert into keranjang_belanja (no_anggota,kode_produk,qty,status) values (?, ?, ?, ?)', [$no_anggota, $kode_produk, $qty,'0']);

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            DB::rollback();
            $data          = '2';
        }
        return redirect()->route('e_comm');
    }

  

    public function ecom_regist(Request $request)
    {
      $nama             = $request->nama;
      $jenis_kelamin    = $request->jenis_kelamin;
      $alamat           = $request->alamat;
      $email            = $request->email;
      $no_hp            = $request->no_hp;
      $username         = $request->username;
      $password         = $request->password;
      $password2        = $request->password2;
      if($password != $password2){
         die('password berbeda');
      }
      
      $no_anggota = strtotime('Y-m-d H:i:s');
      try {
             DB::insert('insert into anggota (no_anggota,nama_anggota,alamat,status,
                        jenis_kelamin,no_hp

                        ) values (?, ?, ?, ?, ?, ?)', [$no_anggota, $nama, $alamat,'0',$jenis_kelamin,$no_hp]);
             $id = DB::getPdo()->lastInsertId();
             DB::insert('insert into user (username,password,id_anggota
                        ) values (?, ?, ?)', [$username, Enkrip($password), $id]);
            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
         throw $e;
            DB::rollback();
            $data          = '2';
        }
        return redirect()->route('e_comm');
    }

    public function e_ecom_detail($kode_produk)
    {
      if(isset($_SESSION['id_anggota'])){
         $keranjang    = $this->eproduk->getKeranjang($this->id_anggota);
         $harga_total   = 0;
         $item     = 0;
         if($keranjang){
            foreach ($keranjang as $row) {
               $status_anggota   = $row->status_anggota;
               $flag_grosir      = $row->flag_grosir;

                  if($status_anggota == '1'){
                     $harga = $row->harga_anggota;
                  }else{
                     $harga = $row->harga_umum;
                  }

               $sub_total = $harga * $row->qty;
               $harga_total += $harga_total + $sub_total;
               $item ++; 

            }
            $data['harga_total']    = $harga_total;
            $data['item']           = $item;

         }
         $data['harga_total']    = $harga_total;
         $data['item']      = $item; 
         
      }else{
         $data['harga_total']    = 0;
         $data['item']      = 0; 
      }
      
      $data['list_kategori'] = $this->produk->list_kategori();
        $data['side_produk'] = $this->eproduk->side_produk();
        $detProduk = $this->eproduk->detProduk($kode_produk);
        foreach($detProduk as $row){
            $data['kode_produk']          = $row->kode_produk;
            $data['nama_produk']          = $row->nama_produk;
            $data['harga_anggota']        = $row->harga_anggota;
            $data['harga_umum']           = $row->harga_umum;
            $data['harga_grosir_anggota'] = $row->harga_grosir_anggota;
            $data['harga_grosir_umum']    = $row->harga_grosir_umum;
            $data['flag_grosir']          = $row->flag_grosir;
            $data['minimum_grosir']       = $row->minimum_grosir;
            $data['kode_kategori']        = $row->kode_kategori;
            $data['kode_sub_kategori']    = $row->kode_sub_kategori;
            $data['kode_sub_kategori2']   = $row->kode_sub_kategori2;
            $data['nama_kategori']        = $row->nama_kategori;
            $data['nama_sub_kategori']    = $row->nama_sub_kategori;
            $data['nama_sub_kategori2']   = $row->nama_sub_kategori2;
            $data['deskripsi']            = $row->deskripsi; 
        }
        $data['foto']                 = $this->eproduk->fotoProduk($kode_produk);
        $data['id_anggota']           =  $this->id_anggota;
        
         return view('ecomm.detail', ["data" => $data]);
    }

    public function checkout()
    {
      if(isset($_SESSION['id_anggota'])){
         $keranjang    = $this->eproduk->getKeranjang($this->id_anggota);
         $harga_total   = 0;
         $item     = 0;
         if($keranjang){
            foreach ($keranjang as $row) {
               $status_anggota   = $row->status_anggota;
               $flag_grosir      = $row->flag_grosir;

                  if($status_anggota == '1'){
                     $harga = $row->harga_anggota;
                  }else{
                     $harga = $row->harga_umum;
                  }

               $sub_total = $harga * $row->qty;
               $harga_total += $harga_total + $sub_total;
               $item ++; 

            }
            $data['harga_total']    = $harga_total;
            $data['item']           = $item;

         }
         $data['harga_total']    = $harga_total;
         $data['item']      = $item; 
         
      }else{
         $data['harga_total']    = 0;
         $data['item']      = 0; 
      }
      
      $data['list_kategori'] = $this->produk->list_kategori();
        $data['side_produk'] = $this->eproduk->side_produk();
       
        $data['id_anggota']           =  $this->id_anggota;
        
         return view('ecomm.checkout', ["data" => $data]);
    }
}
