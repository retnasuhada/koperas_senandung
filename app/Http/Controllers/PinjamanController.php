<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\Anggota;
use App\Models\Pinjaman;



class PinjamanController extends Controller
{
    public function __construct()
    {
        $this->Anggota = new Anggota();
        $this->pinjaman = new Pinjaman();
    }



    public function jenis_pinjaman()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }

        $data['jenis_pinjaman'] = $this->pinjaman->jenis_pinjaman();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pinjaman';
        $data['Halaman']        = 'Pinjaman';
        $data['Sub_Halaman']    = 'Daftar Pinjaman';
        $data['Active']         = 'jenis_pinjaman';
        $data['menu']           = 'master';
        return view("pinjaman.jenis_pinjaman", ["data" => $data]);
    }



    public function edit_jenis_pinjaman($id)
    {
        $data['jenis_pinjaman'] = $this->pinjaman->jenis_pinjaman();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['old']            = $this->pinjaman->getJenisPinjaman($id);
        $data['title']          = 'Halaman Pinjaman';
        $data['Halaman']        = 'Pinjaman';
        $data['Sub_Halaman']    = 'Daftar Pinjaman';
        $data['Active']         = 'jenis_pinjaman';
        $data['menu']           = 'master';

        foreach ($data['old'] as $row) {
            $data['id_jenis_pinjaman']      = $row->id_jenis_pinjaman;
            $data['nama_jenis_pinjaman']    = $row->nama_jenis_pinjaman;
            $data['status']                 = $row->status;
            $data['keterangan']             = $row->keterangan;
            $data['margin']                 = $row->margin;
            $data['max_pinjaman']           = $row->max_pinjaman;
            $data['max_tenor']              = $row->max_tenor;
        }

        return view("pinjaman.jenis_pinjaman", ["data" => $data]);
    }

    public function add_jenis_pinjaman(Request $request)
    {
        $this->validate($request, [
            'nama_pinjaman'    => 'required',
            'margin'                 => 'required|max:5',
            'keterangan'             => 'required',
            'status'                 => 'required',
            'max_pinjaman'           => 'required',
            'max_tenor'              => 'required'

        ]);

        $nama_jenis_pinjaman    = $request->nama_pinjaman;
        $margin                 = $request->margin;
        $keterangan             = $request->keterangan;
        $status                 = $request->status;
        $max_pinjaman           = InsertRupiah($request->max_pinjaman);
        $max_tenor              = $request->max_tenor;
        $data['title']          = 'Halaman Pinjaman';

        // print_r($max_pinjaman);
        // die();

        $save = DB::insert('insert into jenis_pinjaman (nama_jenis_pinjaman,margin,keterangan, status, max_pinjaman, max_tenor) values (?, ?, ?, ?, ?, ?)', [$nama_jenis_pinjaman, $margin, $keterangan, $status, $max_pinjaman, $max_tenor]);
        if ($save == false) {
            $data    = '0';
        } elseif ($save == true) {
            $data   = '1';
        }

        return redirect()->route('jenis_pinjaman')->with(['data' => $data]);
    }

    public function edit_jenis_pinjaman_act(Request $request)
    {

        $data['flag_edit']          = '0';
        $data['title']              = 'Data Pinjaman';

        $this->validate($request, [
            'nama_pinjaman'    => 'required',
            'margin'                 => 'required',
            'keterangan'             => 'required',
            'status'                 => 'required',
            'max_pinjaman'           => 'required',
            'max_tenor'              => 'required'

        ]);

        $nama_jenis_pinjaman    = $request->nama_pinjaman;
        $margin                 = $request->margin;
        $keterangan             = $request->keterangan;
        $status                 = $request->status;
        $max_pinjaman           = InsertRupiah($request->max_pinjaman);
        $max_tenor              = $request->max_tenor;
        $id_jenis_pinjaman      = $request->id_jenis_pinjaman;

        try {
            DB::update(
                'UPDATE jenis_pinjaman set 
                                    nama_jenis_pinjaman = ?, 
                                    margin = ?,
                                    keterangan = ?, 
                                    status = ?,
                                    max_pinjaman = ?, 
                                    max_tenor = ?
                                  WHERE id_jenis_pinjaman =?',
                [
                    $nama_jenis_pinjaman,
                    $margin,
                    $keterangan,
                    $status,
                    $max_pinjaman,
                    $max_tenor,
                    $id_jenis_pinjaman
                ]
            );

            DB::commit();
            $data          = '1';
        } catch (\Exception $e) {
            DB::rollback();
            $data           = '2';
        }

        return redirect()->route('jenis_pinjaman')->with(['data' => $data]);
    }


    public function hapus_jenis_pinjaman($id)
    {
        try {
            DB::delete('DELETE FROM jenis_pinjaman WHERE id_jenis_pinjaman =?', [$id]);

            DB::commit();
            $data           = '3';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data           = '2';
            // something went wrong
        }


        return redirect()->route('jenis_pinjaman')->with(['data' => $data]);
    }
}
