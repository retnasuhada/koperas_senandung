<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
  
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\Anggota;
use App\Models\Simpanan;

  
  
class ArusKasController extends Controller
{
     public function __construct(){
        $this->Anggota = new Anggota();
        // $this->simpanan = new Simpanan();

    }
    

    public function index()
    {       
        if (Session::get('data')!=NUll OR Session::get('data')!="") {
          $data['save'] = Session::get('data');

       }else{
        $data['save']           = '0';
       }
        $data['arus_kas']       = $this->simpanan->list_sw('PV-K002'); 
        $data['title']          = 'Simpanan Wajib';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan wajib';
        $data['Active']         = 'list_sw';
        $data['menu']           ='simpanan';


        return view("simpanan", ["data"=>$data]);
    }

    
    public function edit_jenis_simpanan($id)
    {
        $data['jenis_simpanan'] = $this->simpanan->jenis_simpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Data Simpanan';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'jenis_simpanan';
        $data['Active']         = 'jenis_simpanan';
        $data['menu']           ='master';

        $data['old']            = $this->simpanan->getJenisSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['nama_jenis_simpanan'] = $row->nama_jenis_simpanan;
            $data['id_jenis_simpanan']   = $row->id_jenis_simpanan;
            $data['status']              = $row->status;
        }
       
        return view("simpanan.jenis_simpanan", ["data"=>$data]);
    }

    public function edit_sw_act(Request $request)
    {
        $data['flag_edit']          = '0';
        $data['title']              = 'Data Simpanan';

        $this->validate($request,[
           'id_anggota'  => 'required',
           'jumlah_simpanan'  => 'required',
           'tgl_simpanan'  => 'required',
           'jenis_penerimaan'  => 'required']);

        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $tgl_simpanan       = date('Y-m-d',strtotime($request->tgl_simpanan));
        $unik_kode          = $request->unik_kode;
        $jenis_penerimaan   = $request->jenis_penerimaan;
        $kode_akun          = $jenis_penerimaan;
        $kode_akun_asal     = 'PV-K002';
        $bulan_periode      = $request->bulan_periode;
        $tahun_periode      = $request->tahun_periode;
        $id_simpanan        = $request->id_simpanan;
        $periode            = $request->bulan_periode."-".$request->tahun_periode;
        
        $getTransaksi       = $this->simpanan->getTransaksi($unik_kode);
        foreach($getTransaksi as $row_transaksi){
            $id_transaksi = $row_transaksi->id_transaksi;
            $nominal_transaksi = $row_transaksi->nominal_transaksi;
            $min_jumlah_simpanan = 0 - $nominal_transaksi;
        }
       
        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach($data_anggota as $res){
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota." / ".$no_anggota;
        $keterangan         = "Simpanan wajib ".$nama_anggota." periode ".$periode;
        $keterangan_rek     = "Rekonsiloasi Simpanan wajib ".$nama_anggota." periode ".$periode; 

        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach($saldo_akun_tujuan as $arr_tujuan){
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + ($min_jumlah_simpanan + $jumlah_simpanan);

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach($saldo_akun_asal as $arr_asal){
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + ($min_jumlah_simpanan + $jumlah_simpanan);


        DB::beginTransaction();
        try{
            DB::update('UPDATE simpanan set 
                                    id_anggota = ?, 
                                    tgl_simpanan = ?,
                                    jumlah_simpanan = ?, 
                                    id_jenis_penerimaan = ?,
                                    periode_simpanan = ?

                                  WHERE id_simpanan =?'
                                , [$id_anggota, 
                                    $tgl_simpanan, 
                                    $jumlah_simpanan,
                                    $kode_akun,
                                    $periode,
                                    $id_simpanan]);
            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $min_jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan_rek, $unik_kode, $id_transaksi]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan, $unik_kode, $id_transaksi]);

            // DB::update('UPDATE jenis_penerimaan set total_saldo = ? WHERE id_jenis_penerimaan =?', [$new_saldo, $jenis_penerimaan]);

            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);

            DB::commit();
                $data['success'] = '1';
                $data['title']      = 'Data Simpanan';
                $data['message'] = 'Berhasil Disimpan';
                return redirect()->route('simpanan_wajib_list');
            } catch (\Exception $e) {
            DB::rollback();
             // throw $e;
                $data['success'] = '0';
                $data['title']      = 'Data Simpanan';
                $data['message'] = 'Gagal Menyimpan Data !';
                return redirect()->route('edit_simpanan/'.$id_simpanan);
            
        }
    }


}
  
  