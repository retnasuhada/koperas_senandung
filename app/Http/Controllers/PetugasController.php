<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\Petugas;
use App\Models\Jabatan;
use App\Models\Anggota;

use Illuminate\Support\Facades\Storage;




class PetugasController extends Controller
{
    public function __construct()
    {
        $this->petugas = new Petugas();
        $this->jabatan = new Jabatan();
        $this->anggota = new Anggota();
    }



    public function petugas()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_petugas']   = $this->petugas->list_petugas();
        $data['list_jabatan']   = $this->jabatan->list_jabatan();
        $data['list_anggota']   = $this->anggota->list_anggota();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Petugas';
        $data['edit']           = '0';
        $data['Halaman']        = 'Petugas';
        $data['Sub_Halaman']    = 'Daftar Petugas';
        $data['Active']         = 'petugas';
        $data['menu']           = 'master';
        return view("petugas.list_petugas", ["data" => $data]);
    }



    public function edit_petugas($id)
    {
        $data['list_petugas']   = $this->petugas->list_petugas();
        $data['list_jabatan']   = $this->jabatan->list_jabatan();
        $data['list_anggota']   = $this->anggota->list_anggota();
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Petugas';
        $data['edit']           = '1';
        $data['save']           = '0';
        $data['Halaman']        = 'Petugas';
        $data['Sub_Halaman']    = 'Daftar Petugas';
        $data['Active']         = 'petugas';
        $data['menu']           = 'master';
        $data['old']            = $this->petugas->getPetugas($id);

        foreach ($data['old'] as $row) {
            $data['id_petugas']     = $row->id_petugas;
            $data['id_anggota']     = $row->id_anggota;
            $data['id_jabatan']     = $row->id_jabatan;
        }
        return view("petugas.list_petugas", ["data" => $data]);
    }

    public function hapus_petugas($id)
    {


        try {
            DB::delete('DELETE FROM petugas WHERE id_petugas =?', [$id]);

            DB::commit();
            $data          = '3';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data           = '2';
            // something went wrong
        }
        return redirect()->route('petugas')->with(['data' => $data]);
    }

    public function edit_petugas_act(Request $request)
    {

        $this->validate($request, [
            'id_anggota'     => 'required',
            'id_jabatan'   => 'required'
        ]);
        $id_anggota     = $request->post('id_anggota');
        $id_jabatan     = $request->post('id_jabatan');
        $id_petugas     = $request->post('id_petugas');
        // print_r($id_petugas);
        // die();
        //  $update = DB::update('UPDATE petugas set id_anggota=?, id_jabatan=? WHERE id_petugas=?', [$id_anggota,$id_jabatan,$id_petugas]);
        //  print_r($update);
        //  die();
        try {
            DB::update('UPDATE petugas set 
                                        id_anggota = ?, 
                                        id_jabatan = ?
                                      WHERE id_petugas =?', [
                $id_anggota,
                $id_jabatan,
                $id_petugas
            ]);

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            DB::rollback();
            $data           = '2';
        }


        return redirect()->route('petugas')->with(['data' => $data]);
    }

    public function add_petugas(Request $request)
    {
        $this->validate($request, [
            'id_anggota'     => 'required',
            'id_jabatan'   => 'required'
        ]);

        $id_anggota   = $request->post('id_anggota');
        $id_jabatan   = $request->post('id_jabatan');

        $save = DB::insert('insert into petugas (id_anggota,id_jabatan,status) values (?, ?, ?)', [$id_anggota, $id_jabatan, '1']);
        if ($save == TRUE) {
            $data          = '1';
        } else {
            $data           = '2';
        }

        return redirect()->route('petugas')->with(['data' => $data]);
    }
}
