<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\M_User;
use App\Models\Anggota;
use App\Models\Laporan;
use App\Exports\LaporanPenjualanExport;
use Maatwebsite\Excel\Facades\Excel;


use Illuminate\Support\Facades\Storage;




class LaporanController extends Controller
{
   public function __construct()
   {

      $this->laporan = new Laporan();
      $this->anggota = new Anggota();
   }

   public function laporan_pembelian()
   {

      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      // $data['list_suplier']      = $this->suplier->list_suplier();
      // $data['list_anggota']   = $this->anggota->list_anggota();
      $data['cetak']      = '0';
      $data['title']          = 'Laporan Pembelian';
      $data['edit']           = '0';
      $data['Halaman']        = 'Laporan';
      $data['Sub_Halaman']    = 'Laporan Pembelian';
      $data['Active']         = 'laporan_pembelian';
      $data['menu']           = 'laporan';

      return view("laporan.laporan_pembelian", ["data" => $data]);
   }

   public function laporan_penjualan()
   {

      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      // $data['list_suplier']      = $this->suplier->list_suplier();
      // $data['list_anggota']   = $this->anggota->list_anggota();
      $data['cetak']      = '0';
      $data['title']          = 'Laporan Penjualan';
      $data['edit']           = '0';
      $data['Halaman']        = 'Laporan';
      $data['Sub_Halaman']    = 'Laporan Penjualan';
      $data['Active']         = 'laporan_penjualan';
      $data['menu']           = 'laporan';


      return view("laporan.laporan_penjualan", ["data" => $data]);
   }

  
   public function cetak_lap_pembelian(Request $request)
   {

      $jenis_laporan = $request->jenis_laporan;
      $start         = convertTgl($request->start);
      $end           =convertTgl($request->end);
      
      $data['lap_pembelian'] = $this->laporan->GetLaporaPembelian($jenis_laporan,$start,$end);
      
      $data['cetak']      = '1';
      $data['title']          = 'Laporan Pembelian';
      $data['edit']           = '0';
      $data['Halaman']        = 'Laporan';
      $data['Sub_Halaman']    = 'Laporan Pembelian';
      $data['Active']         = 'laporan_pembelian';
      $data['menu']           = 'laporan';

      return view("laporan.laporan_pembelian", ["data" => $data]);
   }

   public function cetak_lap_penjualan(Request $request)
   {

      
      $start         = convertTgl($request->start);
      $end           =convertTgl($request->end);
       
      $laporan_penjualan = $this->laporan->GetLaporaPenjualan($start,$end);
      $filename = 'Laporan Penjualan Periode '.$start.' s-d '.$end. '.xlsx';
      $data=array();
      $i=0;
      foreach($laporan_penjualan as $row){
         $data[$i] = [
                'no'                => $i+1,
                'tanggal_penjualan' => $row->tanggal_penjualan,
                'nama_anggota'      => $row->nama_anggota,
                'nama_produk'       => $row->nama_produk,
                'jumlah'            => $row->jumlah,
                'harga_produk'      => $row->harga_produk,
                'total_h_jual'      => $row->total_h_jual,
                'harga_pendapatan'  => $row->harga_pendapatan,
                't_harga_pendapatan'=> $row->t_harga_pendapatan
                 ];
         $i++;
      }

      return Excel::download(new \App\Exports\LaporanPenjualanExport($data),$filename);
     
   }

   public function detail_suplier($kode_suplier)
   {

      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      $data['kode_suplier'] = $kode_suplier;
      $old = $this->suplier->get_suplier($kode_suplier);
      $data['list_anggota'] = $this->anggota->list_anggota();
      foreach($old as $row){
         $data['kode_suplier']   = $row->kode_suplier;
         $data['nama_suplier']   = $row->nama_suplier;
         $data['no_hp']          = $row->no_hp;
         $data['telp']           = $row->telp;
         $data['alamat']         = $row->alamat;
         $data['deskripsi']      = $row->deskripsi;
         $data['no_anggota']     = $row->no_anggota;
      }
      $data['produk_suplier'] = $this->suplier->produk_suplier($kode_suplier);
      $data['invoice_suplier'] = $this->suplier->invoice_suplier($kode_suplier);

      $data['flag_edit']      = '1';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'Edit Supier';
      $data['Active']         = 'suplier';
      $data['menu']           = 'toko';

      return view("suplier.detail_suplier", ["data" => $data]);
   }

   public function add_suplier(Request $request)
   {

      $kode_suplier  = $request->kode_suplier;
      $nama_suplier  = $request->nama_suplier;
      $no_hp         = $request->no_hp;
      $no_telp       = $request->no_telp;
      $alamat        = $request->alamat;
      $rating        = $request->rating;
      $status        = $request->status;
      $join_date     = date('Y-m-d', strtotime($request->join_date));
      $deskripsi     = $request->deskripsi;

      try {
         DB::insert('INSERT INTO suplier (kode_suplier, nama_suplier, no_hp, telp, alamat, rating, status, join_date, deskripsi) values (?,?,?,?,?,?,?,?,?)', [$kode_suplier, $nama_suplier, $no_hp, $no_telp, $alamat, $rating, $status, $join_date, $deskripsi]);

         DB::commit();
         $data           = '3';
         // all good
      } catch (\Exception $e) {
         // throw $e;
         DB::rollback();
         $data           = '2';
         // something went wrong
      }
      return redirect()->route('suplier');

      // return redirect() $this->suplier();
   }

   public function edit_suplier_act(Request $request)
   {

      $kode_suplier  = $request->kode_suplier;
      $nama_suplier  = $request->nama_suplier;
      $no_hp         = $request->no_hp;
      $no_telp       = $request->no_telp;
      $alamat        = $request->alamat;
      
      $status        = $request->status;
      $no_anggota    = $request->no_anggota;
      $deskripsi     = $request->deskripsi;

      try {
          DB::update('UPDATE suplier set 
                                    nama_suplier = ?, 
                                    no_hp = ?,
                                    telp = ?, 
                                    alamat = ?,
                                    status = ?,
                                    no_anggota = ?, 
                                    deskripsi = ?
                                  WHERE kode_suplier =?'
                                , [$nama_suplier, 
                                    $no_hp,
                                    $no_telp,
                                    $alamat, 
                                    $status,
                                    $no_anggota, 
                                    $deskripsi, 
                                    $kode_suplier]);

         DB::commit();
         $data           = '3';
         // all good
      } catch (\Exception $e) {
         throw $e;
         // DB::rollback();
         $data           = '2';
         // something went wrong
      }
      return redirect()->route('suplier');

      // return redirect() $this->suplier();
   }
}
