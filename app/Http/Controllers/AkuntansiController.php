<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\Akuntansi;
use App\Models\Simpanan;



class AkuntansiController extends Controller
{
    public function __construct()
    {
        $this->akuntansi = new Akuntansi();
        $this->simpanan = new Simpanan();
    }


    public function list_aset()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['aktiva']       = $this->akuntansi->list_aset(1);
        $data['pasiva']       = $this->akuntansi->list_aset(2);
        $data['total_aktiva']       = $this->akuntansi->TotalAset(1);
        $data['total_pasiva']       = $this->akuntansi->TotalAset(2);
        foreach ($data['total_aktiva'] as $aktiva) {
            $data['t_aktiva'] = $aktiva->total;
        }
        foreach ($data['total_pasiva'] as $pasiva) {
            $data['t_pasiva'] = $pasiva->total;
        }
        $data['title']          = 'Aset Koperasi';
        $data['Halaman']        = 'Aset Koperasi';
        $data['Sub_Halaman']    = 'Aset Koperasi';
        $data['Active']         = 'aset';
        $data['menu']           = 'akuntansi';


        return view("akuntansi.aset", ["data" => $data]);
    }

    public function dashboard()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['aktiva']       = $this->akuntansi->list_aset(1);
        $data['pasiva']       = $this->akuntansi->list_aset(2);
        $data['total_aktiva']       = $this->akuntansi->TotalAset(1);
        $data['total_pasiva']       = $this->akuntansi->TotalAset(2);
        foreach ($data['total_aktiva'] as $aktiva) {
            $data['t_aktiva'] = $aktiva->total;
        }
        foreach ($data['total_pasiva'] as $pasiva) {
            $data['t_pasiva'] = $pasiva->total;
        }
        $data['title']          = 'Aset Koperasi';
        $data['Halaman']        = 'Aset Koperasi';
        $data['Sub_Halaman']    = 'Aset Koperasi';
        $data['Active']         = 'null';
        $data['menu']           = 'null';


        return view("akuntansi.dash", ["data" => $data]);
    }

    public function jurnal_umum()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }

        $data['list_jurnal']     = $this->akuntansi->jurnal_umum();
        $data['title']          = 'Jurnal Umum';
        $data['Halaman']        = 'Jurnal Umum';
        $data['Sub_Halaman']    = 'Jurnal Umum';
        $data['Active']         = 'jurnal_umum';
        $data['menu']           = 'akuntansi';


        return view("akuntansi.jurnal_umum", ["data" => $data]);
    }

    public function getForm()
    {
        $data = $this->akuntansi->list_aset2(1, 2);

        return response()->json($data);
    }
    public function getForm2()
    {
        $data = $this->akuntansi->list_aset3();

        return response()->json($data);
    }

    public function add_jurnal_umum()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }

        $data['list_jurnal']     = $this->akuntansi->jurnal_umum();
        $data['title']          = 'Jurnal Umum';
        $data['Halaman']        = 'Jurnal Umum';
        $data['Sub_Halaman']    = 'Jurnal Umum';
        $data['Active']         = 'jurnal_umum';
        $data['menu']           = 'akuntansi';
        $data['flag_edit']      = '0';


        return view("akuntansi.form_jurnal_umum", ["data" => $data]);
    }
    public function add_aktiva()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['title']          = 'Aset Koperasi';
        $data['Halaman']        = 'Aset Koperasi';
        $data['Sub_Halaman']    = 'Aset Koperasi';
        $data['Active']         = 'aset';
        $data['menu']           = 'akuntansi';

        $data['jenis_aset']     = 'Aktiva';


        return view("akuntansi.add_aset", ["data" => $data]);
    }

    public function add_pasiva()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['title']          = 'Aset Koperasi';
        $data['Halaman']        = 'Aset Koperasi';
        $data['Sub_Halaman']    = 'Aset Koperasi';
        $data['Active']         = 'aset';
        $data['menu']           = 'akuntansi';

        $data['jenis_aset']     = 'Pasiva';


        return view("akuntansi.add_aset", ["data" => $data]);
    }

    public function add_detail_aset($id)
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['title']          = 'Aset Koperasi';
        $data['Halaman']        = 'Aset Koperasi';
        $data['Sub_Halaman']    = 'Aset Koperasi';
        $data['Active']         = 'aset';
        $data['menu']           = 'akuntansi';

        $data['jenis_aset']     = 'Aktiva';
        $data['info_aset']      = $this->akuntansi->getInfoAset($id);
        foreach ($data['info_aset'] as $row) {
            $data['nama_aset'] = $row->nama_akun;
            $data['kode_akun'] = $row->kode_akun;
        }


        return view("akuntansi.add_detail_aset", ["data" => $data]);
    }
    public function add_aset_act(Request $request)
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['title']          = 'Aset Koperasi';
        $data['Halaman']        = 'Aset Koperasi';
        $data['Sub_Halaman']    = 'Aset Koperasi';
        $data['Active']         = 'aset';
        $data['menu']           = 'akuntansi';

        $this->validate($request, [
            'kode_akun'  => 'required',
            'jenis_akun' => 'required',
            'nama_akun'  => 'required'
        ]);
        $kode_akun      = $request->kode_akun;
        $jenis_akun     = $request->jenis_akun;
        $nama_akun      = $request->nama_akun;
        if ($jenis_akun == '1' or $jenis_akun == '2') {
            $aktiva = '1';
        } else {
            $aktiva = '2';
        }
        $save = DB::insert(
            'insert into akun (  kode_akun,
                                                nama_akun,
                                                aktiva_pasiva,
                                                jenis)
                             values (?, ?, ?, ?)',
            [
                $kode_akun,
                $nama_akun,
                $aktiva,
                $jenis_akun
            ]
        );
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return redirect()->route('aset')->with(['data' => $data]);
    }

    public function add_detail_aset_act(Request $request)
    {
        $data['title']          = 'Aset Koperasi';
        $data['Halaman']        = 'Aset Koperasi';
        $data['Sub_Halaman']    = 'Aset Koperasi';
        $data['Active']         = 'aset';
        $data['menu']           = 'akuntansi';

        $this->validate($request, [
            'kode_akun'  => 'required',
            'nama'       => 'required',
            'no_aset'    => 'required',
            'deskripsi'  => 'required',
            'nilai'      => 'required'
        ]);

        $kode_akun  = $request->kode_akun;
        $nama       = $request->nama;
        $no_aset    = $request->no_aset;
        $deskripsi  = $request->deskripsi;
        $nilai      = InsertRupiah($request->nilai);



        $save = DB::insert(
            'insert into detail_akun (  kode_akun,
                                                nama_detail_akun,
                                                nilai,
                                                deskripsi,
                                                no_aset)
                             values (?, ?, ?, ?, ?)',
            [
                $kode_akun,
                $nama,
                $nilai,
                $deskripsi,
                $no_aset
            ]
        );
        if ($save == TRUE) {
            $data['save']           = '1';
        } else {
            $data['save']           = '2';
        }

        $data['detail_aset']    = $this->akuntansi->getDetailAset($kode_akun);
        $data['info_aset']      = $this->akuntansi->getInfoAset($kode_akun);
        foreach ($data['info_aset'] as $row) {
            $data['nama_aset'] = $row->nama_akun;
            $data['kode_akun'] = $row->kode_akun;
        }

        return redirect('detail_aset/' . $kode_akun);
        // return view("akuntansi.detail_aset", ["data"=>$data]);
    }

    public function add_arus_kas_act(Request $request)
    {
        $data['title']          = 'Arus Kas';
        $data['Halaman']        = 'Arus Kas';
        $data['Sub_Halaman']    = 'Arus Kas';
        $data['Active']         = 'A';
        $data['menu']           = 'akuntansi';

        $this->validate($request, [
            'jenis_transaksi'    => 'required',
            'kode_akun'          => 'required',
            'nominal'            => 'required',
            'tgl_transaksi'      => 'required',
            'keterangan'         => 'required'
        ]);

        $jenis_transaksi    = $request->jenis_transaksi;
        $kode_akun          = $request->kode_akun;
        $tgl_transaksi      = date('Y-m-d', strtotime($request->tgl_transaksi));
        $keterangan         = $request->keterangan;
        $nominal            = InsertRupiah($request->nominal);
        $created_at         = date('Y-m-d H:i:s');
        $unik_kode          = $kode_akun . "-" . strtotime(date('Y-m-d h:i:s'));

        $kode_kas = "AK-AL02";
        // cek saldo kas sebelum di update
        $saldo_kas  = $this->simpanan->getSaldoAkun($kode_kas);
        foreach ($saldo_kas as $arr_kas) {
            $kas = $arr_kas->nilai;
        }
        // kondisi jika jenis transaksi adalah penambahan kas
        if ($jenis_transaksi == '1') {
            // saldo kas + penambahan kas
            $new_kas = $kas + $nominal;
            // cek saldo akun sumber
            $saldo_akun  = $this->simpanan->getSaldoAkun($kode_akun);
            foreach ($saldo_akun as $arr_akun) {
                $akun = $arr_akun->nilai;
            }
            $new_akun = $akun - $nominal;
        } else {

            // saldo kas - pengeluaran kas
            $new_kas = $kas - $nominal;
            // cek saldo akun sumber
            $saldo_akun  = $this->simpanan->getSaldoAkun($kode_akun);
            foreach ($saldo_akun as $arr_akun) {
                $akun = $arr_akun->nilai;
            }
            $new_akun = $akun + $nominal;
        }

        DB::beginTransaction();
        try {

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, tujuan, asal) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', ['1', $nominal, $tgl_transaksi, $created_at, '110', $keterangan, $unik_kode, $kode_akun, $kode_kas]);

            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_kas, $kode_kas]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_akun, $kode_akun]);

            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Data Simpanan';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('jurnal_umum');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            $data['success'] = '0';
            $data['title']      = 'Data Simpanan';
            $data['message'] = 'Gagal Menyimpan Data !';
            return redirect()->route('add_jurnal_umum');
        }

        // return redirect('detail_aset/'.$kode_akun);
        // return view("akuntansi.detail_aset", ["data"=>$data]);
    }

    public function edit_jenis_simpanan($id)
    {
        $data['jenis_simpanan'] = $this->simpanan->jenis_simpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Data Simpanan';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'jenis_simpanan';
        $data['Active']         = 'jenis_simpanan';
        $data['menu']           = 'master';

        $data['old']            = $this->simpanan->getJenisSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['nama_jenis_simpanan'] = $row->nama_jenis_simpanan;
            $data['id_jenis_simpanan']   = $row->id_jenis_simpanan;
            $data['status']              = $row->status;
        }

        return view("simpanan.jenis_simpanan", ["data" => $data]);
    }
    public function detail_aset($id)
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['title']          = 'Aset Koperasi';
        $data['Halaman']        = 'Aset Koperasi';
        $data['Sub_Halaman']    = 'Aset Koperasi';
        $data['Active']         = 'aset';
        $data['menu']           = 'akuntansi';

        $data['detail_aset']    = $this->akuntansi->getDetailAset($id);
        $data['info_aset']      = $this->akuntansi->getInfoAset($id);
        foreach ($data['info_aset'] as $row) {
            $data['nama_aset'] = $row->nama_akun;
            $data['kode_akun'] = $row->kode_akun;
        }
        return view("akuntansi.detail_aset", ["data" => $data]);
    }
    public function edit_jenis_simpanan_act(Request $request)
    {

        $data['flag_edit']          = '0';
        // $data['save']               = '0';
        $data['title']              = 'Data Simpanan';

        $data['save']               = '0';
        $data['title']              = 'Data Simpanan';
        $data['Halaman']            = 'Halaman Simpanan';
        $data['Sub_Halaman']        = 'jenis_simpanan';
        $data['Active']             = 'jenis_simpanan';
        $data['menu']               = 'master';
        // return view("simpanan.jenis_simpanan", ["data"=>$data]);

        $this->validate($request, [
            'nama_simpanan'  => 'required',
            'status'         => 'required'
        ]);

        $nama_jenis_simpanan    = $request->nama_simpanan;
        $status                 = $request->status;
        $id_jenis_simpanan      = $request->id_jenis_simpanan;

        try {
            DB::update(
                'UPDATE jenis_simpanan set 
                                    nama_jenis_simpanan = ?, 
                                    status = ?
                                  WHERE id_jenis_simpanan =?',
                [
                    $nama_jenis_simpanan,
                    $status,
                    $id_jenis_simpanan
                ]
            );

            DB::commit();
            $data['save']           = '1';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data['save']           = '2';
            // something went wrong
        }
        $data['jenis_simpanan']     = $this->simpanan->jenis_simpanan();
        return view("simpanan.jenis_simpanan", ["data" => $data]);
    }

    public function hapus_jenis_simpanan($id)
    {
        try {
            DB::delete('DELETE FROM jenis_simpanan WHERE id_jenis_simpanan =?', [$id]);

            DB::commit();
            $data['save']           = '3';
        } catch (\Exception $e) {
            DB::rollback();
            $data['save']           = '2';
        }

        $data['jenis_simpanan']     = $this->simpanan->jenis_simpanan();
        $data['flag_edit']          = '0';
        // $data['save']               = '0';
        $data['title']              = 'Data Simpanan';
        // $data['title']          = 'Data Simpanan';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'jenis_simpanan';
        $data['Active']         = 'jenis_simpanan';
        $data['menu']           = 'simpanan';

        return view("simpanan.jenis_simpanan", ["data" => $data]);
    }
}
