<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\Anggota;
use App\Models\Simpanan;
use App\Models\Penjualan;
use PDF;
use Image;



use Illuminate\Support\Facades\Storage;




class AnggotaController extends Controller
{
    public function __construct()
    {
        $this->Anggota = new Anggota();
        $this->Penjualan = new Penjualan();
        $this->Simpanan = new Simpanan();
        if (session_status() !== PHP_SESSION_ACTIVE) session_start();
    }


    public function anggota()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }


        $new_kode = KodeAnggota();


        $data['list_anggota']   = $this->Anggota->list_anggota();
        $data['no_anggota']     = $new_kode;
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Anggota';
        $data['edit']           = '0';
        $data['Halaman']        = 'Anggota';
        $data['Sub_Halaman']    = 'Daftar Anggota';
        $data['Active']         = 'anggota';
        $data['menu']           = 'master';
        return view("anggota.list_anggota", ["data" => $data]);
    }
    

    public function add()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $y = date('Y');
        $m = date('m');
        $like = substr($y, 2) . $m;


        $new_kode = KodeAnggota();


        $data['no_anggota']     = $new_kode;
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Anggota';
        $data['edit']           = '0';
        $data['Halaman']        = 'Anggota';
        $data['Sub_Halaman']    = 'Daftar Anggota';
        $data['Active']         = 'anggota';
        $data['menu']           = 'master';
        return view("anggota.form_anggota", ["data" => $data]);
    }



    public function edit_anggota($id)
    {
        $data['list_anggota']   = $this->Anggota->list_anggota();
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Anggota';
        $data['save']           = '0';
        $data['Halaman']        = 'Anggota';
        $data['Sub_Halaman']    = 'Daftar Anggota';
        $data['Active']         = 'anggota';
        $data['menu']           = 'master';
        $data['old']            = $this->Anggota->getAnggota($id);

        foreach ($data['old'] as $row) {
            $data['no_anggota']     = $row->no_anggota;
            $data['id_anggota']     = $row->id_anggota;
            $data['nama_anggota']   = $row->nama_anggota;
            $data['nik']            = $row->nik;
            $data['alamat']         = $row->alamat;
            $data['join_date']      = $row->join_date;
            $data['jenis_kelamin']  = $row->jenis_kelamin;
            $data['status']         = $row->status;
            $data['foto']           = $row->foto;
            $data['created_at']     = $row->created_at;
            $data['created_by']     = $row->created_by;
            $data['npwp']           = $row->npwp;
            $data['no_hp']          = $row->no_hp;
        }

        return view("anggota.form_anggota", ["data" => $data]);
    }


    public function profil_anggota($id)
    {
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Anggota';
        $data['save']           = '0';
        $data['Halaman']        = 'Anggota';
        $data['Sub_Halaman']    = 'Profil Anggota';
        $data['Active']         = 'null';
        $data['menu']           = 'null';
        $data['old']            = $this->Anggota->getAnggota($id);

        foreach ($data['old'] as $row) {
            $data['no_anggota']     = $row->no_anggota;
            $data['id_anggota']     = $row->id_anggota;
            $data['nama_anggota']   = $row->nama_anggota;
            $data['nik']            = $row->nik;
            $data['alamat']         = $row->alamat;
            $data['join_date']      = $row->join_date;
            $data['jenis_kelamin']  = $row->jenis_kelamin;
            $data['status']         = $row->status;
            $data['foto']           = $row->foto;
            $data['created_at']     = $row->created_at;
            $data['created_by']     = $row->created_by;
            $data['npwp']           = $row->npwp;
            $data['no_hp']          = $row->no_hp;
            $data['jabatan']        = $row->jabatan;
        }

        $simpanan_pokok     = $this->Simpanan->detail_smpianan_agt('PV-K001', $id);
        $simpanan_wajib     = $this->Simpanan->detail_smpianan_agt('PV-K002', $id);
        $simpanan_sukarela  = $this->Simpanan->detail_smpianan_agt('PV-K003', $id);
        $modal_anggota      = $this->Simpanan->detail_smpianan_agt('PV-K004', $id);
        $data['n_pokok']    = 0;
        $data['n_wajib']    = 0;
        $data['n_sukarela'] = 0;
        $data['n_modal']    = 0;

        foreach ($simpanan_pokok as $row_pokok) {
            $data['n_pokok'] = $data['n_pokok'] + $row_pokok->total;
        }
        foreach ($simpanan_wajib as $row_wajib) {
            $data['n_wajib'] = $data['n_wajib'] + $row_wajib->total;
        }
        foreach ($simpanan_sukarela as $row_ss) {
            $data['n_sukarela'] = $data['n_sukarela'] + $row_ss->total;
        }
        foreach ($modal_anggota as $row_modal) {
            $data['n_modal'] = $data['n_modal'] + $row_modal->total;
        }
        $data['n_total']    = $data['n_pokok'] + $data['n_wajib'] + $data['n_sukarela'] + $data['n_modal'];

    $data['pembelanjaan'] = $this->Penjualan->pembelanjan($data['no_anggota']);

        return view("profile.detail_user", ["data" => $data]);
    }

    public function profil_anggota2()
    {
        $id = $_SESSION['id_anggota'];
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Anggota';
        $data['save']           = '0';
        $data['Halaman']        = 'Anggota';
        $data['Sub_Halaman']    = 'Profil Anggota';
        $data['Active']         = 'null';
        $data['menu']           = 'null';
        $data['old']            = $this->Anggota->getAnggota($id);

        foreach ($data['old'] as $row) {
            $data['no_anggota']     = $row->no_anggota;
            $data['id_anggota']     = $row->id_anggota;
            $data['nama_anggota']   = $row->nama_anggota;
            $data['nik']            = $row->nik;
            $data['alamat']         = $row->alamat;
            $data['join_date']      = $row->join_date;
            $data['jenis_kelamin']  = $row->jenis_kelamin;
            $data['status']         = $row->status;
            $data['foto']           = $row->foto;
            $data['created_at']     = $row->created_at;
            $data['created_by']     = $row->created_by;
            $data['npwp']           = $row->npwp;
            $data['no_hp']          = $row->no_hp;
            $data['jabatan']        = $row->jabatan;
        }

        $simpanan_pokok     = $this->Simpanan->detail_smpianan_agt('PV-K001', $id);
        $simpanan_wajib     = $this->Simpanan->detail_smpianan_agt('PV-K002', $id);
        $simpanan_sukarela  = $this->Simpanan->detail_smpianan_agt('PV-K003', $id);
        $modal_anggota      = $this->Simpanan->detail_smpianan_agt('PV-K004', $id);

        $data['n_pokok']    = 0;
        $data['n_wajib']    = 0;
        $data['n_sukarela'] = 0;
        $data['n_modal']    = 0;

        foreach ($simpanan_pokok as $row_pokok) {
            $data['n_pokok'] = $data['n_pokok'] + $row_pokok->total;
        }
        foreach ($simpanan_wajib as $row_wajib) {
            $data['n_wajib'] = $data['n_wajib'] + $row_wajib->total;
        }
        foreach ($simpanan_sukarela as $row_ss) {
            $data['n_sukarela'] = $data['n_sukarela'] + $row_ss->total;
        }
        foreach ($modal_anggota as $row_modal) {
            $data['n_modal'] = $data['n_modal'] + $row_modal->total;
        }

        $data['n_total']    = $data['n_pokok'] + $data['n_wajib'] + $data['n_sukarela'] + $data['n_modal'];
        $data['pembelanjaan'] = $this->Penjualan->pembelanjan($data['no_anggota']);


        return view("profile.detail_user", ["data" => $data]);
    }

    public function hapus_anggota($id)
    {
        try {
            DB::delete('DELETE FROM anggota WHERE id_anggota =?', [$id]);

            DB::commit();
            $data           = '3';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data           = '2';
            // something went wrong
        }

        return redirect()->route('anggota')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function edit_anggota_act(Request $request)
    {
        $data['list_anggota']   = $this->Anggota->list_anggota();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Anggota';
        $data['save']           = '0';
        $data['Halaman']        = 'Anggota';
        $data['Sub_Halaman']    = 'Daftar Anggota';
        $data['Active']         = 'anggota';
        $data['menu']           = 'master';

        $this->validate($request, [
            'no_anggota'     => 'required|min:5|max:20',
            'nama_anggota'   => 'required',
            'nik'            => 'required|min:10|max:20',
            'alamat'         => 'required',
            'join_date'      => 'required',
            'jenis_kelamin'  => 'required',
            'status'         => 'required'
        ]);
        try {
            $file       = $request->file('foto');
            $id_anggota     = $request->post('id_anggota');
            if ($file != null or !empty($file)) {
               
                $date       =  date('Y-m-d h:i:s');
                $strtotime  = strtotime($date);
                $ext        = $file->getClientOriginalExtension();
                $no_anggota = $request->no_anggota;
                $nama_file  = $no_anggota.'-'.$strtotime . '.' . $ext;
                
                $image              = $request->file('foto');
                $size               = $image->getSize();
                $destinationPath    = public_path('/foto');
                $img                = Image::make($image->path());

                $img->resize(575, 521, function ($constraint) {

                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $nama_file);

                DB::update('UPDATE anggota set foto = ? WHERE id_anggota =?', [$nama_file, $id_anggota]);
            }

            // data from edit
            $date           =  date('Y-m-d h:i:s');

            $no_anggota     = $request->post('no_anggota');
            $nama_anggota   = $request->post('nama_anggota');
            $nik            = $request->post('nik');
            $alamat         = $request->post('alamat');
            $join_date      = $request->post('join_date');
            $jenis_kelamin  = $request->post('jenis_kelamin');
            $npwp           = $request->post('npwp');
            $no_hp          = $request->post('no_hp');
            // $foto           = $request->file('foto');
            $status         = $request->post('status');
            $create_at      = $date;
            $created_by     = '110';


            DB::update(
                'UPDATE anggota set 
                                        no_anggota = ?, 
                                        nama_anggota = ?, 
                                        nik = ?, 
                                        alamat = ?, 
                                        jenis_kelamin = ?,
                                        status = ?,
                                        npwp = ?,
                                        no_hp = ?
                                      WHERE id_anggota =?',
                [
                    $no_anggota,
                    $nama_anggota,
                    $nik,
                    $alamat,
                    $jenis_kelamin,
                    $status,
                    $npwp,
                    $no_hp,
                    $id_anggota
                ]
            );

            DB::commit();
            $data           = '1';
            // all good
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
            $data           = '2';
            // something went wrong
        }
        $arr_admin = array('1', '2', '3');
        if (in_array($_SESSION['id_jabatan'], $arr_admin)) {
            return redirect()->route('anggota')->with(['data' => $data]);
        }
        return redirect()->route('home')->with(['data' => $data]);
    }

    public function add_anggota(Request $request)
    {
        $this->validate($request, [
            'no_anggota'     => 'required|min:5|max:30',
            'nama_anggota'   => 'required',
            'nik'            => 'required|min:10|max:20',
            'alamat'         => 'required',
            'join_date'      => 'required',
            'jenis_kelamin'  => 'required',
            'status'         => 'required',
            'foto'           => 'required'
        ]);

        $tgl_join   = $request->post('join_date');
        $convert    = date('Y-m-d', strtotime($tgl_join));

        // print_r($convert);
        // die();
        // for upload
        $file       = $request->file('foto');
        $date       =  date('Y-m-d h:i:s');
        $ext        = $file->getClientOriginalExtension();
        $no_anggota = $request->no_anggota;
        $nama_file  = $no_anggota . '.' . $ext;

        $image              = $request->file('foto');
        $size               = $image->getSize();
        // $nama_file          = time().'.'.$image->extension();
        $destinationPath    = public_path('/foto');
        $img                = Image::make($image->path());

        $img->resize(575, 521, function ($constraint) {

            $constraint->aspectRatio();
        })->save($destinationPath . '/' . $nama_file);





        // for input database
        $no_anggota     = $request->post('no_anggota');
        $nama_anggota   = $request->post('nama_anggota');
        $nik            = $request->post('nik');
        $alamat         = $request->post('alamat');
        $join_date      = $request->post('join_date');
        $jenis_kelamin  = $request->post('jenis_kelamin');
        $foto           = $nama_file;
        $status         = $request->post('status');
        $create_at      = $date;
        $created_by     = '110';
        if ($request->post('npwp') != '' or $request->post('npwp') != NUll) {
            $npwp = $request->post('npwp');
        } else {
            $npwp = '-';
        }

        $save = DB::insert(
            'insert into anggota (  
                                                no_anggota,
                                                nama_anggota,
                                                nik,
                                                alamat,
                                                jenis_kelamin,
                                                status,
                                                created_at,
                                                created_by,
                                                foto,
                                                join_date,
                                                npwp,
                                                no_hp)
                             values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            [
                $no_anggota,
                $nama_anggota,
                $nik,
                $alamat,
                $jenis_kelamin,
                $status,
                $create_at,
                $created_by,
                $nama_file,
                $convert,
                $npwp,
                $request->post('no_hp')
            ]
        );
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }

        return redirect()->route('anggota')->with(['data' => $data]);
    }

    public function cetak_kartu($id)
    {
        // $pdf   = PDF::loadview('penjualan.cetak');
        $get_anggota = $this->Anggota->getAnggota2($id);
        foreach ($get_anggota as $row) {
            $data['nama_anggota'] = $row->nama_anggota;
            $data['no_anggota'] = $row->no_anggota;
            $data['nik'] = $row->nik;
            $data['join_date'] = $row->join_date;
            $data['foto'] = $row->foto;
        }
        // $data = 0;



        //for cetak pdf
        $customPaper = array(0, 0, 153.00, 243.80);
        $pdf = PDF::loadView('anggota.cetak', ['data' => $data])->setPaper($customPaper, 'landscape');
        return $pdf->stream('laporan-post.pdf');
        // return view('penjualan.cetak');
    }
}
