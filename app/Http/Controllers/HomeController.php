<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;

use App\Models\Pembelian;
use App\Models\Produk;
use App\Models\Penjualan;
use App\Models\Anggota;
use App\Models\Suplier;

use App\Models\Eproduk;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->pembelian    = new Pembelian();
        $this->produk       = new Produk();
        $this->penjualan    = new Penjualan();
        $this->anggota    = new Anggota();
        $this->suplier    = new Suplier();
        $this->eproduk    = new Eproduk();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function e_comm()
    {
        $data['list_kategori'] = $this->produk->list_kategori();
        $data['new_produk'] = $this->eproduk->new_produk();
        $data['count_new_produk'] = count($this->eproduk->new_produk());
        $data['list_produk'] = $this->eproduk->list_produk();
        $data['side_produk'] = $this->eproduk->side_produk();

        return view('ecomm.produk', ["data" => $data]);
        // return view('ecomm.detail', ["data" => $data]);
    }

   public function ecommerce()
    {
        $data['list_kategori'] = $this->produk->list_kategori();
        $data['new_produk'] = $this->eproduk->new_produk();
        $data['list_produk'] = $this->eproduk->list_produk();
        $data['side_produk'] = $this->eproduk->side_produk();
        // die('disini');
        return view('ecommerce.index', ["data" => $data]);
    }
   
}
