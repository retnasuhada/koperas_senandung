<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;

use App\Models\Pembelian;
use App\Models\Produk;
use App\Models\Penjualan;
use App\Models\Anggota;
use App\Models\Suplier;
use PDF;

use Illuminate\Support\Facades\Storage;




class PenjualanController extends Controller
{
    public function __construct()
    {
        $this->pembelian    = new Pembelian();
        $this->produk       = new Produk();
        $this->penjualan    = new Penjualan();
        $this->anggota    = new Anggota();
        $this->suplier    = new Suplier();
        if (session_status() !== PHP_SESSION_ACTIVE) session_start();
          
          $id_anggota = $_SESSION['id_anggota'];
          $id_jabatan = $_SESSION['id_jabatan'];
    }



    public function index()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_penjualan']   = $this->penjualan->list_penjualan();

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Penjualan';
        $data['edit']           = '0';
        $data['Halaman']        = 'Penjualan';
        $data['Sub_Halaman']    = 'Daftar Penjualan';
        $data['Active']         = 'penjualan';
        $data['menu']           = 'toko';
        $data['error'] = '0';
        return view("penjualan.list", ["data" => $data]);
    }


    public function add_penjualan()
    {


        $data['kode_penjualan']     = kodePenjualan();
        $data['list_penjualan_det'] = $this->penjualan->list_penjualan_det($data['kode_penjualan']);
        $data['list_produk']        = $this->produk->list_produk();
        $data['list_anggota']        = $this->anggota->list_anggota();
        $data['list_suplier']        = $this->suplier->list_suplier();

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Penjualan';
        $data['edit']           = '0';
        $data['Halaman']        = 'Penjualan';
        $data['Sub_Halaman']    = 'Daftar Penjualan';
        $data['Active']         = 'penjualan';
        $data['menu']           = 'toko';
        $data['error']           = '0';

        return view("penjualan.add_penjualan", ["data" => $data]);
    }

    public function info_penjualan($kode_penjualan)
    {


        $data['kode_penjualan']     = $kode_penjualan;
        $old                        = $this->penjualan->getPenjualan($kode_penjualan);
        foreach ($old as $row) {
            $data['tanggal_penjualan']      = $row->tanggal_penjualan;
            $data['total_penjualan']    = $row->total;
            $data['biaya_tambahan']     = $row->biaya_tambahan;
            $data['ppn']                = $row->ppn;
            $data['no_anggota']         = $row->no_anggota;
            $data['nama_anggota']       = $row->nama_anggota;
            $data['detail_pembelian']   = $this->penjualan->detPembelianH($kode_penjualan);
        }

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Penjualan';
        $data['edit']           = '0';
        $data['Halaman']        = 'Penjualan';
        $data['Sub_Halaman']    = 'Detail Penjualan';
        $data['Active']         = 'penjualan';
        $data['menu']           = 'toko';
        $data['error']           = '0';

        return view("penjualan.detail_penjualan", ["data" => $data]);
    }

    public function add_det_penjualan(Request $request)
    {

        $this->validate($request, [
            'kode_penjualan'    => 'required',
            'kode_produk'       => 'required',
            'jumlah'            => 'required'
        ]);

        $kode_penjualan = $request->post('kode_penjualan');
        $kode_produk = $request->post('kode_produk');
        $jumlah         = $request->post('jumlah');

        $cek_stok_produk = $this->penjualan->cek_stok($kode_produk);
        foreach ($cek_stok_produk as $row) {
            $stok = $row->stok;
        }
        if ($jumlah > $stok) {
            $data['error'] = '1';
            $data['message'] = 'stok produk hanya ada = ' . $stok;
            $data['kode_penjualan']     = kodePenjualan();
            $data['list_penjualan_det'] = $this->penjualan->list_penjualan_det($data['kode_penjualan']);
            $data['list_produk']        = $this->produk->list_produk();
            $data['list_anggota']        = $this->anggota->list_anggota();

            $data['flag_edit']      = '0';
            $data['title']          = 'Halaman Penjualan';
            $data['edit']           = '0';
            $data['Halaman']        = 'Penjualan';
            $data['Sub_Halaman']    = 'Daftar Penjualan';
            $data['Active']         = 'penjualan';
            $data['menu']           = 'toko';

            return view("penjualan.add_penjualan", ["data" => $data]);
        } else {
            $data['error'] = '0';
            try {
                DB::insert(
                    'insert into detail_penjualan (kode_penjualan,kode_produk, jumlah, status) 
                                                                            values (?, ?, ?, ?)',
                    [$kode_penjualan, $kode_produk, $jumlah,  '0']
                );

                DB::commit();
            } catch (\Exception $e) {
                // throw $e;
                DB::rollback();
            }
            return redirect()->route('add_penjualan')->with(["data" => $data]);
        }
    }

    public function edit_detail_penjualan($id)
    {
        $old = $this->penjualan->get_detail_penjualan($id);

        foreach ($old as $row) {
            $data['kode_produk']            = $row->kode_produk;
            $data['nama_produk']            = $row->nama_produk;
            $data['jumlah']                 = $row->jumlah;
            $data['id_detail_penjualan']    = $row->id_detail_penjualan;
        }

        $data['kode_penjualan']     = kodePenjualan();
        $data['list_penjualan_det']     = $this->penjualan->list_penjualan_det($data['kode_penjualan']);
        $data['list_produk']        = $this->produk->list_produk();
        $data['list_anggota']        = $this->anggota->list_anggota();

        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Penjualan';
        $data['edit']           = '0';
        $data['Halaman']        = 'Penjualan';
        $data['Sub_Halaman']    = 'Daftar Penjualan';
        $data['Active']         = 'penjualan';
        $data['menu']           = 'toko';
        $data['error'] = '0';
        return view("penjualan.add_penjualan", ["data" => $data]);
    }

    public function edit_det_penjualan_act(Request $request)
    {
        // 
        $id_detail_penjualan    = $request->post('id_detail_penjualan');
        $kode_penjualan         = $request->post('kode_penjualan');
        $kode_produk            = $request->post('kode_produk');
        $jumlah                 = $request->post('jumlah');
        $data['error'] = '0';

        try {
            DB::update(
                'UPDATE detail_penjualan set 
                                        kode_penjualan = ?, 
                                        kode_produk = ?,
                                        jumlah = ? 
                                       
                                      WHERE id_detail_penjualan =?',
                [
                    $kode_penjualan,
                    $kode_produk,
                    $jumlah,
                    $id_detail_penjualan
                ]
            );

            DB::commit();
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
        }

        return redirect()->route('add_penjualan');
    }

    public function hapus_detail_penjualan($id)
    {

        try {
            DB::delete('DELETE FROM detail_penjualan WHERE id_detail_penjualan =?', [$id]);

            DB::commit();
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
        }

        return redirect()->route('add_penjualan');
    }

    public function save_penjualan_act(Request $request)
    {
        // die('disini');
         $id_anggota = $_SESSION['id_anggota'];
        $this->validate($request, [
            'kode_penjualan'    => 'required',
            'tgl_penjualan'       => 'required'
        ]);
        $data['error'] = '0';
        $kode_penjualan =  $request->post('kode_penjualan');
        $tgl_penjualan  =  date('Y-m-d', strtotime($request->post('tgl_penjualan')));
        $no_anggota     =  $request->post('no_anggota');
        $kode_suplier   =  $request->post('kode_suplier');
        // $ppn            =  $request->post('ppn'); 
        $ppn            =  InsertPersen($request->post('ppn'));
        $created_at      = date('Y-m-d h:i:s');
        $biaya_tambahan =  InsertRupiah($request->post('biaya_tambahan'));
        $cek_status_anggota = $this->penjualan->status_anggota($no_anggota);
        foreach($cek_status_anggota as $rs_agt){
            $status_anggota = $rs_agt->status;
        }
        $data_detail_penjualan = $this->penjualan->temp_det_penjualan($kode_penjualan);
        $total_harga = 0;
        try {
            foreach ($data_detail_penjualan as $row) {
                $id_detail_penjualan    = $row->id_detail_penjualan;
                $kode_produk            = $row->kode_produk;
                $flag_grosir            = $row->flag_grosir;
                $jumlah                 = $row->jumlah;
                $min_jumlah             = 0 - $jumlah;
                $harga_prod = $this->produk->harga_produk($kode_produk);
                // $sisa = $jumlah - 0;
                foreach ($harga_prod as $row_prod) {
                    $harga_anggota          = $row_prod->harga_umum;
                    $harga_umum             = $row_prod->harga_umum;
                    $harga_grosir_anggota   = $row_prod->harga_umum;
                    $harga_grosir_umum      = $row_prod->harga_umum;
                }

                $cek_stok = $this->produk->getdetproduk3($kode_produk);
                $count = count($cek_stok);
                $sisa = $jumlah;
                $next = '1';

                foreach ($cek_stok as $key => $value) {
                    $id_detail_produk[$key]   = $value->id_detail_produk;
                    $stok_produk        = $value->stok;


                    if ($sisa - $stok_produk > 0) {
                        $sisa       = $sisa - $stok_produk;
                        $det_sisa   = $stok_produk;
                        DB::update(
                            'UPDATE detail_produk set 
                                        stok = ?
                                        WHERE id_detail_produk =?',
                            [
                                '0',
                                $id_detail_produk[$key]
                            ]
                        );

                        DB::insert(
                                    'insert into tr_det_penjualan (kode_penjualan,
                                                            id_detail_penjualan,  
                                                            id_detail_produk, jumlah)
                                                    values (?, ?, ?,?)',
                                    [
                                        $kode_penjualan,
                                        $id_detail_penjualan,
                                        $id_detail_produk[$key], $det_sisa
                                    ]
                         );
                        $next = '1';
                    } else {
                        $new_stok = $stok_produk - $sisa;
                        $det_sisa = $stok_produk - $new_stok;
                        $sisa       = $sisa - $stok_produk;
                        DB::update(
                            'UPDATE detail_produk set 
                                        stok = ?
                                        WHERE id_detail_produk =?',
                            [
                                $new_stok,
                                $id_detail_produk[$key]
                            ]
                        );
                        // return false;
                        $next = '2';
                        DB::insert(
                                    'insert into tr_det_penjualan (kode_penjualan,
                                                            id_detail_penjualan,  
                                                            id_detail_produk, jumlah)
                                                    values (?, ?, ?,?)',
                                    [
                                        $kode_penjualan,
                                        $id_detail_penjualan,
                                        $id_detail_produk[$key], $det_sisa
                                    ]
                         );
                    }
                   
                    
                }

                // die();
                // cek apakah anggota atau bukan, untuk menentukan harga anggota dan non anggota
                if ($status_anggota == '1') {

                    // cek apakah produk tersebut menyediakan harga grosir
                    if ($flag_grosir == '1') {
                        //cek apakah jumlah pembelian memenuhi jumlah minimum grosir
                        if ($jumlah >= $row->minimum_grosir) {
                            // jika memenuhi syarat grosir maka dapat harga grosir anggota
                            $harga_produk = $row_prod->harga_grosir_anggota;
                        } else {
                            // jika tidak memenuhi syarat grosir maka dapat harga anggota
                            $harga_produk = $row_prod->harga_anggota;
                        }
                    } else {
                        // harga anggota non grosir
                        $harga_produk = $row_prod->harga_anggota;
                    }
                } else {
                    // cek apakah produk bisa grosir
                    if ($flag_grosir == '1') {
                        //cek apakah jumlah pembelian memenuhi jumlah minimum grosir
                        if ($jumlah >= $row->minimum_grosir) {
                            // jika memenuhi syarat grosir maka dapat harga grosir umum
                            $harga_produk = $row_prod->harga_grosir_umum;
                        } else {
                            // jika tidak memenuhi syarat grosir maka dapat harga umum
                            $harga_produk = $row_prod->harga_umum;
                        }
                    } else {

                        $harga_produk = $row_prod->harga_umum;
                    }
                }
                $total_harga = $total_harga + ($harga_produk * $jumlah);
                DB::update(
                    'UPDATE detail_penjualan set 
                                    harga_produk = ?,
                                    status = ?
                                    WHERE id_detail_penjualan =?',
                    [
                        $harga_produk,
                        '1',
                        $id_detail_penjualan
                    ]
                );
            }
            $ppn = ($ppn * $total_harga) / 100;
            $total_harga = $total_harga + $ppn + $biaya_tambahan;

            DB::insert(
                'insert into penjualan (kode_penjualan,
                                                no_anggota,  
                                                tanggal_penjualan,
                                                ppn,
                                                total,
                                                biaya_tambahan,
                                                created_at,
                                                created_by)
                                        values (?, ?, ?, ?, ?, ?, ?, ?)',
                [
                    $kode_penjualan,
                    $no_anggota,
                    $tgl_penjualan,

                    $ppn,
                    $total_harga,
                    $biaya_tambahan,
                    $created_at,
                    $id_anggota
                ]
            );

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            // return $e->getMessage();
            DB::rollback();
        }

        // return $e->getMessage();
        return redirect()->route('penjualan');
    }

    public function cetak_struk_penjualan($kode_penjualan)
    {

        $data['kode_penjualan']     = $kode_penjualan;
        $old                        = $this->penjualan->getPenjualan($kode_penjualan);
        foreach ($old as $row) {
            $data['tanggal_penjualan']  = $row->tanggal_penjualan;
            $data['total_penjualan']    = $row->total;
            $data['biaya_tambahan']     = $row->biaya_tambahan;

            $data['created_by']         = $row->created_by;
            $data['created_at']         = $row->created_at;
            $data['ppn']                = $row->ppn;
            if($row->no_anggota == 110){
                $data['no_anggota']         = 'Non - Anggota';
                $data['nama_anggota']       = '-';
            }else{
                $data['no_anggota']         = $row->no_anggota;
                $data['nama_anggota']       = $row->nama_anggota;
            }
            
            $data['detail_pembelian']   = $this->penjualan->list_penjualan_det($kode_penjualan);
        }

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Penjualan';
        $data['edit']           = '0';
        $data['Halaman']        = 'Penjualan';
        $data['Sub_Halaman']    = 'Detail Penjualan';
        $data['Active']         = 'penjualan';
        $data['menu']           = 'toko';
        $data['error']           = '0';



        // $pdf   = PDF::loadview('penjualan.struk_penjualan',['data' => $data]);
        $customPaper = array(0, 0, 396.00, 544.00);
       $pdf = PDF::loadView('penjualan.struk_penjualan', ['data' => $data])->setPaper('A4', 'portrait');
        return $pdf->stream('laporan-post.pdf');
        // return view('penjualan.struk_penjualan',['data' => $data]);
    }
}
