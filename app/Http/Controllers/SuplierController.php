<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\M_User;
use App\Models\Anggota;
use App\Models\Suplier;
use App\Models\Pembelian;
use App\Models\Produk;


use Illuminate\Support\Facades\Storage;




class SuplierController extends Controller
{
   public function __construct()
   {

      $this->suplier = new Suplier();
      $this->anggota = new Anggota();
      $this->produk       = new Produk();
      $this->pembelian = new Pembelian();
       if (session_status() !== PHP_SESSION_ACTIVE) session_start();
      if(isset($_SESSION['id_anggota'])){
          $this->id_anggota = $_SESSION['id_anggota'];
          $this->id_jabatan = $_SESSION['id_jabatan'];
          $this->kd_suplier = $_SESSION['kode_suplier'];
        }else{
          $this->id_anggota = 'xx';
          $this->id_jabatan = 'xx';
        }

   }

   public function add_penjualanspl_act(Request $request)
    {

        $this->validate($request, [
            'kode_pembelian'    => 'required',
            'kode_produk'       => 'required',
            'jumlah'            => 'required',
            'harga'             => 'required'
        ]);


        $kode_pembelian = $request->post('kode_pembelian');
        $kode_produk    = $request->post('kode_produk');
        $jumlah         = $request->post('jumlah');
        $harga          = InsertRupiah($request->post('harga'));

        try {
            DB::insert(
                'insert into detail_pembelian (kode_pembelian,kode_produk, jumlah, harga_semua, status,verified) 
                                                                        values (?, ?, ?, ?, ?, ?)',
                [$kode_pembelian, $kode_produk, $jumlah, $harga, '0','0']
            );

            DB::commit();
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
        }
        return redirect()->route('add_m_penjualan_suplier');
    }

   public function save_penjualanspl_act(Request $request)
    {

        $this->validate($request, [
            'kode_pembelian'    => 'required',
            'tgl_pembelian'       => 'required'

        ]);


        $kode_pembelian = $request->post('kode_pembelian');
        $tgl_pembelian  = date('Y-m-d', strtotime($request->post('tgl_pembelian')));
        $no_invoice     = $request->post('no_invoice');
        $kode_suplier   = $this->kd_suplier;
        $biaya_tambahan = InsertRupiah($request->post('biaya_tambahan'));
        $diskon         = InsertRupiah($request->post('diskon'));
        $jenis_pembelian = $request->post('jenis_pembelian');
        
        $tambahan       =$biaya_tambahan - $diskon;
      
        $no_invoice     = $request->post('no_invoice');
       
        try {
            // $harga_semua = 0;
            $data_detail_pembelian  = $this->pembelian->temp_det_pembelian($kode_pembelian);
            $count_barang           = $this->pembelian->count_barang($kode_pembelian);

            foreach ($count_barang as $rs_count) {
                $total_barang   = $rs_count->total;
                $total_h        = $rs_count->total_h;
            }

            $total_invoice = $total_h + $tambahan;
            
            foreach ($data_detail_pembelian as $row) {
                $id_detail_pembelian    = $row->id_detail_pembelian;
                $kode_produk            = $row->kode_produk;
                $stok                   = $row->jumlah;
                $harga_satuan           = $row->harga_semua/$row->jumlah;
                $harga_pendapatan       = ($row->harga_semua/$row->jumlah);
               
                DB::update(
                    'UPDATE detail_pembelian set 
                                    harga_pendapatan = ?,
                                    status = ?
                                    WHERE id_detail_pembelian =?',
                    [
                        $harga_pendapatan,
                        '1',
                        $id_detail_pembelian
                    ]
                );
            }

           
            DB::insert(
                'insert into pembelian (kode_pembelian,kode_suplier, tgl_pembelian, biaya_tambahan, status, no_invoice,diskon,jenis_pembelian, verified) 
                                                                        values (?, ?, ?, ?, ?, ?,?,?,?)',
                [$kode_pembelian, $kode_suplier, $tgl_pembelian, $biaya_tambahan, '0', $no_invoice, $diskon, $jenis_pembelian,'0']
            );

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
        }
        return redirect()->route('m_penjualan_suplier');
    }

   public function suplier()
   {

      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      $data['list_suplier']      = $this->suplier->list_suplier();
      // $data['list_anggota']   = $this->anggota->list_anggota();
      $data['flag_edit']      = '0';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'List Supier';
      $data['Active']         = 'suplier';
      $data['menu']           = 'toko';

      return view("suplier.list_suplier", ["data" => $data]);
   }

   public function m_suplier_stok()
   {

     // $kode_suplier = 'SN165-SPL-2108-00003';
      $data['list_produk']      = $this->suplier->m_produk_suplier($_SESSION['kode_suplier']);
     
      $data['flag_edit']      = '0';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'List Supier';
      $data['Active']         = 'stok_suplier';
      $data['menu']           = 'm_suplier';

      return view("suplier.list_produk_suplier", ["data" => $data]);
   }

   public function detail_penjualan_suplier($id)
    {
       
        $old_pembelian = $this->pembelian->getPembelian($id);
        foreach($old_pembelian as $row_pembelian){
            $data['kode_suplier']   = $row_pembelian->kode_suplier;
            $data['nama_suplier']   = $row_pembelian->nama_suplier;
            $data['tgl_pembelian']  = $row_pembelian->tgl_pembelian;
            $data['biaya_tambahan'] = $row_pembelian->biaya_tambahan;
            $data['diskon']         = $row_pembelian->diskon;
        }
        // $data['list_pembelian']

        $data['kode_pembelian']     = $id;
        $data['list_pembelian']     = $this->pembelian->list_det_pembelian($data['kode_pembelian']);
        
        $data['list_produk']        = $this->produk->list_produk();

      $data['flag_edit']    = '0';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'List Supier';
      $data['Active']         = 'm_penjualan_suplier';
      $data['menu']           = 'm_suplier';
        return view("suplier.detail_pembelian", ["data" => $data]);
    }

   public function m_penjualan_suplier()
   {

      $data['penjualan_suplier']      = $this->suplier->penjualan_suplier($_SESSION['kode_suplier']);
      $old                    = $this->suplier->get_suplier($_SESSION['kode_suplier']);
      foreach($old as $row){
         $data['nama_suplier'] = $row->nama_suplier;
         $data['kode_suplier'] = $row->kode_suplier;
      }
      $data['flag_edit']      = '0';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'List Supier';
      $data['Active']         = 'm_penjualan_suplier';
      $data['menu']           = 'm_suplier';

      return view("suplier.list_penjualan_suplier", ["data" => $data]);
   }

   public function add_m_penjualan_suplier()
   {
      $old                    = $this->suplier->get_suplier($_SESSION['kode_suplier']);
      foreach($old as $row){
         $data['nama_suplier'] = $row->nama_suplier;
         $data['kode_suplier'] = $row->kode_suplier;
      }
      $data['kode_pembelian']     = kodePembelianSpl($data['kode_suplier']);
      $data['list_pembelian']     = $this->pembelian->list_det_pembelian($data['kode_pembelian']);
      
      $data['list_produk']        = $this->produk->list_produk();
      $data['list_suplier']        = $this->suplier->list_suplier();
      
      $data['flag_edit']      = '0';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'List Supier';
      $data['Active']         = 'm_penjualan_suplier';
      $data['menu']           = 'm_suplier';

      return view("suplier.add_penjualan", ["data" => $data]);
   }
   

   public function m_detail_pembayaran_suplier($kode_pembayaran)
    {

        
      $data['flag_edit']      = '0';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'List Supier';
      $data['Active']         = 'm_pembayaran_suplier';
      $data['menu']           = 'm_suplier';

        $old = $this->suplier->get_pembayaran_suplier($kode_pembayaran);
        foreach ($old as $row) {
           $data['no_pembayaran']   = $kode_pembayaran;
           $data['tgl_pembayaran']  = $row->tgl_pembayaran;
           $data['nama_suplier']    = $row->nama_suplier;
        }
        $data['list_pembayaran_suplier'] = $this->suplier->detail_pembayaran_suplier($kode_pembayaran);
        return view("suplier.m_cetak_pembayaran_suplier", ["data" => $data]);
    }

   public function m_pembayaran_suplier()
   {

      $data['pembayaran_suplier']      = $this->suplier->pembayaran_suplier($_SESSION['kode_suplier']);
      $old                    = $this->suplier->get_suplier($_SESSION['kode_suplier']);
      foreach($old as $row){
         $data['nama_suplier'] = $row->nama_suplier;
         $data['kode_suplier'] = $row->kode_suplier;
      }
      $data['flag_edit']      = '0';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'List Supier';
      $data['Active']         = 'm_pembayaran_suplier';
      $data['menu']           = 'm_suplier';

      return view("suplier.list_pembayaran_suplier", ["data" => $data]);
   }

   public function add()
   {

      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      $data['kode_suplier'] = kodeSuplier();
      $data['list_anggota'] = $this->anggota->list_anggota();

      // $data['list_suplier']      = $this->suplier->list_suplier();
      // $data['list_anggota']   = $this->anggota->list_anggota();
      $data['flag_edit']      = '0';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'List Supier';
      $data['Active']         = 'suplier';
      $data['menu']           = 'toko';

      return view("suplier.form_suplier", ["data" => $data]);
   }

   public function edit_suplier($kode_suplier)
   {

      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      $data['kode_suplier'] = $kode_suplier;
      $old = $this->suplier->get_suplier($kode_suplier);
      $data['list_anggota'] = $this->anggota->list_anggota();
      foreach($old as $row){
         $data['kode_suplier']   = $row->kode_suplier;
         $data['nama_suplier']   = $row->nama_suplier;
         $data['no_hp']          = $row->no_hp;
         $data['telp']           = $row->telp;
         $data['alamat']         = $row->alamat;
         $data['deskripsi']      = $row->deskripsi;
         $data['no_anggota']     = $row->no_anggota;
      }
      
      $data['flag_edit']      = '1';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'Edit Supier';
      $data['Active']         = 'suplier';
      $data['menu']           = 'toko';

      return view("suplier.edit_suplier", ["data" => $data]);
   }

   public function detail_suplier($kode_suplier)
   {

      if (Session::get('data') != NUll or Session::get('data') != "") {
         $data['save'] = Session::get('data');
      } else {
         $data['save']           = '0';
      }
      $data['kode_suplier'] = $kode_suplier;
      $old = $this->suplier->get_suplier($kode_suplier);
      $data['list_anggota'] = $this->anggota->list_anggota();
      foreach($old as $row){
         $data['kode_suplier']   = $row->kode_suplier;
         $data['nama_suplier']   = $row->nama_suplier;
         $data['no_hp']          = $row->no_hp;
         $data['telp']           = $row->telp;
         $data['alamat']         = $row->alamat;
         $data['deskripsi']      = $row->deskripsi;
         $data['no_anggota']     = $row->no_anggota;
      }
      $data['produk_suplier'] = $this->suplier->produk_suplier($kode_suplier);
      $data['invoice_suplier'] = $this->suplier->invoice_suplier($kode_suplier);

      $data['flag_edit']      = '1';
      $data['title']          = 'Halaman Supier';
      $data['edit']           = '0';
      $data['Halaman']        = 'Halaman Supier';
      $data['Sub_Halaman']    = 'Edit Supier';
      $data['Active']         = 'suplier';
      $data['menu']           = 'toko';

      return view("suplier.detail_suplier", ["data" => $data]);
   }

   public function add_suplier(Request $request)
   {

      $kode_suplier  = $request->kode_suplier;
      $nama_suplier  = $request->nama_suplier;
      $no_hp         = $request->no_hp;
      $no_telp       = $request->no_telp;
      $alamat        = $request->alamat;
      $rating        = $request->rating;
      $status        = $request->status;
      $no_anggota    = $request->no_anggota;
      $join_date     = date('Y-m-d', strtotime($request->join_date));
      $deskripsi     = $request->deskripsi;

      try {
         DB::insert('INSERT INTO suplier (kode_suplier, nama_suplier, no_hp, telp, alamat, rating, status, join_date, deskripsi,no_anggota) values (?,?,?,?,?,?,?,?,?,?)', [$kode_suplier, $nama_suplier, $no_hp, $no_telp, $alamat, $rating, $status, $join_date, $deskripsi, $no_anggota]);

         DB::commit();
         $data           = '3';
         // all good
      } catch (\Exception $e) {
         // throw $e;
         DB::rollback();
         $data           = '2';
         // something went wrong
      }
      return redirect()->route('suplier');

      // return redirect() $this->suplier();
   }

   public function edit_suplier_act(Request $request)
   {

      $kode_suplier  = $request->kode_suplier;
      $nama_suplier  = $request->nama_suplier;
      $no_hp         = $request->no_hp;
      $no_telp       = $request->no_telp;
      $alamat        = $request->alamat;
      
      $status        = $request->status;
      $no_anggota    = $request->no_anggota;
      $deskripsi     = $request->deskripsi;

      try {
          DB::update('UPDATE suplier set 
                                    nama_suplier = ?, 
                                    no_hp = ?,
                                    telp = ?, 
                                    alamat = ?,
                                    status = ?,
                                    no_anggota = ?, 
                                    deskripsi = ?
                                  WHERE kode_suplier =?'
                                , [$nama_suplier, 
                                    $no_hp,
                                    $no_telp,
                                    $alamat, 
                                    $status,
                                    $no_anggota, 
                                    $deskripsi, 
                                    $kode_suplier]);

         DB::commit();
         $data           = '3';
         // all good
      } catch (\Exception $e) {
         throw $e;
         // DB::rollback();
         $data           = '2';
         // something went wrong
      }
      return redirect()->route('suplier');

      // return redirect() $this->suplier();
   }
}
