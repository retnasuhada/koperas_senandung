<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;

use App\Models\Pembelian;
use App\Models\Produk;
use App\Models\Suplier;

use Illuminate\Support\Facades\Storage;




class PembelianController extends Controller
{
    public function __construct()
    {
        $this->pembelian    = new Pembelian();
        $this->produk       = new Produk();
        $this->suplier       = new Suplier();
    }



    public function index()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_pembelian']   = $this->pembelian->list_pembelian();

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.list", ["data" => $data]);
    }

    public function add_pembelian()
    {
        $data['kode_pembelian']     = kodePembelian();
        $data['list_pembelian']     = $this->pembelian->list_det_pembelian($data['kode_pembelian']);
        $data['list_produk']        = $this->produk->list_produk();
        $data['list_suplier']        = $this->suplier->list_suplier();

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.add_pembelian", ["data" => $data]);
    }
    public function edit_pembelian($id)
    {
        $data['kode_pembelian']     = $id;
        $data['list_pembelian']     = $this->pembelian->list_det_pembelian($data['kode_pembelian']);
        $data['list_produk']        = $this->produk->list_produk();
        $data['list_suplier']       = $this->suplier->list_suplier();
        $old = $this->pembelian->getPembelian($id);
        foreach($old as $row){
            $data['tgl_pembelian']  = $row->tgl_pembelian;
            $data['no_invoice']     = $row->no_invoice;
            $data['kode_suplier']   = $row->kode_suplier;
            $data['biaya_tambahan']   = $row->biaya_tambahan;
            $data['diskon']         = $row->diskon;
            $data['jenis_pembelian'] = $row->jenis_pembelian;
            $data['lunas'] = $row->lunas;
        }

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.edit_pembelian", ["data" => $data]);
    }

    public function verif_pembelian($id)
    {
        $data['kode_pembelian']     = $id;
        $data['list_pembelian']     = $this->pembelian->list_verif_pembelian($id);
        if ($data['list_pembelian'] =="" OR $data['list_pembelian'] == NULL) {
          $data['jml'] = 0;
        }else{
           $data['jml'] = 1; 
        }
        $data['list_produk']        = $this->produk->list_produk();
        $list_terverifikasi         = $this->pembelian->terverifikasi($id);
        $data['jml_terverifikasi']  = count($list_terverifikasi);
        

        $data['list_suplier']       = $this->suplier->list_suplier();
        $old = $this->pembelian->getPembelian($id);
        foreach($old as $row){
            $data['tgl_pembelian']  = $row->tgl_pembelian;
            $data['no_invoice']     = $row->no_invoice;
            $data['kode_suplier']   = $row->kode_suplier;
            $data['biaya_tambahan']   = $row->biaya_tambahan;
            $data['diskon']         = $row->diskon;
            $data['jenis_pembelian'] = $row->jenis_pembelian;
            $data['lunas'] = $row->lunas;
        }

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.verif_pembelian", ["data" => $data]);
    }

    public function view_verified($id)
    {

        $data['list_terverifikasi']  = $this->pembelian->terverifikasi($id);
        $old = $this->pembelian->getPembelian($id);
        foreach($old as $row){
            $data['kode_suplier'] = $row->kode_suplier;
            $data['nama_suplier'] = $row->nama_suplier;
            $data['tgl_pembelian'] = $row->tgl_pembelian;
            $data['kode_pembelian'] = $row->kode_pembelian;
        }
        $data['kode_pembelian']     = $id;
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.view_verified", ["data" => $data]);
    }

    public function verif_pembelian2()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['kode_pembelian'] = Session::get('data');
        } else {
            $data['kode_pembelian']           = '0';
        }

        
        $data['list_pembelian']     = $this->pembelian->list_verif_pembelian($data['kode_pembelian']);
        
        $data['list_suplier']       = $this->suplier->list_suplier();
        

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.verif_pembelian", ["data" => $data]);
    }

    public function edit_detail_pembelian($id)
    {
        $old = $this->pembelian->get_detail_pembelian($id);

        foreach ($old as $row) {
            $data['kode_produk']    = $row->kode_produk;
            $data['jumlah']         = $row->jumlah;
            $data['harga']          = $row->harga_semua;
            $data['id_detail_pembelian'] = $row->id_detail_pembelian;
        }

        $data['kode_pembelian']     = kodePembelian();
        $data['list_pembelian']     = $this->pembelian->list_det_pembelian($data['kode_pembelian']);
        $data['list_produk']        = $this->produk->list_produk();

        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.add_pembelian", ["data" => $data]);
    }

    public function detail_pembelian($id)
    {
       
        $old_pembelian = $this->pembelian->getPembelian($id);
        foreach($old_pembelian as $row_pembelian){
            $data['kode_suplier']   = $row_pembelian->kode_suplier;
            $data['nama_suplier']   = $row_pembelian->nama_suplier;
            $data['tgl_pembelian']  = $row_pembelian->tgl_pembelian;
            $data['biaya_tambahan'] = $row_pembelian->biaya_tambahan;
            $data['diskon']         = $row_pembelian->diskon;
        }
        // $data['list_pembelian']

        $data['kode_pembelian']     = $id;
        $data['list_pembelian']     = $this->pembelian->list_det_pembelian($data['kode_pembelian']);
        
        $data['list_produk']        = $this->produk->list_produk();

        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.detail_pembelian", ["data" => $data]);
    }


    public function add_pembelian_act(Request $request)
    {

        $this->validate($request, [
            'kode_pembelian'    => 'required',
            'kode_produk'       => 'required',
            'jumlah'            => 'required',
            'harga'             => 'required'
        ]);


        $kode_pembelian = $request->post('kode_pembelian');
        $kode_produk    = $request->post('kode_produk');
        $jumlah         = $request->post('jumlah');
        $harga          = InsertRupiah($request->post('harga'));

        try {
            DB::insert(
                'insert into detail_pembelian (kode_pembelian,kode_produk, jumlah, harga_semua, status) 
                                                                        values (?, ?, ?, ?, ?)',
                [$kode_pembelian, $kode_produk, $jumlah, $harga, '0']
            );

            DB::commit();
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
        }
        return redirect()->route('add_pembelian');
    }

    public function verifikasi_detail($id)
    {
        $kode_pembelian = $this->pembelian->getKodePembelian($id);
        foreach($kode_pembelian as $row){
            $data['kode_pembelian'] = $row->kode_pembelian;
        }

        try {
            DB::update(
                    'UPDATE detail_pembelian set 
                                    verified = ?
                                    WHERE id_detail_pembelian =?',
                    [
                        
                        '1',
                        $id
                    ]
                );

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            // DB::rollback();
        }
       return $this->verif_pembelian($data['kode_pembelian']);
    }

    public function save_pembelian_act(Request $request)
    {

        $this->validate($request, [
            'kode_pembelian'    => 'required',
            'tgl_pembelian'       => 'required'

        ]);


        $kode_pembelian = $request->post('kode_pembelian');
        $tgl_pembelian  = date('Y-m-d', strtotime($request->post('tgl_pembelian')));
        $no_invoice     = $request->post('no_invoice');
        $kode_suplier   = $request->post('kode_suplier');
        $biaya_tambahan = InsertRupiah($request->post('biaya_tambahan'));
        $diskon         = InsertRupiah($request->post('diskon'));
        $jenis_pembelian = $request->post('jenis_pembelian');
        $tambahan       = $biaya_tambahan - $diskon;
        $no_invoice     = $request->post('no_invoice');
        // // $kode_suplier
        // $total
       

        try {
            // $harga_semua = 0;
            $data_detail_pembelian  = $this->pembelian->temp_det_pembelian($kode_pembelian);
            $count_barang           = $this->pembelian->count_barang($kode_pembelian);

            foreach ($count_barang as $rs_count) {
                $total_barang   = $rs_count->total;
                $total_h        = $rs_count->total_h;
            }

            $total_invoice = $total_h + $tambahan;
            $additional = $tambahan / $total_barang;

            foreach ($data_detail_pembelian as $row) {
                $id_detail_pembelian    = $row->id_detail_pembelian;
                $kode_produk            = $row->kode_produk;
                $stok                   = $row->jumlah;
                $harga_satuan           = $row->harga_semua/$row->jumlah;
                $harga_pendapatan       = ($row->harga_semua/$row->jumlah) + $additional;
                DB::insert(
                    'insert into detail_produk (kode_produk,stok, harga_pendapatan, harga_satuan, id_detail_pembelian) 
                                                                        values (?, ?, ?, ?, ?)',
                    [$kode_produk, $stok, $harga_pendapatan, $harga_satuan, $id_detail_pembelian]
                );
                DB::update(
                    'UPDATE detail_pembelian set 
                                    harga_pendapatan = ?,
                                    status = ?
                                    WHERE id_detail_pembelian =?',
                    [
                        $harga_pendapatan,
                        '1',
                        $id_detail_pembelian
                    ]
                );
            }

            DB::insert(
                'insert into invoice (no_invoice,kode_suplier, total,tgl_invoice, status) 
                                                                        values (?, ?, ?, ?,?)',
                [$no_invoice, $kode_suplier, $total_invoice, date('Y-m-d'), '0']
            );
            DB::insert(
                'insert into pembelian (kode_pembelian,kode_suplier, tgl_pembelian, biaya_tambahan, status, no_invoice,diskon,jenis_pembelian) 
                                                                        values (?, ?, ?, ?, ?, ?,?,?)',
                [$kode_pembelian, $kode_suplier, $tgl_pembelian, $biaya_tambahan, '0', $no_invoice, $diskon, $jenis_pembelian]
            );

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
        }
        return redirect()->route('pembelian');
    }

    public function save_verif_pembelian_act(Request $request)
    {

        $this->validate($request, [
            'kode_pembelian'    => 'required',
            'tgl_pembelian'       => 'required'

        ]);


        $kode_pembelian = $request->post('kode_pembelian');
        $kode_suplier = $request->post('kode_suplier');
        $tgl_pembelian  = date('Y-m-d', strtotime($request->post('tgl_pembelian')));
        
        $biaya_tambahan = InsertRupiah($request->post('biaya_tambahan'));
        $diskon         = InsertRupiah($request->post('diskon'));
        $jenis_pembelian = $request->post('jenis_pembelian');
        $tambahan       = $biaya_tambahan - $diskon;
        $no_invoice     = $request->post('no_invoice');
        // // $kode_suplier
        // $total
       

        try {
            // $harga_semua = 0;
            $data_detail_pembelian  = $this->pembelian->temp_det_verif_pembelian($kode_pembelian);
            $count_barang           = $this->pembelian->count_barang_verified($kode_pembelian);

            foreach ($count_barang as $rs_count) {
                $total_barang   = $rs_count->total;
                $total_h        = $rs_count->total_h;
            }

            $total_invoice = $total_h + $tambahan;

            $additional = $tambahan / $total_barang;

            foreach ($data_detail_pembelian as $row) {
                $id_detail_pembelian    = $row->id_detail_pembelian;
                $kode_produk            = $row->kode_produk;
                $stok                   = $row->jumlah;
                $harga_satuan           = $row->harga_semua/$row->jumlah;
                $harga_pendapatan       = ($row->harga_semua/$row->jumlah) + $additional;
                DB::insert(
                    'insert into detail_produk (kode_produk,stok, harga_pendapatan, harga_satuan, id_detail_pembelian) 
                                                                        values (?, ?, ?, ?, ?)',
                    [$kode_produk, $stok, $harga_pendapatan, $harga_satuan, $id_detail_pembelian]
                );
                DB::update(
                    'UPDATE detail_pembelian set 
                                    harga_pendapatan = ?,
                                    status = ?
                                    WHERE id_detail_pembelian =?',
                    [
                        $harga_pendapatan,
                        '1',
                        $id_detail_pembelian
                    ]
                );
            }

            DB::insert(
                'insert into invoice (no_invoice,kode_suplier, total,tgl_invoice, status) 
                                                                        values (?, ?, ?, ?,?)',
                [$no_invoice, $kode_suplier, $total_invoice, date('Y-m-d'), '0']
            );
            DB::update(
                    'UPDATE pembelian set 
                                    tgl_pembelian = ?,
                                    status = ?,
                                    no_invoice = ?,
                                    diskon = ?,
                                    verified = ?,
                                    jenis_pembelian = ?
                                    WHERE kode_pembelian =?',
                    [
                        $tgl_pembelian,
                        '1',
                        $no_invoice,
                        $diskon,
                        '1',
                        $jenis_pembelian,
                        $kode_pembelian
                    ]
                );

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
        }
        return redirect()->route('pembelian');
    }

    public function edit_pembelian_act(Request $request)
    {
        // 
        $id_detail_pembelian = $request->post('id_detail_pembelian');
        $kode_pembelian = $request->post('kode_pembelian');
        $kode_produk    = $request->post('kode_produk');
        $jumlah         = $request->post('jumlah');
        $harga          = InsertRupiah($request->post('harga'));
        // print_r($kode_pembelian.'||'.$kode_produk.'||'.$jumlah.'||'.$harga);
        // die();
        try {
            DB::update(
                'UPDATE detail_pembelian set 
                                        kode_pembelian = ?, 
                                        kode_produk = ?,
                                        jumlah = ?, 
                                        harga_semua = ?
                                      WHERE id_detail_pembelian =?',
                [
                    $kode_pembelian,
                    $kode_produk,
                    $jumlah,
                    $harga,
                    $id_detail_pembelian
                ]
            );

            DB::commit();
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
        }

        return redirect()->route('add_pembelian');
    }

    public function act_edit_pembelian(Request $request)
    {
        // 
        $kode_pembelian = $request->post('kode_pembelian');
        $kode_suplier = $request->post('kode_suplier');
        
        $tgl_pembelian = $request->post('tgl_pembelian');
        $no_invoice    = $request->post('no_invoice');
        $biaya_tambahan         = InsertRupiah($request->post('biaya_tambahan'));
        $jenis_pembelian         =$request->post('jenis_pembelian');
        $no_invoice_old         = $request->post('no_invoice_old');
        $diskon         = InsertRupiah($request->post('diskon'));
        // print_r($kode_pembelian.'||'.$kode_produk.'||'.$jumlah.'||'.$harga);
        // die();
        $count_barang           = $this->pembelian->count_barang($kode_pembelian);
        $tambahan       = $biaya_tambahan - $diskon;

            foreach ($count_barang as $rs_count) {
                $total_barang   = $rs_count->total;
                $total_h        = $rs_count->total_h;
            }

            $total_invoice = $total_h + $tambahan;
            

        try {
            DB::update(
                'UPDATE pembelian set 
                                        tgl_pembelian = ?, 
                                        no_invoice = ?,
                                        biaya_tambahan = ?, 
                                        jenis_pembelian = ?,
                                        diskon = ?
                                      WHERE kode_pembelian =?',
                [
                    $tgl_pembelian,
                    $no_invoice,
                    $biaya_tambahan,
                    $jenis_pembelian,
                    $diskon,
                    $kode_pembelian
                ]
            );
             DB::delete('DELETE FROM invoice WHERE no_invoice =?', [$no_invoice_old]);
              DB::insert(
                'insert into invoice (no_invoice,kode_suplier, total,tgl_invoice, status) 
                                                                        values (?, ?, ?, ?,?)',
                [$no_invoice, $kode_suplier, $total_invoice, date('Y-m-d'), '0']
            );


            DB::commit();
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
        }

        return redirect()->route('pembelian');
    }

    public function hapus_detail_pembelian($id)
    {

        try {
            DB::delete('DELETE FROM detail_pembelian WHERE id_detail_pembelian =?', [$id]);

            DB::commit();
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
        }

        return redirect()->route('add_pembelian');
    }
}
