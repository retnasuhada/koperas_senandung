<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\M_User;
use App\Models\Anggota;
use App\Models\Invoice;
use App\Models\Suplier;
use App\Models\Pembelian;


use Illuminate\Support\Facades\Storage;

class InvoiceController extends Controller
{
    public function __construct()
    {

        $this->invoice = new Invoice();
        $this->suplier = new Suplier();
        $this->pembelian = new Pembelian();
        if (session_status() !== PHP_SESSION_ACTIVE) session_start();
        if(isset($_SESSION['id_anggota'])){
          $this->id_anggota = $_SESSION['id_anggota'];
          $this->id_jabatan = $_SESSION['id_jabatan'];
        }else{
          $this->id_anggota = 'xx';
          $this->id_jabatan = 'xx';
        }
 
    }

    public function invoice()
    {

        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_invoice']      = $this->invoice->list_invoice();

        // $data['list_anggota']   = $this->anggota->list_anggota();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Invoice';
        $data['edit']           = '0';
        $data['Halaman']        = 'Halaman Invoice';
        $data['Sub_Halaman']    = 'List Invoice';
        $data['Active']         = 'invoice';
        $data['menu']           = 'toko';

        return view("invoice.list_invoice", ["data" => $data]);
    }

    public function bayar_suplier()
    {

        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_pembayaran_suplier']      = $this->invoice->list_pembayaran_suplier();
        $data['list_penjualan ']   = '0';
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembayaran Suplier';
        $data['edit']           = '0';
        $data['Halaman']        = 'Halaman Pembayaran Suplier';
        $data['Sub_Halaman']    = 'List Pembayaran Suplier';
        $data['Active']         = 'bayar_suplier';
        $data['menu']           = 'toko';

        return view("invoice.bayar_suplier", ["data" => $data]);
    }

    public function add_bayar_suplier()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_suplier']      = $this->suplier->list_suplier();
        $data['KodePembayaran']    = KodePembayaran();
        $data['list_penjualan']    = '0';
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembayaran Suplier';
        $data['edit']           = '0';
        $data['Halaman']        = 'Halaman Pembayaran Suplier';
        $data['Sub_Halaman']    = 'List Pembayaran Suplier';
        $data['Active']         = 'bayar_suplier';
        $data['menu']           = 'toko';
        $data['list_penjualan ']   = '0';

        return view("pembayaran_suplier.add_pembayaran", ["data" => $data]);
    }

    public function cek_penjualan(Request $request)
    {
        
        $kode_suplier = $request->kode_suplier;
        $data['list_suplier']      = $this->suplier->list_suplier();
        $data['KodePembayaran']    = KodePembayaran();
        
        $data['data_penjualan'] = $this->invoice->penjualan_produk_suplier($kode_suplier);
        $suplier   = $this->suplier->get_suplier($kode_suplier);
        foreach($suplier as $row_suplier){
            $data['nama_suplier'] = $row_suplier->nama_suplier;
            $data['kode_suplier'] = $row_suplier->kode_suplier;
        }
        $data['data_pembayaran'] = $this->invoice->data_ost_pembayaran($data['KodePembayaran']);
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembayaran Suplier';
        $data['edit']           = '0';
        $data['Halaman']        = 'Halaman Pembayaran Suplier';
        $data['Sub_Halaman']    = 'List Pembayaran Suplier';
        $data['Active']         = 'bayar_suplier';
        $data['menu']           = 'toko';
        $data['list_penjualan ']   = '0';

        return view("pembayaran_suplier.list_terjual", ["data" => $data]);
    }

    public function cek_penjualan_get()
    {
        $data['KodePembayaran']    = KodePembayaran();
        $old = $this->invoice->get_data_ost_pembayaran($data['KodePembayaran']);
        foreach($old as $row_sp){
            $kode_suplier = $row_sp->kode_suplier;
        }

        $data['KodePembayaran']    = KodePembayaran();
        
        $data['data_penjualan'] = $this->invoice->penjualan_produk_suplier($kode_suplier);
        $suplier   = $this->suplier->get_suplier($kode_suplier);
        foreach($suplier as $row_suplier){
            $data['nama_suplier'] = $row_suplier->nama_suplier;
            $data['kode_suplier'] = $row_suplier->kode_suplier;
        }
        $data['data_pembayaran'] = $this->invoice->data_ost_pembayaran($data['KodePembayaran']);
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembayaran Suplier';
        $data['edit']           = '0';
        $data['Halaman']        = 'Halaman Pembayaran Suplier';
        $data['Sub_Halaman']    = 'List Pembayaran Suplier';
        $data['Active']         = 'bayar_suplier';
        $data['menu']           = 'toko';
        $data['list_penjualan ']   = '0';

        return view("pembayaran_suplier.list_terjual", ["data" => $data]);
    }

    public function add_prd_bayar($id_tr_penjualan)
    {
        $data_input = $this->invoice->get_penjualan_produk_suplier($id_tr_penjualan);
        // print_r($data_input);
        // die();
        foreach($data_input as $row){
            $kode_pembayaran    = KodePembayaran();
            $kode_produk        = $row->kode_produk;
            $harga              = $row->harga_suplier;
            $jumlah             = $row->jumlah;
            $total              = $row->total;
            $kode_suplier       = $row->kode_suplier;
            $id_detail_pembelian= $row->id_detail_pembelian;
            $status             = '0';

        }

        try{
        DB::insert(
                'insert into detail_pembayaran_suplier (kode_pembayaran_suplier,
                                                        id_tr_det_penjualan,
                                                       kode_produk,
                                                       harga,
                                                       jumlah, 
                                                       total,
                                                       id_detail_pembelian, 
                                                       status) 
                                                values (?, ?, ?,?, ?, ?, ?, ?)',
                [
                    $kode_pembayaran,
                    $id_tr_penjualan,
                    $kode_produk,
                    $harga,
                    $jumlah,
                    $total,
                    $id_detail_pembelian,
                    $status
                ]
            );
        DB::commit();
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
        }
        return redirect()->route('cek_penjualan_get');
        
    }



    public function detail_invoice($id)
    {

        $detail_invoice      = $this->invoice->get_invoice($id);
        foreach ($detail_invoice as $row) {
            $data['tgl_invoice']     = $row->tgl_invoice;
            $data['no_invoice']      = $row->no_invoice;
            $data['total']           = $row->total;
            $data['biaya_tambahan']  = $row->biaya_tambahan;
            $data['diskon']          = $row->diskon;
            $data['det_suplier']     = $this->suplier->get_suplier($row->kode_suplier);
            $data['det_pembelian']   = $this->pembelian->list_det_pembelian($row->kode_pembelian);
        }


        $data['flag_edit']      = '0';
        $data['title']          = 'Detail Invoice';
        $data['edit']           = '0';
        $data['Halaman']        = 'Detail Invoice';
        $data['Sub_Halaman']    = 'Detail Invoice';
        $data['Active']         = 'invoice';
        $data['menu']           = 'toko';

        return view("invoice.detail_invoice", ["data" => $data]);
    }



    public function info_pembayaran($id)
    {

        $detail_invoice      = $this->invoice->get_invoice($id);
        foreach ($detail_invoice as $row) {
            $data['tgl_invoice']     = $row->tgl_invoice;
            $data['no_invoice']      = $row->no_invoice;
            $data['total']           = $row->total;
            $data['biaya_tambahan']  = $row->biaya_tambahan;
            $data['diskon']          = $row->diskon;
            $data['det_suplier']     = $this->suplier->get_suplier($row->kode_suplier);
            $data['det_pembayaran']   = $this->invoice->detail_pembayaran($id);
        }
        $reff_pembayaran = $this->invoice->reff_pembayaran($id);
        if ($reff_pembayaran > 0 or $reff_pembayaran != null) {
            $data['reff_pembayaran'] = $reff_pembayaran;
        } else {
            $data['reff_pembayaran'] = '0';
        }
        $data['flag_edit']      = '0';
        $data['title']          = 'Pembayaran Invoice';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembayaran Invoice';
        $data['Sub_Halaman']    = 'Pembayaran Invoice';
        $data['Active']         = 'invoice';
        $data['menu']           = 'toko';

        return view("invoice.detail_pembayaran", ["data" => $data]);
    }

    public function edit_invoice($id)
    {

        $detail_invoice      = $this->invoice->get_invoice($id);
        foreach ($detail_invoice as $row) {
            $data['tgl_invoice']     = $row->tgl_invoice;
            $data['id_invoice']     = $row->id_invoice;
            $data['no_invoice']      = $row->no_invoice;
            $data['total']           = $row->total;
            $suplier                 = $this->suplier->get_suplier($row->kode_suplier);
            foreach ($suplier as $row_suplier) {
                $data['kode_suplier'] = $row_suplier->kode_suplier;
                $data['nama_suplier'] = $row_suplier->nama_suplier;
            }
            $pembelian              = $this->pembelian->getInvoice($row->no_invoice);
            foreach ($pembelian as $row_pembelian) {
                $data['kode_pembelian'] = $row_pembelian->kode_pembelian;
            }
        }
        $data['list_suplier']   = $this->suplier->list_suplier();
        $data['title']          = 'Edit Invoice';
        $data['edit']           = '0';
        $data['Halaman']        = 'Edit Invoice';
        $data['Sub_Halaman']    = 'Edit Invoice';
        $data['Active']         = 'invoice';
        $data['menu']           = 'toko';

        return view("invoice.edit_invoice", ["data" => $data]);
    }

    public function bayar_invoice($no_invoice)
    {


        $data['no_pembayaran_invoice'] = kodeByrInvoice();
        $data['no_invoice']  = $no_invoice;
        $detail_invoice      = $this->invoice->get_invoice($no_invoice);
        foreach ($detail_invoice as $row) {
            $total          = $row->total;
        }
        $get_record_bayar = $this->invoice->getRecordByr($no_invoice);
        if (empty($get_record_bayar)) {
            $pengurangan_total = 0;
        }
        if (!empty($get_record_bayar)) {
            foreach ($get_record_bayar as $row_bayar) {
                $pengurangan_total = $row_bayar->total_pembayaran;
            }
        }

        $data['total']           = $total - $pengurangan_total;
        $data['list_suplier']   = $this->suplier->list_suplier();
        $data['title']          = 'Edit Invoice';
        $data['edit']           = '0';
        $data['Halaman']        = 'Edit Invoice';
        $data['Sub_Halaman']    = 'Edit Invoice';
        $data['Active']         = 'invoice';
        $data['menu']           = 'toko';

        return view("invoice.bayar_invoice", ["data" => $data]);
    }

    public function cetak_pembayaran_suplier($kode_pembayaran)
    {

        
        $data['flag_edit']      = '0';
        $data['title']          = 'Pembayaran Invoice';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembayaran Invoice';
        $data['Sub_Halaman']    = 'Pembayaran Invoice';
        $data['Active']         = 'invoice';
        $data['menu']           = 'toko';

        $old = $this->invoice->get_pembayaran_suplier($kode_pembayaran);
        foreach ($old as $row) {
           $data['no_pembayaran']   = $kode_pembayaran;
           $data['tgl_pembayaran']  = $row->tgl_pembayaran;
           $data['nama_suplier']    = $row->nama_suplier;
        }
        $data['list_pembayaran_suplier'] = $this->invoice->detail_pembayaran_suplier($kode_pembayaran);
        $sub_total=$this->invoice->sub_total_detail_pembayaran($kode_pembayaran);
        foreach($sub_total as $row_total){
            $data['sub_total'] = $row_total->total;
        }

        return view("pembayaran_suplier.cetak_pembayaran_suplier", ["data" => $data]);
    }

    public function edit_invoice_act(Request $request)
    {
        $no_invoice       = $request->no_invoice;
        $tgl_invoice      = date('Y-m-d', strtotime($request->tgl_invoice));
        $kode_suplier     = $request->kode_suplier;
        $kode_pembelian   = $request->kode_pembelian;
        $total            = InsertRupiah($request->total);
        $id_invoice       = $request->id_invoice;
        try {
            DB::update(
                'UPDATE invoice set 
                                    no_invoice = ?,
                                    kode_suplier = ?,
                                    total =?
                                    WHERE id_invoice =?',
                [
                    $no_invoice,
                    $kode_suplier,
                    $total,
                    $id_invoice
                ]
            );
            DB::update(
                'UPDATE pembelian set 
                                    no_invoice = ?,
                                    kode_suplier =?
                                    WHERE kode_pembelian =?',
                [
                    $no_invoice,
                    $kode_suplier,
                    $kode_pembelian
                ]
            );
            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            // DB::rollback();
        }

        return redirect()->route('invoice');
    }

    public function act_bayar_suplier($kode_pembayaran,$kode_suplier)
    {
       $data_pembayaran = $this->invoice->data_ost_pembayaran($kode_pembayaran);
       $id_anggota = $this->id_anggota;
       $date = date('Y-m-d');
       $datetime = date('Y-m-d H:i:s');
        try {
            $total = 0;
            foreach($data_pembayaran as $row){
                $id_detail_pembayaran_suplier = $row->id_detail_pembayaran_suplier;
                $total += $total + $row->total;
            DB::update(
                'UPDATE detail_pembayaran_suplier set 
                                    status = ?
                                    WHERE id_detail_pembayaran_suplier =?',
                [
                    '1',
                    $id_detail_pembayaran_suplier
                ]
            );
           
        }
         DB::insert(
                    'insert into pembayaran_suplier (kode_pembayaran_suplier,
                                                       kode_suplier,
                                                       tgl_pembayaran,
                                                       biaya_tambahan, 
                                                       total,
                                                       created_at, 
                                                       created_by, 
                                                       status) 
                                                values (?, ?, ?,?, ?, ?,?,?)',
                    [
                        $kode_pembayaran,
                        $kode_suplier,
                        $date,
                        0,
                        $total,
                        $datetime,
                        $id_anggota,
                        '1'
                    ]
                );
            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            // DB::rollback();
        }
        
        return redirect()->route('bayar_suplier');
        // return view("pembayaran_suplier.cetak_pembayaran_suplier", ["data" => $data]);
    }


    public function byr_invoice_act(Request $request)
    {
        $no_invoice       = $request->no_invoice;
        $no_pembayaran    = $request->no_pembayaran;
        $tgl_bayar        = date('Y-m-d', strtotime($request->tgl_bayar));
        $dibayar          = InsertRupiah($request->dibayar);
        $return           = InsertRupiah($request->return);

        $total            = $request->total;
        $sum_byr          = $dibayar + $return;

        if ($sum_byr > $total) {
            die('jumlah bayar melebihi tagihan');
            return redirect()->route('bayar_invoice/' . $no_invoice);
        }
        if ($sum_byr < $total) {
            $flag_lunas = '2';
        } else {
            $flag_lunas = '1';
        }

        try {
            if ($return > 0) {
                DB::insert(
                    'insert into pembayaran_invoice (no_pembayaran_invoice,
                                                       no_invoice,
                                                       tgl_pembayaran,
                                                       total_pembayaran, 
                                                       metode_pembayaran, 
                                                       status) 
                                                values (?, ?, ?,?, ?, ?)',
                    [
                        $no_pembayaran,
                        $no_invoice,
                        $tgl_bayar,
                        $return,
                        '2', '0'
                    ]
                );
            }

            DB::insert(
                'insert into pembayaran_invoice (no_pembayaran_invoice,
                                                       no_invoice,
                                                       tgl_pembayaran,
                                                       total_pembayaran, 
                                                       metode_pembayaran, 
                                                       status) 
                                                values (?, ?, ?,?, ?, ?)',
                [
                    $no_pembayaran,
                    $no_invoice,
                    $tgl_bayar,
                    $dibayar,
                    '1', $flag_lunas
                ]
            );

            DB::update('UPDATE invoice set status = ? WHERE no_invoice =?', [$flag_lunas, $no_invoice]);

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            // DB::rollback();
        }

        return redirect()->route('invoice');
    }
}
