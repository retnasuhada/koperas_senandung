<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
  
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\DB;
  
  
class AuthEcomController extends Controller
{
   
  
    public function login(Request $request)
    {

        // die('disini');
        $rules = [
            'username'              => 'required|username',
            'password'              => 'required|string'
        ];
  
        $messages = [
            'username.required'     => 'username wajib diisi',
            'username.username'     => 'username tidak valid',
            'password.required'     => 'Password wajib diisi',
            'password.string'       => 'Password harus berupa string'
        ];
        // $asli = 'senandung'.$request->password.'yatim';
        $password = Enkrip($request->password);
        // print_r($password);
        // die();
        $data = DB::select('SELECT a.*, c.id_jabatan, b.foto, b.nama_anggota, b.no_anggota FROM user as a
                            LEFT JOIN anggota as b on a.id_anggota=b.id_anggota
                            LEFT JOIN petugas as c on c.id_anggota = b.id_anggota
                            where a.username= ? AND a.password= ?', array($request->username,$password));
        $jml = count($data);

        if ($jml>0) {
            session_start();
            $data_suplier = DB::select('SELECT * FROM suplier where no_anggota=?', array($data[0]->no_anggota));
            $jml_spl = count($data_suplier);
            if ($jml_spl > 0) {
                $_SESSION['kode_suplier'] = $data_suplier[0]->kode_suplier;
            }else{
                $_SESSION['kode_suplier'] = '0';
            }
            
            // print_r(Auth::guard('anggota'));
            // die();
            // $request->session()->put('username',$data[0]->username);
            // $request->session()->put('id_anggota',$data[0]->id_anggota);
            // $request->session()->put('id_user',$data[0]->id_user);
            // $request->session()->put('id_jabatan',$data[0]->id_jabatan);
           
            $_SESSION['id_anggota'] = $data[0]->id_anggota;
            $_SESSION['id_jabatan'] = $data[0]->id_jabatan;
            $_SESSION['foto']       = $data[0]->foto; 
            $_SESSION['nama']       = $data[0]->nama_anggota;      
            return redirect()->route('home');

        } else { // false
  
            //Login Fail
            Session::flash('error', 'Email atau password salah');
            return redirect()->route('keluar');
        }
  
    }
  
    public function showFormRegister()
    {
        return view('register');
    }
  
    public function register(Request $request)
    {
        $rules = [
            'name'                  => 'required|min:3|max:35',
            'email'                 => 'required|email|unique:users,email',
            'password'              => 'required|confirmed'
        ];
  
        $messages = [
            'name.required'         => 'Nama Lengkap wajib diisi',
            'name.min'              => 'Nama lengkap minimal 3 karakter',
            'name.max'              => 'Nama lengkap maksimal 35 karakter',
            'email.required'        => 'Email wajib diisi',
            'email.email'           => 'Email tidak valid',
            'email.unique'          => 'Email sudah terdaftar',
            'password.required'     => 'Password wajib diisi',
            'password.confirmed'    => 'Password tidak sama dengan konfirmasi password'
        ];
  
        $validator = Validator::make($request->all(), $rules, $messages);
  
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }
  
        $user = new User;
        $user->name = ucwords(strtolower($request->name));
        $user->email = strtolower($request->email);
        $user->password = Hash::make($request->password);
        $user->email_verified_at = \Carbon\Carbon::now();
        $simpan = $user->save();
  
        if($simpan){
            Session::flash('success', 'Register berhasil! Silahkan login untuk mengakses data');
            return redirect()->route('login');
        } else {
            Session::flash('errors', ['' => 'Register gagal! Silahkan ulangi beberapa saat lagi']);
            return redirect()->route('register');
        }
    }
  
    public function logout(Request $request)
    {
       
        session_start();
        $_SESSION['id_anggota'] = '';
        unset($_SESSION['id_anggota']);
        $_SESSION['id_jabatan'] = '';
        unset($_SESSION['id_jabatan']);
        session_unset();
        session_destroy();
        // return redirect()->route('e_comm');
        return redirect()->route('keluar2');
        
    }


    public function under_maintenance()
    {
        
        return view('404');
    }
  
  
}