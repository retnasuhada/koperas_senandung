<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\Anggota;
use App\Models\Jabatan;



class JabatanController extends Controller
{
    public function __construct()
    {
        $this->Anggota = new Anggota();
        $this->jabatan = new Jabatan();
    }



    public function list_jabatan()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_jabatan']   = $this->jabatan->list_jabatan();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Jabatan';
        $data['Halaman']        = 'Jabatan';
        $data['Sub_Halaman']    = 'Daftar Jabatan';
        $data['Active']         = 'jabatan';
        $data['menu']               = 'master';
        return view("jabatan.list_jabatan", ["data" => $data]);
    }



    public function edit_jabatan($id)
    {
        $data['list_jabatan']   = $this->jabatan->list_jabatan();
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Jabatan';
        $data['save']           = '0';
        $data['Halaman']        = 'Jabatan';
        $data['Sub_Halaman']    = 'Daftar Jabatan';
        $data['menu']               = 'master';
        $data['Active']         = 'jabatan';
        $data['old']            = $this->jabatan->getJabatan($id);
        foreach ($data['old'] as $row) {
            $data['nama_jabatan'] = $row->nama_jabatan;
        }

        return view("jabatan.list_jabatan", ["data" => $data]);
    }

    public function add_jabatan(Request $request)
    {
        $nama_jabatan    = $request->nama_jabatan;
        $save = DB::insert('insert into jabatan (nama_jabatan) values (?)', [$nama_jabatan]);
        if ($save == false) {
            $data = '2';
        } elseif ($save == true) {
            $data = '1';
        }
        return redirect()->route('jabatan')->with(['data' => $data]);
    }
}
