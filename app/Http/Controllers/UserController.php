<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\M_User;
use App\Models\Anggota;
use App\Models\Jabatan;


use Illuminate\Support\Facades\Storage;




class UserController extends Controller
{
    public function __construct()
    {
        $this->user = new M_User();
        $this->anggota = new Anggota();
    }

    public function user()
    {

        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_user']      = $this->user->list_user();
        $data['list_anggota']   = $this->anggota->list_anggota();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman user';
        $data['edit']           = '0';
        $data['Halaman']        = 'Halaman User';
        $data['Sub_Halaman']    = 'List User';
        $data['Active']         = 'user';
        $data['menu']           = 'master';

        return view("user.list_user", ["data" => $data]);
    }



    public function edit_user($id)
    {
        $data['list_user']      = $this->user->list_user();
        $data['list_anggota']   = $this->anggota->list_anggota();
        $data['old']            = $this->user->getUser($id);
        $data['Halaman']        = 'Halaman User';
        $data['Sub_Halaman']    = 'List User';
        $data['Active']         = 'user';
        $data['menu']           = 'master';

        foreach ($data['old'] as $row) {

            $data['id_user']        = $row->id_user;
            $data['id_anggota']     = $row->id_anggota;
            $data['nama_anggota']   = $row->nama_anggota;
            $data['username']       = $row->username;
            $data['password']       = $row->password;
        }
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman user';
        $data['edit']           = '0';
        $data['save']           = '0';

        return view("user.list_user", ["data" => $data]);
    }

    public function edit_user_agt($id)
    {
        $data['old']            = $this->user->get_user($id);
        $data['Halaman']        = 'Halaman User';
        $data['Sub_Halaman']    = 'List User';
        $data['Active']         = 'null';
        $data['menu']           = 'null';

        foreach ($data['old'] as $row) {

            $data['id_user']        = $row->id_user;
            $data['id_anggota']     = $row->id_anggota;
            $data['username']       = $row->username;
            $data['password']       = $row->password;
        }
        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman user';
        $data['edit']           = '0';
        $data['save']           = '0';

        return view("user.edit_user", ["data" => $data]);
    }

    public function hapus_anggota($id)
    {
        $data['list_anggota']   = $this->Anggota->list_anggota();
        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Anggota';
        $data['save']           = '0';
        $data['Halaman']        = 'Halaman User';
        $data['Sub_Halaman']    = 'List User';
        $data['Active']         = 'user';
        $data['menu']           = 'master';
        try {
            DB::delete('DELETE FROM anggota WHERE id_anggota =?', [$id]);

            DB::commit();
            $data['save']           = '3';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data['save']           = '2';
            // something went wrong
        }

        return view("anggota.list_anggota", ["data" => $data]);
    }

    public function edit_user_act(Request $request)
    {

        $this->validate($request, [
            'username'   => 'required',
            'password'   => 'required'
        ]);

        // data from edit
        $id_user        = $request->post('id_user');
        $username       = $request->post('username');
        $password       = Enkrip($request->post('password'));

        try {
            DB::update(
                'UPDATE user set 
                                username = ?, 
                                password = ?
                              WHERE id_user =?',
                [
                    $username,
                    $password,
                    $id_user
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            DB::rollback();
            $data          = '2';
        }

        return redirect()->route('user')->with(['data' => $data]);
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function edit_user_agt_act(Request $request)
    {

        $this->validate($request, [
            'username'   => 'required',
            'password'   => 'required'
        ]);

        // data from edit
        $id_user        = $request->post('id_user');
        $username       = $request->post('username');
        $password       = Enkrip($request->post('password'));

        try {
            DB::update(
                'UPDATE user set 
                                username = ?, 
                                password = ?
                              WHERE id_user =?',
                [
                    $username,
                    $password,
                    $id_user
                ]
            );

            DB::commit();
            $data           = '1';
        } catch (\Exception $e) {
            DB::rollback();
            $data          = '2';
        }
        // profil_anggota
        return redirect()->route('home');
        // return view("anggota.list_anggota", ["data"=>$data]);
    }

    public function add_user(Request $request)
    {
        $this->validate($request, [
            'id_anggota' => 'required',
            'username'   => 'required',
            'password'   => 'required'
        ]);


        // for input database
        $id_anggota     = $request->post('id_anggota');
        $username       = $request->post('username');
        $password       = Enkrip($request->post('password'));
        $save = DB::insert('insert into user (id_anggota,username,password) values (?, ?, ?)', [$id_anggota, $username, $password]);
        if ($save == TRUE) {
            $data           = '1';
        } else {
            $data           = '2';
        }
        // die($data['save']);


        // return view("user.list_user", ["data"=>$data]);
        return redirect()->route('user')->with(['data' => $data]);

        // return redirect()->route('home');

    }
}
