<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\Anggota;



class HomeControllers extends Controller
{
    public function __construct()
    {
        $this->Anggota = new Anggota();
    }

    public function index()
    {
        $id_anggota = Session::get('id_anggota');
        // $data = array();
        $data['data'] = $this->Anggota->get_anggota($id_anggota);
        $data['title']      = 'Halaman Home';
        $data['Halaman']      = 'Home';
        $data['Sub_Halaman']  = '';
        $data['Active'] = '';
        $data['menu'] = '-';
        if (!empty($data)) {
            foreach ($data as $row) {
                $data['data'] = $row;
            }
            return view("produk.produk", ["data" => $data]);
        } else {
            die('tidak ada data');
        }
    }

    public function top()
    {
       return view("layouts.top_nav");
    }


    public function detail()
    {
        $id_anggota = Session::get('id_anggota');
        // $data = array();
        $data['data'] = $this->Anggota->get_anggota($id_anggota);
        $data['title']      = 'Halaman Home';
        $data['Halaman']      = 'Home';
        $data['Sub_Halaman']  = '';
        $data['Active'] = '';
        $data['menu'] = '-';
        if (!empty($data)) {
            foreach ($data as $row) {
                $data['data'] = $row;
            }
            return view("welcome", ["data" => $data]);
        } else {
            die('tidak ada data');
        }
    }
}
