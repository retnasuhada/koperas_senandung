<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\Anggota;
use App\Models\Simpanan;



class SimpananController extends Controller
{
    public function __construct()
    {
        $this->Anggota = new Anggota();
        $this->simpanan = new Simpanan();
        session_start();
    }


    public function list_sw()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->list_sw('PV-K002');
        $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002');
        $data['title']          = 'Simpanan Wajib';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan wajib';
        $data['Active']         = 'list_sw';
        $data['menu']           = 'simpanan';


        return view("simpanan", ["data" => $data]);
    }

    public function agt_sw()
    {
        $id_anggota = $_SESSION['id_anggota'];
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->agt_list_sw('PV-K002', $id_anggota);
        // $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002'); 
        $data['title']          = 'Simpanan Wajib';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan wajib';
        $data['Active']         = 'list_sw';
        $data['menu']           = 'simpanan';


        return view("simpanan.anggota.list_sw", ["data" => $data]);
    }
    public function agt_ss()
    {
        $id_anggota = $_SESSION['id_anggota'];
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->agt_list_sw('PV-K003', $id_anggota);
        // $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002'); 
        $data['title']          = 'Simpanan Sukarela';
        $data['Halaman']        = 'Halaman Sukarela';
        $data['Sub_Halaman']    = 'Simpanan Sukarela';
        $data['Active']         = 'list_ss';
        $data['menu']           = 'simpanan';


        return view("simpanan.anggota.list_sw", ["data" => $data]);
    }

    public function agt_sp()
    {
        $id_anggota = $_SESSION['id_anggota'];
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->agt_list_sw('PV-K001', $id_anggota);
        // $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002'); 
        $data['title']          = 'Simpanan Pokok';
        $data['Halaman']        = 'Halaman Pokok';
        $data['Sub_Halaman']    = 'Simpanan Pokok';
        $data['Active']         = 'list_sp';
        $data['menu']           = 'simpanan';


        return view("simpanan.anggota.list_sw", ["data" => $data]);
    }

    public function agt_modal()
    {
        $id_anggota = $_SESSION['id_anggota'];
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->agt_list_sw('PV-K004', $id_anggota);
        // $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002'); 
        $data['title']          = 'Modal Anggota';
        $data['Halaman']        = 'Modal Anggota';
        $data['Sub_Halaman']    = 'Modal Anggota';
        $data['Active']         = 'modal_anggota';
        $data['menu']           = 'simpanan';


        return view("simpanan.anggota.list_sw", ["data" => $data]);
    }

    public function agt_hibah()
    {
        $id_anggota = $_SESSION['id_anggota'];
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->agt_list_sw('PV-K006', $id_anggota);
        // $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002'); 
        $data['title']          = 'Dana Hibah';
        $data['Halaman']        = 'Halaman Dana Hibah';
        $data['Sub_Halaman']    = 'Dana Hibah';
        $data['Active']         = 'dana_hibah';
        $data['menu']           = 'simpanan';



        return view("simpanan.anggota.list_sw", ["data" => $data]);
    }





    public function detail_simpanan_agt($id)
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        // $data['simpanan']       = $this->simpanan->list_sw_agt('PV-K002');
        $data['simpanan_det']       = $this->simpanan->get_simpanan_agt('PV-K002', $id);
        $data['title']          = 'Simpanan Wajib';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan wajib';
        $data['Active']         = 'list_sw';
        $data['menu']           = 'simpanan';


        return view("simpanan_sw_det", ["data" => $data]);
    }

    public function detail_simpanan_sp($id)
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        // $data['simpanan']       = $this->simpanan->list_sw_agt('PV-K002');
        $data['simpanan']       = $this->simpanan->get_simpanan_agt('PV-K001', $id);
        $data['title']          = 'Simpanan Pokok';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan Pokok';
        $data['Active']         = 'list_sp';
        $data['menu']           = 'simpanan';


        return view("simpanan_pokok_det", ["data" => $data]);
    }

    public function detail_simpanan_ss($id)
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->get_simpanan_agt('PV-K003', $id);
        $data['title']          = 'Simpanan Sukarela';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan Sukarela';
        $data['Active']         = 'list_ss';
        $data['menu']           = 'simpanan';


        return view("simpanan_sukarela_det", ["data" => $data]);
    }

    public function list_sp()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->list_sw('PV-K001');
        $data['simpanan_det']   = $this->simpanan->get_simpanan_det('PV-K001');
        $data['title']          = 'Simpanan Pokok';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan Pokok';
        $data['Active']         = 'list_sp';
        $data['menu']           = 'simpanan';


        return view("simpanan_pokok", ["data" => $data]);
    }

    public function list_ss()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->list_sw('PV-K003');
        $data['simpanan_det']   = $this->simpanan->get_simpanan_det('PV-K003');
        $data['title']          = 'Simpanan Sukarela';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan Sukarela';
        $data['Active']         = 'list_ss';
        $data['menu']           = 'simpanan';


        return view("simpanan_sukarela", ["data" => $data]);
    }

    public function modal_anggota()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->list_sw('PV-K004');
        $data['simpanan_det']   = $this->simpanan->get_simpanan_det('PV-K004');
        $data['title']          = 'Modal Anggota';
        $data['Halaman']        = 'Modal Anggota';
        $data['Sub_Halaman']    = 'Modal Anggota';
        $data['Active']         = 'modal_anggota';
        $data['menu']           = 'simpanan';

        return view("simpanan.modal_anggota", ["data" => $data]);
    }

    public function detail_modal_det($id)
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        // $data['simpanan']       = $this->simpanan->list_sw('PV-K004'); 
        $data['simpanan']   = $this->simpanan->get_simpanan_agt('PV-K004', $id);
        $data['title']          = 'Modal Anggota';
        $data['Halaman']        = 'Modal Anggota';
        $data['Sub_Halaman']    = 'Modal Anggota';
        $data['Active']         = 'modal_anggota';
        $data['menu']           = 'simpanan';

        return view("simpanan.modal_anggota_det", ["data" => $data]);
    }

    public function dana_hibah()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->list_sw('PV-K006');
        $data['simpanan_det']   = $this->simpanan->get_simpanan_det('PV-K006');
        $data['title']          = 'Dana Hibah';
        $data['Halaman']        = 'Halaman Dana Hibah';
        $data['Sub_Halaman']    = 'Dana Hibah';
        $data['Active']         = 'dana_hibah';
        $data['menu']           = 'simpanan';

        return view("simpanan.dana_hibah", ["data" => $data]);
    }

    public function detail_hibah_agt($id)
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        // $data['simpanan']       = $this->simpanan->list_sw('PV-K006'); 
        $data['simpanan']   = $this->simpanan->get_simpanan_agt('PV-K006', $id);
        $data['title']          = 'Dana Hibah';
        $data['Halaman']        = 'Halaman Dana Hibah';
        $data['Sub_Halaman']    = 'Dana Hibah';
        $data['Active']         = 'dana_hibah';
        $data['menu']           = 'simpanan';

        return view("simpanan.dana_hibah_det", ["data" => $data]);
    }

    public function jenis_simpanan()
    {

        $data['jenis_simpanan']     = $this->simpanan->jenis_simpanan();
        $data['flag_edit']          = '0';
        $data['save']               = '0';
        $data['title']              = 'Data Simpanan';
        $data['Halaman']            = 'Halaman Simpanan';
        $data['Sub_Halaman']        = 'jenis_simpanan';
        $data['Active']             = 'jenis_simpanan';
        $data['menu']               = 'master';
        return view("simpanan.jenis_simpanan", ["data" => $data]);
    }




    public function edit_jenis_simpanan($id)
    {
        $data['jenis_simpanan'] = $this->simpanan->jenis_simpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Data Simpanan';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'jenis_simpanan';
        $data['Active']         = 'jenis_simpanan';
        $data['menu']           = 'master';

        $data['old']            = $this->simpanan->getJenisSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['nama_jenis_simpanan'] = $row->nama_jenis_simpanan;
            $data['id_jenis_simpanan']   = $row->id_jenis_simpanan;
            $data['status']              = $row->status;
        }

        return view("simpanan.jenis_simpanan", ["data" => $data]);
    }

    public function edit_simpanan($id)
    {
        // $data['data_simpanan'] = $this->simpanan->getSimpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Data Simpanan';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan wajib';
        $data['Active']         = 'list_sw';
        $data['menu']           = 'simpanan';
        // $unik_kode          = "SW-".strtotime(date('Y-m-d h:i:s'));
        $data['list_anggota']            = $this->Anggota->list_anggota();
        // $data['list_jenis_penerimaan']   = $this->simpanan->list_jenis_penerimaan();
        $data['list_akun']   = $this->simpanan->listAkunlancar();

        $data['old']            = $this->simpanan->getSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['id_simpanan']            = $row->id_simpanan;
            $data['unik_kode']              = $row->unik_kode;
            $data['tgl_simpanan']           = $row->tgl_simpanan;
            $data['id_jenis_simpanan']      = $row->id_jenis_simpanan;
            $data['id_anggota']             = $row->id_anggota;
            $data['jumlah_simpanan']        = "Rp" . number_format($row->jumlah_simpanan, 0, ',', '.');
            $data['kode_akun']              = $row->id_jenis_penerimaan;
            $periode_simpanan               = explode("-", $row->periode_simpanan);
            $data['bln_simpanan']         = $periode_simpanan[0];
            $data['thn_simpanan']           = $periode_simpanan[1];
        }

        return view("simpanan.form_simpanan", ["data" => $data]);
    }

    public function edit_sp($id)
    {
        // $data['data_simpanan'] = $this->simpanan->getSimpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Simpanan Pokok';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan Pokok';
        $data['Active']         = 'list_sp';
        $data['menu']           = 'simpanan';
        // $unik_kode          = "SW-".strtotime(date('Y-m-d h:i:s'));
        $data['list_anggota']            = $this->Anggota->list_anggota();
        $data['list_akun']      = $this->simpanan->listAkunlancar();

        $data['old']            = $this->simpanan->getSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['id_simpanan']            = $row->id_simpanan;
            $data['unik_kode']              = $row->unik_kode;
            $data['tgl_simpanan']           = $row->tgl_simpanan;
            $data['id_jenis_simpanan']      = $row->id_jenis_simpanan;
            $data['id_anggota']             = $row->id_anggota;
            $data['jumlah_simpanan']        = "Rp" . number_format($row->jumlah_simpanan, 0, ',', '.');
            $data['kode_akun']              = $row->id_jenis_penerimaan;
            $periode_simpanan               = explode("-", $row->periode_simpanan);
            $data['bln_simpanan']           = $periode_simpanan[0];
            $data['thn_simpanan']           = $periode_simpanan[1];
        }

        return view("simpanan.form_simpanan_pokok", ["data" => $data]);
    }


    public function edit_ss($id)
    {
        // $data['data_simpanan'] = $this->simpanan->getSimpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Simpanan Sukarela';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan Sukarela';
        $data['Active']         = 'list_ss';
        $data['menu']           = 'simpanan';
        // $unik_kode          = "SW-".strtotime(date('Y-m-d h:i:s'));
        $data['list_anggota']            = $this->Anggota->list_anggota();
        $data['list_akun']   = $this->simpanan->listAkunlancar();

        $data['old']            = $this->simpanan->getSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['id_simpanan']            = $row->id_simpanan;
            $data['unik_kode']              = $row->unik_kode;
            $data['tgl_simpanan']           = $row->tgl_simpanan;
            $data['id_jenis_simpanan']      = $row->id_jenis_simpanan;
            $data['id_anggota']             = $row->id_anggota;
            $data['jumlah_simpanan']        = "Rp" . number_format($row->jumlah_simpanan, 0, ',', '.');
            $data['kode_akun']              = $row->id_jenis_penerimaan;
            $periode_simpanan               = explode("-", $row->periode_simpanan);
            $data['bln_simpanan']           = $periode_simpanan[0];
            $data['thn_simpanan']           = $periode_simpanan[1];
        }

        return view("simpanan.form_simpanan_sukarela", ["data" => $data]);
    }


    public function edit_modal_anggota($id)
    {
        // $data['data_simpanan'] = $this->simpanan->getSimpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Modal Anggota';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Modal Anggota';
        $data['Active']         = 'modal_anggota';
        $data['menu']           = 'simpanan';
        $data['list_anggota']            = $this->Anggota->list_anggota();
        $data['list_akun']   = $this->simpanan->listAkunlancar();

        $data['old']            = $this->simpanan->getSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['id_simpanan']            = $row->id_simpanan;
            $data['unik_kode']              = $row->unik_kode;
            $data['tgl_simpanan']           = $row->tgl_simpanan;
            $data['id_jenis_simpanan']      = $row->id_jenis_simpanan;
            $data['id_anggota']             = $row->id_anggota;
            $data['jumlah_simpanan']        = "Rp" . number_format($row->jumlah_simpanan, 0, ',', '.');
            $data['kode_akun']              = $row->id_jenis_penerimaan;
        }

        return view("simpanan.form_modal_anggota", ["data" => $data]);
    }

    public function edit_dana_hibah($id)
    {
        // $data['data_simpanan'] = $this->simpanan->getSimpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Dana Hibah';
        $data['Halaman']        = 'Halaman Dana Hibah';
        $data['Sub_Halaman']    = 'Dana Hibah';
        $data['Active']         = 'dana_hibah';
        $data['menu']           = 'simpanan';
        $data['list_anggota']            = $this->Anggota->list_anggota();
        $data['list_akun']   = $this->simpanan->listAkunlancar();

        $data['old']            = $this->simpanan->getSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['id_simpanan']            = $row->id_simpanan;
            $data['unik_kode']              = $row->unik_kode;
            $data['tgl_simpanan']           = $row->tgl_simpanan;
            $data['id_jenis_simpanan']      = $row->id_jenis_simpanan;
            $data['id_anggota']             = $row->id_anggota;
            $data['jumlah_simpanan']        = "Rp" . number_format($row->jumlah_simpanan, 0, ',', '.');
            $data['kode_akun']              = $row->id_jenis_penerimaan;
        }

        return view("simpanan.form_dana_hibah", ["data" => $data]);
    }

    public function detail_simpanan($id)
    {
        // $data['data_simpanan'] = $this->simpanan->getSimpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Data Simpanan';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan wajib';
        $data['Active']         = 'list_sw';
        $data['menu']           = 'simpanan';
        // $unik_kode          = "SW-".strtotime(date('Y-m-d h:i:s'));

        $data['old']            = $this->simpanan->getDetailSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['id_simpanan']            = $row->id_simpanan;
            $data['unik_kode']              = $row->unik_kode;
            $data['tgl_simpanan']           = $row->tgl_simpanan;
            $data['nama_jenis_simpanan']    = $row->nama_jenis_simpanan;
            $data['no_anggota']             = $row->no_anggota;
            $data['nama_anggota']           = $row->nama_anggota;
            $data['nama_jenis_penerimaan']  = $row->nama_akun;
            $data['jumlah_simpanan']        = "Rp" . number_format($row->jumlah_simpanan, 0, ',', '.');

            $periode_simpanan               = explode("-", $row->periode_simpanan);
            $data['bln_simpanan']           = getBulan($periode_simpanan[0]);
            $data['thn_simpanan']           = $periode_simpanan[1];
            $data['periode_simpanan']       = $data['bln_simpanan'] . " - " . $data['thn_simpanan'];
        }
        $data['jenis_simpanan'] = "Wajib";
        $data['menu_back'] = "route('simpanan_wajib_list')";
        return view("simpanan.detail_simpanan", ["data" => $data]);
    }

    public function detail_simpanan_pokok($id)
    {
        // $data['data_simpanan'] = $this->simpanan->getSimpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Simpanan Pokok';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan Pokok';
        $data['Active']         = 'list_sp';
        $data['menu']           = 'simpanan';
        // $unik_kode          = "SW-".strtotime(date('Y-m-d h:i:s'));

        $data['old']            = $this->simpanan->getDetailSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['id_simpanan']            = $row->id_simpanan;
            $data['unik_kode']              = $row->unik_kode;
            $data['tgl_simpanan']           = $row->tgl_simpanan;
            $data['nama_jenis_simpanan']    = $row->nama_jenis_simpanan;
            $data['no_anggota']             = $row->no_anggota;
            $data['nama_anggota']           = $row->nama_anggota;
            $data['nama_jenis_penerimaan']  = $row->nama_akun;
            $data['jumlah_simpanan']        = "Rp" . number_format($row->jumlah_simpanan, 0, ',', '.');

            $periode_simpanan               = explode("-", $row->periode_simpanan);
            $data['bln_simpanan']           = getBulan($periode_simpanan[0]);
            $data['thn_simpanan']           = $periode_simpanan[1];
            $data['periode_simpanan']       = $data['bln_simpanan'] . " - " . $data['thn_simpanan'];
        }
        $data['jenis_simpanan'] = "Pokok";
        $data['menu_back'] = "route('simpanan_pokok_list')";
        return view("simpanan.detail_simpanan", ["data" => $data]);
    }

    public function detail_simpanan_sukarela($id)
    {
        // $data['data_simpanan'] = $this->simpanan->getSimpanan();
        $data['flag_edit']      = '1';
        $data['save']           = '0';
        $data['title']          = 'Simpanan Sukarela';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan Sukarela';
        $data['Active']         = 'list_ss';
        $data['menu']           = 'simpanan';
        // $unik_kode          = "SW-".strtotime(date('Y-m-d h:i:s'));

        $data['old']            = $this->simpanan->getDetailSimpanan($id);
        foreach ($data['old'] as $row) {
            $data['id_simpanan']            = $row->id_simpanan;
            $data['unik_kode']              = $row->unik_kode;
            $data['tgl_simpanan']           = $row->tgl_simpanan;
            $data['nama_jenis_simpanan']    = $row->nama_jenis_simpanan;
            $data['no_anggota']             = $row->no_anggota;
            $data['nama_anggota']           = $row->nama_anggota;
            $data['nama_jenis_penerimaan']  = $row->nama_akun;
            $data['jumlah_simpanan']        = "Rp" . number_format($row->jumlah_simpanan, 0, ',', '.');

            $periode_simpanan               = explode("-", $row->periode_simpanan);
            $data['bln_simpanan']           = getBulan($periode_simpanan[0]);
            $data['thn_simpanan']           = $periode_simpanan[1];
            $data['periode_simpanan']       = $data['bln_simpanan'] . " - " . $data['thn_simpanan'];
        }
        $data['jenis_simpanan'] = "Sukarela";
        $data['menu_back'] = "route('simpanan_sukarela_list')";
        return view("simpanan.detail_simpanan", ["data" => $data]);
    }

    public function edit_jenis_simpanan_act(Request $request)
    {

        $data['flag_edit']          = '0';
        // $data['save']               = '0';
        $data['title']              = 'Data Simpanan';

        $data['save']               = '0';
        $data['title']              = 'Data Simpanan';
        $data['Halaman']            = 'Halaman Simpanan';
        $data['Sub_Halaman']        = 'jenis_simpanan';
        $data['Active']             = 'jenis_simpanan';
        $data['menu']               = 'master';
        // return view("simpanan.jenis_simpanan", ["data"=>$data]);

        $this->validate($request, [
            'nama_simpanan'  => 'required',
            'status'         => 'required'
        ]);

        $nama_jenis_simpanan    = $request->nama_simpanan;
        $status                 = $request->status;
        $id_jenis_simpanan      = $request->id_jenis_simpanan;

        try {
            DB::update(
                'UPDATE jenis_simpanan set 
                                    nama_jenis_simpanan = ?, 
                                    status = ?
                                  WHERE id_jenis_simpanan =?',
                [
                    $nama_jenis_simpanan,
                    $status,
                    $id_jenis_simpanan
                ]
            );

            DB::commit();
            $data['save']           = '1';
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $data['save']           = '2';
            // something went wrong
        }
        $data['jenis_simpanan']     = $this->simpanan->jenis_simpanan();
        return view("simpanan.jenis_simpanan", ["data" => $data]);
    }

    public function edit_sw_act(Request $request)
    {
        $data['flag_edit']          = '0';
        $data['title']              = 'Data Simpanan';

        $this->validate($request, [
            'id_anggota'  => 'required',
            'jumlah_simpanan'  => 'required',
            'tgl_simpanan'  => 'required',
            'jenis_penerimaan'  => 'required'
        ]);

        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $tgl_simpanan       = date('Y-m-d', strtotime($request->tgl_simpanan));
        $unik_kode          = $request->unik_kode;
        $jenis_penerimaan   = $request->jenis_penerimaan;
        $kode_akun          = $jenis_penerimaan;
        $kode_akun_asal     = 'PV-K002';
        $bulan_periode      = $request->bulan_periode;
        $tahun_periode      = $request->tahun_periode;
        $id_simpanan        = $request->id_simpanan;
        $periode            = $request->bulan_periode . "-" . $request->tahun_periode;

        $getTransaksi       = $this->simpanan->getTransaksi($unik_kode);
        foreach ($getTransaksi as $row_transaksi) {
            $id_transaksi = $row_transaksi->id_transaksi;
            $nominal_transaksi = $row_transaksi->nominal_transaksi;
            $min_jumlah_simpanan = 0 - $nominal_transaksi;
        }

        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach ($data_anggota as $res) {
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota . " / " . $no_anggota;
        $keterangan         = "Simpanan wajib " . $nama_anggota . " periode " . $periode;
        $keterangan_rek     = "Rekonsiloasi Simpanan wajib " . $nama_anggota . " periode " . $periode;

        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach ($saldo_akun_tujuan as $arr_tujuan) {
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + ($min_jumlah_simpanan + $jumlah_simpanan);

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach ($saldo_akun_asal as $arr_asal) {
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + ($min_jumlah_simpanan + $jumlah_simpanan);


        DB::beginTransaction();
        try {
            DB::update(
                'UPDATE simpanan set 
                                    id_anggota = ?, 
                                    tgl_simpanan = ?,
                                    jumlah_simpanan = ?, 
                                    id_jenis_penerimaan = ?,
                                    periode_simpanan = ?

                                  WHERE id_simpanan =?',
                [
                    $id_anggota,
                    $tgl_simpanan,
                    $jumlah_simpanan,
                    $kode_akun,
                    $periode,
                    $id_simpanan
                ]
            );
            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $min_jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan_rek, $unik_kode, $id_transaksi]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan, $unik_kode, $id_transaksi]);

            // DB::update('UPDATE jenis_penerimaan set total_saldo = ? WHERE id_jenis_penerimaan =?', [$new_saldo, $jenis_penerimaan]);

            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);

            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Data Simpanan';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('simpanan_wajib_list');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            $data['success'] = '0';
            $data['title']      = 'Data Simpanan';
            $data['message'] = 'Gagal Menyimpan Data !';
            return redirect()->route('edit_simpanan/' . $id_simpanan);
        }
    }

    public function edit_sp_act(Request $request)
    {
        $data['flag_edit']          = '0';
        $data['title']              = 'Simpanan Pokok';

        $this->validate($request, [
            'id_anggota'  => 'required',
            'jumlah_simpanan'  => 'required',
            'tgl_simpanan'  => 'required',
            'jenis_penerimaan'  => 'required'
        ]);

        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $tgl_simpanan       = date('Y-m-d', strtotime($request->tgl_simpanan));
        $unik_kode          = $request->unik_kode;
        $jenis_penerimaan    = $request->jenis_penerimaan;
        $bulan_periode       = $request->bulan_periode;
        $tahun_periode       = $request->tahun_periode;
        $id_simpanan         = $request->id_simpanan;
        $periode            = $request->bulan_periode . "-" . $request->tahun_periode;

        $getTransaksi       = $this->simpanan->getTransaksi($unik_kode);
        foreach ($getTransaksi as $row_transaksi) {
            $id_transaksi = $row_transaksi->id_transaksi;
            $nominal_transaksi = $row_transaksi->nominal_transaksi;
            $min_jumlah_simpanan = 0 - $nominal_transaksi;
        }

        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach ($data_anggota as $res) {
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota . " / " . $no_anggota;
        $keterangan         = "Simpanan Pokok " . $nama_anggota . " periode " . $periode;
        $keterangan_rek         = "Rekonsiloasi Simpanan Pokok " . $nama_anggota . " periode " . $periode;

        $kode_akun          = $jenis_penerimaan;
        $kode_akun_asal     = 'PV-K001';

        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach ($saldo_akun_tujuan as $arr_tujuan) {
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + ($min_jumlah_simpanan + $jumlah_simpanan);

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach ($saldo_akun_asal as $arr_asal) {
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + ($min_jumlah_simpanan + $jumlah_simpanan);



        DB::beginTransaction();
        try {
            DB::update(
                'UPDATE simpanan set 
                                    id_anggota = ?, 
                                    tgl_simpanan = ?,
                                    jumlah_simpanan = ?, 
                                    id_jenis_penerimaan = ?,
                                    periode_simpanan = ?

                                  WHERE id_simpanan =?',
                [
                    $id_anggota,
                    $tgl_simpanan,
                    $jumlah_simpanan,
                    $kode_akun,
                    $periode,
                    $id_simpanan
                ]
            );

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $min_jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan_rek, $unik_kode, $id_transaksi]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan, $unik_kode, $id_transaksi]);

            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);


            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Simpanan Pokok';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('simpanan_pokok_list');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            // $data['success'] = '0';
            // $data['title']      = 'Data Simpanan';
            // $data['message'] = 'Gagal Menyimpan Data !';
            // return redirect()->route('edit_simpanan/'.$id_simpanan);

        }
    }

    public function edit_ss_act(Request $request)
    {
        $data['flag_edit']          = '0';
        $data['title']              = 'Simpanan Sukarela';

        $this->validate($request, [
            'id_anggota'  => 'required',
            'jumlah_simpanan'  => 'required',
            'tgl_simpanan'  => 'required',
            'jenis_penerimaan'  => 'required'
        ]);

        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $tgl_simpanan       = date('Y-m-d', strtotime($request->tgl_simpanan));
        $unik_kode          = $request->unik_kode;
        $jenis_penerimaan    = $request->jenis_penerimaan;
        $bulan_periode       = $request->bulan_periode;
        $tahun_periode       = $request->tahun_periode;
        $id_simpanan         = $request->id_simpanan;
        $periode            = $request->bulan_periode . "-" . $request->tahun_periode;

        $getTransaksi       = $this->simpanan->getTransaksi($unik_kode);
        foreach ($getTransaksi as $row_transaksi) {
            $id_transaksi = $row_transaksi->id_transaksi;
            $nominal_transaksi = $row_transaksi->nominal_transaksi;
            $min_jumlah_simpanan = 0 - $nominal_transaksi;
        }

        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach ($data_anggota as $res) {
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota . " / " . $no_anggota;
        $keterangan         = "Simpanan Sukarela " . $nama_anggota . " periode " . $periode;
        $keterangan_rek         = "Rekonsiloasi Simpanan Sukarela " . $nama_anggota . " periode " . $periode;

        $kode_akun          = $jenis_penerimaan;
        $kode_akun_asal     = 'PV-K003';

        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach ($saldo_akun_tujuan as $arr_tujuan) {
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + ($min_jumlah_simpanan + $jumlah_simpanan);

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach ($saldo_akun_asal as $arr_asal) {
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + ($min_jumlah_simpanan + $jumlah_simpanan);


        DB::beginTransaction();
        try {
            DB::update(
                'UPDATE simpanan set 
                                    id_anggota = ?, 
                                    tgl_simpanan = ?,
                                    jumlah_simpanan = ?, 
                                    id_jenis_penerimaan = ?,
                                    periode_simpanan = ?

                                  WHERE id_simpanan =?',
                [
                    $id_anggota,
                    $tgl_simpanan,
                    $jumlah_simpanan,
                    $kode_akun,
                    $periode,
                    $id_simpanan
                ]
            );

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $min_jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan_rek, $unik_kode, $id_transaksi]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan, $unik_kode, $id_transaksi]);

            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);


            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Simpanan Sukarela';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('simpanan_sukarela_list');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            $data['success'] = '0';
            $data['title']      = 'Data Simpanan';
            $data['message'] = 'Gagal Menyimpan Data !';
            return redirect()->route('edit_simpanan/' . $id_simpanan);
        }
    }

    public function edit_modal_anggota_act(Request $request)
    {
        $data['flag_edit']          = '0';
        $data['title']              = 'Modal Anggota';

        $this->validate($request, [
            'id_anggota'  => 'required',
            'jumlah_simpanan'  => 'required',
            'tgl_simpanan'  => 'required',
            'jenis_penerimaan'  => 'required'
        ]);

        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $tgl_simpanan       = date('Y-m-d', strtotime($request->tgl_simpanan));
        $unik_kode          = $request->unik_kode;
        $jenis_penerimaan    = $request->jenis_penerimaan;
        $bulan_periode       = $request->bulan_periode;
        $tahun_periode       = $request->tahun_periode;
        $id_simpanan         = $request->id_simpanan;
        $periode            = $request->bulan_periode . "-" . $request->tahun_periode;

        $getTransaksi       = $this->simpanan->getTransaksi($unik_kode);
        foreach ($getTransaksi as $row_transaksi) {
            $id_transaksi = $row_transaksi->id_transaksi;
            $nominal_transaksi = $row_transaksi->nominal_transaksi;
            $min_jumlah_simpanan = 0 - $nominal_transaksi;
        }

        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach ($data_anggota as $res) {
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota . " / " . $no_anggota;
        $keterangan         = "Simpanan Sukarela " . $nama_anggota . " periode " . $periode;
        $keterangan_rek         = "Rekonsiloasi Simpanan Sukarela " . $nama_anggota . " periode " . $periode;

        $kode_akun          = $jenis_penerimaan;
        $kode_akun_asal     = 'PV-K004';

        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach ($saldo_akun_tujuan as $arr_tujuan) {
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + ($min_jumlah_simpanan + $jumlah_simpanan);

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach ($saldo_akun_asal as $arr_asal) {
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + ($min_jumlah_simpanan + $jumlah_simpanan);


        DB::beginTransaction();
        try {
            DB::update(
                'UPDATE simpanan set 
                                    id_anggota = ?, 
                                    tgl_simpanan = ?,
                                    jumlah_simpanan = ?, 
                                    id_jenis_penerimaan = ?,
                                    periode_simpanan = ?

                                  WHERE id_simpanan =?',
                [
                    $id_anggota,
                    $tgl_simpanan,
                    $jumlah_simpanan,
                    $kode_akun,
                    $periode,
                    $id_simpanan
                ]
            );

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $min_jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan_rek, $unik_kode, $id_transaksi]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan, $unik_kode, $id_transaksi]);

            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);


            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Modal Anggota';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('modal_anggota');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            $data['success'] = '0';
            $data['title']      = 'Modal Anggota';
            $data['message'] = 'Gagal Menyimpan Data !';
            return redirect()->route('edit_modal_anggota/' . $id_simpanan);
        }
    }

    public function edit_dana_hibah_act(Request $request)
    {
        $data['flag_edit']          = '0';
        $data['title']              = 'Dana Hibah';

        $this->validate($request, [
            'id_anggota'  => 'required',
            'jumlah_simpanan'  => 'required',
            'tgl_simpanan'  => 'required',
            'jenis_penerimaan'  => 'required'
        ]);

        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $tgl_simpanan       = date('Y-m-d', strtotime($request->tgl_simpanan));
        $unik_kode          = $request->unik_kode;
        $jenis_penerimaan    = $request->jenis_penerimaan;
        $bulan_periode       = $request->bulan_periode;
        $tahun_periode       = $request->tahun_periode;
        $id_simpanan         = $request->id_simpanan;
        $periode            = $request->bulan_periode . "-" . $request->tahun_periode;

        $getTransaksi       = $this->simpanan->getTransaksi($unik_kode);
        foreach ($getTransaksi as $row_transaksi) {
            $id_transaksi = $row_transaksi->id_transaksi;
            $nominal_transaksi = $row_transaksi->nominal_transaksi;
            $min_jumlah_simpanan = 0 - $nominal_transaksi;
        }

        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach ($data_anggota as $res) {
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota . " / " . $no_anggota;
        $keterangan         = "Dana Hibah " . $nama_anggota . " periode " . $periode;
        $keterangan_rek         = "Rekonsiloasi Dana Hibah " . $nama_anggota . " periode " . $periode;

        $kode_akun          = $jenis_penerimaan;
        $kode_akun_asal     = 'PV-K006';

        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach ($saldo_akun_tujuan as $arr_tujuan) {
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + ($min_jumlah_simpanan + $jumlah_simpanan);

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach ($saldo_akun_asal as $arr_asal) {
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + ($min_jumlah_simpanan + $jumlah_simpanan);


        DB::beginTransaction();
        try {
            DB::update(
                'UPDATE simpanan set 
                                    id_anggota = ?, 
                                    tgl_simpanan = ?,
                                    jumlah_simpanan = ?, 
                                    id_jenis_penerimaan = ?,
                                    periode_simpanan = ?

                                  WHERE id_simpanan =?',
                [
                    $id_anggota,
                    $tgl_simpanan,
                    $jumlah_simpanan,
                    $kode_akun,
                    $periode,
                    $id_simpanan
                ]
            );

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $min_jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan_rek, $unik_kode, $id_transaksi]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, reff_id) values (?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, date('Y-m-d h:i:s'), '110', $keterangan, $unik_kode, $id_transaksi]);

            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);


            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Dana Hibah';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('dana_hibah');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            $data['success'] = '0';
            $data['title']      = 'Dana Hibah';
            $data['message'] = 'Gagal Menyimpan Data !';
            return redirect()->route('edit_dana_hibah/' . $id_simpanan);
        }
    }



    public function add_jenis_simpanan(Request $request)
    {
        $nama_jenis_simpanan    = $request->nama_simpanan;
        $status                = $request->status;

        $save = DB::insert('insert into jenis_simpanan (nama_jenis_simpanan, status) values (?, ?)', [$nama_jenis_simpanan, $status]);
        if ($save == false) {
            $data['success'] = '0';
            $data['title']      = 'Data Simpanan';
            $data['message'] = 'Gagal Menyimpan Data !';
            return redirect()->route('jenis_simpanan');
            // return view("simpanan.jenis_simpanan", ["data"=>$data]);

        } elseif ($save == true) {

            $data['success'] = '1';
            $data['title']      = 'Data Simpanan';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('jenis_simpanan');
            // return view("simpanan.jenis_simpanan", ["data"=>$data]);
        }
    }

    public function add_simpanan_act(Request $request)
    {
        $jenis_penerimaan   = $request->jenis_penerimaan;
        $kode_akun          = $jenis_penerimaan; // for update akun tujuan

        $kode_akun_asal     = 'PV-K002'; // default kode for SW
        $tgl_simpanan       = date('Y-m-d', strtotime($request->tgl_simpanan));
        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $create_at          = date('Y-m-d h:i:s');
        $id_jenis_simpanan  = 'PV-K002';
        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach ($data_anggota as $res) {
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota . " / " . $no_anggota;
        $periode            = $request->bulan_periode . "-" . $request->tahun_periode;
        $keterangan         = "Simpanan wajib " . $nama_anggota . " periode " . $periode;
        $unik_kode          = "PV-K002-" . strtotime(date('Y-m-d h:i:s'));



        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach ($saldo_akun_tujuan as $arr_tujuan) {
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + $jumlah_simpanan;

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach ($saldo_akun_asal as $arr_asal) {
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + $jumlah_simpanan;

        DB::beginTransaction();
        try {
            DB::insert('insert into simpanan (id_jenis_simpanan, id_anggota, tgl_simpanan, jumlah_simpanan, status, created_at, id_jenis_penerimaan, created_by, periode_simpanan, unik_kode) values (?, ?, ?, ?, ?, ?, ?,?,?,?)', [$id_jenis_simpanan, $id_anggota, $tgl_simpanan, $jumlah_simpanan, '1', $create_at, $kode_akun, '110', $periode, $unik_kode]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, tujuan, asal) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, $create_at, '110', $keterangan, $unik_kode, $kode_akun, $kode_akun_asal]);


            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);

            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Data Simpanan';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('simpanan_wajib_list');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            // $data['success'] = '0';
            // $data['title']      = 'Data Simpanan';
            // $data['message'] = 'Gagal Menyimpan Data !';
            // return redirect()->route('simpanan_wajib_list');

        }
    }

    public function add_sp_act(Request $request)
    {
        $jenis_penerimaan   = $request->jenis_penerimaan;
        $kode_akun          = $jenis_penerimaan; // for update akun tujuan

        $kode_akun_asal     = 'PV-K001'; // default kode for SW
        $tgl_simpanan       = date('Y-m-d', strtotime($request->tgl_simpanan));
        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $create_at          = date('Y-m-d h:i:s');
        $id_jenis_simpanan  = 'PV-K001';
        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach ($data_anggota as $res) {
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota . " / " . $no_anggota;
        $periode            = $request->bulan_periode . "-" . $request->tahun_periode;
        $keterangan         = "Simpanan Pokok " . $nama_anggota . " periode " . $periode;
        $unik_kode          = "PV-K001-" . strtotime(date('Y-m-d h:i:s'));



        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach ($saldo_akun_tujuan as $arr_tujuan) {
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + $jumlah_simpanan;

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach ($saldo_akun_asal as $arr_asal) {
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + $jumlah_simpanan;

        DB::beginTransaction();
        try {
            DB::insert('insert into simpanan (id_jenis_simpanan, id_anggota, tgl_simpanan, jumlah_simpanan, status, created_at, id_jenis_penerimaan, created_by, periode_simpanan, unik_kode) values (?, ?, ?, ?, ?, ?, ?,?,?,?)', [$id_jenis_simpanan, $id_anggota, $tgl_simpanan, $jumlah_simpanan, '1', $create_at, $kode_akun, '110', $periode, $unik_kode]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, tujuan, asal) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, $create_at, '110', $keterangan, $unik_kode, $kode_akun, $kode_akun_asal]);


            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);


            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Simpanan Pokok';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('simpanan_pokok_list');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            $data['success'] = '0';
            $data['title']      = 'Simpanan Pokok';
            $data['message'] = 'Gagal Menyimpan Data !';
            return redirect()->route('simpanan_pokok_list');
        }
    }


    public function add_ss_act(Request $request)
    {
        $jenis_penerimaan   = $request->jenis_penerimaan;
        $kode_akun          = $jenis_penerimaan; // for update akun tujuan

        $kode_akun_asal     = 'PV-K003'; // default kode for SW
        $tgl_simpanan       = date('Y-m-d', strtotime($request->tgl_simpanan));
        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $create_at          = date('Y-m-d h:i:s');
        $id_jenis_simpanan  = 'PV-K003';
        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach ($data_anggota as $res) {
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota . " / " . $no_anggota;
        $periode            = $request->bulan_periode . "-" . $request->tahun_periode;
        $keterangan         = "Simpanan Sukarela " . $nama_anggota . " periode " . $periode;
        $unik_kode          = "PV-K003-" . strtotime(date('Y-m-d h:i:s'));



        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach ($saldo_akun_tujuan as $arr_tujuan) {
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + $jumlah_simpanan;

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach ($saldo_akun_asal as $arr_asal) {
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + $jumlah_simpanan;

        DB::beginTransaction();
        try {
            DB::insert('insert into simpanan (id_jenis_simpanan, id_anggota, tgl_simpanan, jumlah_simpanan, status, created_at, id_jenis_penerimaan, created_by, periode_simpanan, unik_kode) values (?, ?, ?, ?, ?, ?, ?,?,?,?)', [$id_jenis_simpanan, $id_anggota, $tgl_simpanan, $jumlah_simpanan, '1', $create_at, $kode_akun, '110', $periode, $unik_kode]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, tujuan, asal) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, $create_at, '110', $keterangan, $unik_kode, $kode_akun, $kode_akun_asal]);


            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);




            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Simpanan Sukarela';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('simpanan_sukarela_list');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            $data['success'] = '0';
            $data['title']      = 'Simpanan Sukarela';
            $data['message'] = 'Gagal Menyimpan Data !';
            return redirect()->route('simpanan_sukarela_list');
        }
    }

    public function add_modal_act(Request $request)
    {
        $jenis_penerimaan   = $request->jenis_penerimaan;
        $kode_akun          = $jenis_penerimaan; // for update akun tujuan

        $kode_akun_asal     = 'PV-K004'; // default kode for SW
        $tgl_simpanan       = date('Y-m-d', strtotime($request->tgl_simpanan));
        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $create_at          = date('Y-m-d h:i:s');
        $id_jenis_simpanan  = 'PV-K004';
        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach ($data_anggota as $res) {
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota . " / " . $no_anggota;
        $periode            = $request->bulan_periode . "-" . $request->tahun_periode;
        $keterangan         = "Modal Anggota" . $nama_anggota . " periode " . $periode;
        $unik_kode          = "PV-K004-" . strtotime(date('Y-m-d h:i:s'));



        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach ($saldo_akun_tujuan as $arr_tujuan) {
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + $jumlah_simpanan;

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach ($saldo_akun_asal as $arr_asal) {
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + $jumlah_simpanan;

        DB::beginTransaction();
        try {
            DB::insert('insert into simpanan (id_jenis_simpanan, id_anggota, tgl_simpanan, jumlah_simpanan, status, created_at, id_jenis_penerimaan, created_by, periode_simpanan, unik_kode) values (?, ?, ?, ?, ?, ?, ?,?,?,?)', [$id_jenis_simpanan, $id_anggota, $tgl_simpanan, $jumlah_simpanan, '1', $create_at, $kode_akun, '110', $periode, $unik_kode]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, tujuan, asal) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, $create_at, '110', $keterangan, $unik_kode, $kode_akun, $kode_akun_asal]);


            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);





            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Modal Anggota';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('modal_anggota');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            $data['success'] = '0';
            $data['title']      = 'Modal Anggota';
            $data['message'] = 'Gagal Menyimpan Data !';
            return redirect()->route('modal_anggota');
        }
    }

    public function add_dana_hibah_act(Request $request)
    {
        $jenis_penerimaan   = $request->jenis_penerimaan;
        $kode_akun          = $jenis_penerimaan; // for update akun tujuan

        $kode_akun_asal     = 'PV-K006'; // default kode for SW
        $tgl_simpanan       = date('Y-m-d', strtotime($request->tgl_simpanan));
        $id_anggota         = $request->id_anggota;
        $jumlah_simpanan    = InsertRupiah($request->jumlah_simpanan);
        $create_at          = date('Y-m-d h:i:s');
        $id_jenis_simpanan  = 'PV-K006';
        $data_anggota       = $this->Anggota->getAnggota($id_anggota);
        foreach ($data_anggota as $res) {
            $nama_anggota   = $res->nama_anggota;
            $no_anggota     = $res->no_anggota;
        }
        $nama_anggota       = $nama_anggota . " / " . $no_anggota;
        $periode            = $request->bulan_periode . "-" . $request->tahun_periode;
        $keterangan         = "Dana Hibah" . $nama_anggota . " periode " . $periode;
        $unik_kode          = "PV-K006-" . strtotime(date('Y-m-d h:i:s'));



        $saldo_akun_tujuan  = $this->simpanan->getSaldoAkun($kode_akun);
        foreach ($saldo_akun_tujuan as $arr_tujuan) {
            $total_saldo_tujaun = $arr_tujuan->nilai;
        }
        $new_saldo_tujaun = $total_saldo_tujaun + $jumlah_simpanan;

        $saldo_akun_asal  = $this->simpanan->getSaldoAkun($kode_akun_asal);
        foreach ($saldo_akun_asal as $arr_asal) {
            $total_saldo_asal = $arr_asal->nilai;
        }
        $new_saldo_asal = $total_saldo_asal + $jumlah_simpanan;

        DB::beginTransaction();
        try {
            DB::insert('insert into simpanan (id_jenis_simpanan, id_anggota, tgl_simpanan, jumlah_simpanan, status, created_at, id_jenis_penerimaan, created_by, periode_simpanan, unik_kode) values (?, ?, ?, ?, ?, ?, ?,?,?,?)', [$id_jenis_simpanan, $id_anggota, $tgl_simpanan, $jumlah_simpanan, '1', $create_at, $kode_akun, '110', $periode, $unik_kode]);

            DB::insert('insert into transaksi (id_jenis_transaksi, nominal_transaksi, tgl_transaksi, created_at, created_by, keterangan, unik_kode, tujuan, asal) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', ['1', $jumlah_simpanan, $tgl_simpanan, $create_at, '110', $keterangan, $unik_kode, $kode_akun, $kode_akun_asal]);


            // update akun
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_tujaun, $kode_akun]);
            DB::update('UPDATE detail_akun set nilai = ? WHERE kode_akun =?', [$new_saldo_asal, $kode_akun_asal]);

            DB::commit();
            $data['success'] = '1';
            $data['title']      = 'Dana Hibah';
            $data['message'] = 'Berhasil Disimpan';
            return redirect()->route('dana_hibah');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            // $data['success'] = '0';
            // $data['title']      = 'Dana Hibah';
            // $data['message'] = 'Gagal Menyimpan Data !';
            // return redirect()->route('dana_hibah');

        }
    }


    public function add_sw()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        // $data['simpanan']       = $this->simpanan->list_sw(); 
        $data['list_anggota']   = $this->Anggota->list_anggota();
        $data['list_akun']   = $this->simpanan->listAkunlancar();
        $data['flag_edit']      = '0';
        $data['title']          = 'Data Simpanan';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan wajib';
        $data['Active']         = 'list_sw';
        $data['menu']           = 'simpanan';

        return view("simpanan.form_simpanan", ["data" => $data]);
    }

    public function add_sp()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        // $data['simpanan']       = $this->simpanan->list_sw(); 
        $data['list_anggota']   = $this->Anggota->list_anggota();
        $data['list_akun']   = $this->simpanan->listAkunlancar();
        $data['flag_edit']      = '0';
        $data['title']          = 'Simpanan Pokok';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan Pokok';
        $data['Active']         = 'list_sp';
        $data['menu']           = 'simpanan';

        return view("simpanan.form_simpanan_pokok", ["data" => $data]);
    }

    public function add_ss()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        // $data['simpanan']       = $this->simpanan->list_sw(); 
        $data['list_anggota']   = $this->Anggota->list_anggota();
        // $data['list_jenis_penerimaan']   = $this->simpanan->list_jenis_penerimaan();
        $data['list_akun']   = $this->simpanan->listAkunlancar();
        $data['flag_edit']      = '0';
        $data['title']          = 'Simpanan Sukarela';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan Sukarela';
        $data['Active']         = 'list_ss';
        $data['menu']           = 'simpanan';

        return view("simpanan.form_simpanan_sukarela", ["data" => $data]);
    }

    public function add_modal_anggota()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }

        $data['list_anggota']   = $this->Anggota->list_anggota();
        $data['list_akun']      = $this->simpanan->listAkunlancar();
        $data['flag_edit']      = '0';
        $data['title']          = 'Modal Anggota';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Modal Anggota';
        $data['Active']         = 'modal_anggota';
        $data['menu']           = 'simpanan';

        return view("simpanan.form_modal_anggota", ["data" => $data]);
    }

    public function add_dana_hibah()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }

        $data['list_anggota']   = $this->Anggota->list_anggota();
        $data['list_akun']      = $this->simpanan->listAkunlancar();
        $data['flag_edit']      = '0';
        $data['title']          = 'Dana Hibah';
        $data['Halaman']        = 'Halaman Dana Hibah';
        $data['Sub_Halaman']    = 'Dana Hibah';
        $data['Active']         = 'dana_hibah';
        $data['menu']           = 'simpanan';

        return view("simpanan.form_dana_hibah", ["data" => $data]);
    }


    public function hapus_jenis_simpanan($id)
    {
        try {
            DB::delete('DELETE FROM jenis_simpanan WHERE id_jenis_simpanan =?', [$id]);

            DB::commit();
            $data['save']           = '3';
        } catch (\Exception $e) {
            DB::rollback();
            $data['save']           = '2';
        }

        $data['jenis_simpanan']     = $this->simpanan->jenis_simpanan();
        $data['flag_edit']          = '0';
        // $data['save']               = '0';
        $data['title']              = 'Data Simpanan';
        // $data['title']          = 'Data Simpanan';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'jenis_simpanan';
        $data['Active']         = 'jenis_simpanan';
        $data['menu']           = 'simpanan';

        return view("simpanan.jenis_simpanan", ["data" => $data]);
    }
}
