<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
  
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;
use App\Models\Anggota;
use App\Models\Simpanan;

  
  
class SimpananAnggotaController extends Controller
{
     public function __construct(){
        $this->Anggota = new Anggota();
        $this->simpanan = new Simpanan();
         if (session_status() !== PHP_SESSION_ACTIVE) session_start();
          // session_start();
          $id_anggota = $_SESSION['id_anggota'];
          $id_jabatan = $_SESSION['id_jabatan'];
          $foto_profil = $_SESSION['foto'];
          $nama_profil = $_SESSION['nama'];
    }
    
    public function agt_sw()
    {
        $id_anggota = $_SESSION['id_anggota'];
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->agt_list_sw('PV-K002', $id_anggota);
        // $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002'); 
        $data['title']          = 'Simpanan Wajib';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan wajib';
        $data['Active']         = 'list_sw';
        $data['menu']           = 'simpanan';


        return view("simpanan.anggota.list_sw", ["data" => $data]);
    }

    public function agt_ss()
    {
        $id_anggota = $_SESSION['id_anggota'];
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->agt_list_sw('PV-K003', $id_anggota);
        // $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002'); 
        $data['title']          = 'Simpanan Sukarela';
        $data['Halaman']        = 'Halaman Sukarela';
        $data['Sub_Halaman']    = 'Simpanan Sukarela';
        $data['Active']         = 'list_ss';
        $data['menu']           = 'simpanan';


        return view("simpanan.anggota.list_sw", ["data" => $data]);
    }
    public function agt_sp()
    {
        $id_anggota = $_SESSION['id_anggota'];
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->agt_list_sw('PV-K001', $id_anggota);
        // $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002'); 
        $data['title']          = 'Simpanan Pokok';
        $data['Halaman']        = 'Halaman Pokok';
        $data['Sub_Halaman']    = 'Simpanan Pokok';
        $data['Active']         = 'list_sp';
        $data['menu']           = 'simpanan';


        return view("simpanan.anggota.list_sw", ["data" => $data]);
    }

    public function agt_modal()
    {
        $id_anggota = $_SESSION['id_anggota'];
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->agt_list_sw('PV-K004', $id_anggota);
        // $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002'); 
        $data['title']          = 'Modal Anggota';
        $data['Halaman']        = 'Modal Anggota';
        $data['Sub_Halaman']    = 'Modal Anggota';
        $data['Active']         = 'modal_anggota';
        $data['menu']           = 'simpanan';


        return view("simpanan.anggota.list_sw", ["data" => $data]);
    }

    public function agt_hibah()
    {
        $id_anggota = $_SESSION['id_anggota'];
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['simpanan']       = $this->simpanan->agt_list_sw('PV-K006', $id_anggota);
        // $data['simpanan_det']       = $this->simpanan->get_simpanan_det('PV-K002'); 
        $data['title']          = 'Dana Hibah';
        $data['Halaman']        = 'Halaman Dana Hibah';
        $data['Sub_Halaman']    = 'Dana Hibah';
        $data['Active']         = 'dana_hibah';
        $data['menu']           = 'simpanan';



        return view("simpanan.anggota.list_sw", ["data" => $data]);
    }
    public function detail_simpanan_agt($id)
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        // $data['simpanan']       = $this->simpanan->list_sw_agt('PV-K002');
        $data['simpanan_det']       = $this->simpanan->get_simpanan_agt('PV-K002', $id);
        $data['title']          = 'Simpanan Wajib';
        $data['Halaman']        = 'Halaman Simpanan';
        $data['Sub_Halaman']    = 'Simpanan wajib';
        $data['Active']         = 'list_sw';
        $data['menu']           = 'simpanan';


        return view("simpanan_sw_det", ["data" => $data]);
    }

   
}
  
  