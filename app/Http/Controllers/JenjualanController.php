<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Illuminate\Support\Facades\DB;

use App\Models\Pembelian;
use App\Models\Produk;

use Illuminate\Support\Facades\Storage;




class PembelianController extends Controller
{
    public function __construct()
    {
        $this->pembelian    = new Pembelian();
        $this->produk       = new Produk();
    }



    public function index()
    {
        if (Session::get('data') != NUll or Session::get('data') != "") {
            $data['save'] = Session::get('data');
        } else {
            $data['save']           = '0';
        }
        $data['list_pembelian']   = $this->pembelian->list_pembelian();

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.list", ["data" => $data]);
    }

    public function add_pembelian()
    {
        $data['kode_pembelian']     = kodePembelian();
        $data['list_pembelian']     = $this->pembelian->list_det_pembelian($data['kode_pembelian']);
        $data['list_produk']        = $this->produk->list_produk();

        $data['flag_edit']      = '0';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.add_pembelian", ["data" => $data]);
    }

    public function edit_detail_pembelian($id)
    {
        $old = $this->pembelian->get_detail_pembelian($id);

        foreach ($old as $row) {
            $data['kode_produk']    = $row->kode_produk;
            $data['jumlah']         = $row->jumlah;
            $data['harga']          = $row->harga_semua;
            $data['id_detail_pembelian'] = $row->id_detail_pembelian;
        }

        $data['kode_pembelian']     = kodePembelian();
        $data['list_pembelian']     = $this->pembelian->list_det_pembelian($data['kode_pembelian']);
        $data['list_produk']        = $this->produk->list_produk();

        $data['flag_edit']      = '1';
        $data['title']          = 'Halaman Pembelian';
        $data['edit']           = '0';
        $data['Halaman']        = 'Pembelian';
        $data['Sub_Halaman']    = 'Daftar Pembelian';
        $data['Active']         = 'pembelian';
        $data['menu']           = 'toko';
        return view("pembelian.add_pembelian", ["data" => $data]);
    }


    public function add_pembelian_act(Request $request)
    {

        $this->validate($request, [
            'kode_pembelian'    => 'required',
            'kode_produk'       => 'required',
            'jumlah'            => 'required',
            'harga'             => 'required'
        ]);


        $kode_pembelian = $request->post('kode_pembelian');
        $kode_produk    = $request->post('kode_produk');
        $jumlah         = $request->post('jumlah');
        $harga          = InsertRupiah($request->post('harga'));

        try {
            DB::insert(
                'insert into detail_pembelian (kode_pembelian,kode_produk, jumlah, harga_semua, status) 
                                                                        values (?, ?, ?, ?, ?)',
                [$kode_pembelian, $kode_produk, $jumlah, $harga, '0']
            );

            DB::commit();
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
        }
        return redirect()->route('add_pembelian');
    }

    public function save_pembelian_act(Request $request)
    {

        $this->validate($request, [
            'kode_pembelian'    => 'required',
            'tgl_pembelian'       => 'required'

        ]);


        $kode_pembelian = $request->post('kode_pembelian');
        $tgl_pembelian  = date('Y-m-d', strtotime($request->post('tgl_pembelian')));
        $no_invoice     = $request->post('no_invoice');
        $kode_suplier   = $request->post('kode_suplier');
        $biaya_tambahan = InsertRupiah($request->post('biaya_tambahan'));


        try {
            // $harga_semua = 0;
            $data_detail_pembelian = $this->pembelian->temp_det_pembelian($kode_pembelian);
            $count_barang = $this->pembelian->count_barang($kode_pembelian);
            foreach ($count_barang as $rs_count) {
                $total_barang = $rs_count->total;
            }
            $additional = $biaya_tambahan / $total_barang;
            foreach ($data_detail_pembelian as $row) {
                $id_detail_pembelian = $row->id_detail_pembelian;
                $kode_produk    = $row->kode_produk;
                $stok           = $row->jumlah;
                $harga_pendapatan    = $row->harga_semua + $additional;
                DB::insert(
                    'insert into detail_produk (kode_produk,stok) 
                                                                        values (?, ?)',
                    [$kode_produk, $stok]
                );
                DB::update(
                    'UPDATE detail_pembelian set 
                                    harga_pendapatan = ?,
                                    status = ?
                                    WHERE id_detail_pembelian =?',
                    [
                        $harga_pendapatan,
                        '1',
                        $id_detail_pembelian
                    ]
                );
            }


            DB::insert(
                'insert into pembelian (kode_pembelian,kode_suplier, tgl_pembelian, biaya_tambahan, status, no_invoice) 
                                                                        values (?, ?, ?, ?, ?, ?)',
                [$kode_pembelian, $kode_suplier, $tgl_pembelian, $biaya_tambahan, '0', $no_invoice]
            );

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
        }
        return redirect()->route('pembelian');
    }

    public function edit_pembelian_act(Request $request)
    {
        // 
        $id_detail_pembelian = $request->post('id_detail_pembelian');
        $kode_pembelian = $request->post('kode_pembelian');
        $kode_produk    = $request->post('kode_produk');
        $jumlah         = $request->post('jumlah');
        $harga          = InsertRupiah($request->post('harga'));
        // print_r($kode_pembelian.'||'.$kode_produk.'||'.$jumlah.'||'.$harga);
        // die();
        try {
            DB::update(
                'UPDATE detail_pembelian set 
                                        kode_pembelian = ?, 
                                        kode_produk = ?,
                                        jumlah = ?, 
                                        harga_semua = ?
                                      WHERE id_detail_pembelian =?',
                [
                    $kode_pembelian,
                    $kode_produk,
                    $jumlah,
                    $harga,
                    $id_detail_pembelian
                ]
            );

            DB::commit();
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
        }

        return redirect()->route('add_pembelian');
    }

    public function hapus_detail_pembelian($id)
    {

        try {
            DB::delete('DELETE FROM detail_pembelian WHERE id_detail_pembelian =?', [$id]);

            DB::commit();
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
        }

        return redirect()->route('add_pembelian');
    }
}
