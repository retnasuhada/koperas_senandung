<?php

use App\Models\Produk;
use Illuminate\Support\Facades\DB;

 function InsertRupiah($rupiah){
 	$harga_str = preg_replace("/[^0-9]/", "", $rupiah);
	$harga_int = (int) $harga_str;
	
    return $harga_int;   
}

 function InsertPersen($rupiah){
    $arr_persen = explode(",", $rupiah);
    $harga_str = preg_replace("/[^0-9]/", "", $arr_persen[0]);
    
    // $harga_int = (int) $harga_str;
    
    return $harga_str;   
}

function getBulan($bulan){
 	if($bulan=='1'){
 		$nm_bulan = 'Januari';
 	}
 	if($bulan=='2'){
 		$nm_bulan = 'Februari';
 	}
 	if($bulan=='3'){
 		$nm_bulan = 'Maret';
 	}
 	if($bulan=='4'){
 		$nm_bulan = 'April';
 	}
 	if($bulan=='5'){
 		$nm_bulan = 'Mei';
 	}
 	if($bulan=='6'){
 		$nm_bulan = 'Juni';
 	}
 	if($bulan=='7'){
 		$nm_bulan = 'Juli';
 	}
 	if($bulan=='8'){
 		$nm_bulan = 'Agustus';
 	}
 	if($bulan=='9'){
 		$nm_bulan = 'September';
 	}
 	if($bulan=='10'){
 		$nm_bulan = 'Oktober';
 	}
 	if($bulan=='11'){
 		$nm_bulan = 'November';
 	}
 	if($bulan=='12'){
 		$nm_bulan = 'Desember';
 	}

    return $nm_bulan;   
}

function Enkrip($password){
 	$psw1 = base64_encode($password);
	    $psw2 = base64_encode($psw1);
	    	$psw3 = base64_encode($psw2);
	    		$psw4 = base64_encode($psw3);
	    			$psw5 = base64_encode($psw4);
	    				$psw6 = base64_encode($psw5);
			
    return $psw6;   
}

function Deskrip($password){
 	$des6 = base64_decode($password);
        $des5 = base64_decode($des6);
        	$des4 = base64_decode($des5);
        		$des3 = base64_decode($des4);
        			$des2 = base64_decode($des3);
        				$deskripsi = base64_decode($des2);
			
    return $deskripsi;   
}
function getFoto($id){
	$foto = App\Models\Produk::getFoto($id);
}

function convertTgl($tgl){
    $arr_tgl = explode('/',$tgl);
   
    $d = $arr_tgl[0];
    $m = $arr_tgl[1];
    $y = $arr_tgl[2];
    $date = $y.'-'.$m.'-'.$d;
    return $date;

}

function KodeAnggota(){
	$y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_agt = DB::select("SELECT no_anggota FROM anggota
	    					where no_anggota like '%$like%'
							ORDER BY id_anggota
							DESC
							LIMIT 1 ");
        

        if(!empty($get_id_agt)){
            foreach($get_id_agt as $row_agt){
            $no_anggota = substr($row_agt->no_anggota,15,5);
            }

                $l_kode = $no_anggota+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-AGT-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-AGT-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-AGT-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-AGT-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-AGT-'.$like.'-00001';
        }
   return $new_kode;     
}

function KodeKategori(){
	$y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_kategori = DB::select("SELECT kode_kategori FROM kategori
	    					where kode_kategori like '%$like%'
							ORDER BY id_kategori
							DESC
							LIMIT 1 ");
        

        if(!empty($get_id_kategori)){
            foreach($get_id_kategori as $row_ktg){
            $kode_kategori = substr($row_ktg->kode_kategori,15,5);
            }

                $l_kode = $kode_kategori+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-KTG-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-KTG-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-KTG-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-KTG-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-KTG-'.$like.'-00001';
        }
   return $new_kode;     
}

function KodeInvoice(){
    $y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_kategori = DB::select("SELECT no_invoice FROM invoice
                            where no_invoice like '%$like%'
                            ORDER BY id_invoice
                            DESC
                            LIMIT 1 ");
        

        if(!empty($get_id_kategori)){
            foreach($get_id_kategori as $row_ktg){
            $kode_kategori = substr($row_ktg->no_invoice,15,5);
            }

                $l_kode = $kode_kategori+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-INV-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-INV-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-INV-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-INV-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-INV-'.$like.'-00001';
        }
   return $new_kode;     
}

function KodePembayaran(){
    $y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_kategori = DB::select("SELECT kode_pembayaran_suplier FROM pembayaran_suplier
                            where kode_pembayaran_suplier like '%$like%'
                            ORDER BY id_pembayaran_suplier
                            DESC
                            LIMIT 1 ");
        

        if(!empty($get_id_kategori)){
            foreach($get_id_kategori as $row_ktg){
            $kode_kategori = substr($row_ktg->kode_pembayaran_suplier,15,5);
            }

                $l_kode = $kode_kategori+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-PBS-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-PBS-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-PBS-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-PBS-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-PBS-'.$like.'-00001';
        }
   return $new_kode;     
}

function KodeSubKategori(){
	$y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_kategori = DB::select("SELECT kode_sub_kategori FROM sub_kategori
	    					where kode_sub_kategori like '%$like%'
							ORDER BY id_sub_kategori
							DESC
							LIMIT 1 ");
        

        if(!empty($get_id_kategori)){
            foreach($get_id_kategori as $row_ktg){
            $kode_kategori = substr($row_ktg->kode_sub_kategori,15,5);
            }

                $l_kode = $kode_kategori+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-SKT-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-SKT-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-SKT-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-SKT-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-SKT-'.$like.'-00001';
        }
   return $new_kode;     
}

function KodeChildKategori(){
	$y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_kategori = DB::select("SELECT kode_sub_kategori2 FROM sub_kategori2
	    					where kode_sub_kategori2 like '%$like%'
							ORDER BY id_sub_kategori2
							DESC
							LIMIT 1 ");
        

        if(!empty($get_id_kategori)){
            foreach($get_id_kategori as $row_ktg){
            $kode_kategori = substr($row_ktg->kode_sub_kategori2,15,5);
            }

                $l_kode = $kode_kategori+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-CKT-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-CKT-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-CKT-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-CKT-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-CKT-'.$like.'-00001';
        }
   return $new_kode;     
}

function KodeWarna(){
	$y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_kategori = DB::select("SELECT kode_warna FROM warna
	    					where kode_warna like '%$like%'
							ORDER BY id_warna
							DESC
							LIMIT 1 ");
        if(!empty($get_id_kategori)){
            foreach($get_id_kategori as $row_ktg){
            $kode_kategori = substr($row_ktg->kode_warna,15,5);
            }

                $l_kode = $kode_kategori+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-WRN-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-WRN-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-WRN-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-WRN-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-WRN-'.$like.'-00001';
        }
   return $new_kode;     
}

function kodeProduk(){
	$y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_kategori = DB::select("SELECT kode_produk FROM produk
	    					where kode_produk like '%$like%'
							ORDER BY kode_produk
							DESC
							LIMIT 1 ");
        if(!empty($get_id_kategori)){
            foreach($get_id_kategori as $row_ktg){
            $kode_kategori = substr($row_ktg->kode_produk,15,5);
            }

                $l_kode = $kode_kategori+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-PRD-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-PRD-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-PRD-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-PRD-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-PRD-'.$like.'-00001';
        }
   return $new_kode;     
}

function kodePembelian(){
	$y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_kategori = DB::select("SELECT kode_pembelian FROM pembelian
	    					where kode_pembelian like '%$like%'
							ORDER BY id_pembelian
							DESC
							LIMIT 1 ");
        if(!empty($get_id_kategori)){
            foreach($get_id_kategori as $row_ktg){
            $kode_kategori = substr($row_ktg->kode_pembelian,15,5);
            }

                $l_kode = $kode_kategori+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-PMP-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-PMP-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-PMP-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-PMP-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-PMP-'.$like.'-00001';
        }
   return $new_kode;     
}


function kodePembelianSpl($kode_suplier){
   
       $like = $kode_suplier;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_kategori = DB::select("SELECT kode_pembelian FROM pembelian
                            where kode_pembelian like '%$like%'
                            ORDER BY kode_pembelian
                            DESC
                            LIMIT 1 ");
        if(!empty($get_id_kategori)){
            foreach($get_id_kategori as $row_ktg){
            $kode_kategori = substr($row_ktg->kode_pembelian,21,5);
            }

                $l_kode = $kode_kategori+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = $like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = $like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = $like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = $like.'-0'.$l_kode;
                }

        }else{
            $new_kode = $like.'-00001';
        }
   return $new_kode;     
}

function kodePenjualan(){
    $y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_id_kategori = DB::select("SELECT kode_penjualan FROM penjualan
                            where kode_penjualan like '%$like%'
                            ORDER BY id_penjualan
                            DESC
                            LIMIT 1 ");
        if(!empty($get_id_kategori)){
            foreach($get_id_kategori as $row_ktg){
            $kode_kategori = substr($row_ktg->kode_penjualan,15,5);
            }

                $l_kode = $kode_kategori+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-PJL-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-PJL-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-PJL-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-PJL-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-PJL-'.$like.'-00001';
        }
   return $new_kode;     
}

function kodeSuplier(){
    $y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_suplier = DB::select("SELECT kode_suplier FROM suplier
                            where kode_suplier like '%$like%'
                            ORDER BY id_suplier
                            DESC
                            LIMIT 1 ");
        if(!empty($get_suplier)){
            foreach($get_suplier as $row_suplier){
            $kode_suplier = substr($row_suplier->kode_suplier,15,5);
            }

                $l_kode = $kode_suplier+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-SPL-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-SPL-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-SPL-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-SPL-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-SPL-'.$like.'-00001';
        }
   return $new_kode;     
}
function kodeByrInvoice(){
    $y = date('Y');
       $m = date('m');
       $like = substr($y,2).$m;
        // $get_id_agt = App\Models\Anggota::last_agt($like);
       $get_byr_invoice = DB::select("SELECT no_pembayaran_invoice FROM pembayaran_invoice
                            where no_pembayaran_invoice like '%$like%'
                            ORDER BY id_pembayaran_invoice
                            DESC
                            LIMIT 1 ");
        if(!empty($get_byr_invoice)){
            foreach($get_byr_invoice as $row_byr_inv){
            $no_pembayaran_invoice = substr($row_byr_inv->no_pembayaran_invoice,15,5);
            }

                $l_kode = $no_pembayaran_invoice+1;
                if (strlen($l_kode)=='1') {
                    $new_kode = 'SN165-PIN-'.$like.'-0000'.$l_kode;
                }
                if (strlen($l_kode)=='2') {
                    $new_kode = 'SN165-PIN-'.$like.'-000'.$l_kode;
                }
                if (strlen($l_kode)=='3') {
                    $new_kode = 'SN165-PIN-'.$like.'-00'.$l_kode;
                }
                if (strlen($l_kode)=='4') {
                    $new_kode = 'SN165-PIN-'.$like.'-0'.$l_kode;
                }

        }else{
            $new_kode = 'SN165-PIN-'.$like.'-00001';
        }
   return $new_kode;     
}


     

 function customImagePath($image_name)
{
    return public_path('folder_kamu/sub_folder_kamu/'.$image_name);
}
?>