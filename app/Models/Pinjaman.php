<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pinjaman extends Model
{

  public function jenis_pinjaman()
  {
    $data = DB::select("SELECT id_jenis_pinjaman,nama_jenis_pinjaman,margin,keterangan, 
                            CASE WHEN status = '1' THEN 'Aktif' 
                            ELSE 'Tidak Aktif' END AS status
                            FROM jenis_pinjaman");
    return $data;
  }


  public function getJenisPinjaman($id)
  {
    $data = DB::select("SELECT id_jenis_pinjaman,nama_jenis_pinjaman,margin,keterangan,status,max_pinjaman, max_tenor 
                            FROM jenis_pinjaman
                            WHERE id_jenis_pinjaman = ?", array($id));
    return $data;
  }
}
