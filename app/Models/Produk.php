<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Produk extends Model
{
	public function list_produk()
	{
		$data = DB::select("SELECT a.id_produk, a.kode_produk, a.nama_produk, b.nama_kategori, c.nama_sub_kategori, a.harga_umum, a.harga_anggota, a.harga_grosir_umum, a.harga_grosir_anggota, a.minimum_grosir,
	    					d.nama_sub_kategori2, 
	    					CASE WHEN e.stok > 0 THEN e.stok
	    					ELSE '0' END as stok
	    					FROM produk AS a
							LEFT JOIN kategori AS b ON a.kode_kategori = b.kode_kategori
							LEFT JOIN sub_kategori AS c ON a.kode_sub_kategori = c.id_sub_kategori
							LEFT JOIN sub_kategori2 AS d ON a.kode_sub_kategori2 = d.kode_sub_kategori2
							LEFT JOIN
								(
								SELECT SUM(aa.stok) AS stok, aa.kode_produk FROM detail_produk AS aa
								GROUP BY aa.kode_produk
								) AS e ON a.kode_produk = e.kode_produk");
		return $data;
	}

	public function list_det_produk($id)
	{
		$data = DB::select("SELECT a.id_detail_produk, a.kode_produk, a.kode_warna, c.nm_warna, b. nama_produk, a.stok 
	    	from detail_produk as a join produk as b on a.kode_produk = b.kode_produk
	    	left join warna c on a.kode_warna = c.kode_warna
	    					where a.kode_produk=?", array($id));
		return $data;
	}

	public function list_eproduk()
	{
		$data = DB::select("SELECT a.kd_produk, a.id_kd_produk, a.nama_produk, b.harga_satuan, b.id_produk from kd_produk as a 
							join produk as b on a.id_kd_produk = b.id_kd_produk");
		return $data;
	}

	public function list_size()
	{
		$data = DB::select("SELECT * FROM size");
		return $data;
	}

	public function list_satuan()
	{
		$data = DB::select("SELECT * FROM satuan");
		return $data;
	}

	public function list_kategori()
	{
		$data = DB::select("SELECT * FROM kategori");
		return $data;
	}

	public function list_kategori2()
	{
		$data = DB::select("SELECT * FROM sub_kategori as a join kategori as b on a.kode_kategori = b.kode_kategori");
		return $data;
	}
	public function list_kategori3()
	{
		$data = DB::select("SELECT * FROM sub_kategori2 as a 
	    					join sub_kategori as c on a.kode_sub_kategori = c.kode_sub_kategori
	    					join kategori as b on a.kode_kategori = b.kode_kategori");
		return $data;
	}


	public function subkategori($id)
	{
		$data = DB::select("SELECT * FROM sub_kategori as a 
	    					join kategori as b on a.kode_kategori = b.kode_kategori
	    					where b.kode_kategori = ?", array($id));
		$data = array_map(function ($value) {
			return (array)$value;
		}, $data);
		return $data;
	}

	public function subkategoriA($id)
	{
		$data = DB::select("SELECT * FROM sub_kategori2 as a 
	    					left join sub_kategori as b on a.kode_sub_kategori = b.kode_sub_kategori
	    					where a.kode_sub_kategori = ?", array($id));
		$data = array_map(function ($value) {
			return (array)$value;
		}, $data);
		return $data;
	}

	public function list_warna()
	{
		$data = DB::select("SELECT * FROM warna");
		return $data;
	}


	public function getSize($id)
	{
		$data = DB::select("SELECT * FROM size
                            WHERE id_size = ?", array($id));
		return $data;
	}
	public function GetFotoInfo($id)
	{
		$data = DB::select("SELECT * FROM foto_produk
                            WHERE id_foto_produk = ?", array($id));
		return $data;
	}

	public function getFoto($id)
	{
		$foto = $this->getGambar($id);
		$r_foto = "";
		foreach ($foto as $row) {
			$r_foto = $row->foto;
		}
		return $r_foto;
	}

	public function getGambar($id)
	{

		$data = DB::select("SELECT b.foto FROM produk as a
        					JOIN foto_produk as b on a.id_produk = b.id_produk
                            WHERE a.id_produk = ? LIMIT 1", array($id));
		return $data;
	}

	public function foto_utama($id)
	{
		$data = DB::select("SELECT * FROM foto_produk
                            WHERE flag_utama='1' AND kode_produk = ?", array($id));
		return $data;
	}

	public function fotoProduk($id)
	{
		$data = DB::select("SELECT * FROM foto_produk
                            WHERE kode_produk = ?", array($id));
		return $data;
	}

	public function getDetProduk($id)
	{
		$data = DB::select("SELECT a.id_detail_produk, a.kode_produk, a.stok, c.kode_warna, b.flag_warna, b.nama_produk FROM detail_produk as a
        					left join warna as c on a.kode_warna = c.kode_warna 
        					join produk as b on a.kode_produk = b.kode_produk
							where a.id_detail_produk = ?", array($id));
		return $data;
	}

	public function getDetProduk2($id)
	{
		$data = DB::select("SELECT a.id_detail_produk, a.kode_produk, a.stok, c.kode_warna, b.flag_warna, b.nama_produk FROM detail_produk as a
        					left join warna as c on a.kode_warna = c.kode_warna 
        					join produk as b on a.kode_produk = b.kode_produk
							where a.kode_produk = ? AND a.stok !='0'", array($id));
		return $data;
	}

	public function getProduk($id)
	{
		$data = DB::select("SELECT * FROM produk where kode_produk = ?", array($id));
		return $data;
	}
	public function getInfoProduk($id)
	{
		$data = DB::select("SELECT a.id_produk, a.kode_produk, b.nama_kategori, c.nama_sub_kategori, d.nama_sub_kategori2, a.flag_warna, a.flag_grosir, a.harga_anggota, a.harga_umum, a.harga_grosir_anggota, a.harga_grosir_umum, a.harga_beli, a.deskripsi, a.nama_produk, f.stok, a.kode_kategori, a.minimum_grosir
        					 FROM produk as a
        					left join kategori as b on a.kode_kategori = b.kode_kategori
        					left join sub_kategori as c on a.kode_sub_kategori = c.kode_sub_kategori
        					left join sub_kategori2 as d on a.kode_sub_kategori2 = d.kode_sub_kategori2
        					left join (
        							SELECT SUM(aa.stok) as stok, aa.kode_produk from detail_produk as aa where aa.kode_produk = ? 
        							GROUP BY aa.kode_produk
        					)as f on a.kode_produk = f.kode_produk
        					 where a.kode_produk = ?", array($id, $id));
		return $data;
	}

	public function getDetInfoProduk($id)
	{
		$data = DB::select("SELECT a.kode_produk, b.kode_suplier, b.nama_suplier, b.alamat, b.no_hp 
							FROM detail_pembelian AS a 
							JOIN pembelian AS c ON a.kode_pembelian = c.kode_pembelian
							JOIN suplier AS b ON c.kode_suplier = b.kode_suplier
							WHERE a.kode_produk = ?
							GROUP BY a.kode_produk, b.kode_suplier, b.nama_suplier, b.alamat, b.no_hp", array($id));
		return $data;
	}

	public function get1detproduk($id)
	{
		$data = DB::select("SELECT * FROM detail_produk where kode_produk = ? LIMIT 1", array($id));
		return $data;
	}

	public function getdetproduk3($id)
	{
		$data = DB::select("SELECT * FROM detail_produk where kode_produk = ? AND stok > 0", array($id));
		return $data;
	}



	public function getkategori($id)
	{
		$data = DB::select("SELECT * FROM kategori
                            WHERE id_kategori = ?", array($id));
		return $data;
	}

	public function getSubkategori($id)
	{

		$data = DB::select("SELECT * FROM sub_kategori as a 
        					join kategori as b on a.kode_kategori = b.kode_kategori
    						WHERE a.id_sub_kategori = ?", array($id));
		return $data;
	}

	public function getChildkategori($id)
	{

		$data = DB::select("SELECT c.*, a.nama_sub_kategori, b.nama_kategori
        					 FROM sub_kategori2 as c
        					join sub_kategori as a on a.kode_sub_kategori = c.kode_sub_kategori
        					join kategori as b on a.kode_kategori = b.kode_kategori
    						WHERE c.id_sub_kategori2 = ?", array($id));
		return $data;
	}

	public function getSatuan($id)
	{
		$data = DB::select("SELECT * FROM satuan
                            WHERE id_satuan = ?", array($id));
		return $data;
	}

	public function getWarna($id)
	{
		$data = DB::select("SELECT * FROM warna
                            WHERE id_warna = ?", array($id));
		return $data;
	}

	public function harga_produk($kode)
	{
		$data = DB::select("SELECT * FROM produk
                            WHERE kode_produk = ?", array($kode));
		return $data;
	}
}
