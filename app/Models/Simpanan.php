<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Simpanan extends Model
{

  public function list_sw($id)
  {
    $data = DB::select('SELECT a.id_simpanan, a.tgl_simpanan, a.jumlah_simpanan,c.nama_akun, b.nama_anggota, a.periode_simpanan
                             FROM simpanan as a 
                             join anggota as b on a.id_anggota = b.id_anggota 
                             join akun as c on a.id_jenis_simpanan = c.kode_akun
                             where a.status= 1 AND c.kode_akun=?', array($id));

    return $data;
  }


  public function agt_list_sw($kode, $id)
  {
    $data = DB::select('SELECT a.id_simpanan, a.tgl_simpanan, a.jumlah_simpanan,c.nama_akun, b.nama_anggota, a.periode_simpanan
                                 FROM simpanan as a 
                                 join anggota as b on a.id_anggota = b.id_anggota 
                                 join akun as c on a.id_jenis_simpanan = c.kode_akun
                                 where a.status= 1 AND c.kode_akun=? AND b.id_anggota=?', array($kode, $id));

    return $data;
  }


  public function get_simpanan_det($id)
  {
    $data = DB::select('SELECT a.id_anggota, a.nama_anggota, a.no_anggota, b.total from anggota as a
                            left join (
                            select SUM(aa.jumlah_simpanan) as total, aa.id_anggota from simpanan as aa
                                where aa.id_jenis_simpanan=?
                                GROUP BY aa.id_anggota
                            )as b on a.id_anggota = b.id_anggota
                            ', array($id));

    return $data;
  }

  public function detail_smpianan_agt($kode, $id)
  {
    $data = DB::select('SELECT SUM(aa.jumlah_simpanan) as total, aa.id_anggota from simpanan as aa
                                where aa.id_jenis_simpanan=? AND aa.id_anggota=?
                                GROUP BY aa.id_anggota', array($kode, $id));

    return $data;
  }

  public function get_simpanan_agt($kode, $id)
  {
    $data = DB::select('SELECT a.id_simpanan, a.tgl_simpanan, a.jumlah_simpanan,c.nama_akun, b.nama_anggota, a.periode_simpanan
                             FROM simpanan as a 
                             join anggota as b on a.id_anggota = b.id_anggota 
                             join akun as c on a.id_jenis_simpanan = c.kode_akun
                             where a.status= 1 AND c.kode_akun=? AND b.id_anggota=?', array($kode, $id));


    return $data;
  }

  public function getSaldo($id)
  {
    $data = DB::select('SELECT total_saldo
                             FROM jenis_penerimaan
                             where id_jenis_penerimaan= ?', array($id));

    return $data;
  }



  public function list_jenis_penerimaan()
  {
    $data = DB::select('SELECT * FROM jenis_penerimaan');

    return $data;
  }

  public function listAkunlancar()
  {
    $data = DB::select("SELECT * FROM akun where aktiva_pasiva='1' AND jenis='2'");

    return $data;
  }

  public function getSaldoAkun($id)
  {
    $data = DB::select("SELECT * FROM detail_akun where kode_akun=?", array($id));

    return $data;
  }

  public function jenis_simpanan()
  {
    $data = DB::select("SELECT id_jenis_simpanan,nama_jenis_simpanan, 
                            CASE WHEN status = '1' THEN 'Aktif' 
                            ELSE 'Tidak Aktif' END AS status
                            FROM jenis_simpanan");
    return $data;
  }


  public function getJenisSimpanan($id)
  {
    $data = DB::select("SELECT id_jenis_simpanan,nama_jenis_simpanan,status 
                            FROM jenis_simpanan
                            WHERE id_jenis_simpanan = ?", array($id));
    return $data;
  }

  public function getSimpanan($id)
  {
    $data = DB::select("SELECT * from simpanan
                            WHERE id_simpanan = ?", array($id));
    return $data;
  }

  public function getDetailSimpanan($id)
  {
    $data = DB::select("SELECT a.id_simpanan, a.unik_kode, DATE_FORMAT(a.tgl_simpanan, '%d %M %Y') as tgl_simpanan,
                            b.nama_akun, c.no_anggota, c.nama_anggota, d.nama_jenis_simpanan, a.id_jenis_simpanan,
                            a.id_jenis_penerimaan, a.jumlah_simpanan, a.periode_simpanan
                            FROM simpanan as a 
                            JOIN akun as b on a.id_jenis_penerimaan = b.kode_akun
                            JOIN anggota as c on a.id_anggota = c.id_anggota
                            JOIN jenis_simpanan as d on a.id_jenis_simpanan = d.id_jenis_simpanan
                            WHERE id_simpanan = ?", array($id));
    return $data;
  }

  public function getTransaksi($unik_kode)
  {
    $data = DB::select("SELECT * from transaksi
                            WHERE reff_id is null AND unik_kode = ?", array($unik_kode));
    return $data;
  }
}
