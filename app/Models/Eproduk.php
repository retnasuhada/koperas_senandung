<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Eproduk extends Model
{

	public function list_produk()
	{
		$data = DB::select("SELECT a.kode_produk, a.nama_produk, a.kode_kategori, a.kode_sub_kategori, 
							a.kode_sub_kategori2,e.stok, f.foto, a.harga_umum, a.harga_anggota, 
							a.harga_grosir_anggota, a.harga_grosir_umum, a.flag_grosir, 
							a.minimum_grosir
										 FROM produk AS a
							JOIN kategori AS b ON a.kode_kategori = b.kode_kategori
							LEFT JOIN sub_kategori AS c ON a.kode_sub_kategori = c.kode_sub_kategori
							LEFT JOIN sub_kategori2 AS d ON a.kode_sub_kategori2 = d.kode_sub_kategori2
							LEFT JOIN (
								SELECT SUM(aa.stok) AS stok, aa.kode_produk FROM detail_produk AS aa 
								JOIN produk AS bb ON aa.kode_produk = bb.kode_produk 
								GROUP BY aa.kode_produk
							) AS e ON a.kode_produk = e.kode_produk
							LEFT JOIN (
								SELECT ab.foto, ab.kode_produk FROM foto_produk AS ab
								JOIN produk AS ac ON ab.kode_produk = ac.kode_produk
								WHERE ab.flag_utama = '1' 
								GROUP BY ab.kode_produk, ab.foto
							) AS f ON a.kode_produk = f.kode_produk
							
							");
		return $data;
	}
	public function side_produk()
	{
		$data = DB::select("SELECT a.kode_produk, a.nama_produk, a.kode_kategori, a.kode_sub_kategori, 
							a.kode_sub_kategori2,e.stok, f.foto, a.harga_umum, a.harga_anggota, 
							a.harga_grosir_anggota, a.harga_grosir_umum, a.flag_grosir, 
							a.minimum_grosir
										 FROM produk AS a
							JOIN kategori AS b ON a.kode_kategori = b.kode_kategori
							LEFT JOIN sub_kategori AS c ON a.kode_sub_kategori = c.kode_sub_kategori
							LEFT JOIN sub_kategori2 AS d ON a.kode_sub_kategori2 = d.kode_sub_kategori2
							LEFT JOIN (
								SELECT SUM(aa.stok) AS stok, aa.kode_produk FROM detail_produk AS aa 
								JOIN produk AS bb ON aa.kode_produk = bb.kode_produk 
								GROUP BY aa.kode_produk
							) AS e ON a.kode_produk = e.kode_produk
							LEFT JOIN (
								SELECT ab.foto, ab.kode_produk FROM foto_produk AS ab
								JOIN produk AS ac ON ab.kode_produk = ac.kode_produk
								WHERE ab.flag_utama = '1' 
								GROUP BY ab.kode_produk, ab.foto
							) AS f ON a.kode_produk = f.kode_produk
							where a.show_side='1'
							");
		return $data;
	}

	public function new_produk()
	{
		$data = DB::select("SELECT a.kode_produk, a.nama_produk, a.kode_kategori, a.kode_sub_kategori, 
							a.kode_sub_kategori2,e.stok, f.foto, a.harga_umum, a.harga_anggota, 
							a.harga_grosir_anggota, a.harga_grosir_umum, a.flag_grosir, 
							a.minimum_grosir
										 FROM produk AS a
							JOIN kategori AS b ON a.kode_kategori = b.kode_kategori
							LEFT JOIN sub_kategori AS c ON a.kode_sub_kategori = c.kode_sub_kategori
							LEFT JOIN sub_kategori2 AS d ON a.kode_sub_kategori2 = d.kode_sub_kategori2
							LEFT JOIN (
								SELECT SUM(aa.stok) AS stok, aa.kode_produk FROM detail_produk AS aa 
								JOIN produk AS bb ON aa.kode_produk = bb.kode_produk 
								GROUP BY aa.kode_produk
							) AS e ON a.kode_produk = e.kode_produk
							LEFT JOIN (
								SELECT ab.foto, ab.kode_produk FROM foto_produk AS ab
								JOIN produk AS ac ON ab.kode_produk = ac.kode_produk
								WHERE ab.flag_utama = '1' 
								GROUP BY ab.kode_produk, ab.foto
							) AS f ON a.kode_produk = f.kode_produk
							ORDER BY a.id_produk DESC LIMIT 8");
		return $data;
	}

	public function getSize($id)
	{
		$data = DB::select("SELECT * FROM size
                            WHERE id_size = ?", array($id));
		return $data;
	}

	public function getAnggota($id)
	{
		$data = DB::select("SELECT * FROM anggota
                            WHERE id_anggota = ?", array($id));
		return $data;
	}

	public function getKeranjang($id)
	{
		$data = DB::select("SELECT a.*, b.nama_produk, b.harga_anggota, b.harga_umum,
							b.flag_grosir, b.minimum_grosir, b.harga_grosir_anggota, b.harga_grosir_umum,
							c.status AS status_anggota
							 FROM keranjang_belanja AS a
							JOIN produk AS b ON a.kode_produk = b.kode_produk
							JOIN anggota AS c ON a.no_anggota = c.no_anggota
							WHERE c.id_anggota =? AND a.status ='0'", array($id));	
		return $data;
	}

	public function detProduk($id)
	{
		$data = DB::select("SELECT a.*, b.nama_kategori, c.nama_sub_kategori, d.nama_sub_kategori2
							FROM produk AS a
							JOIN kategori AS b ON a.kode_kategori = b.kode_kategori
							LEFT JOIN sub_kategori AS c ON a.kode_sub_kategori = c.kode_sub_kategori
							LEFT JOIN sub_kategori2 AS d ON a.kode_sub_kategori2 = d.kode_sub_kategori2
							WHERE a.kode_produk=?", array($id));
		return $data;
	}


	public function fotoProduk($id)
	{
		$data = DB::select("SELECT * FROM foto_produk WHERE kode_produk=?", array($id));
		return $data;
	}
}
