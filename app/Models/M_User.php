<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class M_User extends Model
{

	public function get_anggota($id_anggota)
	{
		$data = DB::select('SELECT * FROM anggota where id_anggota= ?', array($id_anggota))[0];
		// print_r($data);
		// die();
		return $data;
	}

	public function getAnggota($id)
	{
		$data = DB::select("SELECT * FROM anggota
                            WHERE id_anggota = ?", array($id));
		return $data;
	}

	public function getUser($id)
	{
		$data = DB::select("SELECT a.id_user,a.username,b.id_anggota, a.password,b.status, b.nama_anggota,
							CASE WHEN d.nama_jabatan IS NULL THEN 'Anggota'
							ELSE d.nama_jabatan END AS nama_jabatan
							from user as a
							join anggota as b on a.id_anggota = b.id_anggota
							left join petugas as c on b.id_anggota = c.id_anggota
							left join jabatan as d on c.id_jabatan = d.id_jabatan
                            WHERE a.id_user = ?", array($id));
		return $data;
	}

	public function get_user($id)
	{
		$data = DB::select("SELECT * FROM user where id_anggota=?", array($id));
		return $data;
	}

	public function list_user()
	{
		$data = DB::select("SELECT a.id_user,a.username, a.password,b.status, b.nama_anggota,
							CASE WHEN d.nama_jabatan IS NULL THEN 'Anggota'
							ELSE d.nama_jabatan END AS nama_jabatan
							from user as a
							join anggota as b on a.id_anggota = b.id_anggota
							left join petugas as c on b.id_anggota = c.id_anggota
							left join jabatan as d on c.id_jabatan = d.id_jabatan");
		// print_r($data);
		// die();
		return $data;
	}
}
