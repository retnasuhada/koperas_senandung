<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Penjualan extends Model
{


  public function getAnggota($id)
  {
    $data = DB::select("SELECT a.id_anggota, a.nik, a.foto, a.nama_anggota, a.alamat, a.no_anggota, a.join_date, a.created_at, a.npwp,a.created_by, a.no_hp, 
							CASE 
								WHEN c.nama_jabatan IS NULL THEN 'Anggota'
								ELSE c.nama_jabatan END AS jabatan,
							CASE 
								WHEN a.status='1' THEN 'Aktif'
								ELSE 'Tidak Aktif' END AS status,
							CASE 
								WHEN a.jenis_kelamin='1' THEN 'Laki-Laki'
								ELSE 'Perempuanss' END AS jenis_kelamin
							FROM anggota AS a 
							LEFT JOIN petugas AS b ON a.id_anggota = b.id_anggota
							LEFT JOIN jabatan AS c ON b.id_jabatan = c.id_jabatan
							WHERE a.id_anggota=?
							", array($id));
    return $data;
  }

  public function status_anggota($id)
  {
    $data = DB::select("SELECT * FROM anggota
              WHERE no_anggota=?
              ", array($id));
    return $data;
  }

  public function getPenjualan($kode_penjualan)
  {
    $data = DB::select("SELECT a.*, b.no_anggota, b.nama_anggota, c.nama_anggota as created_by FROM penjualan AS a 
                            LEFT JOIN anggota AS b ON a.no_anggota = b.no_anggota
                            join anggota as c on a.created_by = c.id_anggota
                            WHERE a.kode_penjualan=?
                            ", array($kode_penjualan));
    return $data;
  }

  public function pembelanjan($no_anggota)
  {
    $data = DB::select("SELECT SUM(c.sub_total) AS total, MONTH(a.tanggal_penjualan) AS bulan 
                        FROM penjualan AS a
                      JOIN anggota AS b ON a.no_anggota = b.no_anggota
                      JOIN (
                        SELECT SUM(harga_produk * jumlah) AS sub_total, kode_penjualan FROM detail_penjualan
                        GROUP BY kode_penjualan
                      ) AS c ON a.kode_penjualan = c.kode_penjualan
                      where a.no_anggota=?
                      GROUP BY MONTH(a.tanggal_penjualan)", array($no_anggota));
    return $data;
  }

  public function list_penjualan()
  {
    $data = DB::select("SELECT a.*, b.nama_anggota, c.total_pcs FROM penjualan as a 
        					left join anggota as b on a.no_anggota = b.no_anggota
        					left join ( 
        								SELECT SUM(x.jumlah) as total_pcs, x.kode_penjualan from detail_penjualan as x GROUP by x.kode_penjualan
        								) as c on a.kode_penjualan = c.kode_penjualan
                  ORDER BY a.tanggal_penjualan DESC
        				");
    return $data;
  }

  public function list_penjualan_det($kode_penjualan)
  {
    $data = DB::select("SELECT a.*, b.nama_produk, b.harga_anggota, b.harga_umum, b.harga_grosir_anggota, b.harga_grosir_umum, b.minimum_grosir FROM detail_penjualan as a
        					left join produk as b on a.kode_produk = b.kode_produk where kode_penjualan=?", array($kode_penjualan));
    return $data;
  }

  public function detPembelianH($kode_penjualan)
  {
    $data = DB::select("SELECT a.id_tr_det_penjualan, d.kode_penjualan, d.tanggal_penjualan,
                             f.nama_produk, a.jumlah, c.harga_pendapatan,
                                b.harga_produk AS harga_jual, (c.harga_pendapatan * a.jumlah) 
                                AS total_harga_pendapatan,
                                 (b.harga_produk * a.jumlah) AS total_harga_jual   FROM tr_det_penjualan 
                                 AS a 
                                JOIN detail_penjualan AS b ON a.id_detail_penjualan = b.id_detail_penjualan
                                JOIN detail_produk AS c ON a.id_detail_produk = c.id_detail_produk
                                JOIN penjualan AS d ON b.kode_penjualan = d.kode_penjualan
                                LEFT JOIN anggota AS e ON d.no_anggota = e.no_anggota
                                LEFT JOIN produk AS f ON c.kode_produk = f.kode_produk
                            WHERE b.kode_penjualan = ?
                            ", array($kode_penjualan));
    return $data;
  }

  public function temp_det_penjualan($kode_penjualan)
  {
    $data = DB::select("SELECT a.*, b.nama_produk, b.harga_anggota, b.harga_umum, b.harga_grosir_anggota, b.harga_grosir_umum, b.flag_grosir, b.minimum_grosir FROM detail_penjualan as a
        					left join produk as b on a.kode_produk = b.kode_produk where a.kode_penjualan=? AND a.status=? ", array($kode_penjualan, '0'));
    return $data;
  }

  public function get_detail_penjualan($id_detail_penjualan)
  {
    $data = DB::select("SELECT a.*, b.nama_produk, b.harga_anggota, b.harga_umum, b.harga_grosir_anggota, b.harga_grosir_umum FROM detail_penjualan as a
        					left join produk as b on a.kode_produk = b.kode_produk where a.id_detail_penjualan=?", array($id_detail_penjualan));
    return $data;
  }

  public function cek_stok($kode_produk)
  {
    $data = DB::select("SELECT SUM(stok) AS stok FROM detail_produk
                            WHERE kode_produk =?
                            GROUP BY kode_produk", array($kode_produk));
    return $data;
  }
}
