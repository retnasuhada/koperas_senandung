<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pembelian extends Model
{


	public function getAnggota($id)
	{
		$data = DB::select("SELECT a.id_anggota, a.nik, a.foto, a.nama_anggota, a.alamat, a.no_anggota, a.join_date, a.created_at, a.npwp,a.created_by, a.no_hp, 
							CASE 
								WHEN c.nama_jabatan IS NULL THEN 'Anggota'
								ELSE c.nama_jabatan END AS jabatan,
							CASE 
								WHEN a.status='1' THEN 'Aktif'
								ELSE 'Tidak Aktif' END AS status,
							CASE 
								WHEN a.jenis_kelamin='1' THEN 'Laki-Laki'
								ELSE 'Perempuanss' END AS jenis_kelamin
							FROM anggota AS a 
							LEFT JOIN petugas AS b ON a.id_anggota = b.id_anggota
							LEFT JOIN jabatan AS c ON b.id_jabatan = c.id_jabatan
							WHERE a.id_anggota=?
							", array($id));
		return $data;
	}

	public function list_pembelian()
	{
		$data = DB::select("SELECT a.*, b.total, c.nama_suplier FROM pembelian as a
							JOIN suplier as c on a.kode_suplier = c.kode_suplier
							 left join 
	    					(SELECT SUM(aa.harga_semua) as total, aa.kode_pembelian from detail_pembelian as aa 
	    					 GROUP by aa.kode_pembelian) as b on a.kode_pembelian = b.kode_pembelian 
	    					 ORDER BY a.tgl_pembelian DESC
	    					");

		return $data;
	}
	public function getPembelian($kode_pembelian)
	{
		$data = DB::select("SELECT a.*, b.status as lunas, c.nama_suplier, c.kode_suplier
							 FROM pembelian as a
							LEFT JOIN invoice as b on a.no_invoice = b.no_invoice
							LEFT JOIN suplier as c on a.kode_suplier = c.kode_suplier
							 where a.kode_pembelian = ?", array($kode_pembelian));

		return $data;
	}

	public function getKodePembelian($id)
	{
		$data = DB::select("SELECT kode_pembelian from detail_pembelian 
							 where id_detail_pembelian = ? LIMIT 1", array($id));

		return $data;
	}

	public function list_det_pembelian($id)
	{
		$data = DB::select("SELECT a.id_detail_pembelian, b.kode_pembelian,a.kode_produk, c.nama_produk, a.jumlah, a.harga_semua, a.harga_satuan, a.harga_pendapatan FROM detail_pembelian as a 
	    					left join pembelian as b on a.kode_pembelian = b.kode_pembelian
	    					left join produk as c on a.kode_produk = c.kode_produk

	    	where a.kode_pembelian = ?", array($id));

		return $data;
	}


	

	public function list_verif_pembelian($id)
	{
		$data = DB::select("SELECT a.id_detail_pembelian, b.kode_pembelian,a.kode_produk, c.nama_produk, a.jumlah, a.harga_semua, a.harga_satuan, a.harga_pendapatan FROM detail_pembelian as a 
	    					left join pembelian as b on a.kode_pembelian = b.kode_pembelian
	    					left join produk as c on a.kode_produk = c.kode_produk

	    	where a.kode_pembelian = ? AND a.verified='0'", array($id));

		return $data;
	}

	public function terverifikasi($id)
	{
		$data = DB::select("SELECT a.id_detail_pembelian, b.kode_pembelian,a.kode_produk, c.nama_produk, a.jumlah, a.harga_semua, a.harga_satuan, a.harga_pendapatan FROM detail_pembelian as a 
	    					left join pembelian as b on a.kode_pembelian = b.kode_pembelian
	    					left join produk as c on a.kode_produk = c.kode_produk

	    	where a.kode_pembelian = ? AND a.verified='1'", array($id));

		return $data;
	}

	public function count_barang($id)
	{
		$data = DB::select("SELECT SUM(jumlah) as total, SUM(harga_semua)as total_h from detail_pembelian where status='0' AND kode_pembelian=?", array($id));
		return $data;
	}

	public function count_barang_verified($id)
	{
		$data = DB::select("SELECT SUM(jumlah) as total, SUM(harga_semua)as total_h from detail_pembelian where status='1' AND verified='1' AND kode_pembelian=?", array($id));
		return $data;
	}

	public function temp_det_pembelian($id)
	{
		$data = DB::select("SELECT a.id_detail_pembelian,a.kode_produk, b.kode_pembelian, c.nama_produk, a.jumlah, a.harga_semua, a.harga_satuan, a.harga_pendapatan FROM detail_pembelian as a 
	    					left join pembelian as b on a.kode_pembelian = b.kode_pembelian
	    					left join produk as c on a.kode_produk = c.kode_produk

	    	where a.status='0' AND a.kode_pembelian = ?", array($id));

		return $data;
	}

	public function temp_det_verif_pembelian($id)
	{
		$data = DB::select("SELECT a.id_detail_pembelian,a.kode_produk, b.kode_pembelian, c.nama_produk, a.jumlah, a.harga_semua, a.harga_satuan, a.harga_pendapatan FROM detail_pembelian as a 
	    					left join pembelian as b on a.kode_pembelian = b.kode_pembelian
	    					left join produk as c on a.kode_produk = c.kode_produk

	    	where a.status='0' AND a.kode_pembelian = ? AND a.verified='1'", array($id));

		return $data;
	}


	public function get_detail_pembelian($id)
	{
		$data = DB::select("SELECT a.id_detail_pembelian, b.kode_pembelian, c.nama_produk, a.jumlah, a.harga_semua, a.harga_satuan, a.harga_pendapatan, a.kode_produk FROM detail_pembelian as a 
	    					left join pembelian as b on a.kode_pembelian = b.kode_pembelian
	    					left join produk as c on a.kode_produk = c.kode_produk

	    	where a.id_detail_pembelian = ?", array($id));

		return $data;
	}

	public function getInvoice($no_invoice)
	{
		$data = DB::select("SELECT * from pembelian where no_invoice=?", array($no_invoice));

		return $data;
	}
}
