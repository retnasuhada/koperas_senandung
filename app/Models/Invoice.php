<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Invoice extends Model
{


	public function list_invoice()
	{
		$data = DB::select("SELECT a.no_invoice, a.tgl_invoice, a.total, b.tgl_pembelian,
	    					c.nama_suplier, c.kode_suplier, a.status, d.pembayaran 
							FROM invoice AS a 
							JOIN pembelian AS b ON a.no_invoice = b.no_invoice
							JOIN suplier AS c ON a.kode_suplier = c.kode_suplier
							LEFT JOIN (
							SELECT SUM(xx.total_pembayaran) AS pembayaran, xx.no_invoice FROM pembayaran_invoice AS xx 
								JOIN invoice AS xy ON xx.no_invoice = xy.no_invoice
								GROUP BY xx.no_invoice
							)AS d ON a.no_invoice = d.no_invoice");
		return $data;
	}

	public function list_detail_pembayaran($kode)
	{
		$data = DB::select("SELECT * FROM detail_pembayaran_suplier where status!='1' AND kode_pembayaran_suplier=?", array($kode));
		return $data;
	}

	public function sub_total_detail_pembayaran($kode)
	{
		$data = DB::select("SELECT SUM(a.total) as total 
							FROM detail_pembayaran_suplier AS a
							JOIN pembayaran_suplier AS b ON a.kode_pembayaran_suplier = b.kode_pembayaran_suplier
							JOIN produk AS c ON a.kode_produk = c.kode_produk
							JOIN detail_pembelian as d on a.id_detail_pembelian = d.id_detail_pembelian
              				JOIN pembelian as e on d.kode_pembelian = e.kode_pembelian
							WHERE a.status='1'
							AND a.kode_pembayaran_suplier = ?", array($kode));
		return $data;
	}

	public function penjualan_produk_suplier($kode_suplier)
	{
		$data = DB::select("SELECT a.id_tr_det_penjualan,g.tanggal_penjualan,h.nama_suplier, e.nama_produk, 
							a.jumlah, c.harga_pendapatan AS harga_suplier,
							(a.jumlah * c.harga_pendapatan) AS total
							FROM tr_det_penjualan AS a
							JOIN detail_produk AS b ON a.id_detail_produk = b.id_detail_produk
							JOIN detail_pembelian AS c ON c.id_detail_pembelian = b.id_detail_pembelian
							JOIN detail_penjualan AS d ON a.id_detail_penjualan = d.id_detail_penjualan
							JOIN produk AS e ON b.kode_produk = e.kode_produk
							JOIN pembelian AS f ON c.kode_pembelian = f.kode_pembelian
							JOIN penjualan AS g ON d.kode_penjualan = g.kode_penjualan
							JOIN suplier AS h ON f.kode_suplier = h.kode_suplier
							WHERE f.kode_suplier = ?
							AND a.id_tr_det_penjualan NOT IN (
								SELECT id_tr_det_penjualan FROM detail_pembayaran_suplier)",
							array($kode_suplier));
		return $data;
                
	}

	public function data_ost_pembayaran($kode_pembayaran)
	{
		$data = DB::select("SELECT a.*, b.nama_produk FROM detail_pembayaran_suplier AS a
							JOIN produk AS b ON a.kode_produk = b.kode_produk
							
							WHERE status='0'
							AND a.kode_pembayaran_suplier = ?",
							array($kode_pembayaran));
		return $data;
                
	}

	public function detail_pembayaran_suplier($kode_pembayaran)
	{
		$data = DB::select("SELECT e.kode_pembelian,c.kode_produk,c.nama_produk, a.harga, a.jumlah, a.total 
							FROM detail_pembayaran_suplier AS a
							JOIN pembayaran_suplier AS b ON a.kode_pembayaran_suplier = b.kode_pembayaran_suplier
							JOIN produk AS c ON a.kode_produk = c.kode_produk
							JOIN detail_pembelian as d on a.id_detail_pembelian = d.id_detail_pembelian
              				JOIN pembelian as e on d.kode_pembelian = e.kode_pembelian
							WHERE a.status='1'
							AND a.kode_pembayaran_suplier = ?",
							array($kode_pembayaran));
		return $data;
                
	}

	public function get_data_ost_pembayaran($kode_pembayaran)
	{
		$data = DB::select("SELECT f.kode_suplier FROM detail_pembayaran_suplier AS a
							JOIN produk AS b ON a.kode_produk = b.kode_produk
							JOIN tr_det_penjualan AS c ON a.id_tr_det_penjualan = c.id_tr_det_penjualan
							JOIN detail_produk AS d ON c.id_detail_produk = d.id_detail_produk
							JOIN detail_pembelian AS e ON d.id_detail_pembelian = e.id_detail_pembelian
							JOIN pembelian AS f ON e.kode_pembelian = f.kode_pembelian
							WHERE a.status='0'
							AND a.kode_pembayaran_suplier = ?
							ORDER BY a.id_detail_pembayaran_suplier DESC
							LIMIT 1 ",
							array($kode_pembayaran));
		return $data;
                
	}

	

	public function get_penjualan_produk_suplier($id_tr_penjualan)
	{
		$data = DB::select("SELECT a.id_tr_det_penjualan,g.tanggal_penjualan,h.nama_suplier,
							c.id_detail_pembelian, e.nama_produk, 
							a.jumlah,e.kode_produk,h.kode_suplier, c.harga_pendapatan AS harga_suplier,
							(a.jumlah * c.harga_pendapatan) AS total
							FROM tr_det_penjualan AS a
							JOIN detail_produk AS b ON a.id_detail_produk = b.id_detail_produk
							JOIN detail_pembelian AS c ON c.id_detail_pembelian = b.id_detail_pembelian
							JOIN detail_penjualan AS d ON a.id_detail_penjualan = d.id_detail_penjualan
							JOIN produk AS e ON b.kode_produk = e.kode_produk
							JOIN pembelian AS f ON c.kode_pembelian = f.kode_pembelian
							JOIN penjualan AS g ON d.kode_penjualan = g.kode_penjualan
							JOIN suplier AS h ON f.kode_suplier = h.kode_suplier
							WHERE a.id_tr_det_penjualan = ?", array($id_tr_penjualan));
		return $data;
                
	}

	public function list_pembayaran_suplier()
	{
		$data = DB::select("SELECT a.id_pembayaran_suplier, a.kode_pembayaran_suplier, a.tgl_pembayaran, a.total,
							b.nama_suplier 
							FROM pembayaran_suplier AS a 
							JOIN suplier AS b ON a.kode_suplier = b.kode_suplier");
		return $data;
	}

	public function get_pembayaran_suplier($kode_pembayaran)
	{
		$data = DB::select("SELECT a.id_pembayaran_suplier, a.kode_pembayaran_suplier, a.tgl_pembayaran, a.total,
							b.nama_suplier 
							FROM pembayaran_suplier AS a 
							JOIN suplier AS b ON a.kode_suplier = b.kode_suplier
							where a.kode_pembayaran_suplier=?", array($kode_pembayaran));
		return $data;
	}

	public function get_invoice($no_invoice)
	{
		$data = DB::select("SELECT a.no_invoice, a.tgl_invoice, a.total, b.tgl_pembelian,
	    					b.kode_pembelian,
	    					c.nama_suplier, c.kode_suplier, a.status, d.pembayaran, b.biaya_tambahan, b.diskon, a.id_invoice 
							FROM invoice AS a 
							JOIN pembelian AS b ON a.no_invoice = b.no_invoice
							JOIN suplier AS c ON a.kode_suplier = c.kode_suplier
							LEFT JOIN (
							SELECT SUM(xx.total_pembayaran) AS pembayaran, xx.no_invoice FROM pembayaran_invoice AS xx 
								JOIN invoice AS xy ON xx.no_invoice = xy.no_invoice
								GROUP BY xx.no_invoice
							)AS d ON a.no_invoice = d.no_invoice
							where a.no_invoice =?
							", array($no_invoice));
		return $data;
	}
	public function getRecordByr($no_invoice)
	{
		$data = DB::select("SELECT SUM(total_pembayaran) as total_pembayaran from pembayaran_invoice where no_invoice =? GROUP BY no_invoice
							", array($no_invoice));
		return $data;
	}

	public function detail_pembayaran($no_invoice)
	{
		$data = DB::select("SELECT * from pembayaran_invoice where no_invoice =? GROUP BY no_invoice
							", array($no_invoice));
		return $data;
	}

	public function reff_pembayaran($no_invoice)
	{
		$data = DB::select("SELECT * from pembayaran_invoice where no_invoice =? 
							", array($no_invoice));
		return $data;
	}
}
