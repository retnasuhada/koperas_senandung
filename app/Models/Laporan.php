<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Laporan extends Model
{

  

  public function GetLaporaPembelian($jenis_laporan,$start,$end)
  {
    if ($jenis_laporan=='All') {
       $where_jenis = '';
    }
    if ($jenis_laporan=='1') {
       $where_jenis = 'AND a.jenis_pembelian = 1';
    }
    if ($jenis_laporan=='2') {
       $where_jenis = 'AND a.jenis_pembelian = 2';
    }
    if ($jenis_laporan=='3') {
       $where_jenis = 'AND a.jenis_pembelian = 3';
    }
    $data = DB::select("SELECT a.* , b.nama_suplier, (c.total + a.biaya_tambahan - a.diskon) AS
                      total,
                      CASE 
                      WHEN a.jenis_pembelian ='1' THEN 'Cash'
                      WHEN a.jenis_pembelian ='2' THEN 'Titip'
                      WHEN a.jenis_pembelian ='3' THEN 'Tempo'
                    ELSE 'Tidak Diketahui' END AS jenis
                     FROM pembelian AS a
                      JOIN suplier AS b ON a.kode_suplier = b.kode_suplier

                      JOIN(
                        SELECT SUM(harga_semua) AS total, kode_pembelian FROM detail_pembelian 
                        GROUP BY kode_pembelian
                      ) AS c ON a.kode_pembelian = c.kode_pembelian
                      WHERE a.tgl_pembelian BETWEEN '$start' AND '$end' $where_jenis");
    return $data;
  }

  public function GetLaporaPenjualan($start,$end)
  {
   
    $data = DB::select("SELECT a.id_tr_det_penjualan,f.tanggal_penjualan,
                        g.nama_anggota,c.nama_produk, a.jumlah, e.harga_produk, (a.jumlah * e.harga_produk) AS total_h_jual,
                        d.harga_pendapatan, (a.jumlah * d.harga_pendapatan) AS t_harga_pendapatan FROM tr_det_penjualan AS a
                        JOIN detail_produk AS b ON a.id_detail_produk = b.id_detail_produk
                        JOIN produk AS c ON b.kode_produk = c.kode_produk
                        JOIN detail_pembelian AS d ON b.id_detail_pembelian = d.id_detail_pembelian
                        JOIN detail_penjualan AS e ON a.id_detail_penjualan = e.id_detail_penjualan
                        JOIN penjualan AS f ON e.kode_penjualan = f.kode_penjualan
                        JOIN anggota AS g ON f.no_anggota = g.no_anggota
                        WHERE f.tanggal_penjualan BETWEEN '$start' AND '$end'
                        AND e.status='1'");
    return $data;
  }

  
}
