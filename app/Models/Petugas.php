<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Petugas extends Model
{


	public function getPetugas($id)
	{
		$data = DB::select("SELECT * FROM petugas
                            WHERE id_petugas = ?", array($id));
		return $data;
	}

	public function list_petugas()
	{
		$data = DB::select("SELECT a.*, b.nama_anggota, c.nama_jabatan from petugas as a
	    					join anggota as b on a.id_anggota = b.id_anggota
	    					join jabatan as c on a.id_jabatan = c.id_jabatan");
		// print_r($data);
		// die();
		return $data;
	}
}
