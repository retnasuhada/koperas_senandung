<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;


class Anggota extends Model
{

	public function get_anggota($id_anggota)
	{
		$data = DB::select('SELECT * FROM anggota where id_anggota= ?', array($id_anggota));

		return $data;
	}

	public function getAnggota($id)
	{
		$data = DB::select("SELECT a.id_anggota, a.nik, a.foto, a.nama_anggota, a.alamat, a.no_anggota, a.join_date, a.created_at, a.npwp,a.created_by, a.no_hp, 
							CASE 
								WHEN c.nama_jabatan IS NULL THEN 'Anggota'
								ELSE c.nama_jabatan END AS jabatan,
							CASE 
								WHEN a.status='1' THEN 'Aktif'
								ELSE 'Tidak Aktif' END AS status,
							CASE 
								WHEN a.jenis_kelamin='1' THEN 'Laki-Laki'
								ELSE 'Perempuanss' END AS jenis_kelamin
							FROM anggota AS a 
							LEFT JOIN petugas AS b ON a.id_anggota = b.id_anggota
							LEFT JOIN jabatan AS c ON b.id_jabatan = c.id_jabatan
							WHERE a.id_anggota=?
							", array($id));
		return $data;
	}

	public function getAnggota2($id)
	{
		$data = DB::select("SELECT a.id_anggota, a.nik, a.foto, a.nama_anggota, a.alamat, a.no_anggota, a.join_date, a.created_at, a.npwp,a.created_by, a.no_hp, 
							CASE 
								WHEN c.nama_jabatan IS NULL THEN 'Anggota'
								ELSE c.nama_jabatan END AS jabatan,
							CASE 
								WHEN a.status='1' THEN 'Aktif'
								ELSE 'Tidak Aktif' END AS status,
							CASE 
								WHEN a.jenis_kelamin='1' THEN 'Laki-Laki'
								ELSE 'Perempuanss' END AS jenis_kelamin
							FROM anggota AS a 
							LEFT JOIN petugas AS b ON a.id_anggota = b.id_anggota
							LEFT JOIN jabatan AS c ON b.id_jabatan = c.id_jabatan
							WHERE a.no_anggota=?
							", array($id));
		return $data;
	}

	public function list_anggota()
	{
		$data = DB::select("SELECT no_anggota, nama_anggota, alamat, nik, id_anggota,
	    					 	CASE WHEN status = '1' THEN 'Aktif' 
                            	ELSE 'Tidak Aktif' END AS status,
                            	CASE WHEN jenis_kelamin = '1' THEN 'Laki-Laki'
                            	ELSE 'Perempuan' END AS jenis_kelamin
	    					 FROM anggota");
		// print_r($data);
		// die();
		return $data;
	}

	public function last_agt($like)
	{
		$data = DB::select("SELECT no_anggota FROM anggota
	    					where no_anggota like '%$like%'
							ORDER BY id_anggota
							DESC
							LIMIT 1 ");

		return $data;
	}
}
