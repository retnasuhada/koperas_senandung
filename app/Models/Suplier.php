<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Suplier extends Model
{

  public function list_suplier()
  {
    $data = DB::select("SELECT * FROM suplier where status='1'");
    return $data;
  }

  public function get_suplier($kode_suplier)
  {
    $data = DB::select("SELECT * FROM suplier where kode_suplier=?", array($kode_suplier));
    return $data;
  }


  public function produk_suplier($kode_suplier)
  {
    $data = DB::select("SELECT a.kode_produk,c.nama_produk, b.kode_pembelian, d.kode_suplier,
                             d.nama_suplier FROM detail_pembelian AS a
                        JOIN pembelian AS b ON a.kode_pembelian = b.kode_pembelian
                        JOIN produk AS c ON a.kode_produk = c.kode_produk
                        JOIN suplier AS d ON b.kode_suplier = d.kode_suplier
                            WHERE b.kode_suplier = ?", array($kode_suplier));
    return $data;
  }

public function m_produk_suplier($kode_suplier)
  {
    $data = DB::select("SELECT a.kode_produk,c.nama_produk, b.kode_pembelian,e.stok, 
                        d.kode_suplier,d.nama_suplier 
                        FROM detail_pembelian AS a
                        JOIN pembelian AS b ON a.kode_pembelian = b.kode_pembelian
                        JOIN produk AS c ON a.kode_produk = c.kode_produk
                        JOIN suplier AS d ON b.kode_suplier = d.kode_suplier
                        JOIN (
                        SELECT aa.kode_produk, SUM(aa.stok) AS stok FROM detail_produk AS aa GROUP BY aa.kode_produk
                        ) AS e ON c.kode_produk = e.kode_produk
                        WHERE b.kode_suplier = ?", array($kode_suplier));
    return $data;
  }

 public function detail_pembayaran_suplier($kode_pembayaran)
  {
    $data = DB::select("SELECT e.kode_pembelian,c.kode_produk,c.nama_produk, a.harga, a.jumlah, a.total 
              FROM detail_pembayaran_suplier AS a
              JOIN pembayaran_suplier AS b ON a.kode_pembayaran_suplier = b.kode_pembayaran_suplier
              JOIN produk AS c ON a.kode_produk = c.kode_produk
              JOIN detail_pembelian as d on a.id_detail_pembelian = d.id_detail_pembelian
              join pembelian as e on d.kode_pembelian = e.kode_pembelian
              
              WHERE a.status='1'
              AND a.kode_pembayaran_suplier = ?",
              array($kode_pembayaran));
    return $data;
                
  }


  public function get_pembayaran_suplier($kode_pembayaran)
  {
    $data = DB::select("SELECT a.id_pembayaran_suplier, a.kode_pembayaran_suplier, a.tgl_pembayaran, a.total,
              b.nama_suplier 
              FROM pembayaran_suplier AS a 
              JOIN suplier AS b ON a.kode_suplier = b.kode_suplier
              where a.kode_pembayaran_suplier=?", array($kode_pembayaran));
    return $data;
  }

  public function pembayaran_suplier($kode_suplier)
  {
    $data = DB::select("SELECT * FROM pembayaran_suplier
                        WHERE kode_suplier=?", array($kode_suplier));

    return $data;
  }

  public function penjualan_suplier($kode_suplier)
  {
    $data = DB::select("SELECT a.*, b.total FROM pembelian as a left join 
                (SELECT SUM(aa.harga_semua) as total, aa.kode_pembelian from detail_pembelian as aa 
                 GROUP by aa.kode_pembelian) as b on a.kode_pembelian = b.kode_pembelian
                 where a.kode_suplier=?", array($kode_suplier));

    return $data;
  }


  public function invoice_suplier($kode_suplier)
  {
    $data = DB::select("SELECT a.no_invoice, a.tgl_invoice, a.total,c.total_pembayaran, 
                        CASE 
                          WHEN a.status = '0' 
                          THEN 'belum lunas' ELSE 'lunas' 
                          END AS lunas 
                          FROM invoice AS a 
                        JOIN suplier AS b ON a.kode_suplier = b.kode_suplier
                        LEFT JOIN (
                          SELECT SUM(aa.total_pembayaran) AS total_pembayaran, ab.no_invoice FROM pembayaran_invoice AS aa
                          JOIN invoice AS ab ON aa.no_invoice = ab.no_invoice
                          GROUP BY ab.no_invoice
                        ) AS c ON a.no_invoice = c.no_invoice

                        WHERE b.kode_suplier =? 
                        ORDER BY tgl_invoice DESC
                        LIMIT 10", array($kode_suplier));
    return $data;
  }

  
}
