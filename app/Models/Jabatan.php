<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Jabatan extends Model
{

  public function list_jabatan()
  {
    $data = DB::select("SELECT * FROM jabatan");
    return $data;
  }


  public function getJabatan($id)
  {
    $data = DB::select("SELECT * FROM jabatan
                            WHERE id_jabatan = ?", array($id));
    return $data;
  }
}
