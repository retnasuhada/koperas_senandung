<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Akuntansi extends Model
{

    public function list_aset($id)
    {
        $data = DB::select("SELECT 
                                a.id_akun, a.kode_akun, a.nama_akun, b.total,
                                CASE 
                                    WHEN a.aktiva_pasiva='1' THEN 'Aktiva'
                                    ELSE 'Pasiva'
                                END AS aktiva_pasiva,
                                CASE 
                                    WHEN a.jenis ='1' THEN 'Aset Tetap'
                                    WHEN a.jenis ='2' THEN 'Aset lancar'
                                    WHEN a.jenis ='3' THEN 'Kewajiban'
                                    WHEN a.jenis ='4' THEN 'Beban'
                                    ELSE 'Lain-Lain'
                                END AS jenis
                                FROM akun AS a
                               LEFT JOIN (SELECT SUM(a.nilai) AS total, a.kode_akun FROM detail_akun AS a
                                            JOIN akun AS b ON a.kode_akun = b.kode_akun
                                            GROUP BY a.kode_akun) AS b ON a.kode_akun = b.kode_akun
                                WHERE a.aktiva_pasiva =?", array($id));

        return $data;
    }

    public function list_aset3()
    {
        $data = DB::select("SELECT 
                                a.id_akun, a.kode_akun, a.nama_akun, b.total,
                                CASE 
                                    WHEN a.aktiva_pasiva='1' THEN 'Aktiva'
                                    ELSE 'Pasiva'
                                END AS aktiva_pasiva,
                                CASE 
                                    WHEN a.jenis ='1' THEN 'Aset Tetap'
                                    WHEN a.jenis ='2' THEN 'Aset lancar'
                                    WHEN a.jenis ='3' THEN 'Kewajiban'
                                    WHEN a.jenis ='4' THEN 'Beban'
                                    ELSE 'Lain-Lain'
                                END AS jenis
                                FROM akun AS a
                               LEFT JOIN (SELECT SUM(a.nilai) AS total, a.kode_akun FROM detail_akun AS a
                                            JOIN akun AS b ON a.kode_akun = b.kode_akun
                                            GROUP BY a.kode_akun) AS b ON a.kode_akun = b.kode_akun
                                WHERE a.jenis IN ('1','2','4')");
        $data = array_map(function ($value) {
            return (array)$value;
        }, $data);
        return $data;
    }

    public function list_aset2($id, $id2)
    {
        $data = DB::select("SELECT 
                                a.id_akun, a.kode_akun, a.nama_akun, b.total,
                                CASE 
                                    WHEN a.aktiva_pasiva='1' THEN 'Aktiva'
                                    ELSE 'Pasiva'
                                END AS aktiva_pasiva,
                                CASE 
                                    WHEN a.jenis ='1' THEN 'Aset Tetap'
                                    WHEN a.jenis ='2' THEN 'Aset lancar'
                                    WHEN a.jenis ='3' THEN 'Kewajiban'
                                    WHEN a.jenis ='4' THEN 'Beban'
                                    ELSE 'Lain-Lain'
                                END AS jenis
                                FROM akun AS a
                               LEFT JOIN (SELECT SUM(a.nilai) AS total, a.kode_akun FROM detail_akun AS a
                                            JOIN akun AS b ON a.kode_akun = b.kode_akun
                                            GROUP BY a.kode_akun) AS b ON a.kode_akun = b.kode_akun
                                WHERE a.aktiva_pasiva =? AND a.jenis=?", array($id, $id2));
        $data = array_map(function ($value) {
            return (array)$value;
        }, $data);
        // print_r($data);
        // die();

        return $data;
    }


    public function TotalAset($id)
    {
        $data = DB::select("SELECT 
                                SUM(b.total) AS total
                            FROM akun AS a
                            LEFT JOIN (SELECT SUM(a.nilai) AS total, a.kode_akun FROM detail_akun AS a
                            JOIN akun AS b ON a.kode_akun = b.kode_akun
                            GROUP BY a.kode_akun) AS b ON a.kode_akun = b.kode_akun
                            WHERE a.aktiva_pasiva =?", array($id));

        return $data;
    }
    public function jurnal_umum()
    {
        $data = DB::select("SELECT * FROM transaksi ORDER BY  tgl_transaksi DESC");

        return $data;
    }

    public function getDetailAset($id)
    {
        $data = DB::select('SELECT *
                             FROM detail_akun
                             where kode_akun= ?', array($id));
        return $data;
    }

    public function getInfoAset($id)
    {
        $data = DB::select('SELECT *
                             FROM akun
                             where kode_akun= ?', array($id));
        return $data;
    }
}
