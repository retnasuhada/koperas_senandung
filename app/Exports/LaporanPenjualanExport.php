<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class LaporanPenjualanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    // private $data;
    public function __construct($data_penjualan)
    {
        $this->data = $data_penjualan;

    }
     public function collection()
    {
        $exportData = collect([]);
        $header = [
            'No',
            'Tanggal Penjualan',
            'Nama Anggota',
            'Nama Produk',
            'Jumlah',
            'Harga Produk',
            'Total Harga Jual',
            'Harga Pendapatan',
            'Total Harga Pendapatan'
        ];

        $exportData->push($header);
        // dd($this->data);//fix
        /**
         * SET ROW
         */
        $i = 1;
       
        foreach ($this->data as $d) {
          
            $exportData->push([
                $d['no'],
                $d['tanggal_penjualan'],
                $d['nama_anggota'],
                $d['nama_produk'],
                $d['jumlah'],

                $d['harga_produk'],
                $d['total_h_jual'],
                $d['harga_pendapatan'],
                $d['t_harga_pendapatan']
            ]);
            $i++;
        }

        //return data
        return $exportData;
    }
}
